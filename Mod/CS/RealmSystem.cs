﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Hearthpyre.Realm;
using Wintellect.PowerCollections;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.Effects;

using static Hearthpyre.Static;

namespace Hearthpyre
{
	[Serializable]
	public partial class RealmSystem : IGameSystem
	{
		public const string TimeFormat = "yyyy'-'MM'-'dd HH':'mm':'ss'Z'";
		
		// ah piss systems use binary formatter
		// cant save gameobjects either as they're read/written before systems
		[NonSerialized] private List<Settlement> Governed;
		[NonSerialized] public DateTime? RestTime;

		public override void OnAdded()
		{
			WorldBuilderExtension.RequireSecrets();
		}

#if BUILD_2_0_204
		public override void SaveGame(SerializationWriter Writer)
#else
		public override bool WantFieldReflection => false;

		public override void Write(SerializationWriter Writer)
#endif
		{
			var fields = new Dictionary<string, object>();
			if (IsInSector(The.Player)) fields["Time"] = DateTime.UtcNow.ToString(TimeFormat, CultureInfo.InvariantCulture);
			Writer.Write(fields);

			
			if (Governed.IsNullOrEmpty()) Writer.Write(0);
			else
			{
				Writer.Write(Governed.Count);
				foreach (var settlement in Governed)
				{
					settlement.Save(Writer);
				}
			}
		}

#if BUILD_2_0_204
		public override void LoadGame(SerializationReader Reader)
#else
		public override void Read(SerializationReader Reader)
#endif
		{
			var fields = Reader.ReadDictionary<string, object>();

			for (int i = 0, c = Reader.ReadInt32(); i < c; i++)
			{
				AddGoverned(new Settlement(Reader));
			}
			
			if (fields.TryGetValue("Time", out var obj))
			{
				var valid = DateTime.TryParseExact(
					(string) obj,
					TimeFormat,
					CultureInfo.InvariantCulture,
					DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal,
					out var date
				);

				RestTime = valid ? date : null;
			}
		}

		private void AddGoverned(Settlement Settlement)
		{
			if (Governed == null) Governed = new List<Settlement>();
			Governed.Add(Settlement);
		}
	}

	[HasGameBasedStaticCache]
	public partial class RealmSystem
	{
		private static RealmSystem System;

		private static RealmSystem Make() => new RealmSystem();
		public static RealmSystem Get() => System ?? (System = The.Game.RequireSystem(Make));

		public static Dictionary<Guid, Settlement> Settlements = new();
		public static Dictionary<string, Settlement> SettlementsByCellID = new();
		public static Dictionary<Guid, Sector> Sectors = new();
		public static Dictionary<string, Sector> SectorsByZoneID = new();
		public static Dictionary<string, Lattice> LatticesByZoneID = new();
		public static Dictionary<Guid, Home> Homes = new();

		[GameBasedCacheInit]
		public static void Clear()
		{
			System = null;
			Settlements.Clear();
			SettlementsByCellID.Clear();
			Sectors.Clear();
			SectorsByZoneID.Clear();
			LatticesByZoneID.Clear();
			Homes.Clear();
		}

		public static Settlement GetSettlement(Guid ID)
		{
			Settlements.TryGetValue(ID, out var result);
			return result;
		}

		public static Settlement GetSettlement(string CellID)
		{
			SettlementsByCellID.TryGetValue(CellID, out var result);
			return result;
		}

		public static Settlement GetSettlement(Zone Zone) => GetSettlement(Zone.wX, Zone.wY, Zone.ZoneWorld);

		public static Settlement GetSettlement(int WX, int WY, string World = "JoppaWorld")
		{
			var worldId = Strings.SB.Clear().Append(World).Append('.')
				.Append(WX).Append('.')
				.Append(WY).ToString();

			return GetSettlement(worldId);
		}

		public static Sector GetSector(Guid ID)
		{
			Sectors.TryGetValue(ID, out var result);
			return result;
		}

		public static Sector GetSector(Zone zone) => GetSector(zone?.ZoneID);

		public static Sector GetSector(string ZoneID)
		{
			if (ZoneID == null) return null;
			SectorsByZoneID.TryGetValue(ZoneID, out var result);
			return result;
		}

		public static Home GetHome(Guid ID)
		{
			Homes.TryGetValue(ID, out var result);
			return result;
		}

		public static Settlement NewSettlement(Zone Zone, bool Visible = true)
		{
			var system = Get();
			var settlement = new Settlement(Zone, Visible: Visible);
			system.AddGoverned(settlement);
			return settlement;
		}
		
		public static bool IsInSector(GameObject Object)
		{
			return SectorsByZoneID.ContainsKey(Object.CurrentZone.ZoneID);
		}
	}
}