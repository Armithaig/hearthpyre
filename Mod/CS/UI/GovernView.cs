using ConsoleLib.Console;
using System;
using System.Collections.Generic;
using XRL;
using XRL.Core;
using XRL.World;
using XRL.World.WorldBuilders;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using XRL.UI;
using Genkit;
using System.Linq;
using System.Diagnostics;
using static Hearthpyre.Static;

namespace Hearthpyre.UI
{
	// TODO: Add scrollable list of villagers (in entire settlement or current screen?) and their assignments.
	[UIView(ID: NAME, ForceFullscreen: true, NavCategory: "Menu", UICanvasHost: 1)]
	public class GovernView : View
	{
		public const string NAME = "HearthpyreGovernView";

		public Element ActiveElement;
		public Element[][] Elements;
		public Box Box;
		public Box Content;

		string Header;
		int HeaderX;

		public GovernView() : base(NAME) {
			Elements = new Element[][] {
				new Element[] {new Locator(), new IconPicker(), new MusicPicker()}
			};

			foreach (var arr in Elements) {
				foreach (var element in arr) {
					element.Reload += OnReload;
				}
			}
		}

		public void OnReload(object obj, EventArgs args) => Enter();
		public override void Enter() {
			base.Enter();

			var name = ColorUtility.StripFormatting(ActiveZone.GetBareDisplayName()).Truncate(16, "...");
			Box = new Box(3, 2, 33, 13);
			Content = new Box(Box.x1 + 3, Box.y1 + 2, Box.x2 - 3, Box.y2 - 2);
			Header = "[ {{W|Govern}} \u00C4" + (name.Length % 2 == 0 ? "" : "\u00C4") + " {{C|" + name + "}} ]";
			HeaderX = ColorUtility.LengthExceptFormatting(Header);
			HeaderX = Box.x1 + RoundToInt(Box.Width * 0.5 - HeaderX * 0.5);

			foreach (var arr in Elements) {
				foreach (var element in arr) {
					element.Enter();
				}
			}
		}

		public override void Leave() {
			base.Leave();
			foreach (var arr in Elements) {
				foreach (var element in arr) {
					element.Leave();
				}
			}
		}

		public override void Update() {
			base.Update();
			ActiveElement.Update();
		}

		public override void Render(ScreenBuffer sb) {
			sb.Fill(Box.x1, Box.y1, Box.x2, Box.y2, 32, 0);
			sb.SingleBox(Box.x1, Box.y1, Box.x2, Box.y2, ColorUtility.MakeColor(TextColor.Grey, TextColor.Black));

			sb.Goto(Box.x1, Box.y1);
			sb.Write(Box.x2 - 10, Box.y2, "[ \0{{W|H}}elp ]");
			sb.Display(Box.x2 - 8, Box.y2, "Creatures/caste_12.bmp", CLB_CYAN, CLB_WHT);

			sb.Write(HeaderX, Box.y1, Header);

			var c = RoundToInt(Content.x1 + (Content.x2 - Content.x1) * 0.5);
			sb.Goto(c, Content.y1);
			Elements[0][0].Render(sb);
			sb.Goto(c, Content.y1 + 3);
			Elements[0][1].Render(sb);
			sb.Goto(c, Content.y1 + 6);
			Elements[0][2].Render(sb);
		}


		public override void Move(int x, int y) {
			var l = Elements.Length;
			if (x >= l) x = 0;
			else if (x < 0) x = l - 1;

			l = Elements[x].Length;
			if (y >= l) y = 0;
			else if (y < 0) y = l - 1;

			if (x != Cursor.x || y != Cursor.y)
				PlayUISound(SND_MENU);

			base.Move(x, y);
			ActiveElement = Elements[Cursor.x][Cursor.y];
		}

		public override bool Input(Keys keys) {
			if (ActiveElement.Input(keys)) return true;
			switch (keys) {
				case Keys.A:
				case Keys.D:
					return true;
			}

			return base.Input(keys);
		}

	}
}
