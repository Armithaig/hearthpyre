using ConsoleLib.Console;
using System;
using System.Collections;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.WorldBuilders;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using XRL.UI;
using Genkit;
using System.Linq;
using System.Diagnostics;
using static Hearthpyre.Static;

namespace Hearthpyre.UI
{
	// TODO: Struct when mono updated?
	public class Index
	{
		public int Value;
		public int Min;
		public int Max;

		public Index(int value = 0, int min = 0, int max = int.MaxValue) {
			Value = Calc.Clamp(value, min, max);
			Min = min;
			Max = max;
		}

		public Index(IList list, int value = 0, int min = 0) : this(value, min, list.Count - 1) { }

		public void Set(int Value)
		{
			this.Value = Calc.Clamp(Value, Min, Max);
		}

		public static implicit operator int(Index i) => i.Value;

		public static int operator +(Index i, int v) {
			var result = i.Value + v;
			while (result > i.Max)
				result = i.Min + (result - i.Max - 1);

			return result;
		}

		public static Index operator ++(Index i) {
			i.Value++;
			if (i.Value > i.Max)
				i.Value = i.Min;

			return i;
		}

		public static int operator -(Index i, int v) {
			var result = i.Value - v;
			while (result < i.Min)
				result = i.Max + (result - i.Min + 1);

			return result;
		}

		public static Index operator --(Index i) {
			i.Value--;
			if (i.Value < i.Min)
				i.Value = i.Max;

			return i;
		}


	}
}
