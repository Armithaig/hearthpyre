using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;


using XRL.UI;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.WorldBuilders;
using XRL.Language;
using ConsoleLib.Console;
using SimpleJSON;
using Qud.API;
using Hearthpyre.Realm;
using Genkit;
using XRL;
using XRL.World.Parts.Skill;
using static Hearthpyre.Static;
//using Rewired;

namespace Hearthpyre.UI
{
	// TODO: Inherit from SingletonWindowBase.
	public abstract class View
	{
		public static TextConsole Console;
		public static ScreenBuffer Buffer;
		public static bool OverridePrerelease;

		public Zone ActiveZone;
		public string Name { get; set; }
		public Point2D Cursor { get; set; }
		public bool End;
		ScreenReturn Yield;

		public View(string name) {
			Name = name;
		}

		// TODO: Use IWantsTextConsoleInit when inheritance is fixed
		public void Init(TextConsole console, ScreenBuffer buffer) {
			Console = console;
			Buffer = buffer;
		}

		public virtual bool Showable(bool silent = false) {
			/*if (!OverridePrerelease && Options.PrereleaseInputManager) {
				if (!silent) {
					var result = Popup.ShowYesNo("This doesn't work great with the prerelease input manager (yet).\nDo you still wish to try?", defaultResult: DialogResult.No);
					if (result == DialogResult.Yes) {
						OverridePrerelease = true;
						return true;
					}
				}
				return false;
			}*/
			return true;
		}

		public ScreenReturn Show() {
			LoadState();
			Enter();
			Shift(0, 0);
			if (!Showable()) return Yield;

			GameManager.Instance.PushGameView(Name);
			var sb = Buffer;
			while (!End) {
				Update();

				// Render buffer to screen.
				XRLCore.Core.RenderMapToBuffer(sb);
				Render(sb);
				Console.DrawBuffer(sb, null, false);

				// Handle keyboard input.
				// TODO: Account for new input manager. Split up keyboard/mouse events with it.
				if (Keyboard.kbhit()) {
					var key = Keyboard.getvk(true, wait: false);
					Input(key);
				}
			}

			Leave();
			SaveState();
			return Yield;
		}

		public virtual void LoadState() { }
		public virtual void SaveState() { }
		public virtual void Enter() {
			PlayUISound(SND_MENU);
			End = false;
			Yield = ScreenReturn.Exit;
			ActiveZone = The.Game.ZoneManager.ActiveZone;
			WorldBuilderExtension.RequireSecrets();

			if (Buffer == null) {
				Buffer = XRLCore._Buffer;
				Console = XRLCore._Console;
			}

			// Reveal whole area if owned.
			if (RealmSystem.SectorsByZoneID.ContainsKey(ActiveZone.ZoneID)) {
				ActiveZone.VisAll();
				ActiveZone.ExploreAll();
				ActiveZone.LightAll();
			}
		}

		public virtual void Leave() {
			ScreenBuffer.ClearImposterSuppression();
			GameManager.Instance.PopGameView();
		}

		public virtual void Update() {
			Event.ResetPool();
		}

		public virtual void Render(ScreenBuffer sb) { }

		public virtual bool Input(Keys keys) {
			// Move cursor based on numpad input.
			switch (keys) {
				case Keys.NumPad1:
					Shift(-1, 1); break;
				case Keys.NumPad2:
					Shift(0, 1); break;
				case Keys.NumPad3:
					Shift(1, 1); break;
				case Keys.NumPad4:
					Shift(-1, 0); break;
				case Keys.NumPad6:
					Shift(1, 0); break;
				case Keys.NumPad7:
					Shift(-1, -1); break;
				case Keys.NumPad8:
					Shift(0, -1); break;
				case Keys.NumPad9:
					Shift(1, -1); break;
				case Keys.A:
					Yield = ScreenReturn.Previous;
					End = true; break;
				case Keys.D:
					Yield = ScreenReturn.Next;
					End = true; break;
				case Keys.F1:
				case Keys.H:
					Help(); break;
				case Keys.Escape:
					End = true; break;
				default: return false;
			}
			return true;
		}

		public void Move(Point2D point) { Move(point.x, point.y); }
		public virtual void Move(int x, int y) {
			Cursor = new Point2D(x, y);
		}

		public void Shift(int x, int y) {
			Move(Cursor.x + x, Cursor.y + y);
		}

		public virtual void Reset() { }

		/*public static void Flow(ref int val, int inc, int count, int min = 0) {
			if (inc == 0) return;

			val += inc;
			if (val >= count) val = min;
			else if (val < min) val = count - 1;
		}*/


		public virtual void Help() {

		}

		public static class Manager
		{
			static List<TargetView> Scheming = new List<TargetView>{
				new SchemeView(),
				new AreaView(),
				new PaintView()
			};

			static GovernView Govern = new GovernView();

			public static void Show(string name)
			{
				var uiState = Options.GetOption("OptionModernUI");
				var pushState = Options.GetOption("OptionPushAZHotkeys");
				var emptyView = false;
				try
				{
					Options.SetOption("OptionModernUI", "No");
					Options.SetOption("OptionPushAZHotkeys", "Yes");
					if (name == GovernView.NAME) {
						Govern.Show();
						return;
					}

					int i = Scheming.FindIndex(x => x.Name == name);
					var view = Scheming[i];
					view.Reset();

					// Push a buffer view so the overlay on the UI thread doesn't cut in when switching views.
					GameManager.Instance.PushGameView("HearthpyreEmptyView");
					emptyView = true;

					while (true)
					{
						var yield = view.Show();

						do
						{
							switch (yield)
							{
								case ScreenReturn.Next:
									i += 1;
									break;
								case ScreenReturn.Previous:
									i -= 1;
									break;
								default: return;
							}

							if (i >= Scheming.Count) i = 0;
							else if (i < 0) i = Scheming.Count - 1;
							var old = view;
							view = Scheming[i];
							view.Cursor = old.Cursor;
							view.CursorSize = old.CursorSize;
						}
						while (!view.Showable(true));
					}
				}
				catch (Exception ex)
				{
					Log("ShowView", ex);
				}
				finally
				{
					Options.SetOption("OptionModernUI", uiState);
					Options.SetOption("OptionPushAZHotkeys", pushState);
					if (emptyView)
					{
						GameManager.Instance.PopGameView();
					}
				}
			}
		}
	}
}
