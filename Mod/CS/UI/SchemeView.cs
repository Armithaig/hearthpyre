using System;
using System.IO;
using System.Linq;
using XRL.UI;
using XRL.Core;
using XRL.Rules;
using XRL.World;
using XRL.World.Parts;
using XRL.World.WorldBuilders;
using XRL.Language;
using ConsoleLib.Console;
using SimpleJSON;
using Qud.API;
using Genkit;
using Hearthpyre.Realm;
using XRL;
using static Hearthpyre.Static;

//using Rewired;

namespace Hearthpyre.UI
{
    [UIView(ID: NAME, ForceFullscreen: true, WantsTileOver: true, NavCategory: "Menu", UICanvasHost: 1)]
    public class SchemeView : TargetView
    {
        // TODO: Shift + Pencil to line place
        public const string NAME = "HearthpyreSchemeView";
        public const int BLP_X = 44;

        public BlueprintPicker Picker;
        public Notitia.Blueprint SelectedBlueprint;
        public SchemeHistory History = new SchemeHistory();

        public GameObject ActiveBlueprint;
        //public string Color;

        public SchemeView() : base(NAME)
        {
            MouseFocus.Max = 2;
            Picker = new BlueprintPicker();
            Picker.Reload += OnReload;
        }

        public override bool Showable(bool silent = false)
        {
            if (The.ActiveZone.IsWorldMap())
            {
                if (!silent) Popup.Show("The power to shape the world you shall not find in a shiny wand, only in your own self.");
            }
            else if (!OptionAllowCombat && The.Player.AreHostilesNearby())
            {
                if (!silent) Popup.Show("You can't scheme with hostiles nearby!");
            }
            else return base.Showable();

            return false;
        }


        public override void Update()
        {
            base.Update();
            //Wall = Notitia.Walls[WallIndex];
            //Floor = Notitia.Floors[FloorIndex];
            //Furniture = Notitia.Furniture[FurnitureIndex];
            Picker.Update();
        }

        public override void LoadState()
        {
            //var game = XRLCore.Core.Game;
            //WallIndex = new Index(Notitia.Walls, game.GetIntGameState(Name + "WallIndex", 0));
            //FloorIndex = new Index(Notitia.Floors, game.GetIntGameState(Name + "FloorIndex", 0));
            //FurnitureIndex = new Index(Notitia.Furniture, game.GetIntGameState(Name + "FurnitureIndex", 0));
            Picker.Enter();
            SelectedBlueprint = Picker.ActiveBlueprint;
        }

        public override void SaveState()
        {
            //var game = XRLCore.Core.Game;
            //game.SetIntGameState(Name + "WallIndex", WallIndex);
            //game.SetIntGameState(Name + "FloorIndex", FloorIndex);
            //game.SetIntGameState(Name + "FurnitureIndex", FurnitureIndex);
        }

        public override void Render(ScreenBuffer sb)
        {
            var txtClr = new[] { "y", "y", "y" };
            txtClr[MouseFocus] = "C";

            // Write previews and command bar to buffer.
            sb.Goto(4, Cursor.y < 5 ? sb.Height - 1 : 0);
            sb.HeaderLine(CLD_WHT);
            sb.Write(0, sb.Y, "<{{W|A}}");
            sb.Write(78, sb.Y, "{{W|D}}>");

            var icoClr = CLB_BLUE;
            var icoDtl = CLB_CYAN;
            if (ActiveBlueprint != null)
            {
                icoClr = CLB_RED;
                icoDtl = CLB_ORNG;
            }

            sb.Write(PEN_X, sb.Y, "[ \0{{W|Q}}-{{" + txtClr[0] + "|Pencil}} ]");
            sb.Display(PEN_X + 2, sb.Y, "Icons/hp_pencil_tool.png", icoClr, icoDtl);

            sb.Write(FILL_X, sb.Y, "[ \0{{W|E}}-{{" + txtClr[1] + "|Bucket}} ]");
            sb.Display(FILL_X + 2, sb.Y, "Icons/hp_bucket_fill.png", icoClr, icoDtl);

            sb.Write(BLP_X, sb.Y, "[ \0{{W|R}}-{{" + txtClr[2] + "|Blueprint}} ]");
            sb[BLP_X + 2, sb.Y].Copy(SelectedBlueprint.Icon);

            sb.Write(HELP_X, sb.Y, "[ \0{{W|H}}elp ]");
            sb.Display(HELP_X + 2, sb.Y, "Creatures/caste_12.bmp", CLB_CYAN, CLB_WHT);

            if (Picker.Visible) Picker.Render(sb);
            else base.Render(sb);
        }

        public override bool Input(Keys keys)
        {
            if (Picker.Visible && Picker.Input(keys)) return true;

            if (!base.Input(keys))
            {
                switch (keys)
                {
                    case Keys.E:
                        FillArea();
                        break;
                    case Keys.Q:
                        PencilArea();
                        break;
                    case Keys.Control | Keys.R:
                    case Keys.Shift | Keys.R:
                    case Keys.R:
                        DisplayPicker();
                        break;
                    case Keys.Control | Keys.S:
                    case Keys.Shift | Keys.S:
                        Picker.SearchBlueprint();
                        break;
                    case Keys.Control | Keys.L:
                    case Keys.Shift | Keys.L:
                        if (Lattice.TryGet(ActiveZone.ZoneID, out var grid))
                            grid.Draw();
                        break;
                    case Keys.Control | Keys.D:
                    case Keys.Shift | Keys.D:
                        DemolishCursor();
                        PlayUISound(SND_REM);
                        break;
                    case Keys.Control | Keys.Z:
                    case Keys.Shift | Keys.Z:
                        if (History.Undo()) PlayUISound(SND_DSEL);
                        break;
                    case Keys.Control | Keys.Shift | Keys.Z:
                    case Keys.Control | Keys.Y:
                    case Keys.Shift | Keys.Y:
                        if (History.Redo()) PlayUISound(SND_SEL);
                        break;
                    default: return false;
                }
            }

            OnInput();
            return true;
        }

        public bool IsDemolishable(GameObject Object)
        {
            return OptionAllDemo
                   || Object.HasTagOrProperty(TAG_OBJ)
                   || Object.HasTag("Floor")
                ;
        }

        public void DemolishCursor()
        {
            if (CursorArea.Count == 1)
            {
                var cell = CursorArea[0];
                var obj = cell.GetFirstObject(IsDemolishable);
                cell.Demolish(obj);
                return;
            }

            foreach (var cell in CursorArea)
            {
                cell.Demolish(IsDemolishable);
            }
        }

        public void OnInput()
        {
            ActiveBlueprint = ActiveCell.GetFirstObject(OBJ_BLPR);
        }

        public void OnReload(object obj, EventArgs args)
        {
            SelectedBlueprint = Picker.ActiveBlueprint;
        }

        public void DisplayPicker()
        {
            Picker.Retranslate(BLP_X - 3, Cursor.y < 5 ? 25 - Picker.Box.Height : 0);
            Picker.Visible = true;
            MenuSound();
        }

        public override void Move(int x, int y)
        {
            base.Move(x, y);
            ActiveBlueprint = ActiveCell.GetFirstObject(OBJ_BLPR);
        }

        void FillArea()
        {
            var design = ActiveBlueprint?.TakePart<HearthpyreBlueprint>()?.Blueprint;
            if (design != null)
            {
                PlayUISound(SND_REM);
                History.Start();
                ActiveCell.YieldCardinalFlood(c =>
                    c.Demolish(o => o.TakePart<HearthpyreBlueprint>()?.Blueprint == design, History.Remove)
                ).All(x => true);

                History.End();
                return;
            }

            if (!ActiveCell.IsClear())
            {
                PlayUISound(SND_ERR);
                return;
            }

            var area = ActiveCell.YieldCardinalFlood(x => x.IsClear());
            History.Start();
            foreach (var cell in area)
            {
                cell.Demolish(PencilPredicate, History.Remove);
                var obj = GameObjectFactory.Factory.CreateObject(OBJ_BLPR);

                if (obj.TryTakePart(out HearthpyreBlueprint blp))
                {
                    blp.SetDesign(SelectedBlueprint);
                }

                History.Add(obj, cell);
                cell.AddObject(obj);
            }
            
            History.End();
            PlayUISound(SND_ADD);
        }


        void PencilArea()
        {
            if (ActiveBlueprint != null)
            {
                PlayUISound(SND_REM);
                History.Start();
                foreach (var cell in CursorArea)
                    cell.Demolish(PencilPredicate, History.Remove);

                History.End();
                return;
            }

            if (!ActiveCell.IsClear())
            {
                PlayUISound(SND_ERR);
                return;
            }

            History.Start();
            foreach (var cell in CursorArea)
            {
                if (!cell.IsClear()) continue;

                cell.Demolish(PencilPredicate);
                var obj = GameObjectFactory.Factory.CreateObject(OBJ_BLPR);

                if (obj.TryTakePart(out HearthpyreBlueprint blp))
                {
                    blp.SetDesign(SelectedBlueprint);
                }

                History.Add(obj, cell);
                cell.AddObject(obj);
            }

            History.End();
            PlayUISound(SND_ADD);
        }

        bool PencilPredicate(GameObject obj)
        {
            return obj.Blueprint == OBJ_BLPR;
        }

        public override void Leave()
        {
            base.Leave();

            int x = Stat.RandomCosmetic(0, 79), y = Stat.RandomCosmetic(0, 24);
            var C = The.Player?.CurrentCell;
            if (C != null)
            {
                x = C.X;
                y = C.Y;
            }

            Lattice.Invalidate(ActiveZone);
        }

        /*public void AskColorString() {
            var str = Popup.AskString("Write a color string.\nUse any combination of &&C, ^^C or *C.", Color, 6);
            if (String.IsNullOrEmpty(str)) return;
            if (str.IndexOfAny(new[] { '&', '^', '*' }) == -1)
                Popup.Show("Invalid color string.");
            else Color = Static.FilterColor(str);
        }

        public bool ApplyColor(Cell C, string clr) {
            // TODO: Preserve colours not specified.
            if (base.ActiveObject == null) return false;

            var render = base.ActiveObject.GetPart("Render") as Render;
            string color = Static.FilterColor(clr, 4), detail = Static.FilterColor(clr, 3);
            if (!String.IsNullOrEmpty(color)) {
                if (!String.IsNullOrEmpty(render.TileColor))
                    render.TileColor = color;
                render.ColorString = color;
            }
            if (!String.IsNullOrEmpty(detail))
                render.DetailColor = detail.Substring(1);
            return true;
        }*/

        /*public void ToggleBlueprint(Cell C, GameObjectBlueprint design, string property, string preclude = null) {
            if (DestroyTag(C, property)) {
                PlayUISound(SND_REM);
                foreach (var cell in CursorArea)
                    DestroyTag(cell, property);
                return;
            }

            if (!C.IsClear()) {
                PlayUISound(SND_ERR);
                return;
            }

            foreach (var cell in CursorArea) {
                if (!cell.IsClear()) continue;

                DestroyTag(cell, preclude);
                var obj = GameObjectFactory.Factory.CreateObject("HearthpyreBlueprint");
                obj.SetIntProperty(property, 1);

                var part = obj.GetPart("HearthpyreBlueprint") as HearthpyreBlueprint;
                if (part != null) part.SetDesign(design);

                cell.AddObject(obj);
            };

            PlayUISound(SND_ADD);
        }*/

        /*public string GetCostColor(GameObjectBlueprint blueprint) {
            int tier;
            if (Int32.TryParse(blueprint.GetTag("Tier", "0"), out tier)) {
                if (tier >= 6) return "&R";
                if (tier >= 3) return "{{W|";
            }
            return String.Empty;
        }*/

        public override void Help()
        {
            var text = MakeshiftPopupTitle("{{W|SCHEME MODE HELP}}") + "\n" +
                       "Press the {{W|Q}}, {{W|E}}, or {{W|R}} keys to pencil the cursor area, bucket fill or change blueprints.\n\n" +
                       "The {{W|A}} and {{W|D}} keys will cycle through available scheming modes.\n\n" +
                       "{{W|Right Click}}: Change mouse focus.\n" +
                       "{{W|Left Click}}: Use focused tool.\n" +
                       "{{W|Ctrl}} + {{W|8}}|{{W|2}}: Change cursor size.\n" +
                       "{{W|Ctrl}} + {{W|Z}}|{{W|Y}}: Undo and redo.\n" +
                       "{{W|Shift}} + {{W|S}}: Search for blueprint.\n" +
                       "{{W|Shift}} + {{W|D}}: Demolish materialized objects.\n" +
                       "{{W|Shift}} + {{W|L}}: Display pathfinding lattice.";

            Popup.Show(text);
        }
    }
}
