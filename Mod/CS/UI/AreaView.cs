using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using XRL.UI;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.WorldBuilders;
using XRL.Language;
using ConsoleLib.Console;
using SimpleJSON;
using Qud.API;
using Genkit;
using Hearthpyre.Realm;
using XRL;
using XRL.World.Parts.Skill;
using XRL.Rules;
using Color = UnityEngine.Color;
using static Hearthpyre.Static;
//using Rewired;

namespace Hearthpyre.UI
{
	[UIView(ID: NAME, ForceFullscreen: true, WantsTileOver: true, NavCategory: "Menu", UICanvasHost: 1)]
	public class AreaView : TargetView
	{
		public const string NAME = "HearthpyreAreaView";
		public const int AREA_X = 44;

		public Sector ActiveSector;
		public Home ActiveHome;
		public Home SelectedHome;
		public List<HearthpyreSettler> Settlers;
		public HearthpyreSettler ActiveSettler;
		public bool IsActiveArea;
		public bool IsActiveSelected;

		int RenderOffset;

		public AreaView() : base(NAME) {
			MouseFocus.Max = 2;
		}

		public override bool Showable(bool silent = false) {
			if (!The.Player.OwnPart<HearthpyreGoverning>()) {
				if (!silent) Popup.Show("You lack the skill to govern.");
			} else if (The.ActiveZone.IsWorldMap()) {
				if (!silent) Popup.Show("You have no claim over the world.");
			} else if (!RealmSystem.SectorsByZoneID.ContainsKey(The.ActiveZone.ZoneID)) {
				if (!silent) Popup.Show("You have no claim over this zone.");
			} else return base.Showable();

			return false;
		}

		public override void Enter() {
			base.Enter();

			SelectedHome = null;
			ActiveSector = RealmSystem.SectorsByZoneID[ActiveZone.ZoneID];

			Settlers = ActiveZone.GetObjectParts<HearthpyreSettler>().ToList();
		}

		public override void Render(ScreenBuffer sb) {
			RenderArea(sb);
			RenderOrigins(sb);

			var txtClr = new[] { "y", "y", "y" };
			txtClr[MouseFocus] = "C";
			sb.Y = Cursor.y < 5 ? sb.Height - 1 : 0;

			var orgIco = ICO_ORG;
			string orgTxt = "New Area";
			Color orgIcoClr = CLB_BLUE, selIcoClr = CLB_BLUE;
			Color orgIcoDtl = CLB_CYAN, selIcoDtl = CLB_CYAN;
			if (ActiveHome != null) {
				if (IsActiveSelected) {
					orgIcoClr = CLB_RED;
					orgIcoDtl = CLB_ORNG;
					orgTxt = "Remove Area";
				} else {
					orgTxt = "Select Area";
				}
			} else if (SelectedHome != null && ActiveSettler != null) {
				orgIco = ActiveSettler.ParentObject.pRender.Tile;
				if (IsActiveSelected) {
					orgIcoClr = CLB_RED;
					orgIcoDtl = CLB_ORNG;
					orgTxt = "Unset Home";
				} else {
					orgTxt = "Set Home";
				}
			}

			sb.HeaderLine(CLD_WHT);
			sb.Write(0, sb.Y, "<{{W|A}}");
			sb.Write(78, sb.Y, "{{W|D}}>");

			if (IsActiveArea) {
				selIcoClr = CLB_RED;
				selIcoDtl = CLB_ORNG;
			} else if (SelectedHome == null) {
				selIcoClr = CLB_BLK;
				selIcoDtl = CLD_WHT;
			}

			sb.Write(PEN_X, sb.Y, "[ \0{{W|Q}}-{{" + txtClr[0] + "|Pencil}} ]");
			sb.Display(PEN_X + 2, sb.Y, "Icons/hp_pencil_tool.png", selIcoClr, selIcoDtl);

			sb.Write(FILL_X, sb.Y, "[ \0{{W|E}}-{{" + txtClr[1] + "|Bucket}} ]");
			sb.Display(FILL_X + 2, sb.Y, "Icons/hp_bucket_fill.png", selIcoClr, selIcoDtl);

			sb.Write(AREA_X, sb.Y, "[ \0{{W|R}}-{{" + txtClr[2] + "|" + orgTxt + "}} ]");
			sb.Display(AREA_X + 2, sb.Y, orgIco, orgIcoClr, orgIcoDtl);

			sb.Write(HELP_X, sb.Y, "[ \0{{W|H}}elp ]");
			sb.Display(HELP_X + 2, sb.Y, "Creatures/caste_12.bmp", CLB_CYAN, CLB_WHT);

			// Render cursor last/top.
			base.Render(sb);
		}


		void RenderArea(ScreenBuffer sb) {
			if (SelectedHome == null) return;
			var num = (XRLCore.FrameTimer.ElapsedMilliseconds) & 4095;
			var color = CLD_CYAN;
			if (num >= 3072) return;
			else if (num >= 2056) color = CLB_CYAN;
			else if (num >= 1024) return;

			foreach (var L in SelectedHome) {
				if (L == SelectedHome.Origin) continue;
				var chr = sb[L.x, L.y];
				chr.Tile = null;
				chr.Char = '\u00B2';
				chr.Foreground = color;
			}

			foreach (var settler in Settlers) {
				if (settler.HomeID != SelectedHome.ID) continue;
				var C = settler.ParentObject.CurrentCell;
				var chr = sb[C.X, C.Y];
				chr.Foreground = color;
				chr.Detail = CLD_WHT;
			}
		}

		void RenderOrigins(ScreenBuffer sb) {
			var t = (XRLCore.FrameTimer.ElapsedMilliseconds + RenderOffset) & 4095;

			foreach (var home in ActiveSector.Homes) {
				var O = home.Origin;
				var chr = sb[O.x, O.y];
				chr.Tile = ICO_ORG;
				chr.Background = CLD_BLK;
				if (home == SelectedHome) {
					if (t < 16) {
						chr.Foreground = CLB_CYAN;
						chr.Detail = CLD_CYAN;
					} else if (t < 32) {
						chr.Foreground = CLD_CYAN;
						chr.Detail = CLB_BLUE;
					} else {
						chr.Foreground = CLB_BLUE;
						chr.Detail = CLB_CYAN;
					}
				} else {
					if (t < 16) {
						chr.Foreground = CLB_ORNG;
						chr.Detail = CLD_ORNG;
					} else if (t < 32) {
						chr.Foreground = CLD_ORNG;
						chr.Detail = CLD_RED;
					} else {
						chr.Foreground = CLD_RED;
						chr.Detail = CLB_ORNG;
					}

				}
			}

			RenderOffset += Stat.RandomCosmetic(0, 512);
		}

		public override void Leave() {
			base.Leave();
			ActiveSector.Flush();
		}

		#region Input
		public override bool Input(Keys keys) {
			if (!base.Input(keys)) {
				switch (keys) {
					case Keys.Q:
						PencilArea();
						break;
					case Keys.E:
						FillArea();
						break;
					case Keys.R:
						ToggleHome();
						break;
					default: return false;
				}
			}

			OnInput();
			return true;
		}

		bool IsSettler(GameObject Object) => Object.OwnPart<HearthpyreSettler>();

		void OnInput() {
			ActiveSettler = ActiveCell.GetFirstObject(IsSettler)?.TakePart<HearthpyreSettler>();
			ActiveHome = ActiveSector.Homes.FirstOrDefault(x => x.Origin == ActiveCell.location);
			IsActiveArea = SelectedHome != null && SelectedHome.Contains(ActiveCell);
			IsActiveSelected = (ActiveHome != null && ActiveHome == SelectedHome) ||
				(ActiveSettler != null && SelectedHome != null && ActiveSettler.HomeID == SelectedHome.ID);
		}

		void FillArea() {
			if (SelectedHome == null) {
				PlayUISound(SND_ERR);
			} else if (IsActiveArea) {
				ActiveCell.YieldCardinalFlood(x => SelectedHome.Remove(x)).Last();
				PlayUISound(SND_DSEL);
			} else {
				ActiveCell.YieldCardinalFlood(x => NoStructure(x) && SelectedHome.Add(x)).Last();
				PlayUISound(SND_SEL);
			}
		}

		void PencilArea() {
			if (SelectedHome == null) {
				PlayUISound(SND_ERR);
			} else if (IsActiveArea) {
				SelectedHome.Remove(CursorArea);
				PlayUISound(SND_DSEL);
			} else {
				SelectedHome.Add(CursorArea);
				PlayUISound(SND_SEL);
			}
		}

		void ToggleHome() {
			if (ActiveHome != null) {
				if (IsActiveSelected) {
					ActiveSector.RemoveHome(SelectedHome);
					SelectedHome = null;
					ActiveHome = null;
					PlayUISound(SND_REM);
				} else {
					SelectedHome = ActiveHome;
					PlayUISound(SND_SEL);
				}
			} else if (SelectedHome != null && ActiveSettler != null) {
				if (IsActiveSelected) {
					ActiveSettler.Home = null;
					PlayUISound(SND_DSEL);
				} else {
					ActiveSettler.Home = SelectedHome;
					PlayUISound(SND_SEL);
				}
			} else {
				ActiveHome = ActiveSector.NewHome(ActiveCell);
				SelectedHome = ActiveHome;
				PlayUISound(SND_ADD);
			}
		}
		#endregion

		public override void Help() {
			var text = MakeshiftPopupTitle("{{W|HOME MODE HELP}}") + "\n" +
				"Press {{W|R}} to contextually place a new home area, select another area, or remove the selected area. Use the {{W|Q}} or {{W|E}} keys to add/remove cells to the selected area.\n" +
				"Hover over a settler and press {{W|R}} to set/unset the selected area as their home. Settlers will not enter private homes without good reason, cells and objects not in any homes are considered public.\n\n" +
				"{{W|Right Click}}: Change mouse focus.\n" +
				"{{W|Left Click}}: Paint with focused tool.\n" +
				"{{W|Ctrl}} + {{W|8}}|{{W|2}}: Change cursor size.\n";

			Popup.Show(text);
		}
	}
}
