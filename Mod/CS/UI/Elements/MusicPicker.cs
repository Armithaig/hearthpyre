using ConsoleLib.Console;
using System;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using XRL.UI;
using System.Threading;
using System.Diagnostics;
using Hearthpyre.Realm;
using XRL;
using XRL.Language;
using static Hearthpyre.Static;

namespace Hearthpyre.UI
{
	public class MusicPicker : Element
	{
		Index MusicIndex;
		bool Disabled;
		bool ActiveFrame;

		public override event EventHandler Reload { add { } remove { } }
		public Zone ActiveZone;
		public Settlement ActiveSettlement;
		public GameObject ActivePerformer;
		public Notitia.Entry ActiveEntry;

		public MusicPicker() {
		}

		public override void Enter()
		{
			ActiveZone = The.Game.ZoneManager.ActiveZone;
			ActiveSettlement = RealmSystem.GetSettlement(ActiveZone);
			ActivePerformer = ActiveZone.GetFirstObject("ZoneMusic");

			Disabled = ActiveSettlement == null || !The.Player.OwnPart<HearthpyreGoverning_Heraldry>();
			MusicIndex = new Index(Notitia.AssertMusic);

			var track = ActivePerformer?.GetStringProperty("Track");
			if (track.IsNullOrEmpty())
			{
				track = SoundManager.MusicTrack;
			}
			if (!track.IsNullOrEmpty())
			{
				track = SoundManager.GetSoundName(track).ToString();
			}

			var found = -1;
			if (track.IsNullOrEmpty())
			{
				found = Notitia.Music.FindIndex(x => x.Name == "Village A");
			}
			else
			{
				found = Notitia.Music.FindIndex(x => x.Value.EqualsNoCase(track));
			}
			
			if (found == -1)
			{
				ActiveEntry = new Notitia.Entry(Grammar.MakeTitleCase(track), track);
			}
			else
			{
				MusicIndex.Set(found);
				ActiveEntry = Notitia.Music[MusicIndex];
			}
		}

		public override void Update() {
			ActiveFrame = true;
		}

		public override void Render(ScreenBuffer SB) {
			int x = SB.X - 3, y = SB.Y;

			var entryPos = (int)Math.Round(x + 3.5 - ActiveEntry.Name.Quantify() * 0.5);
			SB.Write(entryPos, y, (Disabled ? "{{K|" : "{{y|") + ActiveEntry.Name + "}}");

			if (ActiveFrame) {
				if (Disabled) SB.Write(x, y + 1, "{{K|<A {{C|\u000E}} D>}}");
				else SB.Write(x, y + 1, "{{y|<{{W|A}} {{C|\u000E}} {{W|D}}>}}");
				ActiveFrame = false;
			} else {
				SB.Write(x, y + 1, "   {{C|\u000E}}   ");
			}
		}

		public override bool Input(Keys Keys) {
			if (Keys == Keys.H) {
				ShowHelp();
				return true;
			}

			if (Disabled) return false;
			switch (Keys) {
				case Keys.A:
					MusicIndex--;
					ActiveEntry = Notitia.Music[MusicIndex];
					PlayUISound(SND_MENU);
					break;
				case Keys.D:
					MusicIndex++;
					ActiveEntry = Notitia.Music[MusicIndex];
					PlayUISound(SND_MENU);
					break;
				case Keys.Shift | Keys.A:
					Seek(-5); break;
				case Keys.Control | Keys.A:
					Seek(-25); break;
				case Keys.Shift | Keys.D:
					Seek(5); break;
				case Keys.Control | Keys.D:
					Seek(25); break;
				case Keys.Control | Keys.R:
					Enter(); break;
				default: return false;
			}

			SoundManager.PlayMusic(ActiveEntry.Value, Crossfade: true);
			return true;
		}

		public void Seek(float time) {
			GameManager.Instance.uiQueue.queueTask(() => {
				var source = SoundManager.MusicSources.GetValue("music")?.Audio;
				if (source == null) return;

				if (source.time + time > source.clip.length)
					source.time += time - source.clip.length;
				else if (source.time + time < 0f)
					source.time = 0f;
				else
					source.time += time;
			});
		}

		public void ShowHelp() {
			var text = MakeshiftPopupTitle("{{W|MUSIC HELP}} \u00c4 {{C|" + (ActiveEntry.Detail ?? ActiveEntry.Name) + "}}") + "\n";
			if (!The.Player.OwnPart<HearthpyreGoverning_Heraldry>()) text += "{{R|Requires the Heraldry skill.}}\n\n";

			text += "Press the {{W|A}} and {{W|D}} keys to crossfade between songs.\n\n" +
			   "Hold the {{W|Shift}} or {{W|Control}} modifiers while pressing {{W|A}} or {{W|D}} to seek backwards and forwards." +
			   "\n\nPress {{W|Control}} + {{W|R}} to reset the value.";

			Popup.Show(text);
		}

		public override void Leave() {
			base.Leave();
			if (Disabled) return;
			if (ActivePerformer == null)
				ActivePerformer = ActiveZone.GetCell(0, 0).AddObject("ZoneMusic");

			ActivePerformer.SetStringProperty("Track", ActiveEntry.Value);
		}
	}
}
