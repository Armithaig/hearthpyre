using ConsoleLib.Console;
using System;
using System.Linq;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using XRL.UI;
using System.Threading;
using System.Diagnostics;
using Hearthpyre.Realm;
using XRL;
using static Hearthpyre.Static;
using Color = UnityEngine.Color;

namespace Hearthpyre.UI
{
	public class ColorPicker : Element
	{
		public const string NAME = "HearthpyreColorPicker";
		public override event EventHandler Reload;

		ConsoleChar Icon;
		List<ColorPair> Colors;
		Index RowIndex;
		Index FGIndex;
		Index BGIndex;
		Index DTIndex;

		public Box Box;
		public bool FlipBox;
		public bool Visible;

		public string ColorString => "&" + Colors[FGIndex] + "^" + Colors[BGIndex];
		public string DetailColor => Colors[DTIndex].ToString();

		public ColorPicker() {
			Box = new Box(0, 0, 13, 7);
			Colors = new List<ColorPair>();
			Icon = new ConsoleChar();

			foreach (var pair in ColorUtility.ColorMap) {
				Colors.Add(new ColorPair {
					ID = pair.Key,
					Value = pair.Value
				});
			}
		}

		public void Retranslate(int x, int y) {
			Box = Box.Retranslate(x, y);
			FlipBox = Box.y2 > 20;
		}

		public void SetState(string tile, char fg, char bg, char dt) {
			Icon.Tile = tile;
			FGIndex = new Index(Colors, Colors.FindIndex(x => x.ID == fg));
			BGIndex = new Index(Colors, Colors.FindIndex(x => x.ID == bg));
			DTIndex = new Index(Colors, Colors.FindIndex(x => x.ID == dt));
			UpdateIcon();
		}

		public override void Enter() {
			RowIndex = new Index(The.Game.GetIntGameState(NAME + "Row", 0), 0, 2);
		}

		public override void Render(ScreenBuffer SB) {
			var fill = Box.Grow(-1);
			SB.Fill(fill.x1, fill.y1, fill.x2, fill.y2, 32, 0);
			if (FlipBox) {
				SB.FoldUp(Box.x1, Box.y1, Box.x2, Box.y2);
				SB[Box.x1 + 3, Box.y2].Copy(Icon);
			} else {
				SB.FoldDown(Box.x1, Box.y1, Box.x2, Box.y2);
				SB[Box.x1 + 3, Box.y1].Copy(Icon);
			}

			int x = Box.x1 + 2, y = Box.y1 + 1;
			SB.BasicWrite(x, y, "Foreground", CLD_WHT);
			RenderColour(SB, x + 1, y + 1, RowIndex == 0, Colors[FGIndex]);
			SB.BasicWrite(x, y + 2, "Background", CLD_WHT);
			RenderColour(SB, x + 1, y + 3, RowIndex == 1, Colors[BGIndex]);
			SB.BasicWrite(x + 2, y + 4, "Detail", CLD_WHT);
			RenderColour(SB, x + 1, y + 5, RowIndex == 2, Colors[DTIndex]);
		}

		void RenderColour(ScreenBuffer sb, int x, int y, bool selected, ColorPair color) {
			sb.Write(x, y, selected ? "<{{W|A}} \u00DE\u00DD {{W|D}}>" : "   \u00DE\u00DD   ");
			sb[x + 3, y].Foreground = color.Value;
			sb[x + 4, y].Foreground = color.Value;
		}

		public override bool Input(Keys Keys) {
			switch (Keys) {
				case Keys.W:
				case Keys.NumPad8:
					RowIndex--;
					The.Game.SetIntGameState(NAME + "Row", RowIndex);
					break;
				case Keys.S:
				case Keys.NumPad2:
					RowIndex++;
					The.Game.SetIntGameState(NAME + "Row", RowIndex);
					break;
				case Keys.Escape:
				case Keys.Control | Keys.C:
				case Keys.Shift | Keys.C:
				case Keys.C:
					Visible = false;
					break;
				case Keys.A:
					if (RowIndex == 0) FGIndex--;
					else if (RowIndex == 1) BGIndex--;
					else if (RowIndex == 2) DTIndex--;
					UpdateIcon();
					break;
				case Keys.D:
					if (RowIndex == 0) FGIndex++;
					else if (RowIndex == 1) BGIndex++;
					else if (RowIndex == 2) DTIndex++;
					UpdateIcon();
					break;
				case Keys.Enter:
				case Keys.Space:
					Visible = false;
					Reload.Invoke(this, new EventArgs());
					The.Game.SetIntGameState(NAME + "Index", RowIndex);
					break;
				case Keys.H:
					ShowHelp();
					break;
				default: return true;
			}

			MenuSound();
			return true;
		}

		public void UpdateIcon() {
			Icon.Foreground = Colors[FGIndex].Value;
			Icon.Background = Colors[BGIndex].Value;
			Icon.Detail = Colors[DTIndex].Value;
		}

		public void ShowHelp() {
			var text = MakeshiftPopupTitle("{{W|COLOR HELP}}") + "\n" +
				"Press the {{W|W}}, {{W|A}}, {{W|S}} and {{W|D}} keys to change colors.\n\n" +
				"{{W|Space}} or {{W|Enter}} to confirm your selection, {{W|Esc}} to cancel.";

			Popup.Show(text);
		}

		private class ColorPair
		{
			public char ID { get; set; }
			public Color Value { get; set; }

			public override string ToString() => ID.ToString();
		}
	}
}
