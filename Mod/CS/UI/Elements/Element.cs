using ConsoleLib.Console;
using System;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using XRL.UI;

namespace Hearthpyre.UI
{
	// TODO: Consider makin' this abstract class to shove some basic UI methods here.
	// A lot of picker functionality could be handled generally here.
	// Once mono compiler can handle inheritance > 2.
	public abstract class Element
	{
		public abstract event EventHandler Reload;
		// bool FlipBox

		public virtual void Enter() {}
		public virtual void Update() {}
		public virtual void Render(ScreenBuffer SB) {}
		public virtual bool Input(Keys Keys) => false;
		public virtual void Leave() => ScreenBuffer.ClearImposterSuppression();
	}
}
