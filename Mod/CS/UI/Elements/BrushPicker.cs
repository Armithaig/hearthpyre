using System;
using System.Reflection;
using System.Collections.Generic;
using XRL;
using XRL.UI;
using ConsoleLib.Console;

using static Hearthpyre.Static;
using Color = UnityEngine.Color;

namespace Hearthpyre.UI
{
	public class BrushPicker : Element
	{
		public const string NAME = "HearthpyreBrushPicker";

		public override event EventHandler Reload;
		public List<Brush> Brushes;
		public ColorBrush ColorBrush;
		public Brush ActiveBrush;
		Index BrushIndex;

		public Box Box;
		public bool FlipBox;
		public int Rows;
		public bool Visible;

		public BrushPicker() {
			Box = new Box(0, 0, 13, 8);
			Rows = 4;
			InitBrushes();
		}

		void InitBrushes() {
			Brushes = new List<Brush>();
			var brushType = typeof(Brush);
			foreach (var type in Assembly.GetExecutingAssembly().GetTypes()) {
				if (type.IsAbstract) continue;
				if (!type.IsSubclassOf(brushType)) continue;

				var brush = Activator.CreateInstance(type) as Brush;
				var color = brush as ColorBrush;
				Brushes.Add(brush);
				if (color != null) ColorBrush = color;
			}
		}

		public void Retranslate(int x, int y) {
			Box = Box.Retranslate(x, y);
			FlipBox = Box.y2 > 20;
		}

		public override void Enter() {
			BrushIndex = new Index(Brushes, The.Game.GetIntGameState(NAME + "Index", 0));
			ColorBrush.Color = The.Game.GetStringGameState(NAME + "Color", "&B^k");
			ColorBrush.Detail = The.Game.GetStringGameState(NAME + "Detail", "M");
			UpdateBrush();
		}

		public override void Render(ScreenBuffer sb) {
			var fill = Box.Grow(-1);
			sb.Fill(fill.x1, fill.y1, fill.x2, fill.y2, 32, 0);
			if (FlipBox) sb.FoldUp(Box.x1, Box.y1, Box.x2, Box.y2);
			else sb.FoldDown(Box.x1, Box.y1, Box.x2, Box.y2);
			//RenderBox(sb, Box.x1, Box.y1, Box.x2, Box.y2);
			//sb.Write(Box.x1 + 2, Box.y1, "[ &WCategory&y ]");
			//sb.Write(Box.x2 - 14, Box.y1, "[ &WBlueprint&y ]");

			int x = Box.x1 + 3, y = Box.MidY;
			RenderLine(sb, x, y, ActiveBrush, CLB_CYAN);
			for (int i = 1; i < Rows; i++) {
				var clr = (i == Rows - 1) ? CLB_BLK : CLD_WHT;
				RenderLine(sb, x, y + i, Brushes[BrushIndex + i], clr);
				RenderLine(sb, x, y - i, Brushes[BrushIndex - i], clr);
			}
		}

		void RenderLine(ScreenBuffer sb, int x, int y, Brush paint, Color color) {
			sb[x, y].Copy(paint.Icon);
			sb.BasicWrite(x + 1, y, paint.Name, color);
		}

		public override bool Input(Keys keys) {
			switch (keys) {
				case Keys.W:
				case Keys.NumPad8:
					BrushIndex--;
					UpdateBrush();
					break;
				case Keys.S:
				case Keys.NumPad2:
					BrushIndex++;
					UpdateBrush();
					break;
				case Keys.Escape:
				case Keys.Control | Keys.R:
				case Keys.Shift | Keys.R:
				case Keys.R:
					Visible = false;
					break;
				case Keys.Enter:
				case Keys.Space:
					Visible = false;
					Reload.Invoke(this, new EventArgs());
					The.Game.SetIntGameState(NAME + "Index", BrushIndex);
					break;
				case Keys.H:
					ShowHelp();
					break;
				default: return true;
			}

			MenuSound();
			return true;
		}

		public void UpdateBrush() {
			ActiveBrush = Brushes[BrushIndex];
		}

		public void ShowHelp() {
			var text = MakeshiftPopupTitle("{{W|BRUSH HELP}}") + "\n" +
				"Press the {{W|W}} and {{W|S}} keys to flip up and down through brushes .\n\n" +
				"{{W|Space}} or {{W|Enter}} to confirm your selection, {{W|Esc}} to cancel.";

			Popup.Show(text);
		}

	}
}
