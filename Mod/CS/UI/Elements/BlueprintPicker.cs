using ConsoleLib.Console;
using System;
using System.Linq;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using XRL.UI;
using System.Threading;
using System.Diagnostics;
using Hearthpyre.Realm;
using XRL;
using XRL.World.Tinkering;
using static Hearthpyre.Static;
using Color = UnityEngine.Color;

namespace Hearthpyre.UI
{
	public class BlueprintPicker : Element
	{
		public const string NAME = "HearthpyreBlueprintPicker";

		public override event EventHandler Reload;

		public List<Notitia.Category> Categories { get; private set; }
		public Notitia.Category ActiveCategory { get; private set; }
		public Index IndexCategory { get; private set; }

		public List<Notitia.Blueprint> Blueprints { get; private set; }
		public Notitia.Blueprint ActiveBlueprint { get; private set; }
		public Index IndexBlueprint { get; private set; }

		public Box Box;
		public bool FlipBox;
		public Index Column;
		public int Rows;
		public bool Visible;
		public bool Powered;

		public BlueprintPicker() {
			Rows = 4;
			Box = new Box(0, 0, 22, 8);
		}

		public void Retranslate(int x, int y) {
			Box = Box.Retranslate(x, y);
			FlipBox = Box.y2 > 20;
		}

		public override void Enter() {
			var packages = HearthpyreXyloschemer.ACTIVE?.Packages ?? new List<string>();
			Categories = Notitia.AssertCategories.Select(x => x.Value.Filter(packages))
				.Where(x => x.Blueprints.Count > 0)
				.ToList();

			Categories.Sort((x, y) => String.CompareOrdinal(x.Name, y.Name));
			
			Powered = !The.Game.HasIntGameState(GST_PWR);
			IndexCategory = new Index(Categories, The.Game.GetIntGameState(NAME + "Category", 0));
			IndexBlueprint = new Index();
			Column = new Index(max: 1);
			
			UpdateCategory();
		}

		public override void Render(ScreenBuffer SB) {
			var fill = Box.Grow(-1);
			SB.Fill(fill.x1, fill.y1, fill.x2, fill.y2, 32, 0);
			if (FlipBox) SB.FoldUp(Box.x1, Box.y1, Box.x2, Box.y2);
			else SB.FoldDown(Box.x1, Box.y1, Box.x2, Box.y2);
			//RenderBox(sb, Box.x1, Box.y1, Box.x2, Box.y2);
			//sb.Write(Box.x1 + 2, Box.y1, "[ &WCategory&y ]");
			//sb.Write(Box.x2 - 14, Box.y1, "[ &WBlueprint&y ]");

			// Left column, categories
			int x = Box.x1 + 1, y = Box.MidY;
			RenderLine(SB, x, y, ActiveCategory, Column == 0 ? "&C" : "&y");
			for (int i = 1; i < Rows; i++) {
				var clr = (Column == 1 || i == Rows - 1) ? "&K" : "&y";
				RenderLine(SB, x, y + i, Categories[IndexCategory + i], clr);
				RenderLine(SB, x, y - i, Categories[IndexCategory - i], clr);
			}

			RenderSplitter(SB, Box.x1 + 7, CLD_WHT);

			// Right column, blueprints
			x = Box.x1 + 8;
			RenderLine(SB, x, y, ActiveBlueprint, Column == 1 ? "&C" : "&y");
			for (int i = 1; i < Rows; i++) {
				var clr = (Column == 0 || i == Rows - 1) ? "&K" : "&y";
				RenderLine(SB, x, y + i, Blueprints[IndexBlueprint + i], clr);
				RenderLine(SB, x, y - i, Blueprints[IndexBlueprint - i], clr);
			}

			// Charge cost indicator
			var pos = Box.y2 - 1;
			RenderCharge(SB, Box.x2 - 1, ref pos);
			RenderTinker(SB, Box.x2 - 1, ref pos);
			RenderElectrical(SB, Box.x2 - 1, ref pos);
			RenderMechanical(SB, Box.x2 - 1, ref pos);
			RenderHydraulic(SB, Box.x2 - 1, ref pos);
		}


		void RenderLine(ScreenBuffer sb, int x, int y, Notitia.Category cat, string color = "") {
			sb[x, y].Copy(cat.Icon);
			sb.Write(x + 1, y, color + cat.Name);
		}

		void RenderLine(ScreenBuffer sb, int x, int y, Notitia.Blueprint blp, string color = "") {
			sb[x, y].Copy(blp.Icon);
			sb.Write(x + 1, y, color + blp.Name);
		}

		void RenderSplitter(ScreenBuffer sb, int x, Color color) {
			for (int y = Box.y1; y <= Box.y2; y++) {
				var chr = sb[x, y];
				chr.Foreground = color;
				chr.Background = CLD_BLK;
				if (y == Box.y1) chr.Char = '\u00C2';
				else if (y == Box.y2) chr.Char = '\u00C1';
				else chr.Char = '\u00B3';
			}
		}

		void RenderBox(ScreenBuffer sb, int x1, int y1, int x2, int y2) {
			for (int i = x1; i <= x2; i++) {
				var b = sb[i, y2];

				b.Char = '\u00C4';
				b.Foreground = CLD_WHT;
				b.Background = CLD_BLK;
			}
			for (int i = y1; i <= y2; i++) {
				var l = sb[x1, i];
				var r = sb[x2, i];

				l.Char = '\u00B3';
				l.Foreground = CLD_WHT;
				l.Background = CLD_BLK;
				r.Char = '\u00B3';
				r.Foreground = CLD_WHT;
				r.Background = CLD_BLK;
			}
			sb[x1, y1].Char = '\u00C2';
			sb[x2, y1].Char = '\u00C2';
			sb[x1, y2].Char = '\u00C0';
			sb[x2, y2].Char = '\u00D9';
		}

		void RenderCharge(ScreenBuffer SB, int X, ref int Y)
		{
			var tier = ActiveBlueprint.Value.Tier;
			var icon = "Icons/hp_charge_1.png";
			var detail = CLB_GRN;

			if (tier > 5)
			{
				icon = "Icons/hp_charge_3.png";
				detail = CLB_RED;
			}
			else if (tier > 2)
			{
				icon = "Icons/hp_charge_2.png";
				detail = CLB_YEL;
			}

			//sb.Write(x - 1, y, " \0 ");
			SB.Display(X, Y--, icon, CLD_CYAN, detail);
		}

		void RenderElectrical(ScreenBuffer SB, int X, ref int Y)
		{
			if (!ActiveBlueprint.Electrical) return;
			SB.Display(X, Y--, "Mutations/electrical_generation.bmp", Powered ? CLB_YEL : CLB_BLK, Powered ? CLB_YEL : CLD_BLK);
		}
		
		void RenderMechanical(ScreenBuffer SB, int X, ref int Y)
		{
			if (!ActiveBlueprint.Mechanical) return;
			SB.Display(X, Y--, "Icons/hp_pulleys.png", Powered ? CLB_CYAN : CLB_BLK, Powered ? CLB_RED : CLD_BLK);
		}

		void RenderHydraulic(ScreenBuffer SB, int X, ref int Y)
		{
			if (!ActiveBlueprint.Hydraulic) return;
			SB.Display(X, Y--, "Mutations/slime_glands.bmp", Powered ? CLB_BLUE : CLB_BLK, Powered ? CLB_CYAN : CLD_BLK);
		}

		void RenderTinker(ScreenBuffer SB, int X, ref int Y)
		{
			if (!ActiveBlueprint.Tinkered || OptionNoBits) return;
			SB.Display(X, Y--, "Mutations/dystechnia.bmp", CLD_WHT, CLB_CYAN);
		}

		public override bool Input(Keys Keys) {
			if (!Visible) return false;

			switch (Keys) {
				case Keys.W:
				case Keys.NumPad8:
					if (Column == 0) {
						IndexCategory--;
						UpdateCategory();
					} else {
						IndexBlueprint--;
						UpdateBlueprint();
					}
					break;
				case Keys.S:
				case Keys.NumPad2:
					if (Column == 0) {
						IndexCategory++;
						UpdateCategory();
					} else {
						IndexBlueprint++;
						UpdateBlueprint();
					}
					break;
				case Keys.A:
				case Keys.NumPad4:
					Column--;
					break;
				case Keys.D:
				case Keys.NumPad6:
					Column++;
					break;
				case Keys.L:
					try
					{
						//var sample = GameObjectFactory.Factory.CreateObject(ActiveBlueprint.Value, BonusModChance: -99999, beforeObjectCreated: Brand);
						var sample = GameObjectFactory.Factory.CreateObject(ActiveBlueprint.Value, BonusModChance: -99999);
						Brand(sample);
						sample.StripContents(Silent: true);
						sample.AddPart(new HearthpyreBlueprint { Tinkered = ActiveBlueprint.Tinkered && !OptionNoBits });
						InventoryActionEvent.Check(sample, The.Player, sample, "Look");
					}
					catch (Exception)
					{
						// ignored
					}

					return true;
				case Keys.Control | Keys.X:
				case Keys.Shift | Keys.X:
				case Keys.X:
					Powered = !Powered;
					if (Powered) The.Game.RemoveIntGameState(GST_PWR);
					else The.Game.SetIntGameState(GST_PWR, 1);
					return true;
				case Keys.Control | Keys.S:
				case Keys.Control | Keys.F:
				case Keys.Shift | Keys.S:
				case Keys.Shift | Keys.F:
					SearchBlueprint();
					return true;
				case Keys.Escape:
				case Keys.Control | Keys.R:
				case Keys.Shift | Keys.R:
				case Keys.R:
					Visible = false;
					break;
				case Keys.Enter:
				case Keys.Space:
					Visible = false;
					Reload.Invoke(this, new EventArgs());
					The.Game.SetIntGameState(NAME + "Category", IndexCategory);
					break;
				case Keys.H:
					ShowHelp();
					break;
				default: return true;
			}

			MenuSound();
			return true;
		}

		public void UpdateCategory(int blueprint = -1) {
			ActiveCategory = Categories[IndexCategory];
			Blueprints = ActiveCategory.Blueprints;
			IndexBlueprint.Max = Blueprints.Count - 1;
			IndexBlueprint.Min = 0;
			IndexBlueprint.Set(blueprint >= 0 ? blueprint : The.Game.GetIntGameState(NAME + ActiveCategory.Name, 0));
			UpdateBlueprint();
		}

		public void UpdateBlueprint() {
			ActiveBlueprint = Blueprints[IndexBlueprint];
			The.Game.SetIntGameState(NAME + ActiveCategory.Name, IndexBlueprint);
		}


		public void SearchBlueprint() {
			var query = Popup.AskString("Search for a blueprint.", Default: "", MaxLength: 100);
			if (string.IsNullOrEmpty(query)) return;

			var indexCategory = -1;
			var indexBlueprint = -1;
			var match = Int32.MaxValue;
			for (int ic = 0; ic < Categories.Count; ic++) {
				var category = Categories[ic];
				for (int ib = 0; ib < category.Blueprints.Count; ib++) {
					var blueprint = category.Blueprints[ib];
					var m = StrDist(query, blueprint.Name);
					var display = blueprint.Value.CachedDisplayNameStripped;
					if (!String.IsNullOrEmpty(display)) m = Math.Min(m, StrDist(query, display));

					if (match > m) {
						indexCategory = ic;
						indexBlueprint = ib;
						match = m;
					}
				}
			}

			if (indexCategory >= 0 && indexBlueprint >= 0) {
				IndexCategory.Set(indexCategory);
				Column.Set(1);
				UpdateCategory(indexBlueprint);
				if (!Visible) {
					Reload.Invoke(this, new EventArgs());
					The.Game.SetIntGameState(NAME + "Category", IndexCategory);
				}
				MenuSound();
			} else {
				PlayUISound(SND_ERR);
			}
		}

		public void ShowHelp() {
			var text = MakeshiftPopupTitle("{{W|BLUEPRINT HELP}}") + "\n" +
				"Press the {{W|W}}, {{W|A}}, {{W|S}}, {{W|D}} or {{W|Arrow}} keys to navigate the blueprints.\n" +
				"Categories are displayed in the left column and blueprints within said category in the right column.\n\n" +
				"Icons will appear on the right detailing the blueprint's charge cost, bit requirement, or power system.\n\n" +
				"{{W|Space}} | {{W|Enter}}: Confirm selection.\n" +
				"{{W|Shift}} + {{W|S}}: Search for blueprint.\n" +
				"{{W|L}}: Examine blueprint.\n" +
				"{{W|X}}: Toggle power systems.\n" +
				"{{W|Esc}}: Cancel.";

			Popup.Show(text);
		}
	}
}
