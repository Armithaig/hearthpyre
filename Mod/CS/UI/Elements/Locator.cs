using ConsoleLib.Console;
using System;
using System.Linq;
using XRL.Core;
using XRL.World;
using XRL.World.WorldBuilders;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.UI;
using XRL.Language;
using Hearthpyre.Realm;
using Qud.API;
using System.Diagnostics;
using XRL;
using XRL.World.Effects;
using static Hearthpyre.Static;

namespace Hearthpyre.UI
{
	public class Locator : Element
	{
		bool Disabled;
		bool ActiveFrame;

		public override event EventHandler Reload;
		public ZoneManager Manager;
		public Zone ActiveZone;
		public GameObject ActiveTerrain;
		public Settlement ActiveSettlement;
		public Sector ActiveSector;
		public bool ActiveMutable;
		public bool AllClaim;

		public Locator()
		{ }

		public override void Enter()
		{
			Manager = The.Game.ZoneManager;
			AllClaim = OptionAllClaim;
			
			Disabled = !The.Player.OwnPart<HearthpyreGoverning_Locate>();
#if !BUILD_2_0_203
			if (Manager.ActiveZone is InteriorZone)
			{
				Disabled = true;
			}
#endif
			
			ActiveZone = Manager.ActiveZone;
			ActiveTerrain = ActiveZone.GetTerrainObject();
			ActiveSettlement = RealmSystem.GetSettlement(ActiveZone);
			ActiveSector = ActiveSettlement?.GetSector(ActiveZone);
			ActiveMutable = Manager.GetCellBlueprints(ActiveZone.ZoneID).All(b => b.Mutable);
		}

		public override void Update()
		{
			ActiveFrame = true;
		}

		public override void Render(ScreenBuffer SB)
		{
			int x = SB.X - 4, y = SB.Y;

			var text = "Claim";
			if (ActiveSettlement != null)
			{
				if (ActiveSector != null)
				{
					text = "Label";
				}
				else
				{
					text = "Annex";
				}
			}

			if (ActiveFrame)
			{
				if (Disabled) SB.Write(x, y, "{{K|> " + text + " <}}");
				else SB.Write(x, y, "> {{C|" + text + "}} <");
				ActiveFrame = false;
			}
			else
			{
				SB.Write(x, y, "  " + text + "  ");
			}
		}

		public override bool Input(Keys Keys)
		{
			if (Disabled) return false;
			switch (Keys)
			{
				case Keys.Space:
				case Keys.Enter:
					Locate();
					break;
				case Keys.H:
					ShowHelp();
					break;
				case Keys.Control | Keys.C:
				case Keys.Shift | Keys.C:
					if (Context()) PlayUISound(SND_TEXT);
					break;
				case Keys.Control | Keys.D:
				case Keys.Shift | Keys.D:
					if (Describe()) PlayUISound(SND_TEXT);
					break;
				default: return false;
			}


			return true;
		}

		void Locate()
		{
			if (ActiveSettlement != null)
			{
				if (ActiveSector != null)
				{
					if (Label()) PlayUISound(SND_TEXT);
				}
				else
				{
					if (Annex()) PlayUISound(SND_ADD);
				}
			}
			else
			{
				Claim();
			}
		}

		bool IsPrimeZone()
		{
			if (ActiveMutable) return false;
			
			var name = ColorUtility.StripFormatting(ActiveTerrain.pRender.DisplayName);
			var blueprint = Manager.GetZoneBlueprint(ActiveZone.ZoneID);
			return name == ColorUtility.StripFormatting(blueprint.Name)
			       || name == ColorUtility.StripFormatting(blueprint.NameContext);
		}

		bool IsLair()
		{
			return Manager.ZoneHasBuilder(ActiveZone.ZoneID, "BasicLair");
		}

		private bool HasGeneratedLocation()
		{
			var world = (WorldInfo) The.Game.GetObjectGameState("JoppaWorldInfo");
			var locations = world.GetGeneratedLocationsAt(Zone.zoneIDTo240x72Location(ActiveZone.ZoneID));
			return locations != null && locations.Any(i => i.targetZone == ActiveZone.ZoneID);
		}

		private int CheckInvalid()
		{
			if (The.Player.OwnEffect<Lost>())
			{
				Popup.ShowFail("You can't do that while lost.");
				return 0;
			}

			var protocol = WorldFactory.Factory.getWorld(ActiveZone.ZoneWorld)?.Protocol;
			if (protocol == "THIN")
			{
				if (!AllClaim) Popup.ShowFail("You lack a bodily tether to manifest.");
				return AllClaim ? 1 : 0;
			}

			if (protocol == "CLAM")
			{
				if (!AllClaim) Popup.ShowFail("You are unsure how you came to be here.");
				return AllClaim ? 1 : 0;
			}

			if (HasGeneratedLocation() || IsPrimeZone() || IsLair())
			{
				if (!AllClaim) Popup.ShowFail("This zone is owned by someone else and can't be claimed by you.");
				return AllClaim ? 1 : 0;
			}

			return 2;
		}

		private int GetClaimability()
		{
			try
			{
				var player = The.Player;
				var count = RealmSystem.Settlements.Count;
				if (count >= 1 && !player.OwnPart<HearthpyreGoverning_GovernI>() ||
				    count >= 2 && !player.OwnPart<HearthpyreGoverning_GovernII>() ||
				    count >= 4 && !player.OwnPart<HearthpyreGoverning_GovernIII>())
				{
					Popup.ShowFail("Your governing skills are at capacity.");
					return 0;
				}

				var invalid = CheckInvalid();
				if (invalid == 0) return 0;

				if (invalid < 2 || !ActiveMutable || ActiveTerrain.GetIntProperty("ForceMutableSave") >= 1)
				{
					var result = Popup.ShowYesNo("This parasang is already owned by someone else." +
					                             "\nDo you want to claim the zone anyway?" +
					                             "\n\n{{R|You will be unable to modify the world map tile.}}");
					return result == DialogResult.Yes ? 1 : 0;
				}
			}
			catch (Exception e)
			{
				Log("Error checking claimability", e);
				Popup.ShowFail("{{K|" + Grammar.Weirdify("You are not welcome here." + "}}"));
				return 0;
			}

			return 2;
		}

		void Claim()
		{
			var claimability = GetClaimability();
			if (claimability == 0) return;
			if (!Label()) return;

			var name = ActiveZone.GetBareDisplayName();
			ActiveSettlement = RealmSystem.NewSettlement(ActiveZone, Visible: claimability >= 2);
			ActiveSector = ActiveSettlement.NewSector(ActiveZone);
			PlayUISound(SND_CLM);

			if (!JournalAPI.GetMapNotesForZone(ActiveZone.ZoneID).Any(x => x.category == "Settlements"))
			{
				JournalAPI.AddMapNote(ActiveZone.ZoneID, name, "Settlements", null, ActiveSettlement.ID.ToString(), true, true);
			}

			if (ActiveSettlement.Visible) AddMapTile(name);
			Reload.Invoke(this, new EventArgs());
		}

		void AddMapTile(string Name)
		{
			var terrain = ActiveTerrain;
			terrain.SetIntProperty("HearthpyreSettlement", 1);
			terrain.SetIntProperty("ForceMutableSave", 1);
			terrain.SetIntProperty("ProperNoun", 1);
			terrain.SetStringProperty("OverlayColor", "&W");
			terrain.RemovePart<AnimatedMaterialSaltDunes>();
			terrain.RemovePart<AnimatedMaterialOverlandWater>();

			var render = terrain.pRender;
			render.Tile = ICO_CLM;
			render.RenderString = "#";
			render.DetailColor = The.Player.pRender.DetailColor;
			render.DisplayName = Name;

			var color = render.DetailColor;
			while (color == render.DetailColor)
				color = Crayons.GetRandomColorAll();
			render.ColorString = '&' + color;
		}

		bool Label()
		{
			var name = Popup.AskString("Enter a name for this zone. {{W|ESC}} to cancel.", Default: ActiveZone.GetBareDisplayName(), MaxLength: 99);
			if (String.IsNullOrEmpty(name)) return false;

			ActiveZone.HasProperName = true;
			ActiveZone.NamedByPlayer = true;
			ActiveZone.IncludeContextInZoneDisplay = true;
			ActiveZone.IncludeStratumInZoneDisplay = false;
			ActiveZone.DisplayName = name;
			if (ActiveSector?.Prime == true)
			{
				if (ActiveSettlement.Visible) ActiveTerrain.DisplayName = name;
				var note = JournalAPI.GetMapNote(ActiveSettlement.ID.ToString());
				if (note != null)
				{
					note.text = name;
					note.Updated();
				}
			}

			Reload.Invoke(this, new EventArgs());
			return true;
		}

		bool Context()
		{
			var name = Popup.AskString("Enter a name context for this zone. {{W|ESC}} to cancel.", Default: ActiveZone.NameContext, MaxLength: 99);
			if (String.IsNullOrEmpty(name)) return false;

			ActiveZone.NameContext = name;
			return true;
		}

		bool Annex()
		{
			if (CheckInvalid() == 0) return false;
			
			ActiveSector = ActiveSettlement.NewSector(ActiveZone);
			Reload.Invoke(this, new EventArgs());
			return true;
		}

		bool Describe()
		{
			var part = ActiveSettlement?.Terrain?.TakePart<Description>();
			if (part == null) return false;
			if (!ActiveSettlement.Visible) return false;

			var desc = Popup.AskString("Compose a description for this settlement. {{W|ESC}} to cancel.", Default: part._Short, MaxLength: 256);
			if (String.IsNullOrEmpty(desc)) return false;

			part.Short = desc;
			return true;
		}

		public void ShowHelp()
		{
			var text = MakeshiftPopupTitle("{{W|LOCATE HELP}}") + "\n" +
			           "Press the {{W|Enter}} or {{W|Space}} keys to claim, label or annex the current zone.\n\n" +
			           "Claiming erects a new settlement, labeling will rename the current zone, and annexing will attach this zone to the existing settlement.\n\n" +
			           "{{W|Shift}} + {{W|C}}: Set name context.\n" +
			           "{{W|Shift}} + {{W|D}}: Set description.";

			Popup.Show(text);
		}

		public bool IsWidget(GameObject Object) => Object.OwnPart<HearthpyreSectorWidget>();

		public override void Leave()
		{
			base.Leave();
			if (ActiveSector != null)
			{
				var C = ActiveZone.GetCell(0, 0);
				if (!C.HasObject(IsWidget))
				{
					C.AddObject(OBJ_SECT);
				}
				
				ActiveSector.RequireCheckpoint(ActiveZone);
			}
		}
	}
}
