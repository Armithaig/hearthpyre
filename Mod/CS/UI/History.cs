﻿using System;
using System.Linq;
using System.Collections.Generic;
using ConsoleLib.Console;
using XRL.World;
using static Hearthpyre.Static;

namespace Hearthpyre.UI
{
    public abstract class History<T>
    {
        protected class Entry
        {
            public T Memory;
            public Cell Cell;
            public bool Add;
        }

        protected List<Entry> Current;
        protected List<List<Entry>> Chunks = new List<List<Entry>>(8);
        protected int Step = 0;

        protected abstract bool CheckEntry(Entry Entry);
        protected abstract void UndoEntry(Entry Entry);
        protected abstract void RedoEntry(Entry Entry);
        protected abstract void ReleaseEntry(Entry Entry);

        public bool Undo()
        {
            var n = Chunks.Count;
            if (n == 0 || Step >= n) return false;

            var chunk = Chunks[n -= ++Step];
            for (int i = chunk.Count - 1; i >= 0; i--)
            {
                if (!CheckEntry(chunk[i])) chunk.RemoveAt(i);
                else UndoEntry(chunk[i]);
            }

            if (chunk.Count == 0)
            {
                Chunks.RemoveAt(n);
                Step--;
                return false;
            }

            return true;
        }

        public bool Redo()
        {
            var n = Chunks.Count;
            if (n == 0 || Step == 0) return false;

            var chunk = Chunks[n -= Step--];
            for (int i = chunk.Count - 1; i >= 0; i--)
            {
                if (!CheckEntry(chunk[i])) chunk.RemoveAt(i);
                else RedoEntry(chunk[i]);
            }

            if (chunk.Count == 0)
            {
                Chunks.RemoveAt(n);
                return false;
            }

            return true;
        }

        public void Start()
        {
            if (Current != null && Current.Count == 0) return;
            Current = new List<Entry>();
        }

        public void Add(T Memory, Cell Cell)
        {
            Current.Add(new Entry { Memory = Memory, Cell = Cell, Add = true });
        }

        public void Remove(T Memory, Cell Cell)
        {
            Current.Add(new Entry { Memory = Memory, Cell = Cell, Add = false });
        }

        public void End()
        {
            if (Current.Count == 0) return;

            for (; Step > 0; Step--)
            {
                //Release(Chunks[i = Chunks.Count - 1], true);
                Chunks.RemoveAt(Chunks.Count - 1);
            }

            if (Chunks.Count >= 8)
            {
                //Release(Chunks[0]);
                Chunks.RemoveAt(0);
            }

            Chunks.Add(Current);
        }

        protected void Release(List<Entry> Chunk, bool Active = false)
        {
            for (int i = 0, c = Chunk.Count; i < c; i++)
            {
                if (Chunk[i].Add == Active && CheckEntry(Chunk[i]))
                {
                    ReleaseEntry(Chunk[i]);
                }
            }
        }
    }

    public class SchemeHistory : History<GameObject>
    {
        protected override bool CheckEntry(Entry Entry)
        {
#if RC1_PLAGUE
            return !Entry.Cell.ParentZone.Suspended
#else
            return !Entry.Cell.ParentZone.bSuspended
#endif
                   && Entry.Memory.IsValid()
                   && !Entry.Memory.IsInGraveyard()
                ;
        }

        protected override void UndoEntry(Entry Entry)
        {
            if (Entry.Add) Entry.Cell.RemoveObject(Entry.Memory);
            else Entry.Cell.AddObject(Entry.Memory);
        }

        protected override void RedoEntry(Entry Entry)
        {
            if (Entry.Add) Entry.Cell.AddObject(Entry.Memory);
            else Entry.Cell.RemoveObject(Entry.Memory);
        }

        protected override void ReleaseEntry(Entry Entry)
        {
            Entry.Memory.Obliterate();
        }
    }

    public class PaintRenderable
    {
        public string Tile;
        public string ColorString;
        public string DetailColor;
        public string RenderString;
        public string TileColor;
        public GameObject Object;

        public PaintRenderable(Cell Cell, GameObject Object = null)
        {
            Tile = Cell.PaintTile;
            ColorString = Cell.PaintColorString;
            DetailColor = Cell.PaintDetailColor;
            RenderString = Cell.PaintRenderString;
            TileColor = Cell.PaintTileColor;
            this.Object = Object;
        }
    }

    public class PaintMemory : Renderable
    {
        public PaintRenderable Before;
        public PaintRenderable After;

        public PaintMemory()
        {
        }

        public PaintMemory(Cell Cell)
        {
            SetBefore(Cell);
        }

        public void SetBefore(Cell Cell)
        {
            Before = new PaintRenderable(Cell, Cell.GetFirstObjectWithPropertyOrTag(TAG_PNT));
        }

        public void SetAfter(Cell Cell)
        {
            After = new PaintRenderable(Cell, Cell.GetFirstObjectWithPropertyOrTag(TAG_PNT));
        }

        public bool IsEmpty()
        {
            if (Before.Tile != After.Tile) return false;
            if (Before.ColorString != After.ColorString) return false;
            if (Before.DetailColor != After.DetailColor) return false;
            if (Before.RenderString != After.RenderString) return false;
            if (Before.TileColor != After.TileColor) return false;
            if (Before.Object != After.Object) return false;
            return true;
        }
    }

    public class PaintHistory : History<PaintMemory>
    {
        protected override bool CheckEntry(Entry Entry)
        {
#if RC1_PLAGUE
            if (Entry.Cell.ParentZone.Suspended) return false;
#else
            if (Entry.Cell.ParentZone.bSuspended) return false;
#endif
            if (Entry.Memory.Before.Object != null)
            {
                if (Entry.Memory.Before.Object.IsInvalid()) return false;
                if (Entry.Memory.Before.Object.IsInGraveyard()) return false;
            }

            if (Entry.Memory.After.Object != null)
            {
                if (Entry.Memory.After.Object.IsInvalid()) return false;
                if (Entry.Memory.After.Object.IsInGraveyard()) return false;
            }

            return true;
        }

        protected override void UndoEntry(Entry Entry)
        {
            Entry.Cell.PaintTile = Entry.Memory.Before.Tile;
            Entry.Cell.PaintColorString = Entry.Memory.Before.ColorString;
            Entry.Cell.PaintDetailColor = Entry.Memory.Before.DetailColor;
            Entry.Cell.PaintRenderString = Entry.Memory.Before.RenderString;
            Entry.Cell.PaintTileColor = Entry.Memory.Before.TileColor;
            if (Entry.Memory.After.Object != null) Entry.Cell.RemoveObject(Entry.Memory.After.Object);
            if (Entry.Memory.Before.Object != null) Entry.Cell.AddObject(Entry.Memory.Before.Object);
        }

        protected override void RedoEntry(Entry Entry)
        {
            Entry.Cell.PaintTile = Entry.Memory.After.Tile;
            Entry.Cell.PaintColorString = Entry.Memory.After.ColorString;
            Entry.Cell.PaintDetailColor = Entry.Memory.After.DetailColor;
            Entry.Cell.PaintRenderString = Entry.Memory.After.RenderString;
            Entry.Cell.PaintTileColor = Entry.Memory.After.TileColor;
            if (Entry.Memory.Before.Object != null) Entry.Cell.RemoveObject(Entry.Memory.Before.Object);
            if (Entry.Memory.After.Object != null) Entry.Cell.AddObject(Entry.Memory.After.Object);
        }

        protected override void ReleaseEntry(Entry Entry)
        {
            if (Entry.Memory.Before.Object != null)
            {
                if (Entry.Memory.Before.Object.CurrentCell == null) Entry.Memory.Before.Object.Obliterate();
            }

            if (Entry.Memory.After.Object != null)
            {
                if (Entry.Memory.After.Object.CurrentCell == null) Entry.Memory.After.Object.Obliterate();
            }
        }
    }
}
