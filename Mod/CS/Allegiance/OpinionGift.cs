using XRL.World;
using XRL.World.AI;

namespace Hearthpyre
{

#if !BUILD_2_0_206
	public class OpinionGift : IOpinionSubject
	{

		public override int BaseValue => 75;

		public override string GetText(GameObject Actor)
		{
			return $"Gave me a gift.";
		}

	}
#endif

}
