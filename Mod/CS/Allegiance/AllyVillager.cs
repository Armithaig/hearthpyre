﻿using XRL;
using XRL.World;
using XRL.World.AI;

namespace Hearthpyre
{

#if !BUILD_2_0_206
	public class AllyVillager : IAllyReason
	{

		public override ReplaceTarget Replace => ReplaceTarget.Type;

		public override string GetText(GameObject Actor)
		{
			return $"I joined the village governed by {The.Player.t(Stripped: true, SecondPerson: false)}.";
		}

	}
#endif

}