﻿using System;
using XRL;
using XRL.World;

namespace Hearthpyre
{
	[Serializable]
	public class GameObjectSource
	{
		public string ID;
		public GlobalLocation Location;
		
		[NonSerialized] private GameObject Object;
		[NonSerialized] private bool Failed = false;

		public GameObjectSource()
		{ }

		public GameObjectSource(GameObject Object) : this(Object, Object.CurrentCell.GetGlobalLocation())
		{ }

		public GameObjectSource(GameObject Object, GlobalLocation Location)
		{
			this.ID = Object.id;
			this.Object = Object;
			this.Location = Location;
		}

		public GameObject Value
		{
			get
			{
				if (Failed) return null;
				if (!GameObject.validate(Object))
				{
					var C = Location.ResolveCell();
					if (C != null)
					{
						Object = C.findObjectById(ID) ?? C.ParentZone.findObjectById(ID);
						if (Object == null) Failed = true;
					}
					else
					{
						Failed = true;
					}
				}
				else
				{
					Location.Clear();
					Location.SetCell(Object.CurrentCell);
				}

				return Object;
			}
		}

		public static bool Validate(ref GameObjectSource Source)
		{
			if (Source?.Value != null) return true;
			
			Source = null;
			return false;
		}

		public static bool Validate(ref GameObjectSource Source, out GameObject Object)
		{
			if ((Object = Source?.Value) != null) return true;
			
			Source = null;
			return false;
		}
	}
}