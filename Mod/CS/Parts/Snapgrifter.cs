﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hearthpyre;
using Hearthpyre.Dialogue;
using Qud.API;
using XRL.Core;
using XRL.Language;
using XRL.Names;
using XRL.Rules;
using XRL.UI;
using XRL.World.Effects;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	[Serializable]
	public class HearthpyreSnapgrifter : IPart
	{
		public const string TILE_SCMR = "Creatures/hp_grifter.png";
		public const string TILE_ALT = "Creatures/hp_grifter_2.png";

		public string Detail { get; set; }
		public bool Active { get; set; }
		public bool Revealed { get; set; }

		public override bool SameAs(IPart p) => false;

		public override void SaveData(SerializationWriter Writer)
		{
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			fields.Add("Detail", Detail);
			fields.Add("Active", Active);
			fields.Add("Revealed", Revealed);
			Writer.Write(fields);
		}

		public override void LoadData(SerializationReader Reader)
		{
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("Detail", out val)) Detail = (string) val;
			if (fields.TryGetValue("Active", out val)) Active = (bool) val;
			if (fields.TryGetValue("Revealed", out val)) Revealed = (bool) val;
		}
		
		[NonSerialized] int FrameOffset;
		public override bool Render(RenderEvent E)
		{
			if (!Revealed && ThePlayer.GetConfusion() <= 0)
			{
				Revealed = true;
				var note = JournalAPI.GetMapNote(SCT_GRFT);
				if (note != null)
				{
					if (!note.revealed) JournalAPI.RevealMapNote(note);
					else Popup.Show("You spot " + Grammar.InitLower(note.text) + '.');
				}
			}
			
			if (Active)
			{
				var f = (XRLCore.CurrentFrame + FrameOffset) & 511;
				if (f < 8) E.DetailColor = "C";
				else if (f < 16) E.DetailColor = "c";
				else if (f < 24) E.DetailColor = "B";

				FrameOffset = unchecked(FrameOffset + Stat.RandomCosmetic(0, 24));
				if (Stat.RandomCosmetic(1, 400) == 1)
				{
					E.DetailColor = "Y";
				}
			}

			return true;
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == GetShortDescriptionEvent.ID
			       || ID == BeforeObjectCreatedEvent.ID
			       || ID == ObjectCreatedEvent.ID
			       || ID == AfterObjectCreatedEvent.ID
			       || ID == GetInventoryActionsEvent.ID
			       || ID == InventoryActionEvent.ID
			       || ID == AIBoredEvent.ID
				;
		}
		
		public override bool HandleEvent(GetInventoryActionsEvent E)
		{
			if (!ParentObject.OwnEffect<Ecstatic>())
			{
				E.AddAction("Pet", "pet", "Pet", Key: 'p', Default: 5);
			}
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(InventoryActionEvent E)
		{
			if (E.Command == "Pet")
			{
				if (ParentObject.pBrain.GetFeeling(E.Actor) < 50)
				{
					DidXToY("shy", "away from", E.Actor, UsePopup: E.Actor.IsPlayer());
				}
				else
				{
					// i dont know, theres like a lot of petting going on in this mod, but the snapjaws must be
					// they must be petted!
					var name = E.Actor.GetFirstBodyPart("Hand")?.Name ?? E.Actor.GetRandomConcreteBodyPart()?.Name ?? "body";
					DidXToY("lean", "into the motions of", E.Actor, name + ", giggling contentedly", PossessiveObject: true, UsePopup: E.Actor.IsPlayer());
					E.Actor.UseEnergy(1000, "Petting");
					ParentObject.FireEvent(Event.New("ObjectPetted", "Object", ParentObject, "Petter", E.Actor));
					if (!ParentObject.OwnEffect<Ecstatic>()) ParentObject.ApplyEffect(new Ecstatic { Duration = Stat.Random(10, 50)});
				}
				E.RequestInterfaceExit();
			}
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(GetShortDescriptionEvent E)
		{
			AppendRegionDesc(E.Base);
			AppendWandDesc(E.Base);
			return base.HandleEvent(E);
		}

		public StringBuilder AppendRegionDesc(StringBuilder SB)
		{
			var Zone = ParentObject.CurrentZone;
			if (Zone != null)
			{
				if (Zone.Z > 10) return SB.Compound(ParentObject.Its).Append(" barks and yips ring through the channels of earth. Echo or tribe, one will respond.");
				if (Zone.IsInside()) return SB.Compound("Within nooks and halls ").AppendItVerb(ParentObject, "find").Append(" momentary shelter from the elements.");
				if (Zone.Z < 10) return SB.CompoundItVerb(ParentObject, "scour").Append(" the horizon for ").Append(ParentObject.its).Append(" journey's end, and whence it began.");

				var region = Zone.GetRegion();
				switch (region)
				{
					case "Saltmarsh": return SB.Compound(ParentObject.Its).Append(" soil-speckled paws stride deftly through the brine weeds.");
					case "DesertCanyon": return SB.Compound("Familiar salted crags surround ").Append(ParentObject.them).Append(", ").AppendItVerb(ParentObject, "yip").Append(" and ").Append(ParentObject.its).Append(" kin answer.");
					case "Flowerfields": return SB.Compound("Content would ").Append(ParentObject.it).Append(" be to roam these meadows, gorging ").Append(ParentObject.itself).Append(" on the petals of lah until its fields lay bare.");
					case "Fungal": return SB.Compound("Skittish of every cap and spook, ").AppendItVerb(ParentObject, "sneak").Append(" around their viscous hues.");
					case "Mountains":
					case "Hills": return SB.CompoundItVerb(ParentObject, "partake").Append(" of the vivid breeze, a twitch in ").Append(ParentObject.its).Append(" tail betrays a wish to scamper and bolt athwart the earthen waves.");
					case "Jungle":
					case "BananaGrove": return SB.Compound(ParentObject.Its).Append(" claws gleam of residue as ").AppendItVerb(ParentObject, "carve").Append(" through the dense foliage.");
					case "MoonStair":
					case "Deathlands": return SB.CompoundItVerb(ParentObject, "stomp").Append(" about irritably, ears folded still to the ceaseless hum.");
					case "Ruins":  return SB.Compound("Loath to place a soft paw on sharp scrap, ").AppendItVerb(ParentObject, "pace").Append(" warily through the grassy ruins.");
					case "TheSpindle": return SB.Compound("With every due courtesy, ").AppendItVerb(ParentObject, "stalk").Append(" amongst the hallowed and divine.");
					case "Water": return SB.Compound("No sooner ").Append(ParentObject.Has).Append(' ').Append(ParentObject.it).Append(" shaken free of moisture than another splash douses ").Append(ParentObject.its).Append(" efforts.");
					case "LakeHinnom":
					case "PalladiumReef": return SB.Compound("The bloom of algae leaves ").Append(ParentObject.its).Append(" face wrinkled by fetor.");
				}
			}
			
			return SB.Compound("Familiar milieux a distant notion, ").Append(ParentObject.its).Append(" ears now perk at every whisper and sigh.");
		}

		public StringBuilder AppendWandDesc(StringBuilder SB)
		{
			if (Active)
			{
				var understood = false;
				if (Examiner.UnderstandingTable != null &&
				    Examiner.UnderstandingTable.TryGetValue(OBJ_SCMR, out var understanding))
				{
					understood = understanding == Examiner.EPISTEMIC_STATUS_KNOWN;
				}

				SB
					.Compound("Enamored with the ")
					.Append(understood ? "xyloschemer" : "shiny wand")
					.Append(" in ")
					.Append(ParentObject.its)
					.Append(" hands, each swat of the air ebbs to infectious laughter.");
			}

			return SB;
		}

		public override bool HandleEvent(BeforeObjectCreatedEvent E)
		{
			var id = ZoneManager.zoneGenerationContextZoneID;
			var friends = "Wardens";
			if (id != null)
			{
				Faction faction = null;
				var visiting = The.ZoneManager.CachedZones.TryGetValue(id, out var Z) && Z.IsCheckpoint();
				if (Z != null)
				{
					var map = new Dictionary<string, int>();
					Brain brain;
					int x, y, i, c, a;
					for (x = 0; x < Z.Width; x++)
					for (y = 0; y < Z.Height; y++)
					for (i = 0, c = Z.Map[x][y].Objects.Count; i < c; i++)
					{
						brain = Z.Map[x][y].Objects[i].pBrain;
						if (brain == null) continue;
#if BUILD_2_0_206
						foreach(var pair in brain.FactionMembership)
#else
						foreach(var pair in brain.Allegiance)
#endif
						{
							if (pair.Value < 75) continue;
							map.TryGetValue(pair.Key, out a);
							map[pair.Key] = a + 1;
						}
					}

					foreach (var pair in map.OrderByDescending(p => p.Value))
					{
						var match = Factions.getIfExists(pair.Key);
						if (match == null || !match.Visible) continue;

						faction = match;
						break;
					}
				}

				if (faction != null)
				{
					friends += "," + faction.Name;
					ParentObject.pBrain.FactionFeelings[faction.Name] = 0;
					ParentObject.SetStringProperty("staticFaction1", faction.Name + ",friend,");

					if (visiting)
					{
						ParentObject.SetStringProperty("Visiting", faction.Name);
						// let's be more tolerant of the player's bad rep in populated settings
						ParentObject.pBrain.FactionFeelings["Player"] = 0;
					}
				}
				if (id == The.Game.GetStringGameState("FungalTrailEnd"))
				{
					friends += ",Oozes";
					ParentObject.pBrain.FactionFeelings["Oozes"] = 0;
					ParentObject.SetStringProperty("staticFaction2", "Oozes,friend,");
					ParentObject.SetStringProperty("Visiting", "PaxKlanq");
				}
			}

			ParentObject.SetStringProperty("NoHateFactions", friends);
			ParentObject.pBrain.FactionFeelings["Wardens"] = 25;
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(ObjectCreatedEvent E)
		{
			if (NameStyles.NameStyleTable.TryGetValue("Snapjaw", out var style))
			{
				var temp = new NameStyle
				{
					PrefixAmount = "0-1",
					InfixAmount = "1",
					PostfixAmount = "0-1",
					Prefixes = style.Prefixes,
					Infixes = style.Infixes,
					Postfixes = style.Postfixes,
					Format = style.Format
				};
				
				ParentObject.DisplayName = temp.Generate(For: ParentObject);
				ParentObject.HasProperName = true;
			}
			
			var tier = Math.Max(ZoneManager.zoneGenerationContextTier, 1);
			var level = Stat.Random(tier * 3, tier * 5);
			var leveler = ParentObject.TakePart<Leveler>();
			for (int i = ParentObject.Stat("Level"); i < level; i++)
			{
				leveler.LevelUp();
			}

			ParentObject.GetStat("XP").BaseValue = Leveler.GetXPForLevel(level);
			var ap = ParentObject.GetStat("AP");
			if (ap.Value > 0)
			{
				var bag = new BallBag<string>
				{
					{ "Strength", 2 },
					{ "Agility", 2 },
					{ "Toughness", 4 },
					{ "Intelligence", 2 },
					{ "Willpower", 4 },
					{ "Ego", 1 }
				};

				while(ap.Value > 0)
				{
					ParentObject.GetStat(bag.PeekOne()).BaseValue++;
					ap.Penalty++;
				}
			}
			
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(AfterObjectCreatedEvent E)
		{
			if (ParentObject.TryTakePart(out GivesRep Rep))
			{
				var rel = Rep.relatedFactions.GetRandomElement();
				if (rel != null)
				{
					if (rel.status == "friend")
					{
						rel.reason = "bestowing dwellings to their pets";
					}
					else
					{
						rel.reason = "whacking their leader in a sensitive area";
					}
				}
			}

			return base.HandleEvent(E);
		}
		
		public static readonly string[] Quips =
		{
			"dohoho!",
			"yi yi!",
			"ekehe",
			"hoho. asetetlikk!",
			"ahahaha!",
			"heh",
			"hn. good!"
		};

		public bool IsEmpty(Cell C) => C.IsEmptyAtRenderLayer(2) && !C.IsSolid();
		
		public override bool HandleEvent(AIBoredEvent E)
		{
			if (!Active || ParentObject.pBrain.PartyLeader != null) return base.HandleEvent(E);
			if (!1.in100() || RealmSystem.IsInSector(ParentObject))  return base.HandleEvent(E);

			var xyloschemer = ParentObject.FindEquippedObject(OBJ_SCMR)?.Equipped;
			if (xyloschemer == null) return base.HandleEvent(E);
			
			var blueprint = Notitia.Categories?.TakeValue("Decor")?.AllBlueprints?.GetRandomElement()?.Value;
			if (blueprint == null) return base.HandleEvent(E);

			var cells = ParentObject.CurrentCell
				.YieldAdjacentCells(1, LocalOnly: true)
				.Where(IsEmpty)
				.ToList();
			
			if (cells.Count < 7) return base.HandleEvent(E);

			var C = cells.GetRandomElement();
			//var obj = GameObjectFactory.Factory.CreateObject(blueprint, beforeObjectCreated: Brand);
			var obj = GameObjectFactory.Factory.CreateObject(blueprint);
			Brand(obj);
			C.AddObject(obj);
			HoloZap(C);
			DidXToYWithZ(
				"zap",
				obj,
				"into existance with",
				xyloschemer,
				IndefiniteDirectObject: true,
				IndirectObjectPossessedBy: ParentObject
			);
			
			ParentObject.ParticleText(Quips.GetRandomElement(), 'W', floatLength: 8f);
			ParentObject.UseEnergy(1000, "Item");
			return false;
		}

		public void Brand(GameObject Object) => Static.Brand(Object, Powered: false);

		public override bool AllowStaticRegistration() => true;

		public override void Register(GameObject Object)
		{
			Object.RegisterPartEvent(this, "EquipperEquipped");
			Object.RegisterPartEvent(this, "EquipperUnequipped");
		}

		public override bool FireEvent(Event E)
		{
			if (E.ID == "EquipperEquipped")
			{
				var obj = E.GetGameObjectParameter("Object");
				if (!Active && obj.Blueprint == OBJ_SCMR)
				{
					var render = ParentObject.pRender;
					render.Tile = TILE_SCMR;
					render.DetailColor = "B";
					Active = true;
				}
			}
			else if (E.ID == "EquipperUnequipped")
			{
				var obj = E.GetGameObjectParameter("Object");
				if (Active && obj.Blueprint == OBJ_SCMR)
				{
					var render = ParentObject.pRender;
					if (Detail == null)
					{
						var cl = render.GetForegroundColor();
						do Detail = Crayons.GetRandomColor();
						while (Detail == cl && Detail == "B");
					}
					render.Tile = TILE_ALT;
					render.DetailColor = Detail;
					Active = false;
				}
			}
			return base.FireEvent(E);
		}
	}
}
