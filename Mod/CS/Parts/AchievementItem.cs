﻿using System;

namespace XRL.World.Parts
{

	public class HearthpyreAchievementItem : IPart
	{

		public string Achievement;
		public string Blueprint;

		[NonSerialized] private string[] _AchievementIDs;
		public string[] AchievementIDs => _AchievementIDs ??= Achievement.Split(',');

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == ObjectCreatedEvent.ID
			       || ID == ZoneActivatedEvent.ID
				;
		}

		public override bool HandleEvent(ObjectCreatedEvent E)
		{
			if (E.Object == ParentObject)
			{
				Check();
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(ZoneActivatedEvent E)
		{
			Check();
			return base.HandleEvent(E);
		}

		public void Check()
		{
			var trigger = false;
			var ids = AchievementIDs;
			for (int i = 0, l = ids.Length; i < l; i++)
			{
				if (AchievementManager.GetAchievement(ids[i]))
				{
					trigger = true;
					break;
				}
			}
			
			if (trigger && !Blueprint.IsNullOrEmpty())
			{
				ParentObject.ReceiveObject(Blueprint);
				ParentObject.RemovePart(this);
				Blueprint = null;
			}
		}
	}

}
