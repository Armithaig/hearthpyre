using System;
using XRL.UI;
using System.Collections.Generic;
using Hearthpyre;
using System.Linq;
using XRL.Core;
using XRL.World.Parts.Skill;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	[Serializable]
	public class HearthpyreCart : IActivePart
	{
		public const double STAT_MULT_IMP = 0.5d;
		
		public int AV { get; set; }
		public int DV { get; set; }
		public int Capacity { get; set; }
		public int MoveSpeed { get; set; }
		GameObject Deployed { get; set; }

		public HearthpyreCart()
		{
			WorksOnEquipper = true;
		}

		public override bool SameAs(IPart p) => false;

		public override void SaveData(SerializationWriter Writer)
		{
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			fields.Add("AV", AV);
			fields.Add("DV", DV);
			fields.Add("Capacity", Capacity);
			fields.Add("MoveSpeed", MoveSpeed);
			Writer.Write(fields);

			Writer.WriteGameObject(Deployed);
		}

		public override void LoadData(SerializationReader Reader)
		{
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("AV", out val)) AV = (int) val;
			if (fields.TryGetValue("DV", out val)) DV = (int) val;
			if (fields.TryGetValue("Capacity", out val)) Capacity = (int) val;
			if (fields.TryGetValue("MoveSpeed", out val)) MoveSpeed = (int) val;

			Deployed = Reader.ReadGameObject();
		}

		void AdjustStats(GameObject Actor)
		{
			if (!IsObjectActivePartSubject(Actor))
			{
				StatShifter.RemoveStatShifts(Actor);
				return;
			}
			
			var av = AV;
			var dv = DV;
			var ms = MoveSpeed;
			if (Actor.OwnPart<HearthpyreGoverning_Porter>())
			{
				av = RoundToInt(av * STAT_MULT_IMP);
				dv = RoundToInt(dv * STAT_MULT_IMP);
				ms = RoundToInt(ms * STAT_MULT_IMP);
			}

			StatShifter.SetStatShift(Actor, "AV", av);
			StatShifter.SetStatShift(Actor, "DV", dv);
			StatShifter.SetStatShift(Actor, "MoveSpeed", ms);
		}

		void DeployCart(GameObject Object)
		{
			if (Deployed != null || !OptionDeployCart) return;

			var cell = Object.CurrentCell.YieldAdjacentCells(1, LocalOnly: true).Where(x => x.IsEmpty()).GetRandomElement();
			if (cell == null) return;

			Deployed = cell.AddObject("HearthpyreHandcartDeployed");

			var pullable = Deployed.TakePart<HearthpyreTowed>();
			pullable.SetItem(ParentObject);
			pullable.SetOwner(Object);
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == GetIntrinsicWeightEvent.ID
			       || ID == GetMaxCarriedWeightEvent.ID
			       || ID == GetShortDescriptionEvent.ID
			       || ID == GetItemElementsEvent.ID
			       || ID == EquippedEvent.ID
			       || ID == UnequippedEvent.ID
			       || ID == TakenEvent.ID
			       || ID == DroppedEvent.ID
			       || ID == AfterAddSkillEvent.ID
			       || ID == AfterRemoveSkillEvent.ID
				;
		}

		public override bool HandleEvent(EquippedEvent E)
		{
			AdjustStats(E.Actor);
			DeployCart(E.Actor);
			CarryingCapacityChangedEvent.Send(E.Actor);
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(UnequippedEvent E)
		{
			AdjustStats(E.Actor);

			if (Deployed != null)
			{
				Deployed.Destroy("Unequipped");
				Deployed = null;
			}

			CarryingCapacityChangedEvent.Send(E.Actor);
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(TakenEvent E)
		{
			AdjustStats(E.Actor);
			return base.HandleEvent(E);
		}
		
		public override bool HandleEvent(DroppedEvent E)
		{
			AdjustStats(E.Actor);
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(AfterAddSkillEvent E)
		{
			if (E.Skill.IsType<HearthpyreGoverning_Porter>())
			{
				AdjustStats(E.Actor);
			}
			
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(AfterRemoveSkillEvent E)
		{
			if (E.Skill.IsType<HearthpyreGoverning_Porter>())
			{
				AdjustStats(E.Actor);
			}
			
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(GetShortDescriptionEvent E)
		{
			var av = AV;
			var dv = DV;
			var ms = MoveSpeed;
			var observer = ParentObject.Equipped ?? ParentObject.InInventory ?? The.Player;
			if (observer != null && observer.OwnPart<HearthpyreGoverning_Porter>())
			{
				av = RoundToInt(av * STAT_MULT_IMP);
				dv = RoundToInt(dv * STAT_MULT_IMP);
				ms = RoundToInt(ms * STAT_MULT_IMP);
			}

			E.Postfix.Append("\n{{rules|+").Append(Capacity).Append(" lbs. carry capacity}}");
			E.Postfix.AppendRules(Statistic.GetStatAdjustDescription("AV", av));
			E.Postfix.AppendRules(Statistic.GetStatAdjustDescription("DV", dv));
			E.Postfix.AppendRules(Statistic.GetStatAdjustDescription("MoveSpeed", ms));
			return base.HandleEvent(E);
		}
		
		public override bool HandleEvent(GetItemElementsEvent E)
		{
			E.Add("travel", 2);
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(GetIntrinsicWeightEvent E)
		{
			if (IsReady()) E.Weight = 0;
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(GetMaxCarriedWeightEvent E)
		{
			if (IsReady()) E.Weight += Capacity;
			return base.HandleEvent(E);
		}

		public override bool AllowStaticRegistration() => true;
		public override void Register(GameObject Object)
		{
			Object.RegisterPartEvent(this, "AdjustWeaponScore");
			base.Register(Object);
		}

		public override bool FireEvent(Event E)
		{
			if (E.ID == "AdjustWeaponScore")
			{
				var user = E.GetGameObjectParameter("User");
				var carried = user.GetCarriedWeight();
				var max = user.Stat("Strength") * Rules.Stats.MAX_WEIGHT_PER_STRENGTH;
				if (carried > max * 0.8f)
				{
					E.ModParameter("Score", Capacity / 10);
				}
			}
			
			return base.FireEvent(E);
		}
	}
}