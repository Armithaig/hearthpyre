﻿using System.Text;
using Qud.API;
using XRL.UI;

namespace XRL.World.Parts
{
	public class HearthpyreTentWidget : IPart
	{
		public string PaintFilter;
		public string MaterialName;

#if !BUILD_2_0_203
		public override bool AllowStaticRegistration() => true;
		public override void Register(GameObject Object) => RegisterRequired(Object.CurrentZone as InteriorZone);

		public void RegisterRequired(InteriorZone Zone)
		{
			if (Zone == null || !Zone.Built) return;

			foreach (var obj in Zone.YieldObjects())
			{
				var required = obj.GetIntProperty("InteriorRequired");
				if (required > 0) obj.RegisterPartEvent(this, "BeforeDeathRemoval");
			}
		}

		public override bool FireEvent(Event E)
		{
			if (E.ID == "BeforeDeathRemoval")
			{
				GameManager.Instance.gameQueue.queueTask(Collapse);
			}

			return base.FireEvent(E);
		}

		private void Collapse()
		{
			(ParentObject.CurrentZone as InteriorZone)?.ParentObject?.Die();
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == ZoneBuiltEvent.ID
				;
		}

		public override bool HandleEvent(ZoneBuiltEvent E)
		{
			var interior = ParentObject.CurrentZone as InteriorZone;
			if (interior == null) return true;
			RegisterRequired(interior);

			var ornamentation = interior.ParentObject?.GetStringProperty("Ornamentation");
			if (ornamentation.IsNullOrEmpty() || PaintFilter.IsNullOrEmpty())
			{
				return true;
			}

			Render blueprint = null;
			Description bpdesc = null;
			HearthpyreMaterial.LoadChunks(ornamentation);
			foreach (var obj in interior.YieldObjects())
			{
				if (obj.GetTagOrStringProperty("PaintWith") != PaintFilter) continue;
				var render = obj.pRender;
				var desc = obj.GetPart<Description>();
				obj.AddPart(new NoDamage());
				obj.SetIntProperty("Anchoring", 8000);
				if (blueprint == null || obj.Blueprint != PaintFilter)
				{
					if (obj.Blueprint == PaintFilter)
					{
						render.DisplayName = MaterialName;
						blueprint = render;
						bpdesc = desc;
					}
					HearthpyreMaterial.Transform(obj, InheritDescription: true);
				}
				else
				{
					render.DisplayName = blueprint.DisplayName;
					render.ColorString = blueprint.ColorString;
					render.TileColor = blueprint.TileColor;
					render.DetailColor = blueprint.DetailColor;
					desc._Short = bpdesc._Short;
					if (obj.HasTag("Immutable"))
					{
						obj.SetIntProperty("ForceMutableSave", 1);
					}

					obj.SetIntProperty("Inorganic", blueprint.ParentObject.GetIntProperty("Inorganic"));
				}
			}
			
			return base.HandleEvent(E);
		}
#endif
	}
}
