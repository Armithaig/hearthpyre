using System;

using XRL;
using XRL.UI;
using XRL.Wish;
using XRL.World;
using XRL.World.Parts;
using static Hearthpyre.Static;

namespace Hearthpyre
{
	[PlayerMutator, HasWishCommand]
	public class FastStart : IPlayerMutator
	{
		[WishCommand(Command = "learned builder")]
		public static void Wish() {
			AddTo(The.Player);
		}

		public void mutate(GameObject player) {
			if (!OptionFastStart) return;

			AddTo(player);
		}

		public static void AddTo(GameObject Subject) {
			try {
				var factory = GameObjectFactory.Factory;
				var obj = factory.CreateObject(OBJ_SCMR, BonusModChance: -9999);
				obj.TakePart<Commerce>().Value = 0;
				obj.TakePart<Examiner>()?.MakeUnderstood();
				var cell = obj.TakePart<EnergyCellSocket>()?.Cell;
				if (cell != null) {
					cell.TakePart<Commerce>().Value = 0;
					cell.TakePart<Examiner>()?.MakeUnderstood();
					cell.GetPartDescendedFrom<IEnergyCell>()?.MaximizeCharge();
				}

				Subject.TakeObject(obj, Silent: true);
				TakeObject(Subject, "Farmers Token");
				TakeObject(Subject, "Merchant's Token");
				TakeObject(Subject, "Minstrel's Token");
				TakeObject(Subject, "HearthpyreTokenWarden");
				TakeObject(Subject, "HearthpyreTokenTinker");
			} catch (Exception e) {
				MetricsManager.LogError("Hearthpyre fast start error", e);
			}
		}

		public static void TakeObject(GameObject Subject, string Blueprint) {
			var obj = GameObjectFactory.Factory.CreateUnmodifiedObject(Blueprint);
			if (obj.TryTakePart(out Commerce comm))
			{
				comm.Value = 0;
			}

			Subject.TakeObject(obj, Silent: true);
		}
	}
}
