﻿using System;
using Hearthpyre;
using XRL.Rules;
using XRL.World.Parts.Skill;

namespace XRL.World.Parts
{

	[Serializable]
	public class HearthpyreStockUpgrader : IPart
	{

		public long LastUpgradeTick;
		public long LastConversationTick;

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == BeginConversationEvent.ID
			       || ID == ZoneActivatedEvent.ID
				;
		}

		public override bool HandleEvent(BeginConversationEvent E)
		{
			if (!ParentObject.TryTakePart(out GenericInventoryRestocker stock))
			{
				ParentObject.RemovePart(this);
				return true;
			}

			if (The.Game.Turns - LastConversationTick < stock.RestockFrequency / 2) return true;

			LastUpgradeTick -= Stat.Random(stock.RestockFrequency / 4, stock.RestockFrequency / 2);
			LastConversationTick = The.Game.Turns;

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(ZoneActivatedEvent E)
		{
			if (!ParentObject.TryTakePart(out GenericInventoryRestocker stock) || stock.Tables.IsNullOrEmpty())
			{
				return RemoveSelf();
			}
			if (The.Game.Turns - LastUpgradeTick < stock.RestockFrequency * 3)
			{
				return true;
			}

			var index = -1;
			var slice = -1;
			var tier = 0;
			var table = stock.Table;
			for (int i = 0, c = stock.Tables.Count; i < c; i++)
			{
				var str = stock.Tables[i];
				if (TryParseTableTier(str, out slice, out tier))
				{
					table = str;
					index = i;
					break;
				}
			}
			if (index == -1)
			{
				return RemoveSelf();
			}

			var target = The.Player.Stat("Level") / 5 + 1;
			if (++tier > target) return true;

			table = table.Substring(0, slice) + tier;
			if (!PopulationManager.HasPopulation(table))
			{
				return RemoveSelf();
			}

			if (stock.Chance.in100())
			{
				// fudge in some tinker service upgrades
				if (tier > 3 && ParentObject.OwnPart<Tinkering_Tinker1>() && !ParentObject.OwnPart<Tinkering_Tinker2>())
				{
					ParentObject.AddSkill("Tinkering_Tinker2");
				}

				if (tier > 6 && ParentObject.OwnPart<Tinkering_Tinker2>() && !ParentObject.OwnPart<Tinkering_Tinker2>())
				{
					ParentObject.AddSkill("Tinkering_Tinker3");
				}

				stock.Tables[index] = table;
				stock.PerformRestock(Silent: true);
				DidXToY("have", "{{M|improved}}", ParentObject, "inventory", Color: "G", PossessiveObject: true, AlwaysVisible: true);
			}

			LastUpgradeTick = The.Game.Turns;
			return base.HandleEvent(E);
		}

		public bool RemoveSelf()
		{
			ParentObject.RemovePart(this);
			return true;
		}

		public bool TryParseTableTier(string Table, out int Slice, out int Tier)
		{
			Slice = -1;
			if (Table.IsNullOrEmpty())
			{
				Tier = 0;
				return false;
			}
			
			for (int i = Table.Length - 1; i >= 0; i--)
			{
				if (Char.IsNumber(Table[i])) Slice = i;
				else break;
			}
			if (Slice < 0)
			{
				Tier = 0;
				return false;
			}

			return int.TryParse(Table.AsSpan(Slice), out Tier);
		}

	}

}
