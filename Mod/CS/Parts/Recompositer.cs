﻿using System;
using System.Collections.Generic;
using System.Linq;
using AiUnity.Common.Extensions;
using Genkit;
using Hearthpyre;
using UnityEngine;
using XRL.Language;
using XRL.UI;
using static XRL.World.Parts.IProgrammableRecoiler;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	[Serializable]
	public partial class HearthpyreRecompositer : IActivePart
	{
		public bool Active { get; set; }
		public string GameState { get; set; }

		[NonSerialized] private Location _CurrentLocation;
		public Location CurrentLocation
		{
			get
			{
				if (_CurrentLocation == null)
				{
					_CurrentLocation = Locations.Find(x => x.Equals(ParentObject.CurrentCell));
				}

				return _CurrentLocation;
			}
		}
		
		public HearthpyreRecompositer()
		{
			IsRealityDistortionBased = true;
			MustBeUnderstood = true;
			WorksOnCellContents = true;
			WorksOnAdjacentCellContents = true;
		}

		public override void SaveData(SerializationWriter Writer)
		{
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			if (Active) fields["Active"] = Active;
			if (!GameState.IsNullOrEmpty()) fields["GameState"] = GameState;
			Writer.Write(fields);
		}

		public override void LoadData(SerializationReader Reader)
		{
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			object val;
			if (fields.TryGetValue("Active", out val)) Active = (bool) val;
			if (fields.TryGetValue("GameState", out val)) GameState = (string) val;
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == GetShortDescriptionEvent.ID
			       || ID == CanSmartUseEvent.ID
			       || ID == CommandSmartUseEvent.ID
			       || ID == GetInventoryActionsEvent.ID
			       || ID == InventoryActionEvent.ID
			       || ID == ObjectEnteredCellEvent.ID
			       || ID == EnteredCellEvent.ID
			       || ID == LeftCellEvent.ID
				;
		}

		public override bool HandleEvent(GetShortDescriptionEvent E)
		{
			var location = CurrentLocation;
			if (location != null && !location.Label.IsNullOrEmpty())
			{
				E.Infix.Append("\nThe diaphanous screen to ").Append(ParentObject.its)
					.Append(" side reads: \"").Append(location.Label).Append("\".");
			}

			E.Postfix.AppendRules("Imprints recoilers with its position, reducing overall charge consumption.");
			E.Postfix.AppendRules("Linked to other recompositers of any dimension.");
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(ObjectEnteredCellEvent E)
		{
			if (!E.System || E.Type != "Teleporting") return base.HandleEvent(E);

			foreach (var item in E.Object.YieldItems())
			{
				// can cheese this pretty easy... if ya know about it!
				if (item.GetStringProperty("LastInventoryActionCommand") != "ActivateTeleporter") continue;
				if (item.GetLongProperty("LastInventoryActionTurn") + 1 < The.Game.Turns) continue;

				var teleporter = item.GetPartDescendedFrom<ITeleporter>();
				if (teleporter?.DestinationZone != E.Cell.ParentZone.ZoneID) continue;
				if (teleporter.DestinationX != E.Cell.X || teleporter.DestinationY != E.Cell.Y) continue;
				if (teleporter.ChargeUse <= 0) continue;

				foreach (var obj in item.GetContents())
				foreach (var rechargeable in obj.GetPartsDescendedFrom<IRechargeable>())
				{
					if (!rechargeable.CanBeRecharged()) continue;

					var amount = rechargeable.GetRechargeAmount();
					if (amount <= 0) continue;

					rechargeable.AddCharge(Math.Min(amount, Mathf.RoundToInt(teleporter.ChargeUse * 0.9f)));
					E.Object.EmitMessage(E.Object.Poss("bodily tether is recomposited."));
					return base.HandleEvent(E);
				}
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(CanSmartUseEvent E)
		{
			if (E.Actor != ParentObject && WorksFor(E.Actor) && (Locations.Count > 1 || !Active))
			{
				return false;
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(CommandSmartUseEvent E)
		{
			if (!Active) Activate(E.Actor);
			else if (Locations.Count > 1) Teleport(E.Actor);
			else return base.HandleEvent(E);

			return false;
		}

		public new bool WorksFor(GameObject Actor)
		{
			return base.WorksFor(Actor) && Actor.PhaseAndFlightMatches(ParentObject);
		}

		public override bool HandleEvent(GetInventoryActionsEvent E)
		{
			if (WorksFor(E.Actor))
			{
				if (Active)
				{
					E.AddAction("Teleport", "teleport", "TeleportPad", Key: 'p', Default: 120);
					E.AddAction("Label", "label", "LabelPad", Key: 'b', Default: 110);
					E.AddAction("Imprint", "imprint recoiler", "ImprintRecoiler", Key: 'i', Default: 100);
				}
				else
				{
					E.AddAction("Activate", "activate", "ActivatePad", Key: 'c', Default: 100);
				}
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(InventoryActionEvent E)
		{
			if (E.Command == "ActivatePad" && Activate(E.Actor))
			{
				E.Actor.UseEnergy(1000, "Item");
				E.RequestInterfaceExit();
			}
			else if (E.Command == "TeleportPad" && Teleport(E.Actor))
			{
				E.RequestInterfaceExit();
			}
			else if (E.Command == "LabelPad" && Label(E.Actor))
			{
				E.RequestInterfaceExit();
			}
			else if (E.Command == "ImprintRecoiler" && Imprint(E.Actor))
			{
				E.Actor.UseEnergy(1000, "Item Recoiler Imprint");
				E.RequestInterfaceExit();
			}

			return base.HandleEvent(E);
		}

		public bool Activate(GameObject Actor)
		{
			if (!GameState.IsNullOrEmpty() && !The.Game.HasGameState(GameState) || IsDisabled())
			{
				return Actor.Failure(ParentObject.The + ParentObject.DisplayNameOnly + " remains unresponsive.");
			}

			Active = true;
			AddLocation(ParentObject.CurrentCell);
			return Actor.Success(ParentObject.The + ParentObject.DisplayNameOnly + " hums to life.");
		}

		public bool Imprint(GameObject Actor)
		{
			var recoilers = new List<GameObject>();
			foreach (var item in Actor.YieldItems())
			{
				if (!item.TryTakePart(out ProgrammableRecoiler pr)) continue;
				if (pr.Reprogrammable || pr.TimesProgrammed < 1)
				{
					recoilers.Add(item);
				}
			}
			#if MOD_OMNICOILER
				this will not compile
			#endif

			if (recoilers.Count == 0) return Actor.Failure("You do not have any imprintable recoilers.");

			var choice = Actor.IsPlayer() ? Popup.PickGameObject("Select a recoiler to imprint", recoilers, AllowEscape: true) : recoilers.GetRandomElement();
			if (choice == null) return false;


			var name = ParentObject.The + ParentObject.DisplayNameOnly;
			var status = GetActivePartStatus(UseCharge: true);
			switch (status)
			{
				case ActivePartStatus.Operational: break;
				default: return Actor.Failure(name + ParentObject.GetVerb("are") + " unresponsive.");
			}

			var part = choice.TakePart<ProgrammableRecoiler>();
			name = choice.The + choice.DisplayNameOnly;
			status = part.GetActivePartStatus(UseCharge: true, ChargeUse: part.ChargeUse / 10);
			switch (status)
			{
				case ActivePartStatus.Operational: break;
				case ActivePartStatus.Unpowered: return Actor.Failure(name + choice.GetVerb("do") + " not have enough charge to be imprinted with the current location.");
				default: return Actor.Failure(name + " merely" + choice.GetVerb("click") + ".");
			}

			Actor.EmitMessage(
				Actor.Poss(choice.DisplayNameOnly) + choice.GetVerb("vibrate") + " as " + choice.its +
				" geospatial core is imprinted by " + ParentObject.The + ParentObject.DisplayNameOnly + ".",
				UsePopup: Actor.IsPlayer()
			);

			return ProgramObjectForLocation(choice, ParentObject.CurrentZone, ParentObject.CurrentCell, part);
		}

		public bool Label(GameObject Actor)
		{
			if (!Actor.IsPlayer()) return false;

			var location = CurrentLocation;
			if (location == null) return false;

			var label = Popup.AskString("Choose a label for this recompositer.", location.Label ?? "");
			if (label.IsNullOrEmpty()) return false;

			location.Label = label;
			PlayUISound(SND_TEXT);
			return true;
		}

		public bool Teleport(GameObject Actor)
		{
			if (Locations.Count <= 1) return Actor.Failure("You know of no other recompositers to receive you.");

			var CC = ParentObject.CurrentCell;
			var locations = new List<Location>(Locations);
			locations.RemoveAll(x => x.Equals(CC));
			locations.Sort((a, b) => a.PathDistanceTo(CC).CompareTo(b.PathDistanceTo(CC)));

			var options = new string[locations.Count];
			var sb = Event.NewStringBuilder();
			for (int i = 0, c; i < options.Length; i++)
			{
				if (!locations[i].Label.IsNullOrEmpty())
				{
					sb.Clear();
					sb.Append(locations[i].Label).Append(", ").Append(GetZoneBareDisplayName(locations[i].ZoneID));
					for (c = 0; c < sb.Length; c++)
						if (char.IsLetter(sb[c]))
							break;

					sb[c] = char.ToUpperInvariant(sb[c]);
					options[i] = sb.ToString();
					continue;
				}

				options[i] = GetZoneBareDisplayName(locations[i].ZoneID);
			}

			var choice = Popup.ShowOptionList("Choose a destination", options, AllowEscape: true, context: ParentObject);
			if (choice < 0) return false;
			if (Actor.CurrentCell != CC && !Actor.DirectMoveTo(CC)) return false;
			Actor.UseEnergy(1000);

			if (Actor.IsPlayer())
			{
				CC.PlayWorldSound(SND_TDRO);
				The.Core.RenderDelay(2000, Interruptible: false);
			}

			if (IsDisabled(UseCharge: true))
			{
				AddPlayerMessage("Nothing happens.");
				return true;
			}

			var C = locations[choice].ResolveCell();
			if (C == null || !Actor.TeleportTo(C)) return false;

			GameManager.Instance.Spacefolding = true;
			CC.PlayWorldSound(SND_TLL, Volume: 0.2f);
			C.PlayWorldSound(Actor.IsPlayer() ? SND_TLW : SND_TLS, Volume: 0.2f);
			return true;
		}

		public override bool HandleEvent(EnteredCellEvent E)
		{
			if (Active) AddLocation(E.Cell);
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(LeftCellEvent E)
		{
			if (Active) RemoveLocation(E.Cell);
			return base.HandleEvent(E);
		}

		public void AddLocation(Cell C)
		{
			var locations = Locations;
			for (int i = locations.Count - 1; i >= 0; i--)
			{
				if (locations[i].Equals(C))
				{
					return;
				}
			}

			_CurrentLocation = new Location(C);
			locations.Add(_CurrentLocation);
		}

		public void RemoveLocation(Cell C)
		{
			var locations = Locations;
			for (int i = locations.Count - 1; i >= 0; i--)
			{
				if (locations[i].Equals(C))
				{
					locations.RemoveAt(i);
				}
			}

			_CurrentLocation = null;
		}
	}

	public partial class HearthpyreRecompositer
	{
		public const string LOCATIONS_KEY = "Hearthpyre.Recompositer.Locations";

		[Serializable]
		public class Location : GlobalLocation
		{
			public string Label;

			public Location()
			{ }

			public Location(Cell C) : base(C)
			{ }
		}

		[NonSerialized] private static List<Location> _Locations;

		public static List<Location> Locations
		{
			get
			{
				if (_Locations == null)
				{
					_Locations = The.Game.GetObjectGameState(LOCATIONS_KEY) as List<Location>;
					if (_Locations == null)
					{
						_Locations = new List<Location>();
						The.Game.ObjectGameState[LOCATIONS_KEY] = _Locations;
					}
				}

				return _Locations;
			}
		}
	}
}