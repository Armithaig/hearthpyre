﻿using System;
using System.Collections.Generic;
using System.Text;
using XRL.Core;
using XRL.Rules;
using XRL.Language;
using ConsoleLib.Console;
using Hearthpyre;
using HistoryKit;
using Qud.API;
using XRL.UI;
using XRL.Wish;
using ColorUtility = ConsoleLib.Console.ColorUtility;

namespace XRL.World.Parts
{
	[HasWishCommand]
	public class HearthpyreMaterial : IPart
	{
		private static readonly Chunk[] Chunks = { new Chunk(), new Chunk(), new Chunk() };
		private static readonly HashSet<string> Set = new HashSet<string>();
		private static readonly char[] ColorSequence = { '&', 'y', '^', 'k' };
		private static readonly string[] KnownSemantics = { "SemanticFiberMaterial", "SemanticHardMaterial", "SemanticFiber", "SemanticPlank" };

		public class Chunk
		{
			public GameObjectBlueprint Blueprint;
			public string Tag;
			public char Foreground;
			public char Background;
			public char Detail;
			public string Noun => Blueprint.GetTag("Semantic" + Tag, null);

			public string GetFallbackNoun()
			{
				var options = new List<string>();
				foreach (var semantic in KnownSemantics)
				{
					if (semantic.EndsWith(Tag)) continue;
					if (Blueprint.Tags.TryGetValue(semantic, out var value) && !value.IsNullOrEmpty())
					{
						options.Add(value);
					}
				}

				return options.GetRandomElement();
			}

			/// <summary>
			/// Get relevant colours of the material, correcting for black detailed walls.
			/// </summary>
			public void FillColors()
			{
				var render = Blueprint.GetPart("Render");
#if BUILD_2_0_203
				if (
					(!render.Parameters.TryGetValue("TileColor", out string color) || color.IsNullOrEmpty())
					&& (!render.Parameters.TryGetValue("ColorString", out color) || color.IsNullOrEmpty())
				)
				{
					color = "&y";
				}

				if (!render.Parameters.TryGetValue("DetailColor", out string detail) || detail.IsNullOrEmpty())
				{
					detail = "K";
				}
#else
				if (
					(!render.TryGetParameterValue("TileColor", out string color) || color.IsNullOrEmpty())
					&& (!render.TryGetParameterValue("ColorString", out color) || color.IsNullOrEmpty())
				)
				{
					color = "&y";
				}

				if (!render.TryGetParameterValue("DetailColor", out string detail) || detail.IsNullOrEmpty())
				{
					detail = "K";
				}
#endif

				char? fg = 'y', bg = 'k';
				ColorUtility.FindLastForegroundAndBackground(color, ref fg, ref bg);

				Foreground = fg.Value;
				Background = bg.Value;
				Detail = detail[0];
			}

			public void FromOrnament(string Ornament)
			{
				Blueprint = GameObjectFactory.Factory.Blueprints.GetValue(Ornament.GetDelimitedSubstring('.', 0));
				Tag = Ornament.GetDelimitedSubstring('.', 1);
				FillColors();
			}

			public void Clear()
			{
				Blueprint = null;
				Tag = null;
				Foreground = '\0';
				Background = '\0';
				Detail = '\0';

			}

			public char GetDetailColor(char Exclude, char Default)
			{
				if (Detail != 'k' && Detail != Exclude) return Detail;
				if (Foreground != 'k' && Foreground != Exclude) return Foreground;
				return Default;
			}
		}

		public const int MAX_TRIES = 10;

		public string Primary;
		public string Secondary;
		public string Tertiary;

		// Hints to place the primary/first material into the display name. Append,Plural,Noun.
		// Easier to wish for items by display name if they don't have a bunch of templating markup in 'em.
		public string Hint;

		// Control what tile colours will be inherited and from what material.
		// -1: unchanged, 0-2: inherit from chunk at specified index.
		public int Foreground = 0;
		public int Background = -1;
		public int Detail = 0;

		// Make sure each rolled blueprint is distinct.
		public bool Distinct = true;

		// Just nick the material description whole cloth.
		public bool InheritDescription = false;

		public bool ColorName = false;

		public int PatternChance = 0;
		public int ShimmeringChance = 1;

		public override bool SameAs(IPart part)
		{
			return false;
		}

		public override bool AllowStaticRegistration()
		{
			return true;
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == ObjectCreatedEvent.ID
				;
		}

		public override bool HandleEvent(ObjectCreatedEvent E)
		{
			try
			{
				RollBlueprints();
			}
			catch (Exception e)
			{
				XRLCore.LogError(e);
			}

			ParentObject.RemovePart(this);
			return true;
		}

		public void RollBlueprint(Chunk Chunk, string Value)
		{
			string blueprint, tag;
			int tries = 0;

			if (Value.StartsWith("Table"))
			{
				var table = Value.GetDelimitedSubstring('.', 1);
				do blueprint = PopulationManager.RollOneFrom(table).Blueprint;
				while (Distinct && Set.Contains(blueprint) && tries++ < MAX_TRIES);

				tag = Value.GetDelimitedSubstring('.', 2);
			}
			else if (Value.StartsWith("Semantic"))
			{
				var semantic = Value.GetDelimitedSubstring('.', 1);
				var table = "DynamicSemanticTable:" + semantic;
				do blueprint = PopulationManager.RollOneFrom(table).Blueprint;
				while (Distinct && Set.Contains(blueprint) && tries++ < MAX_TRIES);

				tag = Value.GetDelimitedSubstring('.', 2) ?? semantic;
			}
			else if (Value.StartsWith("Blueprint"))
			{
				blueprint = Value.GetDelimitedSubstring('.', 1);
				tag = Value.GetDelimitedSubstring('.', 2);
			}
			else
			{
				blueprint = Value.GetDelimitedSubstring('.', 0);
				tag = Value.GetDelimitedSubstring('.', 1);
			}

			Set.Add(blueprint);
			Chunk.Blueprint = GameObjectFactory.Factory.GetBlueprint(blueprint);
			Chunk.Tag = tag;
			Chunk.FillColors();
		}

		public static void LoadChunks(string Ornamentation)
		{
			var parts = Ornamentation.Split(',');
			Chunks[0].FromOrnament(parts[0]);

			if (parts.Length >= 2) Chunks[1].FromOrnament(parts[1]);
			else Chunks[1].Clear();

			if (parts.Length >= 3) Chunks[2].FromOrnament(parts[2]);
			else Chunks[2].Clear();
		}

		/// <summary>
		/// Roll material blueprints and store in array.
		/// </summary>
		public void RollBlueprints()
		{
			if (Primary == null) return;

			Set.Clear();
			RollBlueprint(Chunks[0], Primary);

			if (Secondary != null) RollBlueprint(Chunks[1], Secondary);
			else Chunks[1].Blueprint = null;

			if (Tertiary != null) RollBlueprint(Chunks[2], Tertiary);
			else Chunks[2].Blueprint = null;

			Transform(
				Object: ParentObject,
				Hint: Hint,
				Foreground: Foreground,
				Background: Background,
				Detail: Detail,
				PatternChance: PatternChance,
				ShimmeringChance: ShimmeringChance,
				ColorName: ColorName,
				InheritDescription: InheritDescription
			);
			SetProperties();
		}

		/// <summary>
		/// Transform object based on materials.
		/// </summary>
		public static void Transform(
			GameObject Object,
			string Hint = null,
			int Foreground = 0,
			int Background = -1,
			int Detail = 0,
			int PatternChance = 0,
			int ShimmeringChance = 1,
			bool ColorName = false,
			bool InheritDescription = false
		)
		{
			var material = Chunks[0];
			InheritColors(Object, Foreground, Background, Detail);

			var sb = Event.NewStringBuilder();
			if (ColorName)
			{
				var shader = TopShader(material.Blueprint) ?? MakePattern(material, PatternChance, ShimmeringChance);
				sb.Append('{', 2).Append(shader).Append('|');
			}

			AppendName(sb, material, Object.DisplayNameOnlyDirectAndStripped, Hint);
			if (ColorName) sb.Append('}', 2);
			Object.DisplayName = sb.ToString();

			var desc = Object.GetPart<Description>();
			if (desc != null)
			{
				if (InheritDescription)
				{
#if BUILD_2_0_203
					desc._Short = material.Blueprint.GetPartParameter("Description", "Short", "");
#else
					desc._Short = material.Blueprint.GetPartParameter<string>("Description", "Short", "");
#endif
				}
				else if (desc._Short.Length > 0)
				{
					desc._Short = Substitute(desc._Short);
				}
			}
		}

		/// <summary>
		/// Append name and material according to hints, substitute if none provided.
		/// </summary>
		public static void AppendName(StringBuilder SB, Chunk Chunk, string Name, string Hint)
		{
			//var name = ParentObject.DisplayNameOnlyDirectAndStripped;

			if (!Hint.IsNullOrEmpty())
			{
				var append = Hint.Contains("Append");

				if (append) SB.Append(Name).Append(' ');
				SB.Append(Chunk.Blueprint.CachedDisplayNameStripped);
				if (Hint.Contains("Noun"))
				{
					var noun = Chunk.Noun;
					if (noun != null)
					{
						var plural = Hint.Contains("Plural");
						SB.Append(' ').Append(plural ? Grammar.Pluralize(noun) : noun);
					}
				}

				if (!append) SB.Append(' ').Append(Name);
			}
			else
			{
				SB.Append(Substitute(Name));
			}
		}

		/// <summary>
		/// Set a property with materials on the object for other parts to perhaps make use of later.
		/// </summary>
		public void SetProperties()
		{
			var organic = false;
			var sb = Event.NewStringBuilder();
			foreach (var material in Chunks)
			{
				if (material.Blueprint != null)
				{
					organic |= material.Blueprint.IntProps.GetValue("Inorganic", 1) == 0;
				}

				sb.Compound(material.Blueprint?.Name ?? "", ',');
				if (material.Tag != null)
				{
					sb.Append('.').Append(material.Tag);
				}
			}

			while (sb.EndsWith(','))
			{
				sb.Length--;
			}

#if BUILD_2_0_203
			ParentObject.SetStringProperty("Ornamentation", sb.ToString());
#else
			ParentObject.SetStringProperty("Ornamentation", Event.FinalizeString(sb));
#endif
			if (ParentObject.HasTag("Immutable"))
			{
				ParentObject.SetIntProperty("ForceMutableSave", 1);
			}

			ParentObject.SetIntProperty("Inorganic", organic ? 0 : 1);
		}

		/// <summary>
		/// Replace occurrances of =pm=, =sm=, and =tm= with their respecive material.
		/// </summary>
		public static string Substitute(string Name)
		{
			var i = Name.IndexOf('=');
			while (i >= 0)
			{
				var sb = Event.NewStringBuilder(Name);
				if (Chunks[0].Blueprint != null) Substitute(sb, "=pm", Chunks[0], i);
				if (Chunks[1].Blueprint != null) Substitute(sb, "=sm", Chunks[1], i);
				if (Chunks[2].Blueprint != null) Substitute(sb, "=tm", Chunks[2], i);

#if BUILD_2_0_203
				Name = sb.ToString();
#else
				Name = Event.FinalizeString(sb);
#endif
				i = Name.IndexOf('=');
			}

			// todo:someone should make this use/take a string builder
			i = Name.IndexOf('<');
			if (i >= 0)
			{
				Name = HistoricStringExpander.ExpandString(Name);
			}

			return Name;
		}

		public static void Substitute(StringBuilder SB, string Needle, Chunk Chunk, int StartIndex = 0)
		{
			var start = SB.IndexOf(Needle, StartIndex);
			if (start < 0) return;

			var next = start + Needle.Length;
			bool pluralize = false, nounify = false, clip = false, exclude = false;
			while (true)
			{
				var option = SB[next++];
				if (option == 'p') pluralize = true;
				else if (option == 'n') nounify = true;
				else if (option == 'c') clip = true;
				else if (option == 'x') exclude = true;
				else if (option == '=') break;
			}

			var name = Chunk.Blueprint.CachedDisplayNameStripped;
			var len = name.Length;
			SB.Remove(start, next - start);
			if (exclude)
			{
				len = 0;
			}
			else
			{
				SB.Insert(start, name);
				if (clip)
				{
					var space = name.IndexOf(' ');
					if (space >= 0)
					{
						SB.Remove(start + space, len - space);
						len = space;
					}
				}
			}
			if (nounify)
			{
				var noun = Chunk.Noun;
				if (noun == null) return;
				if (name.EndsWith(noun)) // hack: handle cases where noun is in name e.g. "primal grass grass"
				{
					noun = Chunk.GetFallbackNoun();
				}

				if (len > 0)
				{
					start += len;
					SB.Insert(start++, ' ');
				}

				SB.Insert(start, pluralize ? Grammar.Pluralize(noun) : noun);
			}
		}

		/// <summary>
		/// See if the blueprint has a cool shader on it.
		/// ONLY ;;;COOL;;; SHADERS!!!
		/// </summary>
		public static string TopShader(GameObjectBlueprint bp)
		{
			string result = null;
			var node = new Markup(bp.DisplayName());

			foreach (var child in node.Children)
			{
				var control = child as MarkupControlNode;
				var shader = MarkupShaders.Find(control?.Action);
				if (shader == null || shader is SolidColor) continue;

				result = control.Action;
				break;
			}

			node.release();
			return result;
		}

		/// <summary>
		/// Create a #x#x#RAD#x#x# pattern sometimes.
		/// </summary>
		public static string MakePattern(Chunk Chunk, int PatternChance, int ShimmeringChance)
		{
			if (PatternChance.in100())
			{
				var sb = Event.NewStringBuilder();
				if (ShimmeringChance.in100())
					sb.Append("shimmering ");
				sb.Append(Chunk.Foreground);

				for (int i = 0, c = Stat.Rand.Next(2, 5); i < c; i++)
				{
					sb.Append('-').Append(50.in100() ? Chunk.Foreground : Chunk.Detail);
				}

				sb.Append(" sequence");
#if BUILD_2_0_203
				return sb.ToString();
#else
				return Event.FinalizeString(sb);
#endif
			}

			return Chunk.Foreground.ToString();
		}

		/// <summary>
		/// Make object inherit colours of material.
		/// </summary>
		public static void InheritColors(GameObject Object, int Foreground, int Background, int Detail)
		{
			var org = Object.pRender;
			var chars = org.getColorChars();
			var fg = chars.foreground;
			var bg = chars.background;
			var dt = chars.detail;

			if (Foreground >= 0) fg = Chunks[Foreground].Foreground;
			if (Background >= 0) bg = Chunks[Background].Background;
			if (Detail >= 0) dt = Chunks[Detail].GetDetailColor(fg, dt);

			ColorSequence[1] = fg;
			ColorSequence[3] = bg;
			org.DetailColor = dt.ToString();
			if (!org.TileColor.IsNullOrEmpty())
			{
				org.TileColor = new string(ColorSequence);
				ColorSequence[3] = 'k';
				org.ColorString = new string(ColorSequence);
			}
			else
			{
				org.ColorString = new string(ColorSequence);
			}
		}
		
		
		[WishCommand("hpmaterial")]
		public static void Wish(string Spec)
		{
			var parts = Spec.Split(':');
			var sb = new StringBuilder();
			var tag = "Semantic" + parts[0];
			var sample = GameObjectFactory.Factory.CreateSampleObject("Creature");
			foreach (var blueprint in GameObjectFactory.Factory.BlueprintList)
			{
				if (!EncountersAPI.IsEligibleForDynamicEncounters(blueprint)) continue;
				if (!blueprint.HasTag(tag)) continue;
				
				LoadChunks(blueprint.Name + "." + parts[0]);
				sample.pRender.DisplayName = parts.Length > 1 ? parts[1] : "=pmcn=";
				Transform(sample);
				sb.Compound(sample.pRender.DisplayName, '\n');
				var chars = sample.pRender.getColorChars();
				sb.Append(' ').Append(chars.foreground).Append(' ').Append(chars.detail);
			}
			
			Popup.Show(sb.ToString());
		}
	}

#if BUILD_2_0_203
	/// <summary>
	/// Placeholder class for 203.XX-204.XX compat
	/// </summary>
	public class Interior : IPart
	{
		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == ObjectCreatedEvent.ID
				;
		}

		public override bool HandleEvent(ObjectCreatedEvent E)
		{
			E.ReplacementObject = EncountersAPI.GetAnItem();
			return base.HandleEvent(E);
		}
	}
	/// <summary>
	/// Placeholder class for 203.XX-204.XX compat
	/// </summary>
	public class InteriorExit : IPart
	{
		
	}
#endif
}
