using System;
using System.Diagnostics;
using Hearthpyre;

namespace XRL.World.Parts
{
	[Serializable]
	public class HearthpyrePairedStairs : IPart
	{
		public bool StairsUp => ParentObject.OwnPart<StairsUp>();
		public bool StairsDown => ParentObject.OwnPart<StairsDown>();

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade) 
			       || ID == EnterCellEvent.ID 
			       || ID == LeavingCellEvent.ID
				;
		}

		public override bool HandleEvent(EnterCellEvent E)
		{
			Static.RemoveChasm(E.Cell);

			var C = StairsUp ? E.Cell.Above() : E.Cell.Below();
			if (C != null && !C.HasObject(IsBlockingStair))
			{
				C.ClearObjectsWithTag("Wall");
				C.Construct(GetComplement());
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(LeavingCellEvent E)
		{
			RemoveZoneConnection(E.Cell);

			var C = StairsUp ? E.Cell.Above() : E.Cell.Below();
			if (C != null)
			{
				DemolishPaired(C);
				RemoveZoneConnection(C);
			}

			return base.HandleEvent(E);
		}

		public bool IsBlockingStair(GameObject Object)
		{
			if (!Object.HasTag("Stairs")) return false;
			if (Object.OwnPart<HearthpyrePairedStairs>()) return true;
			if (Object.OwnPart<StairsUp>()) return true;
			if (Object.TryTakePart(out StairsDown down))
			{
				return !down.PullDown;
			}

			return false;
		}

		public void DemolishPaired(Cell C)
		{
			if (C.TryTakeFirstObjectWith<HearthpyrePairedStairs>(out var paired))
			{
				paired.RemovePart<HearthpyrePairedStairs>();
				C.Demolish(paired);
			}
		}

		public void RemoveZoneConnection(Cell C)
		{
			var list = The.ZoneManager.GetZoneConnections(C.ParentZone.ZoneID);
			for (int i = list.Count - 1; i >= 0; i--)
			{
				if (list[i].X != C.X || list[i].Y != C.Y) continue;
				if (!list[i].Type.StartsWith("Stairs")) continue;
				list.RemoveAt(i);
			}
		}

		public string GetComplement()
		{
			if (ParentObject.TryTakePart(out StairsUp up) && up.Connected && !up.ConnectionObject.IsNullOrEmpty())
			{
				return up.ConnectionObject;
			}

			if (ParentObject.TryTakePart(out StairsDown down) && down.Connected &&
			    !down.ConnectionObject.IsNullOrEmpty())
			{
				return down.ConnectionObject;
			}

			var name = ParentObject.Blueprint;
			var result = "HearthpyreStairsDown";

			if (StairsUp)
			{
				var i = name.LastIndexOf("Up");
				if (i > 0) result = name.Substring(0, i) + "Down";
			}
			else
			{
				var i = name.LastIndexOf("Down");
				if (i > 0) result = name.Substring(0, i) + "Up";
				else result = "HearthpyreStairsUp";
			}

			return result;
		}
	}
}