using System;
using System.Collections.Generic;
using System.Linq;
using Hearthpyre;
using Hearthpyre.UI;
using Hearthpyre.Realm;
using Hearthpyre.Dialogue;
using XRL.Core;
using XRL.UI;
using XRL.Rules;
using XRL.World.Effects;
using XRL.World.WorldBuilders;
using static Hearthpyre.Static;

namespace XRL.World.Parts.Skill
{
    [Serializable]
    public class HearthpyreGoverning : BaseSkill
    {
        public const string ABL_NAME = "Govern";
        public const string ABL_CMD = "CommandGovern";
        public const string ABL_CAT = "Skill";
        public const string ABL_DESC = "Stake or manage a claim in the world.";

        Guid AbilityID = Guid.Empty;

        public override void SaveData(SerializationWriter writer)
        {
            base.SaveData(writer);

            var fields = new Dictionary<string, object>();
            fields["Ability"] = AbilityID;
            writer.Write(fields);
        }

        public override void LoadData(SerializationReader reader)
        {
            base.LoadData(reader);

            var fields = reader.ReadDictionary<string, object>();
            object val;
            if (fields.TryGetValue("Ability", out val)) AbilityID = (Guid) val;
        }

        public override bool WantEvent(int ID, int cascade)
        {
            return base.WantEvent(ID, cascade)
                   || ID == CommandEvent.ID
                   || ID == AfterGameLoadedEvent.ID
                ;
        }

        private static Action QueueUIShow = () => { GameManager.Instance.gameQueue.queueTask(QueueGameShow); };
        private static Action QueueGameShow = () => { View.Manager.Show(GovernView.NAME); };
        public override bool HandleEvent(CommandEvent E)
        {
            if (E.Command == ABL_CMD)
            {
                E.RequestInterfaceExit();
                // hack: workaround strange bug where pushing a game view prevents ability screen from closing
                GameManager.Instance.uiQueue.queueTask(QueueUIShow, 1);
            }
            return base.HandleEvent(E);
        }

        public override bool HandleEvent(AfterGameLoadedEvent E)
        {
            if (RealmSystem.IsInSector(The.Player))
            {
                var time = The.Game.GetSystem<RealmSystem>()?.RestTime;
                if (time.HasValue)
                {
                    HearthpyreRested.AddMetaExperience(DateTime.UtcNow.Subtract(time.Value));
                }
            }
            
            Hearthpyre.WorldBuilderExtension.RequireSecrets();
            return base.HandleEvent(E);
        }

#if !BUILD_2_0_204
        public override void FinalizeLoad()
        {
            base.FinalizeLoad();
            var ability = ParentObject.GetActivatedAbility(AbilityID);
            if (ability != null && ability.UITileDefault?.Tile == null)
            {
                ability.UITileDefault = null;
            }
        }
#endif

        public override bool AddSkill(GameObject GO)
        {
            AbilityID = AddMyActivatedAbility(Name: ABL_NAME, Command: ABL_CMD, Class: ABL_CAT, Description: ABL_DESC);
            return true;
        }

        public override bool RemoveSkill(GameObject GO)
        {
            RemoveMyActivatedAbility(ref AbilityID);
            return true;
        }

        /*public Settlement NewSettlement(Zone zone) {
            var settlement = new Settlement(this, zone);
            Settlements[settlement.ID] = settlement;
            return settlement;
        }

        public Settlement GetSettlement(Zone zone) => GetSettlement(zone.wX, zone.wY, zone.ZoneWorld);

        public Settlement GetSettlement(int wX, int wY, string world = "JoppaWorld") {
            var worldId = Strings.SB.Clear().Append(world).Append('.')
                .Append(wX).Append('.')
                .Append(wY).ToString();

            return GetSettlement(worldId);
        }

        public Settlement GetSettlement(string worldId) {
            Settlement result;
            SettlementsByWorldID.TryGetValue(worldId, out result);
            return result;
        }*/
    }
}