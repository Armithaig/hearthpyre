using System;
using static Hearthpyre.Static;
using Hearthpyre;
using System.Collections.Generic;
using XRL.World.Capabilities;
using XRL.Language;
using XRL.UI;
using XRL.Rules;
using Genkit;
using System.Linq;
using XRL.World.Effects;

namespace XRL.World.Parts.Skill
{
	[Serializable]
	public class HearthpyreGoverning_Porter : BaseSkill
	{
		public const string ABL_NAME = "Cart Blitz";
		public const string ABL_CMD = "CommandPorterCharge";
		public const string ABL_CAT = "Skill";
		public const string ABL_DESC = "You charge forward 3 to 5 squares and make a splash attack based on your encumbrance.\n\nThe defenders must succeed an agility saving throw or be knocked prone.\n\nRequires a cart.";
		public const int ABL_MIN = 3;
		public const int ABL_MAX = 5;

		Guid AbilityID;

		public override void SaveData(SerializationWriter Writer)
		{
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			fields["Ability"] = AbilityID;
			Writer.Write(fields);
		}

		public override void LoadData(SerializationReader Reader)
		{
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			object val;
			if (fields.TryGetValue("Ability", out val)) AbilityID = (Guid) val;
		}

		public override bool AllowStaticRegistration() => true;

		public override void Register(GameObject Object)
		{
			Object.RegisterPartEvent(this, ABL_CMD);
			Object.RegisterPartEvent(this, "AIGetOffensiveMutationList");
			base.Register(Object);
		}

		public override bool FireEvent(Event E)
		{
			if (E.ID == ABL_CMD)
			{
				AttemptCharge();
			}
			else if (E.ID == "AIGetOffensiveMutationList")
			{
				AIChargeOption(E);
			}

			return base.FireEvent(E);
		}

		public override bool AddSkill(GameObject obj)
		{
			AbilityID = AddMyActivatedAbility(Name: ABL_NAME, Command: ABL_CMD, Class: ABL_CAT, Description: ABL_DESC, IsAttack: true);
			return true;
		}

		public override bool RemoveSkill(GameObject obj)
		{
			RemoveMyActivatedAbility(ref AbilityID);
			return true;
		}

		void AIChargeOption(Event E)
		{
			if (!CanCharge()) return;

			var dist = E.GetIntParameter("Distance");
			var target = E.GetGameObjectParameter("Target");
			if (dist <= ABL_MIN || dist > ABL_MAX + 1) return;
			if (!IsMyActivatedAbilityUsable(AbilityID)) return;
			if (!target.PhaseAndFlightMatches(ParentObject)) return;

			E.AddAICommand(ABL_CMD);
		}

		bool CanCharge()
		{
			if (ParentObject.Body.GetBody().TakeItemPart<HearthpyreCart>() == null)
				return ParentObject.Failure("You cannot charge without a cart.");
			if (ParentObject.OnWorldMap())
				return ParentObject.Failure("You cannot charge on the world map.");
			if (Flight.IsFlying(ParentObject))
				return ParentObject.Failure("You cannot charge while flying.");
			if (ParentObject.AreHostilesAdjacent())
				return ParentObject.Failure("You cannot charge while engaged in melee.");

			return true;
		}

		bool AttemptCharge()
		{
			if (!CanCharge()) return false;

			var line = PickLine(ABL_MAX + 1, AllowVis.OnlyVisible, x => x != null && x.IsCombatObject() && x.FlightMatches(ParentObject));
			if (line == null) return false;
			if (line.Count - 2 < ABL_MIN) return ParentObject.Failure($"You must charge at least {Grammar.Cardinal(ABL_MIN)} spaces.");
			if (line.Count - 2 > ABL_MAX) return ParentObject.Failure($"You can't charge more than {Grammar.Cardinal(ABL_MAX)} spaces.");

			var cell = line.Last();
			var target = ParentObject.Target;
			if (IsPlayer()) target = cell.GetCombatTarget(ParentObject, InanimateSolidOnly: true);
			if (target == null) return ParentObject.Failure("You must charge at a target!");

			Charge(line);
			return true;
		}

		void Charge(List<Cell> line)
		{
			var inventory = ParentObject.TakePart<Inventory>();
			var steps = new string[line.Count - 1];
			for (int i = 0; i < line.Count - 1; i++)
				steps[i] = line[i].GetDirectionFromCell(line[i + 1]);

			for (int i = 0, a = 0, c = steps.Length + ABL_MIN; i < c; i++)
			{
				var step = steps[i % steps.Length];
				var cell = ParentObject.CurrentCell.GetCellFromDirection(step);
				var target = cell.GetCombatTarget(ParentObject, InanimateSolidOnly: true);
				if (target != null)
				{
					DidXToY("crash", "right into", target, null, "!", ColorAsBadFor: target);
					PlayWorldSound("Hit_Default", 0.5f, 0.2f);
					cell.ForeachLocalAdjacentCellAndSelf(x =>
					{
						var hostile = x.GetCombatTarget(ParentObject, InanimateSolidOnly: true);
						if (hostile == null || hostile == ParentObject) return;

						Damage(hostile);
						ParentObject.FireEvent(Event.New("ChargedTarget", "Defender", hostile));
						hostile.FireEvent(Event.New("WasCharged", "Attacker", ParentObject));
						a++;
					});
					
					CombatJuice.cameraShake(0.3f * a);
					break;
				}
				else
				{
					ParentObject.TileParticleBlip("Items/hp_cart.png", "&K", "y", 10 + i * 5);
					if (!ParentObject.Move(step, EnergyCost: 0))
						break;
				}
			}

			ParentObject.UseEnergy(1000, "Charging");
			CooldownMyActivatedAbility(AbilityID, 40);
		}

		void Damage(GameObject target)
		{
			var die = Math.Max(1, RoundToInt(ParentObject.GetCarriedWeight() / 100d));
			var dmg = new Damage(Stat.Roll(die + "d4+" + ParentObject.StatMod("Strength") * 2));
			var take = Event.New("TakeDamage", 0, 0, 0);
			take.AddParameter("Damage", dmg);
			take.AddParameter("Owner", ParentObject);
			take.AddParameter("Attacker", ParentObject);
			take.AddParameter("Message", "from %o cart blitz!");
			target.FireEvent(take);

			if (!target.MakeSave("Agility", die * 2, ParentObject, "Strength", "Cart Blitz Knockdown"))
				target.ApplyEffect(new Prone());
		}
	}
}
