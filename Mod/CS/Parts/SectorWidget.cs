using System;
using System.Collections.Generic;
using Hearthpyre;
using XRL.Core;
using XRL.World;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	// TODO: this could listen for movements of solids in sector and adjust the lattice
	// TODO: Add random events like visiting travelers.
	// How to implement IZ and OOZ?
	// Extensible through attributes?
	public class HearthpyreSectorWidget : IPart
	{

		[NonSerialized] private bool FixMerchants;
		
		public override void SaveData(SerializationWriter Writer)
		{
			base.SaveData(Writer);
			var fields = new Dictionary<string, object>();
			Writer.Write(fields);
		}

		public override void LoadData(SerializationReader Reader)
		{
			base.LoadData(Reader);
			var fields = Reader.ReadDictionary<string, object>();
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == SuspendingEvent.ID
			       || ID == ZoneActivatedEvent.ID
				;
		}

		public override bool HandleEvent(SuspendingEvent E)
		{
			RealmSystem.GetSector(ParentObject.CurrentZone)?.Lattice.Flush();
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(ZoneActivatedEvent E)
		{
			RealmSystem.GetSector(ParentObject.CurrentZone)?.Activated();
			return base.HandleEvent(E);
		}

		public override bool WantTurnTick() => true;
		public override void TurnTick(long TurnNumber)
		{
			SetZoneActive(TurnNumber);
		}

		public override bool WantTenTurnTick() => true;
		public override void TenTurnTick(long TurnNumber)
		{
			SetZoneActive(TurnNumber);
		}

		public override bool WantHundredTurnTick() => true;
		public override void HundredTurnTick(long TurnNumber)
		{
			SetZoneActive(TurnNumber);
		}

		[NonSerialized] private Zone CurrentZone;
		[NonSerialized] private ZoneManager ZoneManager;

		public void SetZoneActive(long TurnNumber)
		{
			if (ZoneManager == null) ZoneManager = The.ZoneManager;
			if (ZoneManager.CachedZones.Count > OptionSimActive) return;
			if (CurrentZone == null) CurrentZone = ParentObject.CurrentZone;

			if (ZoneManager.ActiveZone == CurrentZone) return;
			if (ZoneManager.ActiveZone.wX != CurrentZone.wX || ZoneManager.ActiveZone.wY != CurrentZone.wY) return;

			var dist = Math.Abs(ZoneManager.ActiveZone.X - CurrentZone.X)
			           + Math.Abs(ZoneManager.ActiveZone.Y - CurrentZone.Y)
			           + Math.Abs(ZoneManager.ActiveZone.Z - CurrentZone.Z);

			CurrentZone.LastActive = TurnNumber - dist * 2;
		}
	}
}
