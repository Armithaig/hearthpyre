using Hearthpyre;
using Hearthpyre.Dialogue;
using Hearthpyre.UI;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using XRL.UI;
using XRL.World.Effects;
using System;
using UnityEngine;

namespace XRL.World.Parts
{
	[Serializable]
	public class HearthpyreTowed : IPart
	{
		public GameObject Item;
		public GameObject Owner;

		public void SetItem(GameObject obj)
		{
			Item = obj;

			var render = ParentObject.pRender;
			var irender = obj.pRender;
			render.Tile = irender.Tile;
			render.DisplayName = irender.DisplayName;
			render.ColorString = irender.ColorString;
			render.DetailColor = irender.DetailColor;
			render.TileColor = irender.TileColor;

			var desc = ParentObject.TakePart<Description>();
			var idesc = obj.TakePart<Description>();
			desc._Short = idesc._Short;
			desc.Mark = idesc.Mark;
		}

		public override bool AllowStaticRegistration() => true;

		public override void Register(GameObject Object)
		{
			Owner?.RegisterPartEvent(this, "EnteringCell");
		}

		public void SetOwner(GameObject obj)
		{
			Owner = obj;
			obj.RegisterPartEvent(this, "EnteringCell");
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return
				base.WantEvent(ID, cascade)
				|| ID == BeforeDestroyObjectEvent.ID
				|| ID == BeforeDieEvent.ID
				;
		}

		public override bool HandleEvent(BeforeDestroyObjectEvent E)
		{
			Owner.UnregisterPartEvent(this, "EnteringCell");
			return true;
		}

		public override bool HandleEvent(BeforeDieEvent E)
		{
			Item.ApplyEffect(new Broken());
			return true;
		}

		public override bool FireEvent(Event E)
		{
			// TODO: Possible to listen to MinEvents on arbitrary objects?
			// Only by splitting into two tower/towed parts/effects currently, best as'm able to tell.
			if (E.ID == "EnteringCell")
			{
				var exit = Owner.CurrentCell;
				if (exit == null) return true;

				var enter = E.GetParameter("Cell") as Cell;
				if (enter == null || enter == ParentObject.CurrentCell) return true;
				if (enter.IsAdjacentTo(ParentObject.CurrentCell)) return true;

				ParentObject.SystemMoveTo(exit, 0, true);
			}

			return true;
		}
	}
}