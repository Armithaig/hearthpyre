﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;

namespace XRL.World.Parts
{
	public partial class HearthpyreXyloschemer
	{
		public override void SaveData(SerializationWriter writer) {
			base.SaveData(writer);

			var fields = new Dictionary<string, object>();
			fields["Packages"] = Packages.ToArray();
			fields["Ability"] = AbilityID;
			fields["Skilled"] = Skilled;
			writer.Write(fields);
		}

		public override void LoadData(SerializationReader reader) {
			base.LoadData(reader);

			var fields = reader.ReadDictionary<string, object>();
			object val;
			if (fields.TryGetValue("Packages", out val)) Packages.AddRange((string[]) val);
			if (fields.TryGetValue("Ability", out val)) AbilityID = (Guid) val;
			if (fields.TryGetValue("Skilled", out val)) Skilled = (bool) val;
		}

#if !BUILD_2_0_204
		public override void FinalizeLoad()
		{
			base.FinalizeLoad();
			var ability = ParentObject.Holder?.GetActivatedAbility(AbilityID);
			if (ability != null && ability.UITileDefault?.Tile == null)
			{
				ability.UITileDefault = null;
			}
		}
#endif

		public XmlSchema GetSchema() => null;

		public void WriteXml(XmlWriter writer) {
			foreach (var package in Packages) {
				writer.WriteStartElement("Package");
				writer.WriteAttributeString("Name", package);
				writer.WriteEndElement();
			}
		}

		public void ReadXml(XmlReader reader) {
			var depth = reader.Depth;
			reader.ReadStartElement();
			while (reader.Read() && reader.Depth > depth) {
				if (reader.IsStartElement("Package")) {
					Packages.Add(reader.GetAttribute("Name"));
				}
			}
		}
	}
}