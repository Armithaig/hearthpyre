﻿using System;
using System.Linq;
using System.Collections.Generic;
using Genkit;
using XRL.UI;
using XRL.Core;
using XRL.Rules;
using Hearthpyre;
using Hearthpyre.Realm;
using XRL.World.Tinkering;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	[Serializable]
	public class HearthpyreBlueprint : IPart
	{
		public static readonly string[] PNT_TAGS =
		{
			"PaintedWall", "PaintedWallAtlas", "PaintedWallExtension",
			"PaintedFence", "PaintedFenceAtlas", "PaintedFenceExtension", "PaintWith"
		};

		public const string INV_BLD = "InvCommandBuild";
		public const string INV_BLD_ALL = INV_BLD + "All";

		public string Blueprint { get; set; }
		public bool Powered { get; set; }
		public bool Tinkered { get; set; }
		public int Cost { get; set; }


		public override bool SameAs(IPart p)
		{
			var bp = p as HearthpyreBlueprint;
			if (bp != null)
				return bp.Blueprint == Blueprint;
			return false;
		}

		public override void SaveData(SerializationWriter Writer)
		{
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			fields["Blueprint"] = Blueprint;
			fields["Powered"] = Powered;
			fields["Tinkered"] = Tinkered;
			fields["Cost"] = Cost;
			Writer.Write(fields);
		}

		public override void LoadData(SerializationReader Reader)
		{
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			object val;
			if (fields.TryGetValue("Blueprint", out val)) Blueprint = (string) val;
			if (fields.TryGetValue("Powered", out val)) Powered = (bool) val;
			if (fields.TryGetValue("Tinkered", out val)) Tinkered = (bool) val;
			if (fields.TryGetValue("Cost", out val)) Cost = (int) val;
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == GetInventoryActionsEvent.ID
			       || ID == GetShortDescriptionEvent.ID && Tinkered
			       || ID == CanSmartUseEvent.ID
			       || ID == CommandSmartUseEvent.ID
			       || ID == ForceApplyEffectEvent.ID
			       || ID == InventoryActionEvent.ID
				;
		}

		public override bool HandleEvent(GetInventoryActionsEvent E)
		{
			E.AddAction(Name: "Build", Display: "{{W|b}}uild", Command: INV_BLD, Key: 'b');
			E.AddAction(Name: "BuildAll", Display: "build {{W|a}}ll", Command: INV_BLD_ALL, Key: 'a');
			return true;
		}

		public override bool HandleEvent(InventoryActionEvent E)
		{
			if (E.Command == INV_BLD)
			{
				Build(E.Actor);
			}
			else if (E.Command == INV_BLD_ALL)
			{
				var C = ParentObject.CurrentCell;
				if (!Build(E.Actor, true)) return true;

				XRLCore.Core.RenderDelay(300);

				foreach (var blp in C.YieldCardinalParts<HearthpyreBlueprint>())
					if (blp.SameAs(this)) blp.HandleEvent(E);
			}

			return true;
		}

		public override bool HandleEvent(CanSmartUseEvent E)
		{
			return E.Actor.CurrentCell == ParentObject.CurrentCell;
		}

		public override bool HandleEvent(CommandSmartUseEvent E)
		{
			Build(E.Actor);
			return true;
		}

		public override bool HandleEvent(ForceApplyEffectEvent E)
		{
			return false;
		}

		public override bool HandleEvent(GetShortDescriptionEvent E)
		{
			if (Tinkered)
			{
				var bits = TinkerItem.GetBitCostFor(Blueprint ?? ParentObject.Blueprint);
				E.Prefix.Append("{{rules|Required bits: ").Append(BitType.GetString(bits)).Append(".}}\n\n");
			}
			return base.HandleEvent(E);
		}

		[NonSerialized] private int FrameOffset;
		public override bool Render(RenderEvent E)
		{
			int num = (XRLCore.CurrentFrame + FrameOffset) & 2047;
			if (num < 16)
				E.ColorString = "&C";
			else if (num < 32)
				E.ColorString = "&c";

			FrameOffset = unchecked(FrameOffset + Stat.RandomCosmetic(0, 48));
			return true;
		}

		public bool Build(GameObject Actor, bool Silent = false)
		{
			var C = ParentObject.CurrentCell;
			if (C == Actor.CurrentCell) return false;
			
			var xyloschemer = Actor.GetItemWithBlueprint(OBJ_SCMR);
			if (xyloschemer == null)
			{
				if (!Silent)
				{
					Actor.Failure(CheckEpistemicStatus(OBJ_SCMR)
						? "You can't build that without a xyloschemer."
						: "You don't have anything to build that with.");
				}

				return false;
			}

			var part = xyloschemer.TakePart<HearthpyreXyloschemer>();
			if (!part.UseCharge(ChargeUse: Cost, Silent: Silent)) return false;

			if (Tinkered)
			{
				var locker = Actor.IncludePart<BitLocker>();
				var bits = TinkerItem.GetBitCostFor(Blueprint);
				if (!locker.UseBits(bits))
				{
					if (!Silent)
					{
						var sb = Event.NewStringBuilder("You don't have the required <").Append(BitType.GetString(bits)).Append("> bits!");
						if (locker.BitStorage.Count > 0) sb.Append("\n\n").Append(locker.GetBitsString());
						Actor.Failure(sb.ToString());
					}

					return false;
				}
			}

			HoloZap(C);
			C.RemoveObject(ParentObject);
			ParentObject.Destroy();

			var obj = C.Construct(GameObjectFactory.Factory.CreateObject(Blueprint, Brand));
			if (obj == null) return false;

			if (!Silent)
			{
				Actor.pPhysics.DidXToYWithZ(
					"zap",
					obj,
					"into existance with",
					xyloschemer,
					IndefiniteDirectObject: true,
					IndirectObjectPossessedBy: Actor
				);
			}

			Lattice.Invalidate(C);
			return true;
		}

		public void Brand(GameObject Object) => Static.Brand(Object, Powered: Powered);

		public void SetDesign(Notitia.Blueprint Design)
		{
			var blueprint = Design.Value;
			var render = blueprint.GetPart("Render");
			if (render != null)
			{
				string param;
#if BUILD_2_0_203
				if (render.Parameters.TryGetValue("DisplayName", out param))
					ParentObject.pRender.DisplayName = param + " {{b-b-B sequence|blueprint}}";
				if (render.Parameters.TryGetValue("RenderString", out param))
					ParentObject.pRender.RenderString = Static.RenderStrToChar(param);
				if (render.Parameters.TryGetValue("RenderLayer", out param))
					ParentObject.pRender.RenderLayer = Calc.Clamp(int.Parse(param), 3, 7) * 10;
				if (render.Parameters.TryGetValue("DetailColor", out param))
					ParentObject.pRender.DetailColor = param == "k" ? "k" : "K";
				if (render.Parameters.TryGetValue("Tile", out param))
					ParentObject.pRender.Tile = param;
#else
				if (render.TryGetParameterValue("DisplayName", out param))
					ParentObject.pRender.DisplayName = param + " {{b-b-B sequence|blueprint}}";
				if (render.TryGetParameterValue("RenderString", out param))
					ParentObject.pRender.RenderString = Static.RenderStrToChar(param);
				if (render.TryGetParameterValue("RenderLayer", out int layer))
					ParentObject.pRender.RenderLayer = Calc.Clamp(layer, 3, 7) * 10;
				if (render.TryGetParameterValue("DetailColor", out param))
					ParentObject.pRender.DetailColor = param == "k" ? "k" : "K";
				if (render.TryGetParameterValue("Tile", out param))
					ParentObject.pRender.Tile = param;
#endif
			}

			var tile = blueprint.GetPart("PickRandomTile");
			if (tile != null)
			{
#if BUILD_2_0_203
				var part = new PickRandomTile
				{
					ParentObject = ParentObject,
					Tile = tile.GetParameter("Tile")
				};
#else
				var part = new PickRandomTile { ParentObject = ParentObject };
				tile.TryGetParameterValue("Tile", out part.Tile);
#endif
				//tile.InitializePartInstance(part); // Hmm, this caused tile loading issues for both blueprint and design.
				ParentObject.AddPart(part);

				if (part.WantEvent(ObjectCreatedEvent.ID, ObjectCreatedEvent.CascadeLevel))
				{
					var E = ObjectCreatedEvent.FromPool();
					E.Object = ParentObject;
					part.HandleEvent(E);
				}
				else
				{
					part.FireEvent(Event.New("ObjectCreated"));
				}
			}

			string paint;
			foreach (var key in PNT_TAGS)
			{
				if (blueprint.Tags.TryGetValue(key, out paint))
				{
					ParentObject.SetStringProperty(key, paint);
				}
			}
			
			if (blueprint.Tags.TryGetValue("PaintPart", out paint))
			{
				var fence = ParentObject.GetStringProperty("PaintedFence", "");
#if BUILD_2_0_203
				fence += blueprint.GetPartParameter(paint, "TileAppendWhenUnpowered", "");
				fence += blueprint.GetPartParameter(paint, "TileAppendWhenUnbroken", "");
#else
				fence += blueprint.GetPartParameter<string>(paint, "TileAppendWhenUnpowered", "");
				fence += blueprint.GetPartParameter<string>(paint, "TileAppendWhenUnbroken", "");
#endif
				if (!fence.IsNullOrEmpty()) ParentObject.SetStringProperty("PaintedFence", fence);
			}

			Blueprint = blueprint.Name;
			Cost = (1 + Calc.Clamp(blueprint.Tier, 0, 10)) * 50;
			Powered = !The.Game.HasIntGameState(GST_PWR);
			Tinkered = !OptionNoBits && Design.Tinkered;
		}
	}
}
