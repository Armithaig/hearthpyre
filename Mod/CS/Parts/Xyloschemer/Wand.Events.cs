﻿using System;
using System.Diagnostics;
using System.Linq;
using Hearthpyre;
using Hearthpyre.UI;
using XRL.Core;
using XRL.Rules;
using XRL.UI;
using XRL.World.Effects;
using XRL.World.Parts.Skill;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	public partial class HearthpyreXyloschemer
	{
		public override bool AllowStaticRegistration() => true;

		public override void Register(GameObject Object)
		{
			Object.RegisterPartEvent(this, "ExamineSuccess");
			Object.RegisterPartEvent(this, "WeaponHit");
			GetActivePartFirstSubject()?.RegisterEvent(this, CommandEvent.ID);
		}

		public override bool FireEvent(Event E)
		{
			if (E.ID == "WeaponHit")
			{
				HandleWeaponHit(E);
			}
			else if (E.ID == "ExamineSuccess")
			{
				var subject = GetActivePartFirstSubject();
				if (subject != null)
				{
					AdjustStats(subject);
					AddAbility(subject);
					GetActivePartStatus();
				}
			}

			return base.FireEvent(E);
		}

		public void HandleWeaponHit(Event E)
		{
			var Properties = E.GetStringParameter("Properties") ?? "";
			if (!Properties.Contains("Charging") && Stat.Random(1, 100) <= SpecialChance) return;
			if (IsDisabled(UseCharge: true, ChargeUse: SpecialCost)) return;

			var Defender = E.GetGameObjectParameter("Defender");
			if (Defender.TryTakeEffect(out HearthpyreMirage FX))
			{
				FX.Duration = Math.Max(FX.Duration, Stat.RollCached(SPEC_DUR));
			}
			else
			{
				Defender.ApplyEffect(new HearthpyreMirage
				{
					LightRadius = 3,
					MeleeToHit = -1,
					MissileToHit = -1,
					AimVariance = 1,
					Duration = Stat.RollCached(SPEC_DUR)
				});
			}
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == BeforeObjectCreatedEvent.ID
			       || ID == InventoryActionEvent.ID
			       || ID == GetInventoryActionsEvent.ID
			       || ID == GetShortDescriptionEvent.ID
			       || ID == GetItemElementsEvent.ID
			       || ID == EquippedEvent.ID
			       || ID == UnequippedEvent.ID
			       || ID == TakenEvent.ID
			       || ID == DroppedEvent.ID
			       || ID == AfterAddSkillEvent.ID
			       || ID == AfterRemoveSkillEvent.ID
			       || ID == ApplyEffectEvent.ID
				;
		}

		public override bool HandleEvent(CommandEvent E)
		{
			if (E.Command == ABL_CMD && HandleActivation(E, E.Silent))
			{
				return false;
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(BeforeObjectCreatedEvent E)
		{
			if (!ZoneManager.ZoneGenerationContext?.Built == false)
			{
				var game = The.Game;
				if (game != null && game.HasBooleanGameState(GST_IT_SCMR) && 90.in100())
				{
					E.ReplacementObject = PopulationManager.CreateOneFrom("Artifacts 4R");
				}
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(InventoryActionEvent E)
		{
			if (E.Command == INV_CMD && HandleActivation(E, E.Silent))
			{
				return false;
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(GetInventoryActionsEvent E)
		{
			E.AddAction(Name: "Scheme", Display: "{{W|s}}cheme", Command: INV_CMD, Key: 's');
			return true;
		}

		public override bool HandleEvent(GetShortDescriptionEvent E)
		{
			if (Packages.Count <= 0) return true;

			E.Postfix.Append("\n{{y|Installed: ");
			for (int i = 0; i < Packages.Count; i++)
			{
				if (i != 0) E.Postfix.Append(", ");
				E.Postfix.Append("{{C|").Append(Packages[i]).Append('}', 2);
			}

			E.Postfix.Append('}', 2);

			return true;
		}

		public override bool HandleEvent(GetItemElementsEvent E)
		{
			E.Add("circuitry", 1);
			E.Add("stars", 1);
			E.Add("glass", 1);
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(EquippedEvent E)
		{
			AdjustStats(E.Actor);
			AddAbility(E.Actor);
			GetActivePartStatus();
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(UnequippedEvent E)
		{
			AdjustStats(E.Actor);
			RemoveAbility(E.Actor);
			GetActivePartStatus();
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(TakenEvent E)
		{
			AdjustStats(E.Actor);
			AddAbility(E.Actor);
			GetActivePartStatus();
			if (E.Actor != null && E.Actor.IsPlayerControlled())
			{
				The.Game?.SetBooleanGameState(GST_IT_SCMR, true);
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(DroppedEvent E)
		{
			AdjustStats(E.Actor);
			RemoveAbility(E.Actor);
			GetActivePartStatus();
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(AfterAddSkillEvent E)
		{
			if (E.Skill.IsType<HearthpyreGoverning_Xylogrifter>())
			{
				AdjustStats(E.Actor);
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(AfterRemoveSkillEvent E)
		{
			if (E.Skill.IsType<HearthpyreGoverning_Xylogrifter>())
			{
				AdjustStats(E.Actor);
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(ApplyEffectEvent E)
		{
			if (E.Effect.IsType<Rusted>())
			{
				LastStatus = ActivePartStatus.Rusted;
				SyncRender();
			}
			else if (E.Effect.IsType<Broken>())
			{
				LastStatus = ActivePartStatus.Broken;
				SyncRender();
			}

			return base.HandleEvent(E);
		}

		private static Action QueueUIShow = () => { GameManager.Instance.gameQueue.queueTask(QueueGameShow); };
		private static Action QueueGameShow = () => { View.Manager.Show(SchemeView.NAME); };
		public bool HandleActivation(IEvent E, bool Silent = false)
		{
			if (!UseCharge(Silent: Silent, FromDialog: true)) return false;

			ACTIVE = this;
			E.RequestInterfaceExit();
			// hack: workaround strange bug where pushing a game view prevents ability screen from closing
			GameManager.Instance.uiQueue.queueTask(QueueUIShow, 1);
			return true;
		}

		/// <summary>
		/// Adjust melee weapon stats for xylogrifters.
		/// </summary>
		void AdjustStats(GameObject Actor)
		{
			var skilled = IsObjectActivePartSubject(Actor) && Actor.OwnPart<HearthpyreGoverning_Xylogrifter>();
			if (Skilled == skilled) return;
			Skilled = skilled;

			var weapon = ParentObject.TakePart<MeleeWeapon>();
			if (Skilled)
			{
				var roll = new DieRoll(weapon.BaseDamage).FindType(DieRoll.TYPE_DIE);
				if (roll != null)
				{
					roll.LeftValue += 1;
					roll.RightValue += 2;
					weapon.BaseDamage = roll.ToString();
				}

				weapon.MaxStrengthBonus += 5;
				weapon.HitBonus += 1;
			}
			else
			{
				var roll = new DieRoll(weapon.BaseDamage).FindType(DieRoll.TYPE_DIE);
				if (roll != null)
				{
					roll.LeftValue -= 1;
					roll.RightValue -= 2;
					weapon.BaseDamage = roll.ToString();
				}

				weapon.MaxStrengthBonus -= 5;
				weapon.HitBonus -= 1;
			}
		}

		void AddAbility(GameObject Actor)
		{
			if (!IsObjectActivePartSubject(Actor)) return;
			Actor.RegisterEvent(this, CommandEvent.ID);

			var abilities = Actor.TakePart<ActivatedAbilities>();
			if (abilities == null || AbilityID != Guid.Empty) return;

			var ability = abilities.GetAbility(ABL_CMD);
			if (ability != null)
			{
				AbilityID = ability.ID;
				return;
			}

			AbilityID = abilities.AddAbility(Name: ABL_NAME, Command: ABL_CMD, Class: ABL_CAT, Description: ABL_DESC, Silent: true);
		}

		void RemoveAbility(GameObject Actor)
		{
			if (Actor == null)
			{
				// death removal sends null actor, fine to just empty ID
				AbilityID = Guid.Empty;
				return;
			}

			if (IsObjectActivePartSubject(Actor)) return;
			Actor.UnregisterEvent(this, CommandEvent.ID);
			if (Actor.GetItemWithBlueprint(ParentObject.Blueprint) != null) return;

			var abilities = Actor.TakePart<ActivatedAbilities>();
			if (abilities?.AbilityByGuid == null) return;
			if (AbilityID != Guid.Empty) abilities.RemoveAbility(AbilityID);

			var ids = abilities.AbilityByGuid.Values.Where(x => x.Command == ABL_CMD).Select(x => x.ID).ToArray();
			foreach (var id in ids) abilities.RemoveAbility(id);

			AbilityID = Guid.Empty;
		}
	}
}