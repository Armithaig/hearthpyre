﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using XRL.UI;
using XRL.World.Effects;
using XRL.Core;
using XRL.Rules;
using Hearthpyre;
using Hearthpyre.UI;
using XRL.Language;
using XRL.World.Parts.Skill;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	[Serializable]
	public partial class HearthpyreXyloschemer : IPoweredPart, IXmlSerializable
	{
		// TODO: Make this less garbage.
		public static HearthpyreXyloschemer ACTIVE;

		public const string TILE_RDY = "Items/hp_xyloschemer.png";
		public const string TILE_OFF = "Items/hp_xyloschemer_off.png";
		public const string TILE_BRK = "Items/hp_xyloschemer_broken.png";
		public const string NAME_RDY = "{{shimmering b-b-B sequence|xyloschemer}}";
		public const string NAME_OFF = "xyloschemer";
		public const string ABL_NAME = "Activate Xyloschemer";
		public const string ABL_CMD = "ActivateXyloschemer";
		public const string ABL_CAT = "Items";
		public const string ABL_DESC = "Activate a charged xyloschemer.";
		public const string INV_CMD = "InvCommandScheme";
		public const double CHARGE_MULT = 1.0;
		public const double CHARGE_MULT_IMP = 0.75;
		public const int SPEC_CHANCE = 10;
		public const int SPEC_CHANCE_IMP = SPEC_CHANCE * 2;
		public const int SPEC_COST = 25;
		public const string SPEC_DUR = "3d4";

		Guid AbilityID;
		int FrameOffset;
		bool DoRender = true;

		public List<string> Packages { get; set; } = new List<string>();
		public bool Skilled { get; set; }

		public double ChargeMult => Skilled ? CHARGE_MULT_IMP : CHARGE_MULT;
		public int SpecialChance => Skilled ? SPEC_CHANCE_IMP : SPEC_CHANCE;
		public int SpecialCost => RoundToInt(SPEC_COST * ChargeMult);

		public HearthpyreXyloschemer()
		{
			MustBeUnderstood = true;
			WorksOnEquipper = true;
			WorksOnCarrier = true;
			NameForStatus = "HoloFab";
		}

		public override bool SameAs(IPart p) => false;

		public override void Initialize()
		{
			if (!ParentObject.HasProperName)
			{
				ParentObject.DisplayName = NAME_RDY;
			}
		}

		public override bool Render(RenderEvent E)
		{
			if (!DoRender) return true;

			var f = (XRLCore.CurrentFrame + FrameOffset) & 511;
			if (f < 8) E.DetailColor = "C";
			else if (f < 16) E.DetailColor = "c";
			else if (f < 24) E.DetailColor = "B";
			else if (f < 32) E.DetailColor = "K";

			FrameOffset = unchecked(FrameOffset + Stat.RandomCosmetic(0, 24));
			if (Stat.RandomCosmetic(1, 400) == 1)
			{
				E.DetailColor = "Y";
				E.ColorString = "&Y";
			}

			return true;
		}

		public override void SyncRender()
		{
			var R = ParentObject.pRender;
			switch (LastStatus)
			{
				case ActivePartStatus.NeedsSubject: break;
				case ActivePartStatus.Operational:
					if (!ParentObject.HasProperName)
					{
						R.DisplayName = NAME_RDY;
						R.ColorString = "&b";
						R.DetailColor = "B";
					}
					
					R.Tile = TILE_RDY;
					DoRender = true;
					break;
				case ActivePartStatus.Broken:
					if (!ParentObject.HasProperName)
					{
						R.DisplayName = NAME_OFF;
						R.ColorString = "&b";
						R.DetailColor = "K";
					}

					R.Tile = TILE_BRK;
					DoRender = false;
					break;
				case ActivePartStatus.Rusted:
					if (!ParentObject.HasProperName)
					{
						R.DisplayName = NAME_OFF;
						R.ColorString = "&r";
						R.DetailColor = "W";
					}

					R.Tile = TILE_OFF;
					DoRender = false;
					break;
				default:
					if (!ParentObject.HasProperName)
					{
						R.DisplayName = NAME_OFF;
						R.ColorString = "&b";
						R.DetailColor = "K";
					}

					R.Tile = TILE_OFF;
					DoRender = false;
					break;
			}
		}

		public void StatusMessage(ActivePartStatus Status, GameObject Subject, bool FromDialog)
		{
			switch (Status)
			{
				case ActivePartStatus.Operational: break;
				case ActivePartStatus.Unpowered:
					EmitMessage
					(
						Subject,
						ParentObject.The + ParentObject.ShortDisplayName +
						" hums for a moment before " + ParentObject.it +
						ParentObject.GetVerb("flicker", PronounAntecedent: true) +
						" off again, out of juice.",
						FromDialog: FromDialog
					);
					break;
				case ActivePartStatus.Rusted:
					EmitMessage
					(
						Subject,
						ParentObject.Poss("activation button is rusted in place."),
						FromDialog: FromDialog
					);
					break;
				case ActivePartStatus.Broken:
					EmitMessage
					(
						Subject,
						ParentObject.Poss("projector has shattered."),
						FromDialog: FromDialog
					);
					break;
				default:
					XDidYToZ(
						Subject,
						"press",
						ParentObject,
						"activation button, but nothing happens",
						PossessiveObject: true,
						FromDialog: FromDialog
					);
					break;
			}
		}

		public bool UseCharge(int ChargeUse = 20, bool Silent = false, bool FromDialog = false)
		{
			var subject = GetActivePartFirstSubject();
			if (subject == null) return false;
			
			var status = GetActivePartStatus(UseCharge: true, ChargeUse: RoundToInt(ChargeUse * ChargeMult));
			if (status == ActivePartStatus.Operational) return true;
			if (!Silent && subject.IsPlayer())
			{
				StatusMessage(status, subject, FromDialog);
			}
			
			return false;
		}
	}
}