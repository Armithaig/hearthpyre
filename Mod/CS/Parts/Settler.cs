using System;
using XRL.UI;
using XRL.Rules;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.Names;
using ConsoleLib.Console;
using XRL.Core;
using Hearthpyre;
using Hearthpyre.Realm;
using System.Collections.Generic;
using System.Linq;
using Hearthpyre.AI;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{

	[Serializable]
	public class HearthpyreSettler : IPart
	{

		public static readonly HashSet<string> AICommandBlacklist = new HashSet<string> { "CommandSpinWeb" };

		public SettlerHandler Handler { get; private set; }
		public Door Door { get; set; }
		public bool Disabled { get; set; }
		public bool Grounded { get; set; }

		Settlement settlement;
		public Guid SettlementID { get; private set; }

		public Settlement Settlement
		{
			get
			{
				if (settlement == null && SettlementID != Guid.Empty)
					settlement = RealmSystem.GetSettlement(SettlementID);

				return settlement;
			}
			set
			{
				settlement = value;
				SettlementID = value != null ? value.ID : Guid.Empty;
			}
		}

		public Sector CurrentSector => Settlement.GetSector(ParentObject.CurrentZone.ZoneID);

		Home home;
		public Guid HomeID { get; private set; }

		public Home Home
		{
			get
			{
				// Check if home has been removed.
				if (home != null && home.Sector == null)
				{
					if (home.ID == HomeID) HomeID = Guid.Empty;
					home = null;
				}

				if (home == null && HomeID != Guid.Empty)
					home = RealmSystem.GetHome(HomeID);

				return home;
			}
			set
			{
				home = value;
				HomeID = value != null ? value.ID : Guid.Empty;
			}
		}

		public override bool SameAs(IPart p) => false;

		public override IPart DeepCopy(GameObject Parent, Func<GameObject, GameObject> MapInv)
		{
			var part = (HearthpyreSettler)base.DeepCopy(Parent, MapInv);
			part.Settlement = Settlement;
			part.Home = Home;
			return part;
		}

		public override void Attach()
		{
			ParentObject.RemovePart<AIPilgrim>();
		}

		public override void SaveData(SerializationWriter Writer)
		{
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			if (SettlementID != Guid.Empty) fields.Add("Settlement", SettlementID);
			if (HomeID != Guid.Empty) fields.Add("Home", HomeID);
			if (Grounded) fields.Add("Grounded", Grounded);
			Writer.Write(fields);
		}

		public override void LoadData(SerializationReader Reader)
		{
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			if (fields.TryGetValue("Settlement", out var val)) SettlementID = (Guid)val;
			if (fields.TryGetValue("Home", out val)) HomeID = (Guid)val;
			if (fields.TryGetValue("Grounded", out val)) Grounded = (bool)val;
		}

		public override bool AllowStaticRegistration() => true;

		public override void Register(GameObject Object)
		{
			Disabled = CheckDisabled(Object);
			Object.RegisterPartEvent(this, "AICreateKill");
			Object.RegisterPartEvent(this, "AIGetMovementMutationList");
			Object.RegisterPartEvent(this, "Opened");
			base.Register(Object);
		}

		public override void Remove()
		{
			Handler = null;
			Door = null;
			ParentObject.pBrain.Goals.Clear();
#if BUILD_2_0_206
			ParentObject.pBrain.FactionMembership.Remove(FAC_STLR);
#else
			ParentObject.Brain.RemoveAllegiance<AllyVillager>();
#endif
			ParentObject.RemoveIntProperty(TAG_STLR);
			base.Remove();
		}

		public override bool FireEvent(Event E)
		{
			if (E.ID == "AICreateKill")
			{
				var target = E.GetGameObjectParameter("Target");
				if (target == null) return true;
				if (!target.HasIntProperty(TAG_STLR)) return true;

				ParentObject.StopFighting(target);
#if BUILD_2_0_206
				ParentObject.SetFeeling(target, 25);
#else
				ParentObject.AddOpinion<OpinionMollify>(target);
#endif
				DidXToY("decide", ParentObject.it + ParentObject.GetVerb("aren't", PronounAntecedent: true) + " angry at", target);
				return false;
			}
			else if (E.ID == "AIGetMovementMutationList" && E.GetParameter("List") is List<AICommandList> list)
			{
				for (int i = list.Count - 1; i >= 0; i--)
				{
					if (AICommandBlacklist.Contains(list[i].Command))
					{
						list.RemoveAt(i);
					}
				}
			}
			else if (E.ID == "Opened")
			{
				Door = E.GetGameObjectParameter("Object")?.TakePart<Door>();
			}

			return base.FireEvent(E);
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == LeftCellEvent.ID && Door != null
			       || ID == AIBoredEvent.ID
			       || ID == SuspendingEvent.ID
			       || ID == BeforeDestroyObjectEvent.ID
			       || ID == WasReplicatedEvent.ID
			       || ID == GetDebugInternalsEvent.ID
				;
		}

		public override bool HandleEvent(LeftCellEvent E)
		{
			// lets close a door if we opened it, we live in a society
			if (Door != null)
			{
				if (!Door.bOpen) Door = null;
				else if (!GameObject.validate(Door.ParentObject)) Door = null;
				else if (Door.ParentObject.DistanceTo(E.Cell) > 1) Door = null;
				else if (E.Cell == Door.ParentObject.CurrentCell)
				{
					Door.AttemptClose(ParentObject, FromMove: true);
					Door = null;
				}
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(AIBoredEvent E)
		{
			if (Disabled || !SettlerHandler.IsValidFor(ParentObject)) return base.HandleEvent(E);
			ParentObject.pBrain.PushGoal(Handler ?? (Handler = new SettlerHandler()));
			return false;
		}

		public override bool HandleEvent(SuspendingEvent E)
		{
			if (!Settlement.IsWithin(ParentObject) && SettlerHandler.IsValidFor(ParentObject))
			{
				Settlement.AddLiminal(ParentObject);
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(BeforeDestroyObjectEvent E)
		{
			Settlement.Retainers.Remove(E.Object.id);
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(WasReplicatedEvent E)
		{
			if (E.Replica != null && Stat.Roll("1d10", "HearthpyreSettlerReplicated") <= 3)
			{
				E.Replica.RemovePart<HearthpyreSettler>();
				E.Replica.SetIntProperty("CopyAllowFeelingAdjust", 1);
				E.Replica.pBrain.PartyLeader = null;
				E.Replica.pBrain.Calm = false;
				E.Replica.pBrain.Hostile = true;
				E.Replica.pBrain.WantToKill(ParentObject, "this town ain't big enough...");
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(GetDebugInternalsEvent E)
		{
			E.AddEntry(this, "State", Disabled ? "Disabled" : "Enabled");
			if (Settlement != null)
			{
				E.AddEntry(this, "Settlement", Settlement.Location.ToString());
				if (Settlement.Retainers.TryGetValue(ParentObject.id, out var ret))
				{
					E.AddEntry(this, "Retainer", ret);
				}

				if (CurrentSector != null) E.AddEntry(this, "Sector", CurrentSector.ZoneID);
			}

			if (Home != null) E.AddEntry(this, "Home", Home.Origin.ToString());
			if (Door != null && GameObject.validate(Door.ParentObject)) E.AddEntry(this, "Door", "Closing " + Door.ParentObject.pRender.DisplayName);
			return base.HandleEvent(E);
		}

		public bool CheckDisabled(GameObject Object)
		{
			if (Object.pBrain == null) return false;

			Object.pBrain.checkMobility(out bool immobile, out bool waterbound, out bool wallwalker);
			return immobile || waterbound || wallwalker; // theres not much useful logic we can do for these movement types re: idle behaviors
		}

		public override void Initialize()
		{
			if (ParentObject.HasIntProperty(TAG_STLR)) return;

			if (!ParentObject.HasProperName)
			{
				ParentObject.pRender.DisplayName = NameMaker.MakeName(For: ParentObject);
				ParentObject.SetIntProperty("ProperNoun", 1);
				ParentObject.SetIntProperty("Renamed", 1);
			}

			var brain = ParentObject.pBrain;
#if BUILD_2_0_206
			var faction = brain.GetPrimaryFaction();
			if (brain.FactionMembership.TryGetValue(faction, out var membership) && membership <= 75)
			{
				brain.FactionMembership[faction] = 100;
			}
			brain.setFactionMembership(FAC_STLR, 75);
			if (brain.Hostile) brain.Hostile = false;
			else brain.Calm = true;
#else
			var allegiance = new AllegianceSet();
			allegiance.Copy(brain.FindAllegiance(0));
			allegiance.TryAdd(FAC_STLR, 75);
			allegiance.SourceID = The.Player.BaseID;
			allegiance.Hostile = false;
			allegiance.Calm = true;
			allegiance.Reason = new AllyVillager();
			brain.PushAllegiance(allegiance);
#endif

			brain.Goals.Clear();
			brain.PartyLeader = null;
			brain.StartingCell = null;
			brain.Wanders = true;
			brain.WandersRandomly = false;
			ParentObject.RemovePart<AmbientSoundGenerator>();
			ParentObject.RemovePart<AIPilgrim>();
			ParentObject.SetIntProperty(TAG_STLR, 1);
			ParentObject.FireEvent("VillageInit");
		}

	}

}