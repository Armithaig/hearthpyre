using System;
using System.Text;
using System.Collections.Generic;
using XRL.Core;
using XRL;
using XRL.Rules;
using XRL.World;

namespace Hearthpyre
{
	public static partial class Extensions
	{
		/// <summary>
		/// Truncate string if exceeding length with optional suffix at the cutoff (like an ellipsis).
		/// Does not throw index-based exceptions. Simple string wrangling causing range exceptions is a hyper-correct abomination, you heard me.
		/// </summary>
		/// <param name="str">String.</param>
		/// <param name="len">Length.</param>
		/// <param name="suf">Suffix.</param>
		public static string Truncate(this string str, int len, string suf = "") {
			if (str.Length <= len) return str;
			var sb = Strings.SB.Clear();

			len -= suf.Length;
			sb.Append(str.Substring(0, len));
			return sb.Append(suf).ToString();
		}
		/// <summary>
		/// Count the length of the string, excluding any special colouring characters.
		/// </summary>
		public static int Quantify(this string str) {
			int ico;
			return Quantify(str, out ico);
		}
		/// <summary>
		/// Count the length of the string, excluding any special colouring characters.
		/// Output the index of last NUL character.
		/// </summary>
		public static int Quantify(this string str, out int ico) {
			var result = 0;
			var parsing = false;
			char c = '\0';
			ico = 0;
			for (int i = 0; i < str.Length; i++) {
				if (parsing) {
					if (c == str[i])
						result++; // Double && or ^^ is literal & or ^.
					parsing = false;
					continue;
				}
				c = str[i];
				if (c == '&' || c == '^') {
					parsing = true;
				} else {
					if (c == '\0') ico = i;
					result++;
				}
			}
			return result;
		}
		/// <summary>
		/// Cause a side effect on each element in the enumeration.
		/// </summary>
		/*public static void For<T>(this IEnumerable<T> enumerable, Action<int, T> action) {
			int i = 0;
			foreach (T item in enumerable) action(i++, item);
		}
		/// <summary>
		/// Cause a side effect on each element in the enumeration.
		/// </summary>
		public static void For<T>(this IEnumerable<T> enumerable, Action<T> action) {
			foreach (T item in enumerable) action(item);
		}*/
		/// <summary>
		/// Insert spaces in Pascal or Camel case string.
		/// Optionally lower the case of affected capitals.
		/// </summary>
		public static string Spaceify(this string str, bool lower = false) {
			var sb = Strings.SB.Clear();
			char last = '\0';
			foreach (var c in str) {
				if (Char.IsUpper(c) && Char.IsLower(last))
					sb.Append(' ').Append(lower ? Char.ToLower(c) : c);
				else
					sb.Append(c);
				last = c;
			}
			return sb.ToString();
		}
		public static StringBuilder Remove(this StringBuilder SB, int Start) => SB.Remove(Start, SB.Length - Start - 1);
		public static StringBuilder RemoveAt(this StringBuilder SB, char IndexOf)
		{
			var i = SB.IndexOf(IndexOf);
			return i < 0 ? SB : SB.Remove(i);
		}
		public static StringBuilder Prepend(this StringBuilder SB, string Text) => SB.Insert(0, Text);
		public static StringBuilder Prepend(this StringBuilder SB, char Text) => SB.Insert(0, Text);
		public static StringBuilder Replace(this StringBuilder SB, int Start, int Length, string Text)
		{
			SB.Remove(Start, Length);
			return SB.Insert(Start, Text);
		}
		public static StringBuilder AppendTheName(this StringBuilder SB, GameObject Object, bool Capitalized = false)
		{
			return SB.Append(Capitalized ? Object.The : Object.the)
				.Append(' ')
				.Append(Object.ShortDisplayNameSingle);
		}
		public static StringBuilder CompoundTheName(this StringBuilder SB, GameObject Object, bool Capitalized = true)
		{
			return SB.Compound(Capitalized ? Object.The : Object.the)
				.Append(' ')
				.Append(Object.ShortDisplayNameSingle);
		}

		public static int IndexOf(this StringBuilder SB, string Needle, int Start)
		{
			int m, l = Needle.Length, c = SB.Length - l;
			for (int i = Start; i <= c; i++)
			{
				if (SB[i] != Needle[0]) continue;

				m = 1;
				while (m < l && SB[i + m] == Needle[m])
					m++;

				if (m == l) return i;
			}

			return -1;
		}
		
		public static T TakeValue<S, T>(this Dictionary<S, T> Map, S Key)
		{
			return Map.TryGetValue(Key, out var obj) ? obj : default;
		}

		/// <summary>
		/// Grow box separately per dimension.
		/// </summary>
		public static Box Grow(this Box box, int x, int y) {
			return new Box(box.x1 - x, box.y1 - y, box.x2 + x, box.y2 + y);
		}
		/// <summary>
		/// Translate box location.
		/// </summary>
		public static Box Translate(this Box box, int x, int y) {
			return new Box(box.x1 + x, box.y1 + y, box.x2 + x, box.y2 + y);
		}
		/// <summary>
		/// Set new location while preserving dimensions.
		/// </summary>
		public static Box Retranslate(this Box box, int x, int y) {
			return new Box(x, y, x + box.x2 - box.x1, y + box.y2 - box.y1);
		}

		public static void Bounds<T>(this T[,] Grid, out int X1, out int Y1, out int X2, out int Y2)
		{
			X1 = Grid.GetLowerBound(0);
			Y1 = Grid.GetLowerBound(1);
			X2 = Grid.GetUpperBound(0);
			Y2 = Grid.GetUpperBound(1);
		}

		public static void ClearBit(this int[,] Grid, int Mask)
		{
			Grid.Bounds(out int x1, out int y1, out int x2, out int y2);
			for (int x = x1; x <= x2; x++)
			for (int y = y1; y <= y2; y++)
			{
				Grid[x, y] &= ~Mask;
			}
		}

		public static void ReplaceBit(this int[,] Grid, int Mask, int NewMask)
		{
			Grid.Bounds(out int x1, out int y1, out int x2, out int y2);
			for (int x = x1; x <= x2; x++)
			for (int y = y1; y <= y2; y++)
			{
				if (!Grid[x, y].HasBit(Mask)) continue;
				
				Grid[x, y] &= ~Mask;
				Grid[x, y] |= NewMask;
			}
		}

		/// <summary>
		/// Returns the zero-based index of the minimum value in the array.
		/// </summary>
		public static int IndexOfMin<T>(this IList<T> List) where T : IComparable<T>
		{
			if (List.Count == 0) return -1;
			
			var index = 0;
			var min = List[0];

			for (int i = 1; i < List.Count; i++)
			{
				if (List[i].CompareTo(min) < 0)
				{
					min = List[i];
					index = i;
				}
			}

			return index;
		}
		
		/// <summary>
		/// Returns the zero-based index of the maximum value in the array.
		/// </summary>
		public static int IndexOfMax<T>(this IList<T> List) where T : IComparable<T>
		{
			if (List.Count == 0) return -1;
			
			var index = 0;
			var max = List[0];

			for (int i = 1; i < List.Count; i++)
			{
				if (List[i].CompareTo(max) > 0)
				{
					max = List[i];
					index = i;
				}
			}

			return index;
		}

		public static string PrintBits(this int val)
		{
			var sb = Event.NewStringBuilder();
			for (int i = 0; i < sizeof(int) * 8; i++)
			{
				if (!val.HasBit(1 << i)) continue;
				sb.Compound((i + 1).ToString());
			}

			Event.nStringBuilderPoolCounter--;
			return sb.ToString();
		}
	}
}
