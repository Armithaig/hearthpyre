using System;
using System.Linq;
using System.Collections.Generic;
using XRL.World;
using Genkit;
using XRL;
using XRL.World.AI.Pathfinding;
using XRL.World.Parts;
using static Hearthpyre.Static;

namespace Hearthpyre
{
	public static partial class Extensions
	{
		/// <summary>
		/// Construct an object, "filling" any hole-like objects under it.
		/// </summary>
		public static GameObject Construct(this Cell cell, string blueprint) {
			if (!cell.IsClear()) return null;

			var obj = GameObjectFactory.Factory.CreateObject(blueprint, Brand);
			if (obj.Blueprint != blueprint) {
				obj.Destroy();
				return null;
			}

			return Construct(cell, obj);
		}
		/// <summary>
		/// Construct an object, "filling" any hole-like objects under it.
		/// </summary>
		public static GameObject Construct(this Cell cell, GameObject obj) {
			RemoveChasm(cell);
			cell.AddObject(obj).StripContents(Silent: true);
			return obj;
		}
		/// <summary>
		/// Demolish an object, replacing it with air if the cell was empty and unpainted.
		/// </summary>
		public static bool Demolish(this Cell Cell, GameObject Object, Action<GameObject, Cell> Record = null) {
			if (Object == null) return false;

			Cell.RemoveObject(Object);
			AddChasm(Cell);
            if (Record != null)
            {
                Record(Object, Cell);
                return true;
            }

			return Object.Destroy();
		}
		/// <summary>
		/// Demolish objects that match the predicate, replacing it with air if the cell was empty and unpainted.
		/// </summary>
		public static bool Demolish(this Cell Cell, Func<GameObject, bool> Predicate, Action<GameObject, Cell> Record = null) {
			var objects = Cell.Objects;
			var result = false;

			for (int i = objects.Count - 1; i >= 0; i--) {
				var obj = objects[i];
				if (Predicate(obj)) {
					result |= Cell.Demolish(obj, Record);
				}
			}

			return result;
		}
		/// <summary>
		/// Get cell directly above provided.
		/// </summary>
		public static Cell Above(this Cell cell)
		{
			var id = cell.ParentZone.GetZoneIDFromDirection("U");
			if (!The.ZoneManager.IsZoneBuilt(id)) return null;
			
			return The.ZoneManager.GetZone(id).GetCell(cell.X, cell.Y);
		}
		/// <summary>
		/// Get cell directly below provided.
		/// </summary>
		public static Cell Below(this Cell cell) {
			var id = cell.ParentZone.GetZoneIDFromDirection("D");
			if (!The.ZoneManager.IsZoneBuilt(id)) return null;
			
			return The.ZoneManager.GetZone(id).GetCell(cell.X, cell.Y);
		}
		/// <summary>
		/// Check if cell is open for placement. Ignores dead air.
		/// </summary>
		public static bool IsClear(this Cell cell)
		{
			foreach (var obj in cell.Objects)
			{
				if (obj.pRender == null) continue;
				if (obj.pPhysics.Solid) return false;
				if (obj.IsCombatObject()) return false;
				if (obj.HasTag("Door")) return false;
				if (!obj.HasTag("Stairs")) continue;
				if (obj.OwnPart<ChasmMaterial>()) continue;
				return false;
			}

			return true;
		}
		/// <summary>
		/// Spirals out from cell, yielding each one.
		/// </summary>
		public static IEnumerable<Cell> LazySpiral(this Cell cell, int radius) {
			var Z = cell.ParentZone;
			int x = cell.X, y = cell.Y;
			int x2 = 1, y2 = 0, l = 1;

			for (int i = 0, p = 1, c = radius * radius - 1; i < c; i++, p++) {
				x += x2;
				y += y2;

				if (p >= l) {
					p = 0;

					int b = x2;
					x2 = -y2;
					y2 = b;

					if (y2 == 0) l++;
				}

				var result = Z.GetCell(x, y);
				if (result != null)
					yield return result;
			}
		}
		/// <summary>
		/// Yield object parts in cardinal adjacent cells.
		/// </summary>
		public static IEnumerable<T> YieldCardinalParts<T>(this Cell C) where T : IPart 
		{
			var Z = C.ParentZone;
			int x = C.X, y = C.Y;
			int x2 = 1, y2 = 0;

			for (int i = 0; i < 4; i++) {
				int b = x2;
				x2 = -y2;
				y2 = b;

				var TC = Z.GetCell(x + x2, y + y2);
				if (TC == null) continue;

				for (int k = 0; k < TC.Objects.Count; k++) {
					if (TC.Objects[k].TryTakePart(out T part))
					{
						yield return part;
					}
				}
			}
		}
		/// <summary>
		/// Yield cells in a circular pattern from origin.
		/// </summary>
		public static IEnumerable<Cell> LazyCircle(this Cell cell, int r) {
			var Z = cell.ParentZone;
			int w = Z.Width, h = Z.Height;
			var flood = Static.LazyCircle(cell.X, cell.Y, r);
			foreach (var pair in flood) {
				int x = pair[0], y = pair[1];
				if (x >= 0 && y >= 0 && x < w && y < h)
					yield return Z.GetCell(x, y);
			}
		}
		/// <summary>
		/// Yield cells in a circular pattern from origin.
		/// </summary>
		public static IEnumerable<Cell> YieldCircle(this Cell C, int R)
		{
			if (R == 0)
			{
				yield return C;
				yield break;
			}
			
			var Z = C.ParentZone;
			var rsq = R * R;
			for (int y = -R, cx = C.X, cy = C.Y; y <= R; y++)
			{
				var ysq = y * y;
				for (int x = -R; x <= R; x++)
				{
					if (x * x + ysq <= rsq)
					{
						C = Z.GetCell(cx + x, cy + y);
						if (C != null) yield return C;
					}
				}
			}
		}
		/// <summary>
		/// Check if the enumeration includes any edge of the zone.
		/// </summary>
		public static bool IsOutside(this IEnumerable<Cell> cells) {
			foreach (var C in cells) {
				if (C.X == 0 || C.X >= C.ParentZone.Width - 1)
					return true;
				if (C.Y == 0 || C.Y >= C.ParentZone.Height - 1)
					return true;
			}
			return false;
		}
		
		public static List<T> GetObjectParts<T>(this Zone Z) where T : IPart
		{
			var result = new List<T>();
			for (int x = 0; x < Z.Width; ++x)
			for (int y = 0; y < Z.Height; ++y)
			for (int i = 0, c = Z.Map[x][y].Objects.Count; i < c; i++)
			{
				if (Z.Map[x][y].Objects[i].TryTakePart(out T part))
				{
					result.Add(part);
				}
			}
			
			return result;
		}

		public static bool TryTakeFirstPart<T>(this Cell C, out T Part) where T : IPart
		{
			for (int i = 0, c = C.Objects.Count; i < c; i++)
			{
				if (C.Objects[i].TryTakePart(out Part))
				{
					return true;
				}
			}

			Part = null;
			return false;
		}

		public static bool TryTakeFirstObjectWith<T>(this Cell C, out GameObject Object) where T : IPart
		{
			for (int i = 0, c = C.Objects.Count; i < c; i++)
			{
				if (C.Objects[i].OwnPart<T>())
				{
					Object = C.Objects[i];
					return true;
				}
			}

			Object = null;
			return false;
		}

		public static GetNavigationWeightEvent eNavWeight = new GetNavigationWeightEvent();
		public static int GetImmobileWeight(this Cell C)
		{
			eNavWeight.Reset();
			eNavWeight.Smart = true;
			eNavWeight.Cell = C;
			for (int i = 0, c = C.Objects.Count, j, p; i < c && eNavWeight.Weight < 100; i++)
			{
				if (C.Objects[i].IsPotentiallyMobile()) continue;

				eNavWeight.Object = C.Objects[i];
				eNavWeight.MinWeight(eNavWeight.Object.GetIntProperty("NavigationWeight"));
				eNavWeight.PriorWeight = eNavWeight.Weight;

				for (j = 0, p = eNavWeight.Object.PartsList.Count; j < p; j++)
				{
					if (!eNavWeight.Object.PartsList[j].WantEvent(GetNavigationWeightEvent.ID, GetNavigationWeightEvent.CascadeLevel)) continue;
					eNavWeight.Object.PartsList[j].HandleEvent(eNavWeight);
					if (eNavWeight.Uncacheable)
					{
						eNavWeight.Weight = eNavWeight.PriorWeight;
						eNavWeight.Uncacheable = false;
					}
					else
					{
						/*if (eNavWeight.Weight != eNavWeight.PriorWeight)
						{
							MetricsManager.LogEditorError(eNavWeight.Object.DisplayNameOnlyDirect + "." + eNavWeight.Object.PartsList[j].Name + ": weight +" + (eNavWeight.Weight - eNavWeight.PriorWeight));
						}*/
						eNavWeight.PriorWeight = eNavWeight.Weight;
					}
				}
			}

			return eNavWeight.Weight;
		}

		public static int CourseFrom(this Location2D To, Location2D From)
		{
			int result = 0;

			if (To.x < From.x) result |= WEST;
			else if (To.x > From.x) result |= EAST;
			
			if (To.y < From.y) result |= NORTH;
			else if (To.y > From.y) result |= SOUTH;

			return result;
		}
		
		public static string DirectionFrom(this Location2D From, int DX, int DY)
		{
			if (From.x == DX)
			{
				if (From.y == DY) return ".";
				return From.y < DY ? "S" : "N";
			}
			if (From.x < DX)
			{
				if (From.y == DY) return "E";
				return From.y < DY ? "SE" : "NE";
			}
			if (From.y == DY) return "W";
			return From.y < DY ? "SW" : "NW";
		}

		public static int DistanceTo(this Location2D From, Location2D To)
		{
			return Math.Max(Math.Abs(From.x - To.x), Math.Abs(From.y - To.y));
		}
		
		public static int DistanceTo(this Location2D From, int DX, int DY)
		{
			return Math.Max(Math.Abs(From.x - DX), Math.Abs(From.y - DY));
		}
		
		public static int PixelDistanceTo(this Location2D From, Location2D To)
		{
			return Math.Abs(To.x - From.x) * 16 + Math.Abs(To.y - From.y) * 24; 
		}

		public static List<Location2D> JoiningCells(this Location2D To, int DX, int DY, List<Location2D> Out)
		{
			Out.Clear();
			for (int x = DX - 1; x < DX + 1; x++)
			for (int y = DY - 1; y < DY + 1; y++)
			{
				if (To.DistanceTo(x, y) == 1)
				{
					Out.Add(Location2D.get(x, y));
				}
			}

			return Out;
		}

		public static int PathDistanceTo(this GlobalLocation From, Cell To)
		{
			var Z = To.ParentZone;
			var DX = Z.wX * 3 * 80 + Z.X * 80 + To.X - From.ParasangX * 3 * 80 - From.ZoneX * 80 - From.CellX;
			var DY = Z.wY * 3 * 25 + Z.Y * 25 + To.Y - From.ParasangY * 3 * 25 - From.ZoneY * 25 - From.CellY;
			return Math.Max(Math.Abs(DX), Math.Abs(DY)) + Math.Abs(Z.Z - From.ZoneZ);
		}

		public static int PathDistanceTo(this GlobalLocation From, GlobalLocation To)
		{
			var DX = To.ParasangX * 3 * 80 + To.ZoneX * 80 + To.CellX - From.ParasangX * 3 * 80 - From.ZoneX * 80 - From.CellX;
			var DY = To.ParasangY * 3 * 25 + To.ZoneY * 25 + To.CellY - From.ParasangY * 3 * 25 - From.ZoneY * 25 - From.CellY;
			return Math.Max(Math.Abs(DX), Math.Abs(DY)) + Math.Abs(To.ZoneZ - From.ZoneZ);
		}
		
		public static void SetCell(this GlobalLocation This, GlobalLocation Cell)
		{
			This.SetCell(
				Cell.World,
				Cell.ParasangX,
				Cell.ParasangY,
				Cell.ZoneX,
				Cell.ZoneY,
				Cell.ZoneZ,
				Cell.CellX,
				Cell.CellY
			);
		}

		public static string GetBareDisplayName(this Zone Z) => GetZoneBareDisplayName(Z.ZoneID);

		/// <summary>
		/// Check if cell has a foreign combat object.
		/// </summary>
		/*public static Cell GetRandomElement<T>(this HashSet<Cell> list) {
			int count = list.Count;
			if (count == 0) return null;
			
			if (count != 1) {
				Stat.Rand
				if (R == null) {
					R = Stat.Rand;
				}
				return list[Stat.Rand.Next(0, list.Count)];
			}
			return list[0];
		}*/
	}
}
