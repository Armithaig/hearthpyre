using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XRL.World.Conversations;
using AiUnity.Common.Extensions;
using XRL;
using XRL.UI;
using XRL.Core;
using XRL.World;
using XRL.World.ZoneBuilders;
using XRL.World.Parts;
using HistoryKit;
using ConsoleLib.Console;
using XRL.World.AI;
using XRL.World.Anatomy;
using static Hearthpyre.Static;
using Conversation = XRL.World.Conversations.Dialogue;

namespace Hearthpyre
{
	public static partial class Extensions
	{
		/// <summary>
		/// Pick up an object and remove dropped property.
		/// </summary>
		public static bool Take(this GameObject Actor, GameObject Item, bool Silent = false, int? EnergyCost = 0) {
			if (!Actor.TakeObject(Item, Silent: Silent, EnergyCost: EnergyCost)) return false;
			
			var dropped = Actor.GetStringProperty("Dropped");
			if (string.IsNullOrEmpty(dropped)) return true;

			if (Item.id == dropped)
			{
				Actor.RemoveStringProperty("Dropped");
			}
			else
			{
				var i = dropped.IndexOf(Item.id, StringComparison.Ordinal);
				if (i == -1) return true;
					
				Actor.SetStringProperty("Dropped", dropped.Remove(i == 0 ? 0 : i - 1, Item.id.Length + 1));
			}

			return true;
		}
		/// <summary>
		/// Drop an object and set dropped property for picking it up again later.
		/// </summary>
		public static bool Drop(this GameObject Actor, GameObject Item, bool Silent = false, bool Forced = false) {
			var e = Event.New("CommandDropObject", "Object", Item);
			if (Silent) e.SetSilent(true);
			if (Forced) e.SetParameter("Forced", 1);

			if (!Actor.FireEvent(e)) return false;
			
			var dropped = Actor.GetStringProperty("Dropped");
			if (string.IsNullOrEmpty(dropped)) dropped = Item.id;
			else if (dropped.Contains(Item.id)) return true;
			else dropped += ";" + Item.id;

			Actor.SetStringProperty("Dropped", dropped);
			return true;

		}
		/// <summary>
		/// Loop through target's Inventory items and Body equipment, yielding items matching predicate.
		/// </summary>
		public static IEnumerable<GameObject> YieldItems(this GameObject Object)
		{
			if (Object.TryTakePart(out Inventory inventory))
			{
				for (int i = 0; i < inventory.Objects.Count; i++)
				{
					yield return inventory.Objects[i];
				}
			}

			if (Object.TryTakePart(out Body body))
			{
				foreach (var part in body.LoopParts())
				{
					if (part.Equipped != null) yield return part.Equipped;
				}
			}
		}
		/// <summary>
		/// Recursively loop through all equipped items on object.
		/// </summary>
		public static IEnumerable<GameObject> LazyForeachEquip(this GameObject obj, Func<GameObject, bool> predicate) {
			return LazyForeachEquip(obj.TakePart<Body>().GetBody(), predicate);
		}
		/// <summary>
		/// Recursively loop through all equipped items on and under provided limb.
		/// </summary>
		public static IEnumerable<GameObject> LazyForeachEquip(this BodyPart limb, Func<GameObject, bool> predicate) {
			if (limb == null) yield break;
			if (limb.Equipped != null && limb.IsFirstSlotForEquipped()) {
				if (predicate(limb.Equipped)) yield return limb.Equipped;
			}

			if (limb.Parts == null) yield break;
			foreach (var part in limb.Parts) {
				foreach (var equipped in LazyForeachEquip(part, predicate))
					yield return equipped;
			}
		}
		/// <summary>
		/// Display message if object is player.
		/// </summary>
		public static bool PlayerMessage(this GameObject Object, string Text) {
			if (!Object.IsPlayer()) return false;
			The.Game.Player.Messages.Add(Text);
			return true;
		}
		/// <summary>
		/// Display popup if object is player.
		/// </summary>
		public static bool Popup(this GameObject Object, string Text) {
			if (!Object.IsPlayer()) return false;
			XRL.UI.Popup.Show(Text);
			return true;
		}
		/// <summary>
		/// Display popup if object is player.
		/// </summary>
		public static bool Success(this GameObject Object, string Text) {
			if (!Object.IsPlayer()) return true;
			XRL.UI.Popup.Show(Text);
			return true;
		}
		/// <summary>
		/// Display popup if object is player.
		/// </summary>
		public static bool Failure(this GameObject Object, string Text) {
			if (!Object.IsPlayer()) return false;
			XRL.UI.Popup.Show(Text);
			return false;
		}
		/// <summary>
		/// Re-equip all items with part to reflect potential stat changes.
		/// </summary>
		public static void UpdateEquipped(this GameObject obj, string part) {
			GameManager.Instance.gameQueue.queueTask(() => {
				obj.TakePart<Body>()?.ForeachPart(x => {
					var eqp = x.Equipped;
					if (eqp == null || !eqp.HasPart(part))
						return;

					UnequippedEvent.Send(eqp, obj);
					EquippedEvent.Send(obj, eqp, null);
				});
			});
		}
		/// <summary>
		/// Set title from hero templates/specializations.
		/// </summary>
		public static void SetTitle(this GameObject Object, string Base, string Special)
		{
			//var color = HeroMaker.ResolveTemplateTag(Object, "HeroNameColor", "&M", new string[0], new[] { Special });
			var sb = ColorUtility.StripFormatting(Event.NewStringBuilder(Base));
			sb.RemoveAt(',');

			if (Special.EndsWith("Merchant")) Object.RequireRole(sb, "village merchant");
			else if (Special.EndsWith("Tinker")) Object.RequireRole(sb, "village tinker");
			else if (Special.EndsWith("Warden")) Object.RequireAdjective(sb, "Warden");
			else if (Special.EndsWith("Minstrel")) Object.RequireRole(sb, "village minstrel");
			else if (Special.EndsWith("Apothecary")) Object.RequireRole(sb, "village apothecary");

			var fg = ColorUtility.FindLastForeground(Base);
			if (fg != null) sb.Prepend('|').Prepend(fg.Value).Prepend("{{").Append("}}");
			Object.DisplayName = sb.ToString().Trim();
		}

		public static void RequireRole(this GameObject Object, StringBuilder SB, string Role)
		{
			SB.Replace(Role, "").Replace("  ", "");;
			Object.IncludePart<SocialRoles>().RequireRole(Role);
		}
		public static void RequireAdjective(this GameObject Object, StringBuilder SB, string Adjective)
		{
			SB.Replace(Adjective, "").Replace("  ", "");
			Object.IncludePart<DisplayNameAdjectives>().RequireAdjective(Adjective);
		}

		public static bool IsHero(this GameObject Object) => Object.GetIntProperty("Hero") >= 1 || Object.OwnPart<GivesRep>();
		/// <summary>
		/// Get ability by command, adds a null-check for the dictionary.
		/// </summary>
		public static ActivatedAbilityEntry GetAbility(this ActivatedAbilities abilities, string command) {
			if (abilities.AbilityByGuid == null) return null;

			foreach (var pair in abilities.AbilityByGuid) {
				if (pair.Value.Command != command) continue;
				return pair.Value;
			}

			return null;
		}

		public static GameObject GetItemWithBlueprint(this GameObject Object, string Blueprint)
		{
			if (Object.TryTakePart(out Inventory inventory))
			{
				for (int i = 0; i < inventory.Objects.Count; i++)
				{
					if (inventory.Objects[i].Blueprint == Blueprint)
					{
						return inventory.Objects[i];
					}
				}
			}

			if (Object.TryTakePart(out Body body))
			{
				return body.GetBody().GetItemWithBlueprint(Blueprint);
			}

			return null;
		}

		public static GameObject GetItemWithBlueprint(this BodyPart BodyPart, string Blueprint)
		{
			var item = BodyPart.Equipped;
			if (item?.Blueprint == Blueprint) return item;
			if (BodyPart.Parts == null) return null;
			
			for (int i = 0; i < BodyPart.Parts.Count; i++)
			{
				item = BodyPart.Parts[i].GetItemWithBlueprint(Blueprint);
				if (item != null) return item;
			}

			return null;
		}

		public static T TakeItemPart<T>(this GameObject Object) where T : IPart
		{
			if (Object.TryTakePart(out Inventory inventory))
			{
				for (int i = 0; i < inventory.Objects.Count; i++)
				{
					if (inventory.Objects[i].TryTakePart(out T part))
					{
						return part;
					}
				}
			}

			if (Object.TryTakePart(out Body body))
			{
				return body.GetBody().TakeItemPart<T>();
			}

			return null;
		}

		public static T TakeItemPart<T>(this BodyPart BodyPart) where T : IPart
		{
			if (BodyPart.Equipped != null && BodyPart.Equipped.TryTakePart(out T part))
			{
				return part;
			}
			
			if (BodyPart.Parts != null)
			{
				for (int i = 0; i < BodyPart.Parts.Count; i++)
				{
					part = BodyPart.Parts[i].TakeItemPart<T>();
					if (part != null) return part;
				}
			}

			return null;
		}
		
		// Funny name variants of GetPart, HasPart and RemovePart until namespace friendly update.
		public static T TakePart<T>(this GameObject Object) where T : IPart
		{
			for (int i = 0, j = Object.PartsList.Count; i < j; i++)
			{
				if (ReferenceEquals(Object.PartsList[i].GetType(), typeof(T)))
					return Object.PartsList[i] as T;
			}

			return null;
		}
		
		public static bool OwnPart<T>(this GameObject Object) where T : IPart
		{
			for (int i = 0, j = Object.PartsList.Count; i < j; i++)
			{
				if (ReferenceEquals(Object.PartsList[i].GetType(), typeof(T)))
					return true;
			}

			return false;
		}
		
		
		public static bool TryTakePart<T>(this GameObject Object, out T Part) where T : IPart
		{
			for (int i = 0, j = Object.PartsList.Count; i < j; i++)
			{
				if (ReferenceEquals(Object.PartsList[i].GetType(), typeof(T)))
				{
					Part = Object.PartsList[i] as T;
					return true;
				}
			}

			Part = null;
			return false;
		}
		
		public static T IncludePart<T>(this GameObject Object) where T : IPart, new()
		{
			return Object.TryTakePart(out T part) ? part : Object.AddPart(new T());
		}
		
		public static T IncludeEffect<T>(this GameObject Object) where T : Effect, new()
		{
			if (Object.TryTakeEffect(out T fx)) return fx;
			fx = new T();
			Object.ForceApplyEffect(fx);
			return fx;
		}
		
		public static bool IsType<T>(this object Object)
		{
			return ReferenceEquals(Object.GetType(), typeof(T));
		}
		
		public static bool OwnEffect<T>(this GameObject Object) where T : Effect
		{
			if (Object._Effects == null) return false;
			
			for (int i = 0, j = Object._Effects.Count; i < j; i++)
			{
				if (ReferenceEquals(Object._Effects[i].GetType(), typeof(T)))
					return true;
			}

			return false;
		}
		
		public static T TakeEffect<T>(this GameObject Object) where T : Effect
		{
			if (!Object._Effects.IsNullOrEmpty())
			{
				for (int i = 0, j = Object._Effects.Count; i < j; i++)
				{
					if (ReferenceEquals(Object._Effects[i].GetType(), typeof(T)))
					{
						return (T) Object._Effects[i];
					}
				}
			}

			return null;
		}
		
		public static bool TryTakeEffect<T>(this GameObject Object, out T Effect) where T : Effect
		{
			if (!Object._Effects.IsNullOrEmpty())
			{
				for (int i = 0, j = Object._Effects.Count; i < j; i++)
				{
					if (ReferenceEquals(Object._Effects[i].GetType(), typeof(T)))
					{
						Effect = Object._Effects[i] as T;
						return true;
					}
				}
			}

			Effect = null;
			return false;
		}
		
		public static bool DiscardEffect<T>(this GameObject Object)
		{
			if (Object._Effects.IsNullOrEmpty()) return false;

			var removed = false;
			for (int i = Object._Effects.Count - 1; i >= 0; i--)
			{
				if (!ReferenceEquals(Object._Effects[i].GetType(), typeof(T))) continue;
				
				var fx = Object._Effects[i];
				fx.Remove(Object);
				fx.Unregister(Object);
				fx.Object = null;
				Object._Effects.RemoveAt(i);
				EffectRemovedEvent.Send(Object, fx.ClassName, fx);
				removed = true;
			}
			
			if (removed) Object.CheckStack();
			return removed;
		}

		public static bool Validate(this GameObjectSource Source, out GameObject Value)
		{
			return (Value = Source?.Value) != null;
		}

		public static string GetBaseDisplayNameSingleStripped(this GameObject Object)
		{
			return Object.GetDisplayName(
				BaseOnly: true,
				NoConfusion: true,
				NoColor: true,
				Single: true,
				Stripped: true
			);
		}

        public static bool TryGetSounds(this ConversationScript Script, out bool Speaks, out List<string> Sounds)
        {
            if (Script == null || !Conversation.Blueprints.TryGetValue(Script.ConversationID, out var conversation))
            {
                Speaks = false;
                Sounds = null;
                return false;
            }
            
            Speaks = false;
            Sounds = new List<string>();
            foreach (var start in conversation.Children)
            {
                if (start.Name != "Start" && (start.ID != "Start" || start.Name != "Node")) continue;
                if (start.Children.IsNullOrEmpty()) continue;
                foreach (var texts in start.Children)
                {
                    if (texts.Text.IsNullOrEmpty()) continue;
                    if (texts.Name != "Text") continue;
                    
                    var parts = texts.Text.Split('~');
                    foreach (var part in parts)
                    {
                        if (part.IndexOf('*') == -1)
                        {
                            Speaks = true;
                            Sounds = null;
                            return true;
                        }

                        Sounds.Add(ColorUtility.StripFormatting(part));
                    }
                }
            }

            return true;
        }

	}
}

// backwards compat
namespace XRL.World.Anatomy
{
	
}
