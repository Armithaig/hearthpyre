﻿using System;
using System.Collections.Generic;
using Genkit;

using static Hearthpyre.Static;
using static Hearthpyre.Realm.Lattice;

namespace Hearthpyre.AI
{
	public class GridNode: IComparable<GridNode>, IEquatable<GridNode>
	{
		public class Pool
		{
			private List<GridNode> Items = new List<GridNode>();
			private int Counter = 0;

			public void Reset() => Counter = 0;

			public GridNode Take(int X, int Y, int Flags, Location2D Destination, Strider Strider, GridNode Parent = null)
			{
				if (Counter >= Items.Count)
				{
					Items.Add(new GridNode());
				}
			
				var node = Items[Counter++];
				node.X = X;
				node.Y = Y;
				node.H = node.PixelDistanceTo(Destination);
				node.D = node.ManhattanDistanceTo(Destination);
				node.Closed = false;
				node.SetParent(Parent, Strider, Flags);
				return node;
			}
		}

		public GridNode Parent;
		public int X;
		public int Y;
		
		public int C;
		public int G;
		public int H;
		public int D;
		public int L;
		
		public bool Closed;

		public int F => G + H;
		public Location2D Location => Location2D.get(X, Y);

		private GridNode()
		{
		}

		public void SetParent(GridNode Node, Strider Strider, int Flags)
		{
			Parent = Node;
			if (Node != null)
			{
				C = GetCost(Strider, Flags);
				G = Node.G + C;
				L = Node.L + 1;
			}
			else
			{
				C = 0;
				G = 0;
				L = 1;
			}
		}

		public int ManhattanDistanceTo(GridNode L) => Math.Abs(L.X - X) + Math.Abs(L.Y - Y);
		
		public int ManhattanDistanceTo(Location2D L) => Math.Abs(L.x - X) + Math.Abs(L.y - Y);

		public int PixelDistanceTo(Location2D L)
		{
			// Weighted for X axis.
			return Math.Abs(L.x - X) * 24 + Math.Abs(L.y - Y) * 16; 
		}

		public int GetCost(Strider Strider, int Flags)
		{
			var result = 16;
			if (ManhattanDistanceTo(Parent) > 1) result += 8;
			if (H == 0 || Strider.Flying) return result;

			if (!Strider.Aquatic && Flags.HasBit(GRID_LIQUID))
			{
				result += 64;
			}
			else if (Strider.RoadPreference > 0)
			{
				if (!Flags.HasBit(Strider.RoadMask))
				{
					result += Strider.RoadPreference;
				}
				else if (Flags.HasBit(Strider.LandingMask))
				{
					Strider.LandingMask = 0;
					Strider.RoadPreference <<= 4;
				}
			}
			else if (Flags.HasBit(GRID_FLOOR))
			{
				result /= 8;
			}

			result += ((Flags & GRID_NAV) >> 23) * 16;
			return result;
		}

		public int Direction()
		{
			if (Parent == null) return 0;
	        
			int result = 0;

			if (Parent.X > X) result |= WEST;
			else if (Parent.X < X) result |= EAST;
			
			if (Parent.Y > Y) result |= NORTH;
			else if (Parent.Y < Y) result |= SOUTH;

			return result;
		}

		public bool ChangeDir()
		{
			return Direction().HasAllBits(Parent.Direction());
		}

		public int CompareTo(GridNode Other) => F.CompareTo(Other.F);

		public bool Equals(GridNode other)
		{
			if (other == null) return false;
			return X == other.X && Y == other.Y;
		}

		public override int GetHashCode()
		{
			return (Y << sizeof(int) * 4 - 1) ^ X;
		}
	}
}