﻿using System;
using System.Collections.Generic;

namespace Hearthpyre.AI
{
	public class OrderedQueue<T> where T : IComparable<T>
	{
		public List<T> Items = new List<T>();
		public Dictionary<T, int> Map = new Dictionary<T, int>();

		public int Count => Items.Count;

		public void Clear()
		{
			Items.Clear();
			Map.Clear();
		}

		public void Enqueue(T Item)
		{
			var l = Items.Count;
			Items.Add(Item);
			Map[Item] = l;
			while (l != 0)
			{
				var m = (l - 1) / 2;
				if (Items[l].CompareTo(Items[m]) < 0)
				{
					var b = Items[l];
					Items[l] = Items[m];
					Items[m] = b;
					Map[Items[l]] = l;
					Map[Items[m]] = m;
					l = m;
					continue;
				}
				break;
			}
		}

		public void Requeue(T Item)
		{
			var l = Map[Item];
			while (l > 0)
			{
				var m = (l - 1) / 2;
				if (Items[l].CompareTo(Items[m]) < 0)
				{
					var b = Items[l];
					Items[l] = Items[m];
					Items[m] = b;
					Map[Items[l]] = l;
					Map[Items[m]] = m;
					l = m;
					continue;
				}
				break;
			}
		}

		public T Dequeue()
		{
			var item = Items[0];
			var c = Count - 1;
			Items[0] = Items[c];
			Map[Items[0]] = 0;
			Items.RemoveAt(c);
			Map.Remove(item);
			
			var i = 0;
			while (true)
			{
				var min = i;
				var l = 2 * i + 1;
				var r = l + 1;
				if (l < c && Items[i].CompareTo(Items[l]) > 0)
				{
					i = l;
				}
				if (r < c && Items[i].CompareTo(Items[r]) > 0)
				{
					i = r;
				}
				if (i == min)
				{
					break;
				}
				
				var b = Items[i];
				Items[i] = Items[min];
				Items[min] = b;
				Map[Items[i]] = i;
				Map[Items[min]] = min;
			}
			
			return item;
		}
	}
}