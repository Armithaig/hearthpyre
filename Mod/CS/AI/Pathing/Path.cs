﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Genkit;
using Hearthpyre.Realm;
using XRL.World;

using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public class Path
	{
		public class Step
		{
			public int X;
			public int Y;
			public int Z;
			
			public int CX => X % 80;
			public int CY => Y % 25;

			public int LocalDistanceTo(Cell C)
			{
				var CX = C.ParentZone.wX * 3 * 80 + C.ParentZone.X * 80 + C.X;
				var CY = C.ParentZone.wY * 3 * 25 + C.ParentZone.Y * 25 + C.Y;
				return Math.Max(Math.Abs(X - CX), Math.Abs(Y - CY));
			}
			
			public int DistanceTo(Cell C)
			{
				var CX = C.ParentZone.wX * 3 * 80 + C.ParentZone.X * 80 + C.X;
				var CY = C.ParentZone.wY * 3 * 25 + C.ParentZone.Y * 25 + C.Y;
				return Math.Max(Math.Abs(X - CX), Math.Abs(Y - CY)) + Math.Abs(Z - C.ParentZone.Z);
			}
			
			
			public string DirectionFrom(Cell C)
			{
				var CX = C.ParentZone.wX * 3 * 80 + C.ParentZone.X * 80 + C.X;
				var CY = C.ParentZone.wY * 3 * 25 + C.ParentZone.Y * 25 + C.Y;
				var CZ = C.ParentZone.Z;
				
				if (X == CX)
				{
					if (Y > CY) return "S";
					if (Y < CY) return "N";
					if (Z > CZ) return "D";
					if (Z < CZ) return "U";
					return ".";
				}
				
				if (Y == CY)
				{
					if (X > CX) return "E";
					if (X < CX) return "W";
					if (Z > CZ) return "D";
					return "U";
				}
				
				if (X < CX) return Y < CY ? "NW" : "SW";
				return Y < CY ? "NE" : "SE";
			}

			public int CourseFrom(Cell C)
			{
				var CX = C.ParentZone.wX * 3 * 80 + C.ParentZone.X * 80 + C.X;
				var CY = C.ParentZone.wY * 3 * 25 + C.ParentZone.Y * 25 + C.Y;
				var CZ = C.ParentZone.Z;

				return CourseFrom(CX, CY, CZ);
			}

			public int CourseFrom(Step S) => CourseFrom(S.X, S.Y, S.Z);
			public int CourseFrom(int CX, int CY, int CZ)
			{
				if (Z > CZ) return UP;
				if (Z < CZ) return DOWN;

				var result = 0;
				if (X < CX) result |= WEST;
				else if (X > CX) result |= EAST;
				
				if (Y < CY) result |= NORTH;
				else if (Y > CY) result |= SOUTH;

				return result;
			}
			
			public int CourseTo(int CX, int CY, int CZ)
			{
				if (Z > CZ) return DOWN;
				if (Z < CZ) return UP;

				var result = 0;
				if (X < CX) result |= EAST;
				else if (X > CX) result |= WEST;
				
				if (Y < CY) result |= SOUTH;
				else if (Y > CY) result |= NORTH;

				return result;
			}
		
			public IEnumerable<int> ExpandCourse(int Dir, int Limit = 7) {
				if (Dir == 0) yield break;
				if (Dir.HasBit(UP | DOWN)) yield break;
			
				int x = 0, y = 0, d = 0, xo, yo, xr, yr;

				if (Dir.HasBit(NORTH)) y--;
				else if (Dir.HasBit(SOUTH)) y++;

				if (Dir.HasBit(EAST)) x++;
				else if (Dir.HasBit(WEST)) x--;

				xo = xr = x;
				yo = yr = y;

				for (int i = 0; i < Limit; i++, d = 0) {
					if ((i & 1) == 0) {
						if (xo == yo || yo == 0) yo -= xo;
						else xo += yo;
						x = X + xo; y = Y + yo;
					} else {
						if (xr == yr || xr == 0) xr -= yr;
						else yr += xr;
						x = X + xr; y = Y + yr;
					}

					if (X < x) d |= EAST;
					else if (X > x) d |= WEST;
				
					if (Y < y) d |= SOUTH;
					else if (Y > y) d |= NORTH;
					
					yield return d;
				}
			}
		}
		
		protected static readonly Step[] EMPTY = new Step[0];

		protected Step[] Pool;
		protected int Size = 0;
		public int Count = 0;
		public Lattice Lattice;
		public long Built;

		public bool Valid => Built >= Lattice.Built;

		public Path()
		{
			Pool = EMPTY;
		}

		public Step this[int Index] => Index >= Count ? null : Pool[Index];

		public Path Reset()
		{
			Count = 0;
			Lattice = null;
			return this;
		}

		public void EnsureCapacity(int Capacity)
		{
			if (Pool.Length >= Capacity) return;
			
			var pool = new Step[Capacity];
			Array.Copy(Pool, 0, pool, 0, Size);
			Pool = pool;
		}

		public void Reverse() => Array.Reverse(Pool, 0, Count);
		
		public void Add(int X, int Y, int Z)
		{
			if (Count == Size)
			{
				if (Size == Pool.Length)
				{
					if (Size == 0) EnsureCapacity(4);
					else EnsureCapacity(Size * 2);
				}
				
				Pool[Size++] = new Step();
			}
			
			var node = Pool[Count++];
			node.X = X;
			node.Y = Y;
			node.Z = Z;
		}

		public void Add(Lattice Lattice, GridNode Node) => Add(Lattice, Node.X, Node.Y);
		public void Add(Lattice Lattice, int X, int Y, int Z = 0)
		{
			Add(
				Lattice.WX * 3 * 80 + Lattice.X * 80 + X,
				Lattice.WY * 3 * 25 + Lattice.Y * 25 + Y,
				Lattice.Z + Z
			);
		}
	}
}