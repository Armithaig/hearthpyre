﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ConsoleLib.Console;
using Genkit;
using Hearthpyre.AI;
using Hearthpyre.Realm;
using Wintellect.PowerCollections;
using XRL;
using XRL.Core;
using XRL.Messages;
using XRL.Wish;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Capabilities;
using XRL.World.Parts;
using static Hearthpyre.Static;
using static Hearthpyre.Realm.Lattice;

namespace Hearthpyre.AI
{
	[HasWishCommand]
	public partial class Strider : IDisposable
	{
		public OrderedQueue<GridNode> Queue = new OrderedQueue<GridNode>();
		public GridNode.Pool NodePool = new GridNode.Pool();
		public GridNode[,] Nodes = new GridNode[80, 25];
		public Path Path;
		public Location2D Origin;
		public Location2D Destination;
		public Lattice Lattice;
		public int Range;
		public int Limit;
		public int RoadPreference; // Weight added to nodes not of RoadMask
		public int RoadMask;
		public int LandingMask; // Increase road preference if a matching node is found
		public int ClosedMask;
		public int OpenMask;
		public bool Flying;
		public bool Aquatic;
		public bool WallWalking;

		public Strider()
		{
			ResetFlags();
		}

		public void ResetFlags()
		{
			Range = 0;
			Limit = 500;
			RoadPreference = 0;
			RoadMask = GRID_ROAD;
			LandingMask = 0;
			ClosedMask = GRID_SOLID;
			OpenMask = 0;
			Flying = false;
			Aquatic = false;
			WallWalking = false;
		}

		public void DrawNodes(bool Failed = false)
		{
			int mx = -1, my = -1;
			redraw:
			var SB = TextConsole.ScrapBuffer;
			Lattice.Zone.Render(SB);

			for (int x = 0; x <= Nodes.GetUpperBound(0); x++)
			for (int y = 0; y <= Nodes.GetUpperBound(1); y++)
			{
				var node = Nodes[x, y];
				if (node == null)
				{
					if (x == Destination.x && y == Destination.y)
					{
						SB.WriteAt(x, y, "&MD");
					}
					continue;
				}

				var fl = Lattice[x, y].HasBit(GRID_FLOOR);
				var chr = 'S';
				var cl = fl ? 'G' : 'g';

				if (node.Parent != null)
				{
					var dx = node.Parent.X - node.X;
					var dy = node.Parent.Y - node.Y;

					if (dx == -1 && dy == -1) chr = '\u00DA';
					else if (dx == 0 && dy == -1) chr = '\u0018';
					else if (dx == 1 && dy == -1) chr = '\u00BF';
					else if (dx == -1 && dy == 0) chr = '\u001B';
					else if (dx == 1 && dy == 0) chr = '\u001A';
					else if (dx == -1 && dy == 1) chr = '\u00C0';
					else if (dx == 0 && dy == 1) chr = '\u0019';
					else if (dx == 1 && dy == 1) chr = '\u00D9';
				}

				if (node.H == 0)
				{
					cl = fl ? 'M' : 'O';
				}
				else if (node.Closed)
				{
					cl = fl ? 'B' : 'b';
				}

				var con = SB.Buffer[x, y];
				con.Tile = null;
				con.Char = chr;
				con.Foreground = ColorUtility.ColorMap[cl];
			}

			var eck = false;
			var path = Nodes[Destination.x, Destination.y];
			while (path != null)
			{
				var fl = Lattice[path.X, path.Y].HasBit(GRID_FLOOR);
				eck = true;
				SB.Buffer[path.X, path.Y].Foreground = fl ? ColorUtility.ColorMap['M'] : ColorUtility.ColorMap['O'];
				path = path.Parent;
			}

			if (mx >= 0 && my >= 0 && Nodes[mx, my] != null)
			{
				SB.WriteAt(0, 0, "F:" + Nodes[mx, my].F);
				SB.WriteAt(0, 1, "G:" + Nodes[mx, my].G);
				SB.WriteAt(0, 2, "H:" + Nodes[mx, my].H);
				SB.WriteAt(0, 3, "L:" + Nodes[mx, my].L);
				SB.WriteAt(0, 4, "W:" + ((Lattice[mx, my] & GRID_NAV) >> 23) * 16);
				SB.WriteAt(0, 5, "B:" + Lattice[mx, my].PrintBits());
			}

			SB.Draw();
			if (!eck && !Failed)
			{
				Thread.Sleep(50);
				return;
			}
			
			var vk = Keyboard.getvk(true);
			if (vk == Keys.Enter) return;
			if (vk == Keys.MouseEvent)
			{
				mx = Keyboard.CurrentMouseEvent.x;
				my = Keyboard.CurrentMouseEvent.y;
			}
			
			goto redraw;
		}

		private bool SetObjectFlags(GameObject Object)
		{
			if (!GameObject.validate(Object)) return true;
				
			if (Object.IsFlying || Object.HasTagOrProperty("PathAsIfFlying"))
			{
				Flying = true;
			}
			else if (Object.pBrain?.WallWalker == true)
			{
				WallWalking = true;
				OpenMask |= GRID_WALL;
				ClosedMask = 0;
			}
			else if (Object.LimitToAquatic())
			{
				Aquatic = true;
				OpenMask |= GRID_LIQUID;
			}
			
			if (ReferenceEquals(Origin, null)) Origin = Object.CurrentCell.location;
			if (Lattice == null && !Lattice.TryGet(Object.CurrentZone.ZoneID, out Lattice))
			{
				return false;
			}

			return true;
		}

		private void SetGlobalFlags(GlobalLocation Location, int Flags)
		{
			if (Location == null) return;
			if (Lattice.Equals(Location))
			{
				Destination = Location2D.get(Location.CellX, Location.CellY);
			}
			else if (Lattice.TryGetNextZoneTo(Origin, Location, out var dir, out var id, Flags.HasBit(FLAG_PERMISSIVE)))
			{
				// wow i hate this, please dont look at this garbage
				var valid = Lattice.TryGet(id, out var neighbour, Rebuild: false, Create: false);
				var reach = Lattice.GetReachable(Origin);
				var bag = new BallBag<Location2D>();
				int ox = Origin.x, oy = Origin.y;
				if (dir == NORTH)
				{
					for (int x = 0; x < 80; x++)
					{
						if (!Lattice[x, 0].HasBit(reach)) continue;
						if (valid && !neighbour[x, 24].HasBit(GRID_REACH)) continue;
						bag.Add(Location2D.get(x, 0), 200 / (Math.Abs(x - ox) + 1) - Lattice.GetWeight(x, 0) + (Lattice.HasRoad(x, 0) ? 100 : 0));
					}

					if (bag.Count <= 0) return;
					Destination = bag.PeekOne();
					if (Flags.HasBit(FLAG_TRANSITION)) Path.Add(Lattice, Destination.x, -1);
				}
				else if (dir == EAST)
				{
					for (int y = 0; y < 25; y++)
					{
						if (!Lattice[79, y].HasBit(reach)) continue;
						if (valid && !neighbour[0, y].HasBit(GRID_REACH)) continue;
						bag.Add(Location2D.get(79, y), 200 / (Math.Abs(y - oy) + 1) - Lattice.GetWeight(79, y) + (Lattice.HasRoad(79, y) ? 100 : 0));
					}

					if (bag.Count <= 0) return;
					Destination = bag.PeekOne();
					if (Flags.HasBit(FLAG_TRANSITION)) Path.Add(Lattice, 80, Destination.y);
				}
				else if (dir == SOUTH)
				{
					for (int x = 0; x < 80; x++)
					{
						if (!Lattice[x, 24].HasBit(reach)) continue;
						if (valid && !neighbour[x, 0].HasBit(GRID_REACH)) continue;
						bag.Add(Location2D.get(x, 24), 200 / (Math.Abs(x - ox) + 1) - Lattice.GetWeight(x, 24) + (Lattice.HasRoad(x, 24) ? 100 : 0));
					}

					if (bag.Count <= 0) return;
					Destination = bag.PeekOne();
					if (Flags.HasBit(FLAG_TRANSITION)) Path.Add(Lattice, Destination.x, 25);
				}
				else if (dir == WEST)
				{
					for (int y = 0; y < 25; y++)
					{
						if (!Lattice[0, y].HasBit(reach)) continue;
						if (valid && !neighbour[79, y].HasBit(GRID_REACH)) continue;
						bag.Add(Location2D.get(0, y), 200 / (Math.Abs(y - oy) + 1) - Lattice.GetWeight(0, y) + (Lattice.HasRoad(0, y) ? 100 : 0));
					}

					if (bag.Count <= 0) return;
					Destination = bag.PeekOne();
					if (Flags.HasBit(FLAG_TRANSITION)) Path.Add(Lattice, -1, Destination.y);
				}
				else if (dir.HasBit(UP | DOWN))
				{
					var Z = Lattice.Zone;
					for (int x = 0; x < 80; x++)
					for (int y = 0; y < 25; y++)
					{
						if (!Lattice[x, y].HasBit(GRID_CONN)) continue;
						if (!Lattice[x, y].HasBit(reach)) continue;
						if (dir == UP && !Z.Map[x][y].HasObject(IsUpStair)) continue;
						if (dir == DOWN && !Z.Map[x][y].HasObject(IsDownStair)) continue;
						bag.Add(Location2D.get(x, y), 1);
					}

					if (bag.Count <= 0) return;
					Destination = bag.PeekOne();
					if (Flags.HasBit(FLAG_TRANSITION))
					{
						if (dir == UP) Path.Add(Lattice, Destination.x, Destination.y, -1);
						else if (dir == DOWN) Path.Add(Lattice, Destination.x, Destination.y, 1);
					}
				}
			}
		}

		bool IsDownStair(GameObject Object) => Object.OwnPart<StairsDown>();
		bool IsUpStair(GameObject Object) => Object.OwnPart<StairsUp>();

		private bool IsDestinationReachable()
		{
			if (WallWalking) return Lattice[Destination].HasBit(GRID_WALL);
			if (Aquatic) return Lattice[Destination].HasBit(GRID_LIQUID);
			
			var reach = Lattice.GetReachable(Origin);
			return reach.HasBit(Lattice.GetReachable(Destination, Range));
		}

		private void SetRoadFlags(int Flags)
		{
			if (Flags.HasBit(FLAG_DIRECT)) return;
			if (Lattice.Roads == 0) return;
			if (Flying || Aquatic || WallWalking) return;

			var origin = Lattice.GetRoad(Origin);
			if (origin == 0) origin = Lattice.GetRoad(Origin, 1);
			
			LandingMask = Lattice.GetRoad(Destination);
			if (LandingMask == 0) LandingMask = Lattice.GetRoad(Destination, 1);
			
			if (origin.HasBit(LandingMask))
			{
				// Nice! There's a contiguous road to the destination.
				RoadMask = origin;
				RoadPreference = 1 << 12;
				return;
			}
			if (origin > 0)
			{
				RoadMask = origin | LandingMask;
				RoadPreference = 1 << 5;
				return;
			}
			if (LandingMask > 0)
			{
				RoadMask = LandingMask;
				RoadPreference = 1 << 5;
				return;
			}
			
			origin = Lattice.GetRoad(Origin, 2) | Lattice.GetRoad(Destination, 2);
			if (origin > 0)
			{
				RoadMask = origin;
				RoadPreference = 1 << 3;
			}
		}

		private bool Perform()
		{
			if (!IsDestinationReachable()) return false;
			
			var taken = NodePool.Take(Origin.x, Origin.y, Lattice[Origin], Destination, this);
			var step = taken;
			var counter = 0;
			Queue.Enqueue(taken);
			Limit = Math.Min(Limit, Origin.DistanceTo(Destination) * 16);
			Nodes[Origin.x, Origin.y] = taken;
			
			while (Queue.Count != 0)
			{
				step = Queue.Dequeue();
				step.Closed = true;
				if (step.D <= Range)
				{
					Finalize(End: step);
					return true;
				}
				if (counter++ >= Limit) // hit the search limit, let's either make the search easier or dump out
				{
					if (Limit >= 2000) { break; }
					RoadPreference = 0;
					Limit = 2000;
				}
				
				for (int x = step.X - 1; x <= step.X + 1; x++)
				for (int y = step.Y - 1; y <= step.Y + 1; y++)
				{
					if (x < 0 || y < 0 || x >= 80 || y >= 25) continue; // out of bounds

					var flags = Lattice[x, y];
					if (flags.HasBit(ClosedMask)) continue;
					if (!flags.HasAllBits(OpenMask)) continue;

					taken = Nodes[x, y];
					if (taken != null)
					{
						if (taken.C + step.G >= taken.G) continue;
						taken.SetParent(step, this, flags);
						if (!taken.Closed) Queue.Requeue(taken); // G cost changed, sort up in heap if necessary
					}
					else
					{
						taken = NodePool.Take(x, y, flags, Destination, this, step);
						Queue.Enqueue(taken);
						Nodes[x, y] = taken;
					}
				}
			}

			return false;
		}

		private void Finalize(GridNode End)
		{
			Path.EnsureCapacity(End.L);
			Path.Add(Lattice, End);
			
			var step = End;
			while (step.Parent != null)
			{
				step = step.Parent;
				Path.Add(Lattice, step);
			}

			Path.Reverse();
			Path.Lattice = Lattice;
			Path.Built = Lattice.Built;
		}

		public void Dispose()
		{
			if (Lattice == null) return;
			
			ResetFlags();
			Queue.Clear();
			NodePool.Reset();
			Lattice = null;
			for (int x = 0; x < 80; x++)
			for (int y = 0; y < 25; y++)
			{
				Nodes[x, y] = null;
			}
			
			Pool.Push(this);
		}
	}

	public partial class Strider
	{
		public static Stack<Strider> Pool = new Stack<Strider>();
		public static Strider Get() => Pool.Count > 0 ? Pool.Pop() : new Strider();
		
		public const int FLAG_DIRECT = 1 << 0;
		public const int FLAG_PERMISSIVE = 1 << 1;
		public const int FLAG_TRANSITION = 1 << 2;

		public static bool TryPathTo(
			Path Path,
			Location2D Destination = null,
			GlobalLocation Global = null,
			Location2D Origin = null,
			GameObject Object = null,
			Lattice Lattice = null,
			int Range = 0,
			int Limit = 500,
			int Flags = 0
		)
		{
			using (var strider = Strider.Get())
			{
				strider.Path = Path.Reset();
				strider.Destination = Destination;
				strider.Origin = Origin;
				strider.Lattice = Lattice;
				strider.Range = Range;
				strider.Limit = Limit;
				strider.SetObjectFlags(Object);
				strider.SetGlobalFlags(Global, Flags);
				if (ReferenceEquals(strider.Destination, null)) return false;
				strider.SetRoadFlags(Flags);

				return strider.Perform();
			}
		}

		[WishCommand("testpath")]
		public static void TestRun()
		{
			var DC = The.Player.pPhysics.PickDestinationCell(9999, AllowVis.OnlyExplored, Locked: false, RequireCombat: false);
			Strider.TryPathTo(new Path(), DC.location, Object: The.Player);
		}
		
		[WishCommand("testpath")]
		public static void TestRun(string Range)
		{
			if (!int.TryParse(Range, out var range)) return;
			var DC = The.Player.pPhysics.PickDestinationCell(9999, AllowVis.OnlyExplored, Locked: false, RequireCombat: false);
			Strider.TryPathTo(new Path(), DC.location, Object: The.Player, Range: range);
		}
	}
}