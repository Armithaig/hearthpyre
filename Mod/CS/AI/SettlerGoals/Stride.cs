using System;
using System.Linq;
using System.Collections.Generic;
using Genkit;
using XRL;
using XRL.World;
using XRL.World.AI.Pathfinding;
using XRL.World.AI.GoalHandlers;

using Hearthpyre;
using Hearthpyre.Realm;

using static Hearthpyre.Static;
using static Hearthpyre.Realm.Lattice;

namespace Hearthpyre.AI
{
	[Serializable]
	public class StrideToLiminal : Stride
	{
		public StrideToLiminal(SettlerHandler Origin) : base(Origin) { }

		public override bool Finished()
		{
			return base.Finished() || CurrentSector != null;
		}

		public override void TakeAction()
		{
			if (!ParentObject.IsVisible() || CurrentCell.IsEdge())
			{
				Settlement.AddLiminal(ParentObject);
			}
			else
			{
				base.TakeAction();
			}
		}
	}
	
	[Serializable]
	public class Stride : ISettlerGoal
	{
		public GlobalLocation Target;
		public GameObjectSource Object;
		public Path Path;

		public int Tries;
		public int MaxTries;
		public int Index;
		public int Range;

		public Path.Step Step => Path[Index];

		public Stride(SettlerHandler Origin) : base(Origin)
		{
			Target = new GlobalLocation();
			Path = new Path();
		}

		public Stride Set(string ZoneID, int X, int Y, int Tries = 3, int Range = 0)
		{
			Set(Tries: Tries, Range: Range);
			this.Target.ZoneID = ZoneID;
			this.Target.CellX = X;
			this.Target.CellY = Y;
			return this;
		}

		public Stride Set(GameObject Target, int Tries = 3, int Range = 1)
		{
			return Set(Target.CurrentCell, Target, Tries, Range);
		}
		
		public Stride Set(Cell Target = null, GameObject Object = null, int Tries = 3, int Range = 0)
		{
			this.Target.SetCell(Target);
			this.Object = Object != null ? new GameObjectSource(Object) : null;
			this.Tries = 0;
			this.MaxTries = Tries;
			this.Range = Range;
			
			Reset();
			return this;
		}

		// recalc path if target farther from destination than we are
		void CheckObjectMoved()
		{
			var ourDist = Target.PathDistanceTo(CurrentCell) - Range;
			var theirDist = Object.Location.PathDistanceTo(Target);
			if (theirDist <= ourDist) return;

			Target.SetCell(Object.Location);
			Path.Reset();
		}
		
		public override bool IsBusy() => false;

		public override string GetDetails()
		{
			var sb = Event.NewStringBuilder();
			if (Object.Validate(out var obj)) sb.Append(obj.pRender.DisplayName);
			else if (Target.ZoneID != CurrentZone.ZoneID) sb.Append(Target.ZoneID);
			else sb.Append(Target.CellX).Append(',').Append(Target.CellY);
			sb.Compound(Index, "; ").Append('/').Append(Path.Count);
			sb.Compound(Tries, "; ").Append('/').Append(MaxTries);
			
			return sb.ToString();
		}

		public override bool Finished()
		{
			return Target.PathDistanceTo(CurrentCell) <= Range;
		}

		// TODO: check lattice build time against path time, in case we're pathing on old data
		public bool TryPath()
		{
			Index = 0;
			return Strider.TryPathTo(Path, Global: Target, Object: ParentObject, Range: Range, Flags: Strider.FLAG_TRANSITION);
		}

		public override void TakeAction()
		{
			base.TakeAction();
			if (Tries >= MaxTries)
			{
				FailToParent("I ran out of tries while striding.");
				return;
			}
			
			if (GameObjectSource.Validate(ref Object)) CheckObjectMoved();
			if (Index >= Path.Count && !TryPath())
			{
				FailToParent("I couldn't find a path to my target.");
			}
			if (Index < Path.Count && TakeStride())
			{
				OnActionTaken();
			}
		}

		public bool Recover(Cell C)
		{
			for (int i = Path.Count - 1; i >= 0; i--)
			{
				if (Path[i].DistanceTo(C) <= 1)
				{
					Index = i;
					return true;
				}
			}
			
			return false;
		}

		public bool Reset()
		{
			Index = 0;
			Path.Reset();
			return false;
		}

		public bool Advance(bool Path = false, bool Recover = false)
		{
			Index++;
			if (Path) Tries = 0;
			else Tries++;
			return true;
		}

		//public static List<AICommandList> CommandList = new List<AICommandList>();
		//public static Event MovementMutationEvent = new Event("AIGetMovementMutationList", "Target", (object) null, "List", CommandList);
		public bool TryMovementAbilities()
		{
			// skip this until chaos is done throwing spanners
			return false;

			/*if (Path.Count <= 2) return false;
			var destination = Path[Path.Count - 2];
			var cell = CurrentZone.GetCell(destination.CX, destination.CY);
			
			CommandList.Clear();
			MovementMutationEvent.SetParameter("Target", cell);
			ParentObject.FireEvent(MovementMutationEvent);
			return AICommandList.HandleCommandList(CommandList, ParentObject, cell);*/
		}

		private bool Move(Cell From, string Direction, bool Swap = false, bool Careful = false)
		{
			var C = From.GetCellFromDirection(Direction, BuiltOnly: Careful);
			if (C == null) return false;
			if (!Swap && C.HasCombatObject()) return false;
			if (!CheckFlags(C)) Careful = true;
			if (Careful && C.GetNavigationWeightFor(ParentObject) >= 10) return false;
			
			return ParentObject.Move(Direction: Direction, Peaceful: true);
		}

		private bool CheckFlags(Cell C)
		{
			if (C.ParentZone != Path.Lattice.Zone) return true;
			
			var f1 = MarkCell(C);
			var f2 = Path.Lattice[C.X, C.Y];
			if ((f1 & GRID_FLAGS) != (f2 & GRID_FLAGS))
			{
				if (Path.Valid) Path.Lattice.Rebuild();
				return false;
			}
			if (Math.Abs(((f1 & GRID_NAV) >> 23) - ((f2 & GRID_NAV) >> 23)) >= 10)
			{
				if (Path.Valid) Path.Lattice.Rebuild();
				return false;
			}
			
			return true;
		}
		

		public bool TakeStride()
		{
			var CC = CurrentCell;
			var dist = Step.DistanceTo(CC);
			if (dist >= 2)
			{
				if (!Recover(CC)) return Reset();
				ParentObject.ForfeitTurn(); // Recovered from going off course/being externally moved, wait a moment in case it was a swap
				return true; //dist = Step.DistanceTo(CC);
			}
			if (dist == 0 && ++Index >= Path.Count)
			{
				return Reset();
			}

			// This is our destination cell, swap if need be.
			var destination = Index == Path.Count - 1;
			var permissive = destination;
			NormalMove:
			// Try to move directly.
			if (Move(CC, Step.DirectionFrom(CC), Swap: permissive, Careful: !Path.Valid))
			{
				return Advance(true);
			}
			if (destination)
			{
				Tries++;
				return Reset();
			}

			// Try to move towards a future node's direction.
			var course = Step.CourseFrom(CC);
			for (int i = Index; i < Path.Count; i++)
			{
				if (course == Path[i].CourseFrom(CC)) continue;
				if (Move(CC, Path[i].DirectionFrom(CC), Swap: permissive, Careful: true)) return Advance(true);
				break;
			}

			// Try to move in general direction.
			foreach (var alt in Step.ExpandCourse(course, permissive ? 4 : 2))
			{
				if (Move(CC, DirectionMap[alt], Swap: permissive, Careful: true)) return Advance();
			}
			
			// Retry but allow swapping and expand course further.
			if (!permissive)
			{
				if (TryMovementAbilities()) return Recover(CurrentCell);
				permissive = true;
				goto NormalMove;
			}

			return Reset();
		}
	}
}