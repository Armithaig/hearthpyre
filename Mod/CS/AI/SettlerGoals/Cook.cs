﻿using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;

namespace Hearthpyre.AI
{
	public class Cook : ISettlerGoal
	{
		public GameObject Object;
		public int Actions;

		public Cook(SettlerHandler Origin) : base(Origin)
		{ }

		public Cook Set(GameObject Object)
		{
			this.Object = Object;
			this.Actions = 2;
			return this;
		}

		public override string GetDetails()
		{
			if (GameObject.validate(Object))
			{
				return Object.pRender.DisplayName;
			}
			
			return base.GetDetails();
		}

		public override bool Finished()
		{
			return !GameObject.validate(Object)
			       || Object.CurrentCell == null
			       || Actions == 0
				;
		}

		public override void TakeAction()
		{
			if (ParentObject.DistanceTo(Object) > 1)
			{
				PushChildGoal(Stride.Set(Object));
			}
			else
			{
				if (Actions == 2)
				{
					// TODO:it is surprisingly hard/heavy to get just a recipe name, have to resolve an instance
					ParentObject.pPhysics.DidX("start", "cooking");
				}
				else if (Actions == 1)
				{
					ParentObject.pPhysics.DidXToY("eat", ParentObject, "meal", PossessiveObject: true);
					ParentObject.FireEvent(Event.New("CookedAt", "Object", Object));
					CurrentCell.PlayWorldSound("Human_Eating", Volume: 0.3f);
					if (ParentObject.TryTakePart(out Stomach stomach))
					{
						stomach.CookingCounter = 0;
					}
				}

				ParentObject.UseEnergy(1000, "Cooking");
				PushChildGoal(Wait.Set(10, 20));
				Actions--;
			}
		}

		bool IsCampfire(GameObject Object) => Object.OwnPart<Campfire>();

		public bool Start()
		{
			if (!ParentObject.OwnPart<Stomach>()) return false;
			var campfire = GetRandom(IsCampfire);
			return campfire != null && Set(campfire).Push();
		}
	}
}