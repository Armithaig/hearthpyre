using System;
using System.Linq;
using System.Collections.Generic;
using Genkit;
using Hearthpyre.Realm;
using XRL;
using XRL.Core;
using XRL.Rules;
using XRL.World;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public class Rest : ISettlerGoal
	{
		const int SIT_DUR_MIN = 50;
		const int SIT_DUR_MAX = 200;

		public Cell Target;
		public GameObject Object;
		public int Turns;
		public bool Sleep;
		public bool Dropped;
		public bool Nocturnal;

		public Rest(SettlerHandler Origin) : base(Origin)
		{ }

		public Rest Set(Cell Target = null, GameObject Object = null, int Turns = -1, bool Sleep = false)
		{
			this.Target = Target ?? Object.CurrentCell;
			this.Object = Object;
			this.Turns = Turns;
			this.Sleep = Sleep;
			this.Dropped = false;

			return this;
		}

		public override string GetDetails()
		{
			var sb = Event.NewStringBuilder();
			if (Sleep) sb.Append("Sleep");
			if (Dropped) sb.Compound("Dropped");
			if (Nocturnal) sb.Compound("Nocturnal");
			if (GameObject.validate(Object)) sb.Compound(Object.pRender.DisplayName);
			if (Target != null) sb.Compound(Target.X).Append(',').Append(Target.Y);
			
			return sb.ToString();
		}


		public override void Create()
		{
			Nocturnal = ParentObject.HasTag("Nocturnal");
			Object?.Reserve(ParentObject.DistanceTo(Object) + 5L);
			
			if (Object == null && Sleep)
				Object = ParentObject.TakeItemPart<Bed>()?.ParentObject;

			if (Object == null)
				Object = ParentObject.TakeItemPart<Chair>()?.ParentObject;
		}

		public override void Failed()
		{
			if (Sleep && IdleArea == Home)
			{
				OverrideIdleArea(CurrentSector);
				Target = GetRandom(IsEmpty);
				if (Target != null)
				{
					Object = null;
					Create();
					return;
				}
			}
			
			base.Failed();
		}

		public override bool Finished()
		{
			var result = Turns == 0 || Target == null;
			if (Turns <= -1 && Sleep)
				result = Nocturnal != IsDay();

			if (result && Dropped)
			{
				ParentObject.BodyPositionChanged(To: "NotSitting");
				Dropped = !ParentObject.Take(Object);
			}

			return result;
		}

		public override void TakeAction()
		{
			if (ParentObject.CurrentCell != Target)
			{
				PushChildGoal(Stride.Set(Target: Target));
			}
			else if (!ParentObject.OwnEffect<Prone>() && !ParentObject.OwnEffect<Seated>())
			{
				Flight.StopFlying(ParentObject, ParentObject);

				if (!GameObject.validate(ref Object))
				{
					if (Sleep)ParentObject.ApplyEffect(new Prone(true));
					else ParentObject.ApplyEffect(new Seated(-2));
				}
				else
				{
					if (Object.InInventory == ParentObject)
						Dropped = ParentObject.Drop(Object);

					if (Object.OwnPart<Bed>())
					{
						ParentObject.ApplyEffect(new Prone(LyingOn: Object, Voluntary: true));
						ParentObject.FireEvent(Event.New("SleptIn", "Object", Object));
					}
					else if (Object.TryTakePart(out Chair part))
					{
						ParentObject.ApplyEffect(new Seated(Object, part.EffectiveLevel(), part.DamageAttributes));
					}
				}

				if (Sleep && !ParentObject.OwnEffect<Asleep>())
				{
					// Asleep will clear goals so set a good duration here
					var sleepDuration = Math.Max(GetSleepDuration(), 50);
					ParentObject.ForceApplyEffect(new Asleep(Object, sleepDuration, true, true, true));
				}
			}

			Turns -= 1;
			ParentObject.ForfeitTurn();
		}

		public int GetSleepDuration()
		{
			var tick = (int) (Calendar.TotalTimeTicks % Calendar.turnsPerDay);
			var result = Stat.Random(150, 300) - tick;
			if (tick > 300) result += Calendar.turnsPerDay;

			return result;
		}

		public static bool IsDay()
		{
			var segment = Calendar.CurrentDaySegment;
			return segment >= 1500 && segment < 10000;
		}

		private bool IsBed(GameObject Object) => Object.OwnPart<Bed>();
		private bool IsChair(GameObject Object) => Object.OwnPart<Chair>();
		private bool IsCampfire(GameObject Object) => Object.Blueprint == "Campfire";
		private bool IsEmpty(Cell C) => C.IsEmptyAtRenderLayer(2);

		public bool CheckSleep()
		{
			if (ParentObject.HasTagOrProperty("NoSleep") || ParentObject.OwnPart<Robot>()) return false;
			if (ParentObject.HasTag("Nocturnal") != IsDay()) return false;

			return StartSleep();
		}

		/// <summary>
		/// Find appropriate object/cell in area and push sleep goal.
		/// </summary>
		public bool StartSleep()
		{
			if (Home != null) OverrideIdleArea(Home);
			var target = CurrentCell.GetFirstObject(IsBed)
			             ?? GetRandom(IsBed, AvoidCombat: true)
			             ?? CurrentCell.GetFirstObject(IsChair)
			             ?? GetRandom(IsChair, AvoidCombat: true);

			if (target != null) return Set(Object: target, Sleep: true).Push();

			var floor = GetRandom(IsEmpty);
			if (floor != null) return Set(Target: floor, Sleep: true).Push();
			
			return false;
		}

		/// <summary>
		/// Find appropriate object/cell in area and push sit goal.
		/// </summary>
		public bool StartSit()
		{
			var duration = Stat.Random(SIT_DUR_MIN, SIT_DUR_MAX);
			if (20.in100() && StartSitCampfire(duration)) return true; 
			var target = CurrentCell.GetFirstObject(IsChair)
			             ?? GetRandom(IsChair, AvoidCombat: true);

			if (target != null) return Set(Object: target, Turns: duration).Push();

			var floor = GetRandom(IsEmpty);
			if (floor != null) return Set(Target: floor, Turns: duration).Push();

			return false;
		}

		private bool StartSitCampfire(int Duration)
		{
			var target = GetRandom(IsCampfire)?.CurrentCell;
			if (target == null) return false;
			
			var seat = GetRandomAdjacent(target, IsChair);
			if (seat != null) return Set(Object: seat, Turns: Duration).Push();
			
			var floor = GetRandomAdjacent(target, IsEmpty);
			if (floor != null) return Set(Target: floor, Turns: Duration).Push();

			return false;
		}
	}
}