using System;
using System.Collections.Generic;
using System.Linq;
using AiUnity.Common.Extensions;
using Genkit;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using XRL.World.Parts.Skill;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public class Tinker : ISettlerGoal
	{
		static readonly string[] Sounds = { "Clink1", "Clink2", "Clink3", "Spark1", "Spark2" };
		
		public List<GameObjectSource> Sources;
		public GameObjectSource Source;
		public GameObject Object;
		public long EndTurn;
		public int Counter;

		public Tinker(SettlerHandler Origin) : base(Origin)
		{ }

		public Tinker Set(long EndTurn = -1)
		{
			this.EndTurn = EndTurn;
			
			this.Sources = null;
			this.Source = null;
			this.Object = null;
			this.Counter = 0;
			return this;
		}

		public override void Create()
		{
			if (ParentObject.OwnPart<Tinkering_Repair>())
			{
				var C = CurrentCell;
				Sources = GetAdjacentSectorReferences(IsBrokenNoTake);
				Sources.Sort((a, b) => b.Location.PathDistanceTo(C).CompareTo(a.Location.PathDistanceTo(C)));
			}
		}

		public override string GetDetails()
		{
			var sb = Event.NewStringBuilder();
			if (Source.Validate(out var obj)) sb.Append("(R) ").Append(obj.pRender.DisplayName);
			if (!Sources.IsNullOrEmpty()) sb.Compound('[').Append(Sources.Count).Append(']');
			if (GameObject.validate(Object)) sb.Compound(Object.pRender.DisplayName);
			
			return sb.ToString();
		}

		public override bool Finished()
		{
			return The.Game.Turns >= EndTurn;
		}

		public override void TakeAction()
		{
			if (RepairSource()) return;
			if (TinkerObject()) return;
			
			if (50.in100())
			{
				Object = GetRandom(IsTinkerItem);
				if (Object != null)
				{
					Counter = Stat.Random(8, 16);
					return;
				}
			}

			OriginHandler.Rest.StartSit();
		}

		public bool RepairSource()
		{
			if (Source == null)
			{
				if (Sources.IsNullOrEmpty()) return false;
				Source = Sources[0];
				Sources.RemoveAt(0);
				Counter = Stat.Random(4, 8);
			}

			if (!GameObjectSource.Validate(ref Source, out var obj)) return false;
			if (ParentObject.DistanceTo(obj) > 1)
			{
				PushChildGoal(Stride.Set(obj));
			}
			else if (Counter-- > 0)
			{
				if (50.in100()) obj.CurrentCell.PlayWorldSound(Sounds.GetRandomElement(), Volume: 0.25f);
				obj.DustPuff();
				ParentObject.UseEnergy(1000, "Skill Tinkering Repair");
			}
			else
			{
				obj.DiscardEffect<Broken>();
				obj.DiscardEffect<Rusted>();
				obj.DiscardEffect<ShatterArmor>();
				obj.DiscardEffect<ShatteredArmor>();
				obj.hitpoints = obj.baseHitpoints;
				obj.FireEvent("TinkerRepaired");
				obj.CurrentCell.PlayWorldSound("Chop2", Volume: 0.5f);
				obj.Sparksplatter();
				ParentObject.UseEnergy(1000, "Skill Tinkering Repair");
				ParentObject.pPhysics.DidXToY("repair", obj);
				Source = null;
			}

			return true;
		}

		public bool TinkerObject()
		{
			if (!GameObject.validate(ref Object)) return false;
			
			if (ParentObject.DistanceTo(Object) > 1)
			{
				PushChildGoal(Stride.Set(Object));
			}
			else if (Counter-- > 0)
			{
				if (10.in100())
				{
					Object.CurrentCell.PlayWorldSound(Sounds.GetRandomElement(), Volume: 0.25f);
					Object.Sparksplatter();
				}
				ParentObject.UseEnergy(1000);
			}
			else
			{
				Object.CurrentCell.PlayWorldSound("Chop2", Volume: 0.4f);
				Object.DustPuff();
				ParentObject.UseEnergy(1000);
				Object = null;
			}

			return true;
		}

		public bool Start(long EndTurn)
		{
			if (Home != null) OverrideIdleArea(Home);
			return Set(EndTurn).Push();
		}

		bool IsTinkerItem(GameObject Object) => Object.OwnPart<TinkerItem>();
		bool IsBrokenNoTake(GameObject Object)
		{
			return Object.pPhysics != null
			       && !Object.pPhysics.Takeable
			       && (Object.OwnEffect<Broken>() || Object.OwnEffect<Rusted>())
				;
		}
	}
}