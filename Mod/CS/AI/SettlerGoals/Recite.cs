﻿using System;
using System.Collections.Generic;
using ConsoleLib.Console;
using XRL;
using XRL.Rules;
using XRL.UI;
using XRL.World.Conversations;
using XRL.World;
using XRL.World.Effects;
using XRL.World.Parts;

namespace Hearthpyre.AI
{
	public class Recite : ISettlerGoal
	{
		public static string[] Books = { "Skybear", "Canticles3" };

		public List<BookPage> Pages;
		public BookPage Page;
		public int Line;
		public long EndTurn;

		public Recite(SettlerHandler Origin) : base(Origin) { }

		public Recite Set(long EndTurn = -1)
		{
			if (Pages == null) InitPages();
			if (Sounds == null) InitSounds();
			
			this.EndTurn = EndTurn;
			return this;
		}

		public override string GetDetails()
		{
			if (Page != null)
			{
				return "Line" + (Line + 1) + "/" + Page.Lines.Count;
			}

			return base.GetDetails();
		}

		public override bool Finished()
		{
			return The.Game.Turns >= EndTurn;
		}

		public override void TakeAction()
		{
			base.TakeAction();
			if (Page == null || Line >= Page.Lines.Count)
			{
				Page = Pages.GetRandomElement();
				Line = 0;
				PushChildGoal(Wander.Set(2));
			}
			else if (ReciteLine())
			{
				ParentObject.UseEnergy(1000);
				PushChildGoal(Wander.Set(1));
			}
		}

		public bool ReciteLine()
		{
			if (Page == null || Line >= Page.Lines.Count) return false;

			string text;
			if (!Speaks)
			{
				text = Sounds.GetRandomElement();
				if (text.IsNullOrEmpty()) return false;

				ParentObject.ParticleText(text, 'W', juiceDuration: 3f, floatLength: 8f);
				ParentObject.pPhysics.DidX("sing", "{{W|'" + text + "'}}");
			}
			
			text = Page.Lines[Line++].Replace("\n", "");
			if (!string.IsNullOrWhiteSpace(text) && !text.StartsWith("&"))
			{
				if (ParentObject.TryTakePart(out ConversationScript script))
				{
					text = XRL.Language.TextFilters.Filter(text, script.Filter, script.FilterExtras);
				}

				var i = text.Length - 1;
				while (i >= 0 && char.IsPunctuation(text[i])) i--;
				if (i < text.Length - 1) text = text.Remove(i + 1);

				ParentObject.ParticleText(text, 'W', juiceDuration: 3f, floatLength: 8f);
				ParentObject.pPhysics.DidX("sing", "{{W|'" + text + "'}}");
				return true;
			}

			return false;
		}

		private char Note => 50.in100() ? '\u000D' : '\u000E';

		private void InitPages()
		{
			Pages = new List<BookPage>();
			foreach (var book in Books)
			foreach (var page in BookUI.Books[book])
			{
				Pages.Add(page);
			}
		}

		[NonSerialized] private List<string> Sounds;
		[NonSerialized] private bool Speaks;
		public void InitSounds()
		{
			if (Sounds != null || Speaks) return;
			

			if (ParentObject.OwnEffect<Glotrot>())
			{
                Sounds = new List<string>();
				for (int i = 0; i < 5; i++)
				{
					Sounds.Add(7.in10() ? "N" : "G" + new string('n', Stat.Random(3, 11)));
				}

				Speaks = false;
				return;
			}
			
			ParentObject.TakePart<ConversationScript>().TryGetSounds(out Speaks, out Sounds);
		}

		public bool Start(long EndTurn)
		{
			if (!ParentObject.OwnPart<ConversationScript>()) return false;
			OriginHandler.OverrideIdleArea(CurrentSector);
			return Set(EndTurn).Push();
		}
	}
}