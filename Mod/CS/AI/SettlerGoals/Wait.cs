﻿using XRL.Rules;

namespace Hearthpyre.AI
{
	public class Wait : ISettlerGoal
	{
		public int Actions;

		public Wait(SettlerHandler Origin) : base(Origin)
		{ }

		public Wait Set(int Low, int High)
		{
			this.Actions = Stat.Random(Low, High);
			return this;
		}
		
		public Wait Set(int Actions = -1)
		{
			this.Actions = Actions;
			return this;
		}
		
		public override bool IsBusy() => false;

		public override string GetDetails()
		{
			return Actions.ToString();
		}

		public override bool Finished()
		{
			return Actions == 0;
		}

		public override void TakeAction()
		{
			ParentObject.ForfeitTurn();
			Actions--;
		}
	}
}