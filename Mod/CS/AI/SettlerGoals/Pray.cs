﻿using UnityEngine;
using XRL.Core;
using XRL.Rules;
using XRL.World.AI.GoalHandlers;
using XRL.World.Capabilities;
using XRL.World.Parts;
using Event = XRL.World.Event;
using GameObject = XRL.World.GameObject;

namespace Hearthpyre.AI
{
	public class Pray : ISettlerGoal
	{
		public GameObject Object;
		public int Actions;

		public Pray(SettlerHandler Origin) : base(Origin)
		{ }

		public Pray Set(GameObject Object, int Turns = -1)
		{
			this.Object = Object;
			this.Actions = Turns;
			return this;
		}

		public override void Create()
		{
			Object.Reserve(ParentObject.DistanceTo(Object) + 5L);
		}

		public override string GetDetails()
		{
			if (GameObject.validate(Object))
			{
				return Object.pRender.DisplayName;
			}
			
			return base.GetDetails();
		}

		public override bool Finished()
		{
			return !GameObject.validate(Object)
			       || Object.CurrentCell == null
			       || Actions == 0
				;
		}

		public override void TakeAction()
		{
			if (ParentObject.DistanceTo(Object) > 1)
			{
				PushChildGoal(Stride.Set(Object));
			}
			else
			{
				if (10.in100()) Messaging.XDidYToZ(ParentObject, "voice", "a short prayer beneath", Object);
				ParentObject.UseEnergy(1000, "Item");
				ParentObject.FireEvent(Event.New("Prayed", "Object", Object));
				PushChildGoal(Wait.Set(2, 5));
				Actions--;
			}
		}

		bool IsShrine(GameObject Object) => Object.OwnPart<Shrine>();

		public bool Start()
		{
			var shrine = GetRandom(IsShrine);
			return shrine != null && Set(shrine, Stat.Random(5, 10)).Push();
		}
	}
}