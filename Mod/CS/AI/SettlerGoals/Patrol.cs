﻿using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;

namespace Hearthpyre.AI
{
	public class Patrol : ISettlerGoal
	{
		public HashSet<Sector> Visited = new HashSet<Sector>();
		public long EndTurn;

		public Patrol(SettlerHandler Origin) : base(Origin)
		{ }

		public Patrol Set(long EndTurn)
		{
			this.EndTurn = EndTurn;
			Visited.Clear();
			return this;
		}

		public override bool Finished()
		{
			return The.Game.Turns >= EndTurn;
		}

		public override void TakeAction()
		{
			var current = CurrentSector;
			if (current != null)
			{
				var sector = GetAdjacentSector(current);
				if (sector != null)
				{
					OriginHandler.OverrideIdleArea(sector);
					Visited.Add(sector);
				}
				else
				{
					OriginHandler.OverrideIdleArea(current);
				}
			}
			
			PushChildGoal(Wander.Set(Stat.Random(3, 10)));
		}

		public Sector GetAdjacentSector(Sector Current)
		{
			if (Current.Settlement.Sectors.Count <= 1) return null;

			var adjacent = new List<Sector>();
			var unvisited = new List<Sector>();
			foreach (var sector in Current.Settlement.Sectors)
			{
				if (Current.DistanceTo(sector) != 1) continue;
				if (!Visited.Contains(sector)) unvisited.Add(sector);
				else adjacent.Add(sector);
			}

			if (unvisited.Count == 0)
			{
				Visited.Clear();
				return adjacent.GetRandomElement();
			}

			return unvisited.GetRandomElement();
		}

		public bool Start(long EndTurn)
		{
			return Set(EndTurn).Push();
		}
	}
}