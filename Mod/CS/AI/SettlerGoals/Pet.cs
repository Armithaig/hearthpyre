﻿using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;

namespace Hearthpyre.AI
{
	public class Pet : ISettlerGoal
	{
		public GameObject Object;
		public int Actions;
		public bool Messaged;

		public Pet(SettlerHandler Origin) : base(Origin)
		{ }

		public Pet Set(GameObject Object, int Actions = -1)
		{
			this.Object = Object;
			this.Actions = Actions;
			this.Messaged = false;
			return this;
		}

		public override string GetDetails()
		{
			if (GameObject.validate(Object))
			{
				return Object.pRender.DisplayName;
			}
			
			return base.GetDetails();
		}

		public override bool Finished()
		{
			return !GameObject.validate(Object)
			       || Object.CurrentCell == null
			       || Actions == 0
				;
		}

		public override void TakeAction()
		{
			if (ParentObject.DistanceTo(Object) > 1)
			{
				PushChildGoal(Stride.Set(Object));
			}
			else
			{
				if (!Messaged)
				{
					ParentObject.pPhysics.DidXToY("start", "petting", Object);
					Object.FireEvent(Event.New("ObjectPetted", "Object", Object, "Petter", ParentObject));
				}
				else if (10.in100())
				{
					// wow i am invested in this petting, lets come back to this
					var extra = "belly";
					var end = ".";
					if (10.in100())
					{
						extra += ", such a good " + Object.immaturePersonTerm;
						end = "!";
					}
					ParentObject.pPhysics.DidXToY("rub", Object, extra, end, PossessiveObject: true);
				}
				else if (10.in100() && Object.HasBodyPart("Feet"))
				{
					var extra = "feet";
					var end = ".";
					if (10.in100())
					{
						extra += ", such a good " + Object.immaturePersonTerm;
						end = "!";
					}
					ParentObject.pPhysics.DidXToY("massage", Object, extra, end, PossessiveObject: true);
				}
				ParentObject.UseEnergy(1000, "Petting");
				PushChildGoal(Wait.Set(2, 5));
				Actions--;
				Messaged = true;
			}
		}

		bool IsPettable(GameObject Object) => Object.OwnPart<Pettable>();

		public bool Start()
		{
			if (ParentObject.OwnPart<Pettable>()) return false;
			
			var pettable = GetRandom(IsPettable);
			return pettable != null && Set(pettable, Stat.Random(5, 10)).Push();
		}
	}
}