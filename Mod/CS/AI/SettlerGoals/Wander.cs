using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Core;
using XRL.Rules;
using XRL.World;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public class Wander : ISettlerGoal
	{
		public int Actions;
		public int Pass;

		public Wander(SettlerHandler Origin) : base(Origin)
		{ }

		public Wander Set(int Actions = -1)
		{
			this.Actions = Actions;
			this.Pass = 0;
			return this;
		}
		
		public override bool IsBusy() => false;

		public override string GetDetails()
		{
			return Actions + " actions";
		}

		public override bool Finished()
		{
			return Actions == 0;
		}

		bool IsClose(Cell C) => C.PathDistanceTo(CurrentCell) <= 9;

		public override void TakeAction()
		{
			if (Pass-- > 0)
			{
				ParentObject.ForfeitTurn();
				return;
			}
			
			Cell cell = null;
			if (Stat.Rand.Next(100) >= 10)
			{
				cell = GetRandom(IsClose);
			}

			if (cell == null)
			{
				cell = GetRandomCell();
			}

			if (cell == null)
			{
				FailToParent("I could not find a cell to wander to.");
				return;
			}

			var dist = CurrentCell.PathDistanceTo(cell);
			Pass = Stat.Random(dist * 3, dist * 6);
			PushChildGoal(Stride.Set(cell));
			Actions--;
		}

		public bool Start()
		{
			return Set(Stat.Random(4, 12)).Push();
		}
	}
}