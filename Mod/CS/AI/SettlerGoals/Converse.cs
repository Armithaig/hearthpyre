﻿using XRL.Rules;
using XRL.World;
using XRL.World.Effects;
using XRL.World.Parts;

namespace Hearthpyre.AI
{
	public class Converse : ISettlerGoal
	{
		public GameObject Object;
		public int Actions;
		public bool Messaged;

		public Converse(SettlerHandler Origin) : base(Origin)
		{ }

		public Converse Set(GameObject Object, int Actions = -1)
		{
			this.Object = Object;
			this.Actions = Actions;
			this.Messaged = false;
			return this;
		}

		public override string GetDetails()
		{
			if (GameObject.validate(Object))
			{
				return Object.pRender.DisplayName;
			}
			
			return base.GetDetails();
		}

		public override bool Finished()
		{
			return !GameObject.validate(Object)
			       || Object.CurrentCell == null
			       || Actions == 0
				;
		}

		public override void TakeAction()
		{
			if (ParentObject.DistanceTo(Object) > 2)
			{
				PushChildGoal(Stride.Set(Object));
			}
			else
			{
				if (!Messaged)
				{
					ParentObject.pPhysics.DidXToY("strike", "up a conversation with", Object);
					Object.FireEvent(Event.New("ObjectPetted", "Object", Object, "Petter", ParentObject));
				}
				else if (10.in100())
				{
					// wow i am invested in this petting, lets come back to this
					var extra = "belly";
					var end = ".";
					if (10.in100())
					{
						extra += ", such a good " + Object.immaturePersonTerm;
						end = "!";
					}
					ParentObject.pPhysics.DidXToY("rub", Object, extra, end, PossessiveObject: true);
				}
				else if (10.in100() && Object.HasBodyPart("Feet"))
				{
					var extra = "feet";
					var end = ".";
					if (10.in100())
					{
						extra += ", such a good " + Object.immaturePersonTerm;
						end = "!";
					}
					ParentObject.pPhysics.DidXToY("massage", Object, extra, end, PossessiveObject: true);
				}
				ParentObject.UseEnergy(1000, "Petting");
				PushChildGoal(Wait.Set(2, 5));
				Actions--;
				Messaged = true;
			}
		}

		// wow it is pretty tough to figure out whether something can speak mutually intelligible words
		int Conversability(GameObject Object)
		{
			if (Object.pBrain == null) return 0;
			if (Object.Stat("Intelligence") <= 6) return 0;
			//if (!Object.OwnPart<ConversationScript>()) return 0;
			if (!Object.HasTag("SimpleConversation")) return 0;

			var weight = 10;
			if (Object.GetIntProperty("Humanoid") > 0 && ParentObject.GetIntProperty("Humanoid") > 0) weight += 20;
			else if (Object.GetStringProperty("Species", "None") == ParentObject.GetStringProperty("Species")) weight += 20;
			else if (Object.GetStringProperty("Culture", "None") == ParentObject.GetStringProperty("Culture")) weight += 10;
			
			return weight;
		}

		public bool Start()
		{
			if (Conversability(ParentObject) <= 0) return false;
			
			var partner = GetWeighted(Conversability);
			return partner != null && Set(partner, Stat.Random(5, 10)).Push();
		}
	}
}