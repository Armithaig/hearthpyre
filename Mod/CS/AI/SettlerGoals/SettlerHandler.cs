using System;
using System.CodeDom.Compiler;
using System.Linq;
using System.Collections.Generic;
using Genkit;
using Hearthpyre.Realm;
using XRL;
using XRL.Core;
using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public partial class SettlerHandler : ISettlerGoal
	{
		public const int TASK_SIT = 1;
		public const int TASK_PRAY = 2;
		public const int TASK_SMOKE = 3;
		public const int TASK_WANDER = 4;
		public const int TASK_CONVERSE = 5;
		public const int TASK_PET = 6;
		public const int TASK_COOK = 7;
		public const int TASK_CLEAN = 8;
		public const int TASK_READ = 9;
		public const int TASK_BATHE = 10;

		public static readonly BallBag<int> Tasks = new BallBag<int>
		{
			{ TASK_SIT, 20 },
			{ TASK_PRAY, 5 },
			{ TASK_SMOKE, 10 },
			{ TASK_WANDER, 20 },
			{ TASK_CONVERSE, 0 }, // TODO
			{ TASK_PET, 5 },
			{ TASK_COOK, 10 },
			{ TASK_CLEAN, 0 } // TODO
		};

		public new ICogentArea IdleArea { get; private set; }
		public int AreaCooldown { get; private set; }
		public long LastFailure { get; private set; }

		public SettlerHandler()
		{
		}

		public static bool IsValidFor(GameObject Object)
		{
			return Object.pBrain.PartyLeader == null
			       || Object.pBrain.Staying
				;
		}

		public override bool Finished() => ParentBrain.PartyLeader != null && !ParentBrain.Staying;

		public override void Create()
		{
			Settler = ParentObject.TakePart<HearthpyreSettler>();
			ParentBrain.StartingCell = null;
			ParentBrain.Wanders = true;

			Wait = new Wait(this);
			Stride = new Stride(this);
			Wander = new Wander(this);
		}

		public override string GetDetails()
		{
			if (IdleArea != null)
			{
				var sb = Event.NewStringBuilder();
				sb.Append(IdleArea.GetLattice().ZoneID);
				sb.Compound('[').Append(IdleArea.GetArea().Count).Append(']');
				return sb.ToString();
			}

			return base.GetDetails();
		}

		public override void TakeAction()
		{
			if (Settlement != null)
			{
				RollIdleArea();
				// TODO: Have NPCs converse (effect, [talking to Object.DisplayName], flash a speech tile? There's the particle system, probably better.),
				if (GoToSettlement()) return;
				if (Rest.CheckSleep()) return;
				if (Obtain.Collect()) return;
				if (Work()) return;
				if (RollRandomTask()) return;
				if (Wander.Start()) return;
			}

			Log("FALLBACK: Wandering randomly.");
			PushChildGoal(new WanderRandomly(Stat.Random(5, 10)));
		}

		public override void Failed()
		{
			AreaCooldown = 0;
			if (LastFailure >= The.Game.Turns - 3)
			{
				PushChildGoal(Wait.Set(10, 20));
			}

			LastFailure = The.Game.Turns;
		}

		void RollIdleArea()
		{
			if (IdleArea != null && AreaCooldown-- > 0) return;
			AreaCooldown = Stat.Random(2, 5);

			if (Grounded && Home != null)
			{
				IdleArea = Home;
				return;
			}

			var current = CurrentSector;
			var roll = Stat.Random(1, 100);
			if (roll >= 90 && Settlement.Sectors.Count > 1)
			{
				var adjacent = GetAdjacentTo(current).GetRandomElement();
				if (adjacent != null)
				{
					IdleArea = adjacent;
					return;
				}
			}

			if (roll >= 45 && Home != null)
			{
				IdleArea = Home;
				return;
			}

			if (current != null)
			{
				IdleArea = current;
				return;
			}
		}

		public new void OverrideIdleArea(ICogentArea Area = null)
		{
			if (IdleArea == Area) return;
			if (Area != null) IdleArea = Area;

			AreaCooldown = 0;
		}

		/// <summary>
		/// Travel back to our settlement if in zone outside of it.
		/// </summary>
		bool GoToSettlement()
		{
			if (!Settlement.HasSector(CurrentZone))
			{
				PushChildGoal(new StrideToLiminal(this).Set(Settlement.Prime.ZoneID, 40, 12));
				return true;
			}

			return false;
		}

		/// <summary>
		/// Retainers work: tend shops, tinker and patrol.
		/// </summary>
		// TODO: Assignments for regular settlers.
		bool Work()
		{
			if (Calendar.CurrentDaySegment < 4000 || Calendar.CurrentDaySegment >= 8000) return false;
			if (Calendar.TotalTimeTicks % 8400 >= 6000) return false; // weekends off comrade
			if (Grounded) return false;

			if (Settlement.Retainers.TryGetValue(ParentObject.id, out var retainer))
			{
				var duration = GetWorkEnd();
				switch (retainer)
				{
					case RET_MERCHANT: return TendShop();
					case RET_TINKER: return Tinker.Start(duration);
					case RET_WARDEN: return Patrol.Start(duration);
					case RET_MINSTREL: return Recite.Start(duration);
					case RET_APOTHECARY: return Cultivate.Start(duration);
				}
			}

			return false;
		}

		public long GetWorkEnd()
		{
			return The.Game.Turns + Stat.Rand.Next(700, 800) - Calendar.TotalTimeTicks % Calendar.turnsPerDay;
		}

		bool TendShop()
		{
			if (Home == null) return false;

			OverrideIdleArea(Home);
			return Rest.StartSit();
		}

		bool RollRandomTask()
		{
			switch (Tasks.PeekOne())
			{
				case TASK_SIT when Rest.StartSit():
				case TASK_SMOKE when Smoke.Start():
				case TASK_WANDER when Wander.Start():
				case TASK_PET when Pet.Start():
				case TASK_COOK when Cook.Start():
				case TASK_PRAY when Pray.Start(): return true;
				default: return false;
			}
		}
	}

	public partial class SettlerHandler
	{
		private Obtain _obtain;
		private Rest _rest;
		private Smoke _smoke;
		private Tinker _tinker;
		private Patrol _patrol;
		private Pray _pray;
		private Recite _recite;
		private Cultivate _cultivate;
		private Pet _pet;
		private Cook _cook;

		public new readonly List<Cell> CellBuffer = new List<Cell>();
		public new readonly BallBag<GameObject> ObjectBag = new BallBag<GameObject>();

		public new Wait Wait { get; private set; }
		public new Stride Stride { get; private set; }
		public new Wander Wander { get; private set; }

		public Obtain Obtain => _obtain ?? (_obtain = new Obtain(this));
		public Rest Rest => _rest ?? (_rest = new Rest(this));
		public Smoke Smoke => _smoke ?? (_smoke = new Smoke(this));
		public Tinker Tinker => _tinker ?? (_tinker = new Tinker(this));
		public Patrol Patrol => _patrol ?? (_patrol = new Patrol(this));
		public Pray Pray => _pray ?? (_pray = new Pray(this));
		public Recite Recite => _recite ?? (_recite = new Recite(this));
		public Cultivate Cultivate => _cultivate ?? (_cultivate = new Cultivate(this));
		public Pet Pet => _pet ?? (_pet = new Pet(this));
		public Cook Cook => _cook ?? (_cook = new Cook(this));
	}
}