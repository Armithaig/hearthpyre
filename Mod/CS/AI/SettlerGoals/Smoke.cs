using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public class Smoke : ISettlerGoal
	{
		public GameObject Object { get; set; }
		public GameObject Seat { get; set; }
		public int Turns { get; set; }

		public Smoke(SettlerHandler Origin) : base(Origin)
		{ }

		public Smoke Set(GameObject Object, GameObject Seat, int Turns = -1)
		{
			this.Object = Object;
			this.Seat = Seat;
			this.Turns = Turns;
			return this;
		}

		public override void Create()
		{
			Seat?.Reserve(ParentObject.DistanceTo(Seat) + 5L);
		}

		public override bool Finished()
		{
			return !GameObject.validate(Object)
			       || Object.CurrentCell == null
			       || Turns == 0
				;
		}

		public override void TakeAction()
		{
			base.TakeAction();
			if (ParentObject.CurrentCell != Seat.CurrentCell)
			{
				if (Seat.CurrentCell.HasCombatObject())
				{
					FailToParent("There's someone in my seat!");
					return;
				}

				PushChildGoal(Stride.Set(Seat.CurrentCell));
			}
			else if (!ParentObject.OwnEffect<Seated>())
			{
				Flight.StopFlying(ParentObject, ParentObject);

				var chair = Seat.TakePart<Chair>();
				ParentObject.ApplyEffect(new Seated(Seat, chair.EffectiveLevel(), chair.DamageAttributes));
				ParentObject.UseEnergy(1000, "Position");
				chair.FireEvent(Event.New("BeingSatOn", "Object", ParentObject));
			}
			else
			{
				for (int i = 2; i < 5; i++)
					Object.Smoke(150, 180);

				ParentObject.UseEnergy(1000, "Item");
				ParentObject.FireEvent(Event.New("Smoked", "Object", Object));
				PushChildGoal(Wait.Set(2, 5));
				Turns--;
			}
		}

		bool IsChair(GameObject Object)
		{
			if (Object.IsReserved()) return false;
			if (!Object.OwnPart<Chair>()) return false;
			return true;
		}

		bool IsHookah(GameObject Object) => Object.OwnPart<Hookah>();

		public bool Start()
		{
			if (!ParentObject.Respires) return false;

			var hookah = GetRandom(IsHookah);
			if (hookah == null) return false;

			foreach (var cell in hookah.CurrentCell.YieldAdjacentCells(1, LocalOnly: true))
			{
				if (cell.IsSolidFor(ParentObject)) continue;

				var seat = cell.GetFirstObject(IsChair);
				if (seat == null) continue;
				if (cell != CurrentCell && cell.HasCombatObject()) continue;

				Set(hookah, seat, Stat.Random(10, 30)).Push();
				return true;
			}

			return false;
		}
	}
}