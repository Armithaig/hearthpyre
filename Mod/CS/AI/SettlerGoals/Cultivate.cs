﻿using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Liquids;
using XRL.Rules;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;

namespace Hearthpyre.AI
{
	public class Cultivate : ISettlerGoal
	{
		public List<GameObjectSource> Sources;
		public GameObjectSource Source;
		public GameObject Object;
		public long EndTurn;
		public int Counter;

		public Cultivate(SettlerHandler Origin) : base(Origin) { }

		public Cultivate Set(long EndTurn = -1)
		{
			this.EndTurn = EndTurn;
			
			this.Sources = null;
			this.Source = null;
			this.Object = null;
			this.Counter = 0;
			return this;
		}

		public override string GetDetails()
		{
			var sb = Event.NewStringBuilder();
			if (Source.Validate(out var obj)) sb.Append("(H) ").Append(obj.pRender.DisplayName);
			if (!Sources.IsNullOrEmpty()) sb.Compound('[').Append(Sources.Count).Append(']');
			if (GameObject.validate(Object)) sb.Compound(Object.pRender.DisplayName);
			
			return sb.ToString();
		}
		
		public override void Create()
		{
			if (ParentObject.OwnPart<CookingAndGathering_Harvestry>())
			{
				var C = CurrentCell;
				Sources = GetAdjacentSectorReferences(IsRipeHarvestable);
				Sources.Sort((a, b) => a.Location.PathDistanceTo(C).CompareTo(b.Location.PathDistanceTo(C)));
			}
		}

		public override bool Finished()
		{
			return The.Game.Turns >= EndTurn;
		}

		public override void TakeAction()
		{
			if (HarvestSource()) return;
			if (WaterObject()) return;
			
			if (50.in100())
			{
				Object = GetRandom(IsHarvestable);
				if (Object != null)
				{
					Counter = Stat.Random(0, 2);
					return;
				}
			}

			OriginHandler.Rest.StartSit();
		}

		public bool HarvestSource()
		{
			if (Source == null)
			{
				if (Sources.IsNullOrEmpty()) return false;
				Source = Sources[0];
				Sources.RemoveAt(0);
			}

			if (!GameObjectSource.Validate(ref Source, out var obj)) return false;
			if (ParentObject.DistanceTo(obj) > 1)
			{
				PushChildGoal(Stride.Set(obj));
			}
			else
			{
				var hvst = obj.TakePart<Harvestable>();
				hvst?.AttemptHarvest(ParentObject, Automatic: false);
				Source = null;
			}

			return true;
		}

		public bool WaterObject()
		{
			if (!GameObject.validate(ref Object)) return false;
			if (ParentObject.DistanceTo(Object) > 1)
			{
				PushChildGoal(Stride.Set(Object));
			}
			else if (Counter-- > 0)
			{
				ParentObject.UseEnergy(1000);
			}
			else
			{
				var hvst = Object.TakePart<Harvestable>();
				if (hvst.RegenTimer != int.MaxValue)
				{
					hvst.RegenTimer -= hvst.RegenTimer / 4;
				}
				else if (!hvst.Ripe && !hvst.StartRipeChance.IsNullOrEmpty() && hvst.RegenTime.IsNullOrEmpty())
				{
					var result = 100;
					var parts = hvst.StartRipeChance.Split(':');
					if (parts.Length == 1) result *= int.Parse(parts[0]);
					else result *= int.Parse(parts[1]) / int.Parse(parts[0]);

					hvst.RegenTime = result + "-" + result * 2;
					hvst.RegenTimer = hvst.RegenTime.RollCached();
				}
				
				var water = LiquidVolume.getLiquid(LiquidWater.ID);
				Object.LiquidSplash(water);
				Object.PlayWorldSound(water.SplashSound(null));
				Object.SetLongProperty("Watered", EndTurn);
				
				ParentObject.pPhysics.DidXToY("water", Object);
				ParentObject.UseEnergy(1000);
				Object = null;
			}

			return true;
		}

		public bool IsHarvestable(GameObject Object) => Object.OwnPart<Harvestable>() && Object.GetLongProperty("Watered") != EndTurn;
		public bool IsRipeHarvestable(GameObject Object) => Object.TryTakePart(out Harvestable H) ? H.Ripe && !H.DestroyOnHarvest : false;

		public bool Start(long EndTurn)
		{
			if (Home != null) OverrideIdleArea(Home);
			return Set(EndTurn).Push();
		}
	}
}