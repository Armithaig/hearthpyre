using System;
using System.Collections.Generic;
using Wintellect.PowerCollections;
using XRL.Rules;
using XRL.World;
using XRL.Core;
using Genkit;
using XRL.World.AI.Pathfinding;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	[Serializable]
	public class Obtain : ISettlerGoal
	{
		public GameObject Target;
		public bool Silent;
		public int Energy;

		public Obtain(SettlerHandler Origin) : base(Origin)
		{ }

		public Obtain Set(GameObject Target, bool Silent = false, int Energy = 1000)
		{
			this.Target = Target;
			this.Silent = Silent;
			this.Energy = Energy;

			return this;
		}

		public override string GetDetails()
		{
			if (GameObject.validate(Target))
			{
				return Target.pRender.DisplayName;
			}
			
			return base.GetDetails();
		}

		public override bool Finished()
		{
			return !GameObject.validate(Target)
			       || Target?.CurrentCell == null
				;
		}

		public override void Create()
		{
			Target?.Reserve(ParentObject.DistanceTo(Target) + 5L);
		}

		public override void TakeAction()
		{
			if (ParentObject.DistanceTo(Target) > 1)
			{
				PushChildGoal(Stride.Set(Target));
			}
			else
			{
				ParentObject.Take(Target, Silent, Energy);
			}
		}

		/// <summary>
		/// Pick up items we've dropped from inventory such as chairs or bed rolls that we sat/slept on.
		/// </summary>
		public bool Collect()
		{
			var dropped = ParentObject.GetStringProperty("Dropped");
			if (string.IsNullOrEmpty(dropped)) return false;

			foreach (var str in dropped.Split(';'))
			{
				var obj = CurrentZone.findObjectById(str);
				if (obj == null) continue;
				if (obj.IsReserved()) continue;
				if (obj.CurrentCell != CurrentCell && obj.CurrentCell.HasCombatObject()) continue;
				return Set(obj).Push();
			}

			return false;
		}
	}
}