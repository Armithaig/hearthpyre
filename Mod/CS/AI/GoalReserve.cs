﻿using System.Collections.Generic;
using XRL;
using XRL.World;

namespace Hearthpyre.AI
{
	[HasGameBasedStaticCache]
	public static class GoalReserve
	{
		// Store object reservations to dodge AI collisions.
		[GameBasedStaticCache]
		public static Dictionary<string, long> Reservations;
		
		/// <summary>
		/// Check if an object has been reserved.
		/// </summary>
		public static bool IsReserved(this GameObject Object) {
			// TODO: IsReservedFor()
			if (!Reservations.TryGetValue(Object.id, out var reserved))
				return false;

			return reserved - The.Game.Turns > 0;
		}
		
		/// <summary>
		/// Reserve an object for a set amount of turns.
		/// AI will skip over this object for the duration, giving the reserver exclusive use of the object.
		/// </summary>
		public static void Reserve(this GameObject Object, long Turns = 5L) {
			//obj.SetLongProperty("Reserved", GAME.Turns + turns);
			Reservations[Object.id] = The.Game.Turns + Turns;
		}
		/// <summary>
		/// Reserve an object until a specific turn.
		/// AI will skip over this object for the duration, giving the reserver exclusive use of the object.
		/// </summary>
		public static void ReserveUntil(this GameObject Object, long Turns) {
			//obj.SetLongProperty("Reserved", GAME.Turns + turns);
			Reservations[Object.id] = Turns;
		}
	}
}