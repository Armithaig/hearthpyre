using System;
using System.Collections.Generic;
using Genkit;
using Hearthpyre.Realm;
using XRL;
using XRL.Core;
using XRL.World;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;

namespace Hearthpyre.AI
{
	[Serializable]
	public abstract class ISettlerGoal : IMovementGoal
	{
		public SettlerHandler OriginHandler { get; set; }
		public new ISettlerGoal ParentHandler { get; set; }
		public HearthpyreSettler Settler { get; set; }

		public Home Home => Settler.Home;
		public Settlement Settlement => Settler.Settlement;
		public Sector CurrentSector => Settlement.GetSector(CurrentZone.ZoneID);
		public bool Grounded => Settler.Grounded;

		public ICogentArea IdleArea => OriginHandler.IdleArea;
		public List<Cell> CellBuffer => OriginHandler.CellBuffer;
		public BallBag<GameObject> ObjectBag => OriginHandler.ObjectBag;
		public Wait Wait => OriginHandler.Wait;
		public Stride Stride => OriginHandler.Stride;
		public Wander Wander => OriginHandler.Wander;

		public ISettlerGoal()
		{
		}

		public ISettlerGoal(SettlerHandler Origin)
		{
			base.ParentHandler = Origin;
			OriginHandler = Origin;
			ParentBrain = Origin.ParentBrain;
			Settler = Origin.Settler;
		}

		public void PushChildGoal(ISettlerGoal Goal)
		{
			Goal.ParentHandler = this;
			base.PushChildGoal(Goal);
		}

		public virtual void OnActionTaken()
		{
			ParentHandler?.OnActionTaken();
		}

		public override bool IsBusy() => true;

		public override void Failed()
		{
			FailToParent();
		}

		public void FailToParent(string Reason)
		{
			Think(Reason);
			base.FailToParent();
		}

		public bool Push()
		{
			OriginHandler.PushChildGoal(this);
			return true;
		}

		public bool Finish()
		{
			if (ParentBrain.Goals.Count <= 0) return true;
			ParentBrain.Goals.Pop();
			return true;
		}

		public void OverrideIdleArea(ICogentArea Area) => OriginHandler.OverrideIdleArea(Area);

		public delegate bool CellPredicate(Cell C);
		public delegate bool ObjectPredicate(GameObject Object);
		public delegate int CellWeight(Cell C);
		public delegate int ObjectWeight(GameObject Object);

		public List<Cell> GetCells(bool Careful = false, bool Adjacent = false, bool AvoidCombat = false, bool AvoidEdges = false, bool AvoidWater = false)
		{
			CellBuffer.Clear();
			var CC = CurrentCell;
			var lattice = IdleArea.GetLattice();
			var reach = lattice.GetReachable(CC);
			var Z = lattice.Zone;
			foreach (var L in IdleArea.GetArea())
			{
				if (!Adjacent && !reach.HasBit(lattice.GetReachable(L))) continue;
				if (Adjacent && !reach.HasBit(lattice.GetReachable(L, 1))) continue;
				if (AvoidEdges && lattice.IsEdge(L)) continue;
				if (AvoidWater && lattice.HasLiquid(L)) continue;
				if (Careful && lattice.GetWeight(L) >= 10) continue;

				var C = Z.GetCell(L);
				if (AvoidCombat && C != CC && C.HasCombatObject()) continue;

				CellBuffer.Add(C);
			}

			return CellBuffer;
		}

		public List<Sector> GetAdjacentTo(Sector Current, bool Inclusive = false)
		{
			var adjacent = new List<Sector>();
			if (Inclusive) adjacent.Add(Current);
			if (Current.Settlement.Sectors.Count <= 1) return adjacent;

			foreach (var sector in Current.Settlement.Sectors)
			{
				if (Current.DistanceTo(sector) != 1) continue;
				adjacent.Add(sector);
			}

			return adjacent;
		}

		public List<GameObjectSource> GetAdjacentSectorReferences(ObjectPredicate Predicate, bool LiveOnly = true)
		{
			var refs = new List<GameObjectSource>();
			foreach (var adjacent in GetAdjacentTo(CurrentSector, Inclusive: true))
			{
				if (LiveOnly && !adjacent.IsLive) continue;

				var lattice = adjacent.Lattice.Check();
				var reach = lattice.GetReachable(CurrentCell);
				if (reach == 0) continue;

				var Z = lattice.Zone;
				for (int x = 0; x < Z.Width; x++)
				for (int y = 0; y < Z.Height; y++)
				{
					if (!reach.HasBit(lattice[x, y])) continue;

					var C = Z.Map[x][y];
					for (int i = 0; i < C.Objects.Count; i++)
					{
						if (C.Objects[i] == ParentObject) continue;
						if (C.Objects[i].IsReserved()) continue;
						if (!Predicate(C.Objects[i])) continue;
						refs.Add(new GameObjectSource(C.Objects[i]));
					}
				}
			}

			return refs;
		}

		public GameObject GetRandom(ObjectPredicate Predicate, bool AvoidCombat = false, bool Debug = false)
		{
			ObjectBag.Clear();
			foreach (var C in GetCells(Adjacent: true, AvoidCombat: AvoidCombat))
			foreach (var Object in C.Objects)
			{
				if (Object == ParentObject) continue;
				if (Object.IsReserved()) continue;
				if (!Predicate(Object)) continue;
				ObjectBag.Add(Object, 1);
			}

			return ObjectBag.PeekOne();
		}

		public GameObject GetRandomAdjacent(Cell CC, ObjectPredicate Predicate, bool AvoidCombat = false)
		{
			ObjectBag.Clear();
			foreach (var C in CC.YieldAdjacentCells(1, LocalOnly: true))
			foreach (var Object in C.Objects)
			{
				if (Object == ParentObject) continue;
				if (Object.IsReserved()) continue;
				if (!Predicate(Object)) continue;
				ObjectBag.Add(Object, 1);
			}

			return ObjectBag.PeekOne();
		}

		public GameObject GetWeighted(ObjectWeight Weight, bool AvoidCombat = false)
		{
			ObjectBag.Clear();
			foreach (var C in GetCells(Adjacent: true, AvoidCombat: AvoidCombat))
			foreach (var Object in C.Objects)
			{
				if (Object == ParentObject) continue;
				if (Object.IsReserved()) continue;

				var weight = Weight(Object);
				if (weight > 0) ObjectBag.Add(Object, weight);
			}

			return ObjectBag.PeekOne();
		}

		public Cell GetRandomCell(bool Careful = true, bool AvoidEdges = true, bool AvoidWater = true)
		{
			var area = IdleArea.GetArea();
			if (area.Count == 0) return null;
			if (ParentBrain.Aquatic) AvoidWater = false;

			var lattice = IdleArea.GetLattice();
			var reach = lattice.GetReachable(CurrentCell);
			for (int i = 0; i < 5; i++)
			{
				var L = area.GetRandomElement();
				if (reach.HasBit(lattice.GetReachable(L)))
				{
					if (AvoidEdges && lattice.IsEdge(L)) continue;
					if (AvoidWater && lattice.HasLiquid(L)) continue;
					if (Careful && lattice.GetWeight(L) >= 10) continue;
					return lattice.Zone.GetCell(L);
				}
			}

			var result = GetCells(Careful: Careful, AvoidEdges: AvoidEdges, AvoidWater: AvoidWater).GetRandomElement();
			return result;
		}

		/*public const int FLAG_CAREFUL = 1 << 0;
		public const int FLAG_COMBAT = 1 << 1;
		public const int FLAG_WATER = 1 << 2;
		public const int FLAG_EDGES = 1 << 3;
		
		public static void DestructureFlags(
			int Flags,
			out bool Careful,
			out bool Combat,
			out bool Water,
			out bool Edges
		)
		{
			Careful = (Flags & FLAG_CAREFUL) != 0;
			Combat = (Flags & FLAG_COMBAT) != 0;
			Water = (Flags & FLAG_WATER) != 0;
			Edges = (Flags & FLAG_EDGES) != 0;
		}*/

		public Cell GetRandom(CellPredicate Predicate, bool Careful = true, bool AvoidEdges = true, bool AvoidWater = true)
		{
			var area = IdleArea.GetArea();
			if (area.Count == 0) return null;
			if (ParentBrain.Aquatic) AvoidWater = false;

			CellBuffer.Clear();
			var lattice = IdleArea.GetLattice();
			var reach = lattice.GetReachable(CurrentCell);
			var Z = lattice.Zone;
			foreach (var L in area)
			{
				if (!reach.HasBit(lattice.GetReachable(L))) continue;
				if (AvoidEdges && lattice.IsEdge(L)) continue;
				if (AvoidWater && lattice.HasLiquid(L)) continue;
				if (Careful && lattice.GetWeight(L) >= 10) continue;

				var C = Z.GetCell(L);
				if (Predicate(C)) CellBuffer.Add(C);
			}

			return CellBuffer.GetRandomElement();
		}

		public Cell GetRandomAdjacent(Cell CC, CellPredicate Predicate, bool Careful = true, bool AvoidCombat = true, bool AvoidEdges = true, bool AvoidWater = true)
		{
			CellBuffer.Clear();
			var lattice = IdleArea.GetLattice();
			var reach = lattice.GetReachable(CC);
			foreach (var C in CC.YieldAdjacentCells(1, LocalOnly: true))
			{
				if (!reach.HasBit(lattice.GetReachable(C))) continue;
				if (AvoidEdges && lattice.IsEdge(C)) continue;
				if (AvoidWater && lattice.HasLiquid(C)) continue;
				if (Careful && lattice.GetWeight(C) >= 10) continue;
				if (AvoidCombat && C != CC && C.HasCombatObject()) continue;
				if (!Predicate(C)) continue;
				CellBuffer.Add(C);
			}

			return CellBuffer.GetRandomElement();
		}
	}
}