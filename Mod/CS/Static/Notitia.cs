using System;
using System.Collections;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using XRL;
using XRL.UI;
using XRL.World;
using XRL.Language;
using SimpleJSON;
using ConsoleLib.Console;
using XRL.Core;
using XRL.World.Parts;
using static Hearthpyre.Static;

namespace Hearthpyre
{
	[HasModSensitiveStaticCache]
	public static class Notitia
	{
		public static Dictionary<string, Category> Categories;
		public static List<Entry> Icons;
		public static List<Entry> Music;

		public static Dictionary<string, Category> AssertCategories => Assert(ref Categories);
		public static List<Entry> AssertIcons => Assert(ref Icons);
		public static List<Entry> AssertMusic => Assert(ref Music);

		[ModSensitiveCacheInit]
		public static void ColorInit()
		{
			LoadColorMap();
		}

		public static void Load()
		{
			Loading.LoadTask("Loading Hearthpyre.json", Init);
		}

		public static void Init()
		{
			try
			{
				var factory = GameObjectFactory.Factory;
				if (factory.Blueprints.IsNullOrEmpty()) return;
			}
			catch
			{
				var count = GameObjectFactory.Factory?.Blueprints?.Count ?? 0;
				if (count == 0) return;
			}
			
			if (Categories == null) Categories = new Dictionary<string, Category>();
			else Categories.Clear();

			if (Icons == null) Icons = new List<Entry>();
			else Icons.Clear();

			if (Music == null) Music = new List<Entry>();
			else Music.Clear();

			// Load hearthpyre data from this and other mods.
			ModManager.ForEachFile("Hearthpyre.json", (file, mod) =>
			{
				if (!mod.IsApproved) return;
				Log($"Loading Hearthpyre.json from {mod.ID}...");

				var data = JSON.Parse(File.ReadAllText(file)) as JSONClass;
				LoadCategories(data, "BLUEPRINTS");
				LoadEntries(data, "ICONS", Icons);
				LoadEntries(data, "MUSIC", Music);
			});
		}

		static void LoadEntries(JSONClass json, string key, List<Entry> list)
		{
			if (json[key] == null) return;

			foreach (JSONClass obj in json[key].AsArray)
			{
				list.Add(new Entry(obj["Name"], obj["Value"], obj["Detail"], obj["Package"]));
			}

			list.Sort((x, y) => String.CompareOrdinal(x.Name, y.Name));
		}

		public static void LoadCategories(JSONClass json, string key)
		{
			var blueprints = json[key];
			if (blueprints == null) return;

			foreach (KeyValuePair<string, JSONNode> pair in blueprints.AsObject)
			{
				var name = pair.Key;
				var node = pair.Value;

				if (!Categories.TryGetValue(name, out var category))
					category = new Category(node);

				var items = pair.Value["Items"].AsArray;
				foreach (JSONNode item in items)
				{
					if (GameObjectFactory.Factory.Blueprints.TryGetValue(item["Value"], out var blueprint))
					{
						category.AllBlueprints.Add(new Blueprint(item, blueprint));
					}
					else
					{
						Log("Unknown blueprint: " + item["Value"]);
					}
				}

				if (category.AllBlueprints.Count < 1) continue;
				category.AllBlueprints.Sort((x, y) => String.CompareOrdinal(x.Name, y.Name));
				Categories[name] = category;
			}
		}

		private static T Assert<T>(ref T Collection) where T : ICollection
		{
			if (Collection == null) Load();
			if (Collection.Count == 0)
			{
				Popup.ShowFail(
					"Error loading Hearthpyre blueprints, your mod install may be corrupt.\n\n"
					+ "Try reinstalling the mod and contact Armithaig if the issue persists."
				);
			}

			return Collection;
		}

		public class Blueprint
		{
			public string Name;
			public string Package;
			public string Author;
			public bool Tinkered;
			public bool Electrical;
			public bool Mechanical;
			public bool Hydraulic;

			private GameObjectBlueprint Alt;
			private GameObjectBlueprint Val;
			public GameObjectBlueprint Value
			{
				set => Val = value;
				get
				{
					if (The.Game.HasIntGameState(GST_PWR) && Alt != null) return Alt;
					return Val;
				}
			}

			public ConsoleChar Icon;

			public Blueprint(string Name, GameObjectBlueprint Value, string Bare = null, string Package = null, string Author = null)
			{
				this.Name = Name.Truncate(13, ".");
				this.Value = Value;
				this.Package = Package;
				this.Author = Author;
				this.Tinkered = Value.HasPartParameter(nameof(TinkerItem), "Bits");
				this.Electrical = Value.HasPart(nameof(ElectricalPowerTransmission));
				this.Mechanical = Value.HasPart(nameof(MechanicalPowerTransmission));
				this.Hydraulic = Value.HasPart(nameof(HydraulicPowerTransmission));

				Icon = new ConsoleChar { Foreground = CLD_BLK, Background = CLD_BLK, Detail = CLD_BLK };
				Icon.Imitate(Value);

				if (!Bare.IsNullOrEmpty()) GameObjectFactory.Factory.Blueprints.TryGetValue(Bare, out Alt);
			}

			public Blueprint(JSONNode node, GameObjectBlueprint Value) : this(
				node["Name"],
				Value,
				node["Bare"],
				node["Package"],
				node["Author"]
			)
			{
			}
		}

		public class Entry
		{
			public string Name;
			public string Value;
			public string Detail;
			public string Package;

			public Entry(string name, string value, string detail = null, string package = null)
			{
				Name = name;
				Value = value;
				Detail = detail;
				Package = package;
			}
		}

		public class Category
		{
			public string Name;
			public List<Blueprint> AllBlueprints;
			public List<Blueprint> Blueprints;
			public ConsoleChar Icon;

			private int Hash;

			public Category(string name, string icon = null, string color = null, string detail = null)
			{
				Name = name.Truncate(5, ".");
				AllBlueprints = new List<Blueprint>();
				Icon = new ConsoleChar { Foreground = CLD_BLK, Background = CLD_BLK, Detail = CLD_BLK };

				if (icon != null) Icon.Tile = icon;
				if (color != null) Icon.SetColorString(color);
				if (detail != null) Icon.Detail = ColorUtility.ColorMap[detail[0]];
			}

			public Category(JSONNode node) : this(
				node.Key,
				node["Icon"],
				node["Color"],
				node["Detail"]
			)
			{
			}

			public Category Filter(List<string> packages)
			{
				if (OptionAllBlueprints)
				{
					Blueprints = AllBlueprints;
					Hash = -1;
					return this;
				}

				var hash = packages.Aggregate(17, (h, p) => unchecked(h * 23 + p.GetHashCode()));
				if (Blueprints != null && Hash == hash) return this;

				var set = new HashSet<string>(packages);
				Blueprints = Blueprints ?? new List<Blueprint>(AllBlueprints.Count / 2);
				Blueprints.Clear();
				Hash = hash;

				foreach (var blueprint in AllBlueprints)
				{
					if (!String.IsNullOrEmpty(blueprint.Package) && !set.Contains(blueprint.Package))
						continue;

					Blueprints.Add(blueprint);
				}

				return this;
			}
		}
	}
}
