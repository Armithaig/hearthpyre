﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using XRL;
using XRL.Core;
using XRL.Rules;
using XRL.World;
using XRL.World.Parts;
using XRL.World.WorldBuilders;
using ConsoleLib.Console;
using Hearthpyre.UI;
using System.Linq;
using System.Reflection;
using Genkit;
using SimpleJSON;
using XRL.World.Parts.Skill;

namespace Hearthpyre
{
	public static partial class Static
	{
		/// <summary>
		/// Levenshtein string distance calc.
		/// </summary>
		public static int StrDist(string str, string txt) {
			str = str.ToLower(); txt = txt.ToLower();
			int sl = str.Length, tl = txt.Length;
			var matrix = new int[sl + 1, tl + 1];

			if (sl == 0) return tl;
			if (tl == 0) return sl;

			for (int i = 0; i <= sl; i++) matrix[i, 0] = i;
			for (int i = 0; i <= tl; i++) matrix[0, i] = i;

			for (int i = 1; i <= sl; i++) {
				for (int j = 1; j <= tl; j++) {
					int cost = (txt[j - 1] == str[i - 1]) ? 0 : 1;

					matrix[i, j] = Math.Min(
						Math.Min(matrix[i - 1, j] + 1, matrix[i, j - 1] + 1),
						matrix[i - 1, j - 1] + cost);
				}
			}
			return matrix[sl, tl];
		}
		/// <summary>
		/// Jaro winkler string proximity calc. Shamelessly stolen and modified.
		/// Also buggy. Hmm, don't steal kids.
		/// </summary>
		public static double StrProx(string str1, string str2) {
			str1 = str1.ToLower(); str2 = str2.ToLower();
			int l1 = str1.Length, l2 = str2.Length;
			if (l1 == 0) return l2 == 0 ? 1d : 0d;

			int range = Math.Max(0, Math.Max(l1, l2) / 2 - 1);
			bool[] match1 = new bool[l1], match2 = new bool[l2];

			var common = 0d;
			for (int i = 0; i < l1; i++) {
				for (int j = Math.Max(0, i - range), c = Math.Min(i + range + 1, l2); j < c; j++) {
					if (match2[j] || str1[i] != str2[j]) continue;

					match1[i] = true;
					match2[j] = true;
					common++;
					break;
				}
			}
			if (common <= 0d) return 0d;

			int transposed = 0;
			for (int i = 0, k = 0; i < l1; i++) {
				if (!match1[i]) continue;

				while (!match2[k]) k++;
				if (str1[i] != str2[k])
					transposed++;

				k++;
			}
			transposed /= 2;

			var weight = (common / l1 + common / l2 + (common - transposed) / common) / 3.0;
			if (weight <= 0.7) return weight;

			int lMax = Math.Min(4, Math.Min(str1.Length, str2.Length));
			int lPos = 0;
			while (lPos < lMax && str1[lPos] == str2[lPos]) lPos++;
			if (lPos == 0) return weight;

			return weight + 0.1 * lPos * (1.0 - weight);
		}
		/// <summary>
		/// Wrangle character indexes in RenderStrings of blueprints.
		/// </summary>
		public static string RenderStrToChar(string str) {
			if (str != null && str.Length > 1)
				str = ((char)Convert.ToInt32(str)).ToString();
			return str;
		}
		public static void SplitColorString(string color, out char fg, out char bg) {
			int iFG = -1, iBG = -1;
			for (int i = 0; i < color.Length; i++) {
				var chr = color[i];
				if (chr == '&') iFG = i + 1;
				else if (chr == '^') iBG = i + 1;
			}

			if (iFG >= 0 && iFG < color.Length) fg = color[iFG];
			else fg = 'k';

			if (iBG >= 0 && iBG < color.Length) bg = color[iBG];
			else bg = 'k';
		}
		/// <summary>
		/// Convert string to key.
		/// </summary>
		public static Keys StringToKey(string key) {
			try {
				return (Keys)Enum.Parse(typeof(Keys), key, true);
			} catch (Exception) {
				return Keys.None;
			}
		}
		/// <summary>
		/// Strip the color string of any invalid/unnecessary characters.
		/// </summary>
		public static string FilterColor(string color, int flags = 0) {
			var anchors = new[] { '&', '^', '*' };
			var map = ColorUtility.ColorMap;
			var sb = Strings.SB.Clear();
			var flag = 1;
			foreach (var anchor in anchors) {
				var i = color.LastIndexOf(anchor);
				if (i != -1 && !flags.HasBit(flag) && map.ContainsKey(color[i + 1]))
					sb.Append(anchor).Append(color[i + 1]);
				flag = flag << 1;
			}

			return sb.ToString();
		}

		private static readonly Type[] PowerTypes =
		{
			typeof(ElectricalPowerTransmission),
			typeof(MechanicalPowerTransmission),
			typeof(HydraulicPowerTransmission)
		};
		
		/// <summary>
		/// Brand object for later demolition and discard unwanted parts.
		/// </summary>
		public static void Brand(GameObject Object, bool Powered) {
			if (Object.pPhysics != null)
				Object.pPhysics.Owner = null;
			
			if (Object.TryTakePart(out Commerce commerce))
				commerce.Value = 0.01d;
			
			if (Object.TryTakePart(out TinkerItem tinker))
				tinker.CanDisassemble = false;
			
			if (Object.TryTakePart(out Examiner examiner))
				examiner.Complexity = 0;
			
			if (Object.TryTakePart(out EnergyCellSocket socket))
				socket.ChanceSlotted = 0;

			if (Object.TryTakePart(out Corpse corpse))
				corpse.CorpseChance = 0;

			if (Object.TryTakePart(out LiquidVolume volume))
			{
				Object.RemovePart<PopulationLiquidFiller>();
				volume.StartVolume = "0";
			}

			if (!Powered)
			{
				var removed = false;
				foreach (var type in PowerTypes)
				{
					removed |= Object.RemovePart(type);
				}
				if (removed)
				{
					Object.RemovePartsDescendedFrom<IPoweredPart>();
					if (Object.TryTakePart(out AnimatedMaterialGeneric anim))
					{
						anim.RequiresOperationalActivePart = null;
					}
				}
			}

			if (Object.TryTakePart(out ZeroPointEnergyCollector zpec))
			{
				zpec.World = "*";
			}

			Object.SetIntProperty(TAG_OBJ, 1);
		}

		public static void Brand(GameObject Object) => Brand(Object, Powered: !The.Game.HasIntGameState(GST_PWR));
		
		/// <summary>
		/// Add air to cell if empty and unpainted.
		/// </summary>
		public static void AddChasm(Cell C) {
			if (C.Objects.Count > 0) return;
			if (!String.IsNullOrEmpty(C.PaintTile)) return;
			if (!String.IsNullOrEmpty(C.PaintRenderString)) return;
			C.AddObject("Air");
		}
		/// <summary>
		/// Remove any chasm objects in cell.
		/// </summary>
		public static void RemoveChasm(Cell C)
		{
			for (int i = C.Objects.Count - 1; i >= 0; i--)
			{
				var obj = C.Objects[i];
				if (!obj.HasTag("Stairs")) continue;
				if (obj.TryTakePart(out StairsDown down) && down.PullDown)
				{
					C.RemoveObject(obj);
				}
			}
		}
		
		public static Queue<Cell> CellQueue = new Queue<Cell>();
		public static int FloodValue = 0;
		public static int[,] FloodMap = new int[80, 25];
		/// <summary>
		/// Yield cells in a cardinal flood pattern from origin. Inclusive.
		/// </summary>
		public static IEnumerable<Cell> YieldCardinalFlood(this Cell C, Predicate<Cell> Predicate)
		{
			if (!Predicate(C)) yield break;
			
			var Z = C.ParentZone;
			CellQueue.Clear();
			CellQueue.Enqueue(C);
			FloodValue = unchecked(FloodValue + 1);
			FloodMap[C.X, C.Y] = FloodValue;

			while (CellQueue.Count != 0) {
				int x2 = 1, y2 = 0;
				var SC = CellQueue.Dequeue();
				yield return SC;

				for (int i = 0; i < 4; i++) {
					int b = x2, x3 = SC.X + x2, y3 = SC.Y + y2;
					x2 = -y2;
					y2 = b;

					if (x3 < 0 || x3 >= Z.Width || y3 < 0 || y3 >= Z.Height)
						continue;

					if (FloodMap[x3, y3] == FloodValue) continue;
					FloodMap[x3, y3] = FloodValue;

					if (Predicate(Z.Map[x3][y3]))
						CellQueue.Enqueue(Z.Map[x3][y3]);
				}
			}
		}
		/// <summary>
		/// Yield objects in a cardinal flood pattern from origin. Inclusive.
		/// </summary>
		public static IEnumerable<GameObject> YieldCardinalFlood(this Cell C, Predicate<GameObject> Predicate) {
			var Z = C.ParentZone;
			var queued = false;
			CellQueue.Clear();
			FloodValue = unchecked(FloodValue + 1);
			FloodMap[C.X, C.Y] = FloodValue;
			for (int j = C.Objects.Count - 1; j >= 0; j--)
			{
				if (!Predicate(C.Objects[j])) continue;
				yield return C.Objects[j];
				
				if (queued) continue;
				CellQueue.Enqueue(C);
				queued = true;
			}

			while (CellQueue.Count != 0) {
				int x2 = 1, y2 = 0;
				var SC = CellQueue.Dequeue();

				for (int i = 0; i < 4; i++) {
					int b = x2, x3 = SC.X + x2, y3 = SC.Y + y2;
					x2 = -y2;
					y2 = b;

					if (x3 < 0 || x3 >= Z.Width || y3 < 0 || y3 >= Z.Height)
						continue;

					if (FloodMap[x3, y3] == FloodValue) continue;
					FloodMap[x3, y3] = FloodValue;
					queued = false;

					var FC = Z.Map[x3][y3];
					for (int j = FC.Objects.Count - 1; j >= 0; j--)
					{
						if (!Predicate(FC.Objects[j])) continue;
						yield return FC.Objects[j];
						
						if (queued) continue;
						CellQueue.Enqueue(FC);
						queued = true;
					}
				}
			}
		}

		/// <summary>
		/// Yield coordinates in a circular pattern from origin.
		/// </summary>
		public static IEnumerable<int[]> LazyCircle(int cx, int cy, int r) {
			if (r == 0) { yield return new[] { cx, cy }; yield break; }

			var rsq = r * r;
			for (int y = -r; y <= r; y++) {
				var ysq = y * y;
				for (int x = -r; x <= r; x++) {
					if (x * x + ysq <= rsq)
						yield return new[] { cx + x, cy + y };
				}
			}
		}

		/// <summary>
		/// Spirals out from coordinates, yielding each one.
		/// </summary>
		public static IEnumerable<int[]> LazySpiral(int x, int y) {
			int x2 = 1, y2 = 0, l = 1;

			for (int i = 1; true; i++) {
				x += x2;
				y += y2;

				if (i >= l) {
					i = 0;

					int b = x2;
					x2 = -y2;
					y2 = b;

					if (y2 == 0) l++;
				}

				yield return new[] { x, y };
			}
		}

		/// <summary>
		/// Log message.
		/// </summary>
		public static void Log(string message, Exception e = null) {
			message = "HEARTHPYRE - " + message;
			if (e != null) XRLCore.LogError(message, e);
			else XRLCore.Log(message);
		}

		/// <summary>
		/// Round float to integer.
		/// </summary>
		public static int RoundToInt(float value) {
			return (int)Math.Round(value);
		}
		/// <summary>
		/// Round double to integer.
		/// </summary>
		public static int RoundToInt(double value) {
			return (int)Math.Round(value);
		}

		public static void HoloZap(Cell C) {
			C.PlayWorldSound("Electric", 0.25f);
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 3; j++) {
					C.ParticleText("&B" + (char)(219 + Stat.RandomCosmetic(0, 4)), 2.5f, 4);
				}
				for (int l = 0; l < 3; l++) {
					C.ParticleText("&c" + (char)(219 + Stat.RandomCosmetic(0, 4)), 3.5f, 3);
				}
			}
		}

		public static void PlayUISound(string Clip, float Volume = 1f) => SoundManager.PlaySound(Clip, Volume: Volume);

		public static void MenuSound() {
			PlayUISound(SND_MENU);
		}

		public static int OppositeCourse(int Course)
		{
			switch (Course)
			{
				case UP: return DOWN;
				case DOWN: return UP;
				case NORTH: return SOUTH;
				case NORTHEAST: return SOUTHWEST;
				case EAST: return WEST;
				case SOUTHEAST: return NORTHWEST;
				case SOUTH: return NORTH;
				case SOUTHWEST: return NORTHEAST;
				case WEST: return EAST;
				case NORTHWEST: return SOUTHEAST;
				default: return 0;
			}
		}

		/// <summary>
		/// Shove some space and lines around a makeshift title since we can't set titles on popups at the moment.
		/// </summary>
		public static string MakeshiftPopupTitle(string title) {
			var spacer = " \u00c4 \u00c4 \u00c4 ";
			return spacer + title + spacer;
		}

		public static bool CheckEpistemicStatus(string Blueprint, int Status = Examiner.EPISTEMIC_STATUS_KNOWN)
		{
			return Examiner.GetUnderstanding(Blueprint) == Status;
		}

		public static int GetMaxAppointedRetainers()
		{
			if (!The.Player.OwnPart<HearthpyreGoverning_Mayor>()) return 0;

			var result = 1;
			if (The.Player.OwnPart<HearthpyreGoverning_GovernI>()) result++;
			
			return result;
		}

		public static string GetZoneBareDisplayName(string ZoneID)
		{
			var result = The.Game.GetStringGameState("ZoneName_" + ZoneID, null);
			if (!result.IsNullOrEmpty()) return result;
			
			result = The.ZoneManager.GetZoneBlueprint(ZoneID)?.Name;
			if (!result.IsNullOrEmpty()) return result;
			
			return ZoneID.IndexOf('.') != -1 ? "" : WorldFactory.Factory.getWorld(ZoneID).DisplayName;
		}
		
		public static bool NoStructure(Cell C)
        {
            if (C.Objects.Count == 0) return true;
            GameObject obj;
            for (int i = 0; i < C.Objects.Count; i++)
            {
                obj = C.Objects[i];
                if (obj.pRender == null) continue;
                if (obj.HasIntProperty("Wall")) return false;
                if (obj.HasTag("Door")) return false;
            }

            return true;
		}
	}
}
