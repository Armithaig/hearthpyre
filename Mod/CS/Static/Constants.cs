using System;
using System.Collections.Generic;
using System.Linq;

using XRL;
using XRL.Rules;
using XRL.Core;
using XRL.World;
using ConsoleLib.Console;
using XRL.World.Effects;
using XRL.World.Parts;
using Color = UnityEngine.Color;

using static XRL.UI.Options;

namespace Hearthpyre
{
	[HasOptionFlagUpdate]
	public static partial class Static
	{
		// Directions
		public const int NORTH = 1 << 0;
		public const int EAST = 1 << 1;
		public const int SOUTH = 1 << 2;
		public const int WEST = 1 << 3;
		public const int UP = 1 << 4;
		public const int DOWN = 1 << 5;
		public const int NORTHEAST = NORTH | EAST;
		public const int SOUTHEAST = SOUTH | EAST;
		public const int NORTHWEST = NORTH | WEST;
		public const int SOUTHWEST = SOUTH | WEST;
		public const int OMNIDIR = NORTHEAST | SOUTHWEST | UP | DOWN;
		public static readonly int[] Directions = { NORTH, EAST, SOUTH, WEST, NORTHEAST, SOUTHEAST, NORTHWEST, SOUTHWEST };
		public static readonly int[] CardinalDirections = { NORTH, EAST, SOUTH, WEST };
		public static readonly Dictionary<int, string> DirectionMap = new Dictionary<int, string>
		{
			{ NORTHWEST, "NW" }, { NORTH, "N" }, { NORTHEAST, "NE" },
			{ EAST, "E" }, { 0, "." }, { WEST, "W" },
			{ SOUTHWEST, "SW" }, { SOUTH, "S" }, { SOUTHEAST, "SE" },
			{ UP, "U" }, { DOWN, "D" }
		};
		// IDs
		public const string MOD_ID = "Hearthpyre";
		public const string TAG_OBJ = "HearthpyreObject";
		public const string TAG_PNT = "HearthpyrePaintedObject";
		public const string TAG_STLR = "HearthpyreSettler";
		public const string OBJ_SCMR = "HearthpyreXyloschemer";
		public const string OBJ_BLPR = "HearthpyreBlueprint";
		public const string OBJ_SECT = "HearthpyreSectorWidget";
		public const string OBJ_CART = "HearthpyreHandcart";
		public const string FAC_STLR = "HearthpyreSettlers";
		public const string SCT_GRFT = "$hpsnapgrifter";
		public const string GST_PWR = "HearthpyrePoweredBlueprint";
		public const string GST_IT_SCMR = "HearthpyreXyloschemerAcquired";
		public const string GST_SG_BLPR = "HearthpyreSnapgrifterTrade";
        public const string CST_SG_BLPR = GST_SG_BLPR;
        public const string CST_SG_SLOT = "HearthpyreSnapgrifterSlot";
        public const string CST_SG_CLR = "HearthpyreSnapgrifterColor";
        public const string CST_SG_OPT = "HearthpyreSnapgrifterOptions";
        public const string CST_SG_ITEM = "HearthpyreSnapgrifterItem";
        public const string CST_IV_COST = "HearthpyreInviteCost";
        public const string CST_IV_OPT = "HearthpyreInviteOptions";
		// Options
		public static bool OptionDeployCart { get; private set; }
		public static int OptionSimActive { get; private set; }
		public static int OptionInvCost { get; private set; }
		public static bool OptionAllBlueprints { get; private set; }
		public static bool OptionAllowCombat { get; private set; }
		public static bool OptionAllDemo { get; private set; }
		public static bool OptionAllInvite { get; private set; }
		public static bool OptionAllClaim { get; private set; }
		public static bool OptionNoBits { get; private set; }
		public static bool OptionFastStart { get; private set; }
		// Sounds
		public const string SND_ADD = "ui_option_click";
		public const string SND_SEL = "ui_option_click";
		public const string SND_DSEL = "ui_checkbox_toggle";
		public const string SND_REM = "ui_checkbox_toggle";
		public const string SND_ERR = "ui_invalid";
		public const string SND_MENU = "ui_cursor_scroll";
		public const string SND_TEXT = "sfx_interact_book_pageTurn";
		public const string SND_CLM = "sfx_quest_total_complete";
		public const string SND_ANX = "sfx_ability_mutation_mental_generic_activate";
		public const string SND_LVLO = "sfx_npc_level";
		public const string SND_TDRO = "sfx_interact_door_metal_open";
		public const string SND_TLS = "sfx_ability_teleport_voluntary_out";
		public const string SND_TLL = "sfx_ability_teleport_voluntary_out";
		public const string SND_TLW = "sfx_ability_teleport_voluntary_out";
		public const string SND_CLWH = "compartment_close_whine_up";
		// Colours
		public static Color CLD_BLK { get; private set; }
		public static Color CLD_BLUE { get; private set; }
		public static Color CLD_GRN { get; private set; }
		public static Color CLD_CYAN { get; private set; }
		public static Color CLD_RED { get; private set; }
		public static Color CLD_MAG { get; private set; }
		public static Color CLD_YEL { get; private set; }
		public static Color CLD_WHT { get; private set; }
		public static Color CLD_ORNG { get; private set; }
		public static Color CLB_BLK { get; private set; }
		public static Color CLB_BLUE { get; private set; }
		public static Color CLB_GRN { get; private set; }
		public static Color CLB_CYAN { get; private set; }
		public static Color CLB_RED { get; private set; }
		public static Color CLB_MAG { get; private set; }
		public static Color CLB_YEL { get; private set; }
		public static Color CLB_WHT { get; private set; }
		public static Color CLB_ORNG { get; private set; }
		// Tiles
		public const string ICO_ORG = "Terrain/sw_monument3.bmp";
		public const string ICO_CLM = "Terrain/hp_claim.png";
		// View
		public const int PEN_X = 4;
		public const int FILL_X = 25;
		public const int HELP_X = 67;
		// Retainers
		public const int RET_MERCHANT = 1;
		public const int RET_TINKER = 2;
		public const int RET_WARDEN = 3;
		public const int RET_MINSTREL = 4;
		public const int RET_APOTHECARY = 5;
		public static readonly Dictionary<string, int> RetainerMap = new Dictionary<string, int>
		{
			{ "Merchant", RET_MERCHANT }, { "Tinker", RET_TINKER },
			{ "Warden", RET_WARDEN }, { "Minstrel", RET_MINSTREL },
			{ "Apothecary", RET_APOTHECARY },
		};

		public static void LoadColorMap() {
			CLD_BLK = ColorUtility.ColorMap['k'];
			CLD_BLUE = ColorUtility.ColorMap['b'];
			CLD_GRN = ColorUtility.ColorMap['g'];
			CLD_CYAN = ColorUtility.ColorMap['c'];
			CLD_RED = ColorUtility.ColorMap['r'];
			CLD_MAG = ColorUtility.ColorMap['m'];
			CLD_YEL = ColorUtility.ColorMap['w'];
			CLD_WHT = ColorUtility.ColorMap['y'];
			CLD_ORNG = ColorUtility.ColorMap['o'];
			CLB_BLK = ColorUtility.ColorMap['K'];
			CLB_BLUE = ColorUtility.ColorMap['B'];
			CLB_GRN = ColorUtility.ColorMap['G'];
			CLB_CYAN = ColorUtility.ColorMap['C'];
			CLB_RED = ColorUtility.ColorMap['R'];
			CLB_MAG = ColorUtility.ColorMap['M'];
			CLB_YEL = ColorUtility.ColorMap['W'];
			CLB_WHT = ColorUtility.ColorMap['Y'];
			CLB_ORNG = ColorUtility.ColorMap['O'];
		}

		[OptionFlagUpdate]
		public static void UpdateFlags()
		{
			OptionDeployCart = GetOption("OptionHearthpyreDeployCart", "Yes").EqualsNoCase("Yes");
			OptionSimActive = int.TryParse(GetOption("OptionHearthpyreSimActive", "1"), out var value) ? value : 1;
			OptionInvCost = int.TryParse(GetOption("OptionHearthpyreInvCost", "100"), out value) ? value : 100;
			OptionAllBlueprints = GetOption("OptionHearthpyreAllBlueprints", "No").EqualsNoCase("Yes");
			OptionAllowCombat = GetOption("OptionHearthpyreAllowCombat", "No").EqualsNoCase("Yes");
			OptionAllDemo = GetOption("OptionHearthpyreAllDemo", "No").EqualsNoCase("Yes");
			OptionAllInvite = GetOption("OptionHearthpyreAllInvite", "No").EqualsNoCase("Yes");
			OptionAllClaim = GetOption("OptionHearthpyreAllClaim", "No").EqualsNoCase("Yes");
			OptionNoBits = GetOption("OptionHearthpyreNoBits", "No").EqualsNoCase("Yes");
			OptionFastStart = GetOption("OptionHearthpyreFastStart", "No").EqualsNoCase("Yes");
		}
	}
}
