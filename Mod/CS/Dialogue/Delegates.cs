﻿using XRL.World.Conversations;

namespace Hearthpyre.Dialogue
{
    [HasConversationDelegate]
    public static class Delegates
    {
        [ConversationDelegate]
        public static bool IfHaveSettlements(DelegateContext Context)
        {
            if (int.TryParse(Context.Value, out var val))
            {
                return RealmSystem.Settlements.Count >= val;
            }

            return false;
        }
    }
}