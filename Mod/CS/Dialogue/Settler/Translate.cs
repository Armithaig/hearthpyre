﻿using System;
using System.Collections.Generic;
using XRL;
using XRL.World.Conversations;
using XRL.World.Parts;

namespace Hearthpyre.Dialogue.Settler
{
    public class Translate : IConversationPart
    {
        public Dictionary<string, ConversationXMLBlueprint> Script;
        public Dictionary<string, ConversationXMLBlueprint> Species;
        public ConversationXMLBlueprint Humanoid;

        public Translate()
        {
            Propagation = PROPAGATE_NONE;
        }

        public override bool LoadChild(ConversationXMLBlueprint Blueprint)
        {
            var type = Blueprint.Attributes["Type"];
            if (type == "Script")
            {
                if (Script == null) Script = new Dictionary<string, ConversationXMLBlueprint>(StringComparer.Ordinal);
                Script[Blueprint.ID] = Blueprint;
            }
            else if (type == "Species")
            {
                if (Species == null) Species = new Dictionary<string, ConversationXMLBlueprint>(StringComparer.OrdinalIgnoreCase);
                Species[Blueprint.ID] = Blueprint;
            }
            else if (type == "Property" && Blueprint.ID == "Humanoid")
            {
                Humanoid = Blueprint;
            }

            return base.LoadChild(Blueprint);
        }

        public override void Awake()
        {
            var speaker = The.Speaker;
            if (
                speaker.TryTakePart(out ConversationScript script)
                && !Script.IsNullOrEmpty()
                && Script.TryGetValue(script.ConversationID, out var blueprint)
            )
            {
                ParentElement.LoadChild(blueprint);
                return;
            }
            
            if (
                !Species.IsNullOrEmpty()
                && speaker.GetBlueprint().Tags.TryGetValue("Species", out var species)
                && Species.TryGetValue(species, out blueprint)
            )
            {
                ParentElement.LoadChild(blueprint);
                return;
            }

            if (Humanoid != null && speaker.GetIntProperty("Humanoid") >= 1)
            {
                ParentElement.LoadChild(Humanoid);
                return;
            }

            /*if (script != null && script.TryGetSounds(out var speaks, out var sounds))
            {
                ParentElement.Texts.Add(new ConversationText
                {
                    Text = sounds.GetRandomElement()
                });
            }*/
            
            // TODO: remove this part when safe to do so, releasing fields for now
            Script = null;
            Species = null;
            Humanoid = null;

            base.Awake();
        }
    }
}