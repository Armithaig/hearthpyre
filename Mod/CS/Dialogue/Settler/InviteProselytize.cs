﻿using System;
using XRL;
using XRL.World.AI;
using XRL.World.Capabilities;
using XRL.World.Conversations;
using XRL.World.Parts.Skill;

namespace Hearthpyre.Dialogue.Settler
{
	public class InviteProselytize : IAbilityInvite
	{
		public Persuasion_Proselytize Part;

		public override void Awake()
		{
			Part = The.Player.TakePart<Persuasion_Proselytize>();
			Entry = Part?.MyActivatedAbility(Part.ActivatedAbilityID);
			Act = "solicitation";
			Cooldown = 25;
		}

#if !BUILD_2_0_206
		public override void Success()
		{
			The.Speaker.AddOpinion<OpinionProselytize>(The.Player);
		}
#endif

		public override bool HandleEvent(GetTargetElementEvent E)
		{
			var attacker = The.Player;
			var defender = The.Speaker;
			var difficulty = Math.Max(defender.Stat("Level") - attacker.Stat("Level"), 0);
			Result = Mental.PerformAttack(
				static _ => true,
				Attacker: attacker,
				Defender: defender,
				Command: "Proselytize",
				Dice: "1d8-6",
				Type: Mental.TYPE_SONIC,
				AttackModifier: attacker.StatMod("Ego"),
				DefenseModifier: difficulty
			);

			if (!Result) E.Target = Target;
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(GetChoiceTagEvent E)
		{
			E.Tag = IsCoolingDown ? "{{[R|[Proselytize]}}" : "{{W|[Proselytize]}}";
			return false;
		}
	}
}