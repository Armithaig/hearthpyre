﻿using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.World.Conversations;
using XRL.World.Parts;

namespace Hearthpyre.Dialogue.Settler
{
    public class SelectSettlement : IConversationPart
    {
        public Settlement Settlement;
        public string Directions = "";

        public override bool WantEvent(int ID, int Propagation)
        {
            return base.WantEvent(ID, Propagation)
                   || ID == EnterElementEvent.ID
                   || ID == IsElementVisibleEvent.ID
                   || ID == PrepareTextEvent.ID
                ;
        }

        public override void Awake()
        {
            if (!TryGetState(Static.CST_IV_OPT, out List<Settlement> options)) return;

            Settlement = options.RemoveRandomElement();
            if (Settlement != null)
            {
                Directions = LoreGenerator.GenerateLandmarkDirectionsTo(Settlement.Prime.ZoneID);
            }
        }

        public override bool HandleEvent(IsElementVisibleEvent E)
        {
            if (Settlement == null) return false;
            if (Settlement == The.Speaker.TakePart<HearthpyreSettler>()?.Settlement) return false;
            return base.HandleEvent(E);
        }

        public override bool HandleEvent(PrepareTextEvent E)
        {
            E.Text.Replace("=settlement.name=", Settlement.Name);
            E.Text.Replace("=settlement.directions=", Directions);
            return base.HandleEvent(E);
        }

        public override bool HandleEvent(EnterElementEvent E)
        {
            if (Settlement != null)
            {
                if (!Settlement.Move(The.Speaker)) return false;
                if (TryGetState(Static.CST_IV_COST, out int cost) && cost != 0)
                {
                    The.Game.PlayerReputation.modify(The.Speaker.pBrain.GetPrimaryFaction(), cost, true);
                    State.Remove(Static.CST_IV_COST);
                }
            }

            return base.HandleEvent(E);
        }
    }
}