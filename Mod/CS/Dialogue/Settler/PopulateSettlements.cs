﻿using System;
using System.Collections.Generic;
using Genkit;
using Hearthpyre.Realm;
using XRL;
using XRL.World;
using XRL.World.Conversations;
using XRL.World.Parts.Mutation;
using XRL.World.Parts.Skill;
using static Hearthpyre.Static;

namespace Hearthpyre.Dialogue.Settler
{
    public class PopulateSettlements : IConversationPart
    {
        public ConversationXMLBlueprint Choice;
        public Faction Faction;
        public int Cost;

        public override bool LoadChild(ConversationXMLBlueprint Blueprint)
        {
            Choice = Blueprint;
            return base.LoadChild(Blueprint);
        }

        public override void Awake()
        {
            Faction = Factions.getIfExists(The.Speaker.GetPrimaryFaction());

            var options = new List<Settlement>();
            if (!The.Speaker.HasIntProperty(TAG_STLR) && Faction != null)
            {
                State[CST_IV_COST] = Cost = -CalculateRepCost(The.Speaker);
            }
            
            foreach (var settlement in RealmSystem.Settlements.Values)
            {
                options.Add(settlement);
                ParentElement.LoadChild(Choice);
            }

            State[CST_IV_OPT] = options;
        }

        public override bool WantEvent(int ID, int Propagation)
        {
            if (Cost == 0) return false;
            return ID == DisplayTextEvent.ID;
        }

        public override bool HandleEvent(DisplayTextEvent E)
        {
            var abs = Math.Abs(Cost);
            E.Text.Append("\n\n{{C|-----}}\n{{y|Your reputation with {{C|").Append(Faction.getFormattedName())
                .Append("}} is {{C|").Append(Faction.CurrentReputation).Append("}}.\n")
                .Append("It will ").Append(Cost > 0 ? "award" : "cost").Append(" {{C|").Append(abs).Append("}} reputation to invite ")
                .Append(The.Speaker.t(Stripped: true)).Append('.');

            return base.HandleEvent(E);
        }

        public int CalculateRepCost(GameObject Object) {
            var result = 100d;

            if (Object.Statistics.TryGetValue("Level", out var level))
                result = level.Value * 2.5d;

            if (Object.IsHero())
                result *= 1.25;

            if (The.Player.OwnPart<SociallyRepugnant>())
                result *= 1.1;

            if (The.Player.OwnPart<Customs_Tactful>())
                result *= 0.75;

            return Calc.Clamp((int)(Math.Round(result / 5d) * 5d), 25, 250) * OptionInvCost / 100;
        }
    }
}