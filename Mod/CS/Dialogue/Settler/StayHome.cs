﻿using System;
using XRL;
using XRL.World.Conversations;
using XRL.World.Effects;
using XRL.World.Parts;

namespace Hearthpyre.Dialogue.Settler
{
	public class StayHome : IConversationPart
	{
		public HearthpyreSettler Settler;

		public override void Awake()
		{
			Settler = The.Speaker.TakePart<HearthpyreSettler>();
		}

		public override bool WantEvent(int ID, int Propagation)
		{
			return base.WantEvent(ID, Propagation)
			       || ID == IsElementVisibleEvent.ID
			       || ID == GetTextElementEvent.ID
			       || ID == EnteredElementEvent.ID
				;
		}

		public override bool HandleEvent(IsElementVisibleEvent E)
		{
			if (Settler == null || Settler.HomeID == Guid.Empty) return false;
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(GetTextElementEvent E)
		{
			if (Settler.Grounded)
			{
				E.Selected = E.Texts.Find(x => x.ID == "Retract") ?? E.Selected;
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(EnteredElementEvent E)
		{
			Settler.Grounded = !Settler.Grounded;
			Settler.Handler.OverrideIdleArea();
			The.Speaker.pBrain.Goals.Clear();
			return base.HandleEvent(E);
		}
	}
}