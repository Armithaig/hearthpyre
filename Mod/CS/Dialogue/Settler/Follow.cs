﻿using XRL;
using XRL.World.Conversations;
using XRL.World.Effects;

namespace Hearthpyre.Dialogue.Settler
{
    public class Follow : IConversationPart
    {
        public override bool WantEvent(int ID, int Propagation)
        {
            return base.WantEvent(ID, Propagation)
                   || ID == IsElementVisibleEvent.ID
                   || ID == EnteredElementEvent.ID
                ;
        }

        public override bool HandleEvent(IsElementVisibleEvent E)
        {
            if (!The.Speaker.IsPotentiallyMobile()) return false;
            return base.HandleEvent(E);
        }

        public override bool HandleEvent(EnteredElementEvent E)
        {
            The.Speaker.ApplyEffect(new HearthpyreFollowing(The.Player, 100));
            return base.HandleEvent(E);
        }
    }
}