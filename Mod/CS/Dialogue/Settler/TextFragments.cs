﻿using XRL;
using XRL.World.Conversations;

namespace Hearthpyre.Dialogue.Settler
{
    public class TextFragments : IConversationPart
    {
        private static readonly string[] DefaultPoeticFeatures = { "shiny teeth", "coarse hair", "sunken eyes" };
        private static readonly string[] DefaultActivities = { "roaming around idly" };
        private static readonly string[] DefaultVillageActivities = { "sleeping in our homes" };
        private static readonly string[] DefaultSacredThings = { "the act of procreating" };
        private static readonly string[] DefaultArableLands = { "arable land" };
        private static readonly string[] DefaultValuedOres = { "precious metals" };

        public string[] PoeticFeatures = DefaultPoeticFeatures;
        public string[] Activities = DefaultActivities;
        public string[] VillageActivities = DefaultVillageActivities;
        public string[] SacredThings = DefaultSacredThings;
        public string[] ArableLands = DefaultArableLands;
        public string[] ValuedOres = DefaultValuedOres;
        public string Person;

        public override void Awake()
        {
            if (!The.Speaker.GetBlueprint().xTags.TryGetValue("TextFragments", out var fragments)) return;
            if (TryGetState("SpeakerTextFragments", out TextFragments part))
            {
                PoeticFeatures = part.PoeticFeatures;
                Activities = part.Activities;
                VillageActivities = part.VillageActivities;
                SacredThings = part.SacredThings;
                ArableLands = part.ArableLands;
                ValuedOres = part.ValuedOres;
                Person = part.Person;
                return;
            }

            if (fragments.TryGetValue("PoeticFeatures", out var value) && !value.IsNullOrEmpty())
            {
                PoeticFeatures = value.Split(',');
            }

            if (fragments.TryGetValue("Activity", out value) && !value.IsNullOrEmpty())
            {
                Activities = value.Split(',');
            }

            if (fragments.TryGetValue("VillageActivity", out value) && !value.IsNullOrEmpty())
            {
                VillageActivities = value.Split(',');
            }
            
            if (fragments.TryGetValue("SacredThing", out value) && !value.IsNullOrEmpty())
            {
                SacredThings = value.Split(',');
            }
            
            if (fragments.TryGetValue("ArableLand", out value) && !value.IsNullOrEmpty())
            {
                ArableLands = value.Split(',');
            }
            
            if (fragments.TryGetValue("ValuedOre", out value) && !value.IsNullOrEmpty())
            {
                ValuedOres = value.Split(',');
            }

            var species = The.Speaker.GetPropertyOrTag("Species");
            var self = The.Player.GetPropertyOrTag("Species");
            if (species == null || species == self) Person = The.Speaker.personTerm;
            else Person = species;

            State["SpeakerTextFragments"] = this;
        }

        public override bool WantEvent(int ID, int Propagation)
        {
            return base.WantEvent(ID, Propagation)
                   || ID == PrepareTextEvent.ID
                ;
        }

        public override bool HandleEvent(PrepareTextEvent E)
        {
            if (E.Text.Contains("=fragment."))
            {
                E.Text.Replace("=fragment.poetic=", PoeticFeatures.GetRandomElement());
                E.Text.Replace("=fragment.activity=", Activities.GetRandomElement());
                E.Text.Replace("=fragment.village=", VillageActivities.GetRandomElement());
                E.Text.Replace("=fragment.sacred=", SacredThings.GetRandomElement());
                E.Text.Replace("=fragment.arable=", ArableLands.GetRandomElement());
                E.Text.Replace("=fragment.ore=", ValuedOres.GetRandomElement());
                E.Text.Replace("=fragment.person=", Person);
            }

            return base.HandleEvent(E);
        }
    }
}