﻿using System;
using XRL;
using XRL.World.AI;
using XRL.World.Capabilities;
using XRL.World.Conversations;
using XRL.World.Parts.Mutation;

namespace Hearthpyre.Dialogue.Settler
{
	public class InviteBeguile : IAbilityInvite
	{
		public Beguiling Part;

		public override void Awake()
		{
			Part = The.Player.TakePart<Beguiling>();
			Entry = Part?.MyActivatedAbility(Part.ActivatedAbilityID);
			Act = "coquetry";
			Cooldown = 50;
		}

#if !BUILD_2_0_206
		public override void Success()
		{
			The.Speaker.AddOpinion<OpinionBeguile>(The.Player);
		}
#endif

		public override bool HandleEvent(GetTargetElementEvent E)
		{
			var attacker = The.Player;
			var defender = The.Speaker;
			var bonus = attacker.Stat("Level") + Math.Max(attacker.StatMod("Ego"), Part.Level);
			Result = Mental.PerformAttack(
				x => true,
				Attacker: attacker,
				Defender: defender,
				Command: "Beguiling",
				Dice: "1d8",
				Type: Mental.TYPE_PSIONIC,
				AttackModifier: bonus,
				DefenseModifier: defender.Stat("Level")
			);

			if (!Result) E.Target = Target;
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(GetChoiceTagEvent E)
		{
			E.Tag = IsCoolingDown ? "{{R|[Beguile]}}" : "{{M|[Beguile]}}";
			return false;
		}
	}
}