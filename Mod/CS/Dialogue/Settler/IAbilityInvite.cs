﻿using XRL;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Capabilities;
using XRL.World.Conversations;
using XRL.World.Effects;
using XRL.World.Parts;

namespace Hearthpyre.Dialogue.Settler
{
	public abstract class IAbilityInvite : IConversationPart
	{
		public ActivatedAbilityEntry Entry;
		public bool IsCoolingDown;
		public bool Result;
		public string Target = "End";
		public string Act = "act";
		public int Cooldown = 10;

		public virtual void Success()
		{
#if BUILD_2_0_206
			The.Speaker.SetFeeling(The.Player, 25);
#else
			The.Speaker.AddOpinion<OpinionProselytize>(The.Player);
#endif
		}

		public virtual void Failure()
		{
			if (20.in100())
			{
				Messaging.XDidYToZ(The.Speaker, "are", "infuriated by", The.Player, Act, "!", PossessiveObject: true, ColorAsBadFor: The.Player);
#if BUILD_2_0_206
				The.Speaker.GetAngryAt(The.Player, -10);
#else
				The.Speaker.AddOpinion<OpinionCoquetry>(The.Player);
#endif
				if (The.Speaker.IsHostileTowards(The.Player)) return;
			}

			var dir = The.Speaker.GetDirectionToward(The.Player);
			if (The.Speaker.IsMobile()) The.Speaker.pBrain.PushGoal(new Flee(The.Player, 25));
			Messaging.XDidYToZ(The.Speaker, "push", null, The.Player);
			CombatJuice.punch(The.Speaker, The.Player);
			CombatJuice.playWorldSound(The.Speaker, "Hit_Default");
			The.Player.pPhysics.Push(dir, 1000, 1, Actor: The.Speaker);
			The.Player.ApplyEffect(new Prone());
			The.Speaker.ApplyEffect(new HearthpyreMiffed(The.Player, 25));
			The.Player.ForfeitTurn(); // workaround for prone not working properly, though how it's "supposed" to work is debatable
		}

		public override bool WantEvent(int ID, int Propagation)
		{
			return base.WantEvent(ID, Propagation)
			       || ID == IsElementVisibleEvent.ID
			       || ID == GetTargetElementEvent.ID
			       || ID == EnterElementEvent.ID
			       || ID == EnteredElementEvent.ID
			       || ID == ColorTextEvent.ID
			       || ID == GetChoiceTagEvent.ID
				;
		}

		public override bool HandleEvent(IsElementVisibleEvent E)
		{
			if (Entry == null) return false;

			IsCoolingDown = Entry.Cooldown > 0;
			return true;
		}

		public override bool HandleEvent(ColorTextEvent E)
		{
			if (IsCoolingDown) E.Color = "K";
			return base.HandleEvent(E);
		}

		public override bool HandleEvent(EnterElementEvent E)
		{
			if (IsCoolingDown)
			{
				return The.Player.ShowFailure("You must wait {{C|" + Entry.CooldownDescription + "}} to use that ability again!");
			}

			return base.HandleEvent(E);
		}

		public override bool HandleEvent(EnteredElementEvent E)
		{
			Entry.Cooldown = Cooldown * 10;
			if (Result)
			{
				Success();
			}
			else
			{
				Failure();
			}

			return base.HandleEvent(E);
		}
	}
}