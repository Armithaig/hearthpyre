﻿using System.Collections.Generic;
using XRL;
using XRL.World;
using XRL.World.Conversations;
using static Hearthpyre.Static;

namespace Hearthpyre.Dialogue.Xylogrifter
{
    public class PresentItem : IConversationPart
    {
        public GameObject Item;

        public override bool WantEvent(int ID, int Propagation)
        {
            return base.WantEvent(ID, Propagation)
                   || ID == EnteredElementEvent.ID
                   || ID == IsElementVisibleEvent.ID
                   || ID == PrepareTextEvent.ID
                ;
        }

        public bool IsInvalid(ref GameObject Object)
        {
            return !GameObject.validate(ref Object)
                   || Item.InInventory != The.Player 
                   && Item.Equipped != The.Player;
        }

        public override bool HandleEvent(IsElementVisibleEvent E)
        {
            if (!TryGetState(CST_SG_OPT, out List<GameObject> items)) return false;

            if (IsInvalid(ref Item) || !items.Remove(Item))
            {
                Item = items.RemoveRandomElement();
            }

            if (IsInvalid(ref Item))
            {
                return false;
            }

            return base.HandleEvent(E);
        }

        public override bool HandleEvent(PrepareTextEvent E)
        {
            E.Object = Item;
            E.Text.Replace("=object.single=", Item.GetBaseDisplayNameSingleStripped());
            return base.HandleEvent(E);
        }

        public override bool HandleEvent(EnteredElementEvent E)
        {
            if (GameObject.validate(ref Item))
            {
                State[CST_SG_ITEM] = Item.SplitFromStack();
                The.Speaker.TakeObject(Item);
                The.Speaker.SetIntProperty("SchemerTraded", 1);
                The.Speaker.pBrain.PerformEquip(Silent: true);
            }

            return base.HandleEvent(E);
        }
    }
}