﻿using System.Collections.Generic;
using XRL;
using XRL.World;
using XRL.World.Conversations;

using static Hearthpyre.Static;

namespace Hearthpyre.Dialogue.Xylogrifter
{
    public class WearItem : IConversationPart
    {
        public GameObject Item;

        public override bool WantEvent(int ID, int Propagation)
        {
            return base.WantEvent(ID, Propagation)
                   || ID == EnteredElementEvent.ID && Propagation.HasBit(PROPAGATE_SPEAKER)
                   || ID == PrepareTextEvent.ID
                ;
        }

        public override bool HandleEvent(PrepareTextEvent E)
        {
            if (TryGetState(CST_SG_ITEM, out Item))
            {
                E.Object = Item;
                E.Text.Replace("=object.single=", Item.GetBaseDisplayNameSingleStripped());
            }

            return base.HandleEvent(E);
        }

        public override bool HandleEvent(EnteredElementEvent E)
        {
#if BUILD_2_0_206
            The.Speaker.SetFeeling(The.Player, 75);
#else
            The.Speaker.AddOpinion<OpinionGift>(The.Player);
#endif
            return base.HandleEvent(E);
        }
    }
}