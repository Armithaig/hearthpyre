﻿using System.Collections.Generic;
using System.Linq;
using ConsoleLib.Console;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.Capabilities;
using XRL.World.Conversations;
using XRL.World.Effects;
using XRL.World.Parts;
using static Hearthpyre.Static;

namespace Hearthpyre.Dialogue.Xylogrifter
{
    public class TradeSchemer : IConversationPart
    {
        public static readonly string[] ColorPairs = { "g", "wk", "ro", "bc", "ym" };

        public override bool WantEvent(int ID, int Propagation)
        {
            return base.WantEvent(ID, Propagation)
                   || ID == PrepareTextEvent.ID
                   || ID == EnteredElementEvent.ID
                   || ID == LeftElementEvent.ID
                ;
        }

        public override bool HandleEvent(PrepareTextEvent E)
        {
            if (!TryGetState(CST_SG_CLR, out char color)) color = 'x';

            var l = "=item.color=".Length;
            var i = E.Text.IndexOf("=item.color=");
            if (i < 0) return base.HandleEvent(E);
            
            switch (color)
            {
                case 'g':
                    E.Text.Replace(i, l, "{{leafy|luhoo}}");
                    break;
                case 'w':
                case 'k':
                    E.Text.Replace(i, l, "{{filthy|grekk}}");
                    break;
                case 'r':
                    E.Text.Replace(i, l, "{{rusty|scoru}}");
                    break;
                case 'b':
                case 'c':
                    E.Text.Replace(i, l, "{{psionic|haffyi}}");
                    break;
                case 'y':
                case 'm':
                    E.Text.Replace(i, l, "{{paisley|wofaro}}");
                    break;
                default:
                    E.Text.Replace(i, l, "{{rainbow|skakfalek}}");
                    break;
            }

            return base.HandleEvent(E);
        }

        public override bool HandleEvent(EnteredElementEvent E)
        {
            if (TryGetState(CST_SG_SLOT, out string slot) && TryGetState(CST_SG_CLR, out char color))
            {
                var set = new HashSet<string>();
                var solids = ColorPairs.FirstOrDefault(x => x.IndexOf(color) >= 0) ?? "ym";
                if (TryGetState(CST_SG_OPT, out List<GameObject> items)) items.Clear();
                else items = new List<GameObject>();

                foreach (var item in The.Player.YieldItems())
                {
                    if (!item.Understood()) continue;
                    if (!item.TryTakePart(out Armor armor)) continue;
                    if (armor.WornOn != slot) continue;

                    var fg = char.ToLower(ColorUtility.FindLastForeground(item.pRender.ColorString) ?? 'y');
                    if (!solids.Contains(fg)) continue;
                    if (!set.Add(item.Blueprint)) continue;

                    items.Add(item);
                }

                State[CST_SG_OPT] = items;
            }
            else
            {
                The.Speaker.SetIntProperty("SchemerTraded", 1);
            }

            return base.HandleEvent(E);
        }

        public override bool HandleEvent(LeftElementEvent E)
        {
            if (The.Speaker.HasIntProperty("SchemerTraded"))
            {
                var schemer = GameObjectFactory.Factory.CreateObject(OBJ_SCMR);
                var state = Stat.Roll(0, 100);
                if (state >= 75 && schemer.ApplyEffect(new Rusted { Duration = 1 }))
                {
                    The.Speaker?.SetIntProperty("SchemerBroken", 1);
                }
                else if (state >= 25 && schemer.ApplyEffect(new Broken { Duration = 1 }))
                {
                    The.Speaker?.SetIntProperty("SchemerBroken", 1);
                }

                The.Player.ReceiveObject(schemer);
                Messaging.WDidXToYWithZ(The.Speaker, "give", null, schemer, "to", The.Player, IndefiniteDirectObject: true);
                return true;
            }
            
            return base.HandleEvent(E);
        }
    }
}