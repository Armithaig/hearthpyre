﻿using System;
using ConsoleLib.Console;
using XRL;
using XRL.World;
using XRL.World.Conversations;
using static Hearthpyre.Static;

namespace Hearthpyre.Dialogue.Xylogrifter
{
    public class PredicateWornOn : IConversationPart
    {
        public string WornOn;
        
        public override void Awake()
        {
            var bps = The.Game.GetStringGameState(GST_SG_BLPR);
            if (!bps.IsNullOrEmpty() && GameObjectFactory.Factory.Blueprints.TryGetValue(bps, out var blueprint))
            {
#if BUILD_2_0_203
                var color = blueprint.GetPartParameter("Render", "ColorString");
                State[CST_SG_CLR] = char.ToLower(ColorUtility.FindLastForeground(color) ?? 'y');
                State[CST_SG_BLPR] = blueprint;
                State[CST_SG_SLOT] = WornOn = blueprint.GetPartParameter("Armor", "WornOn");
#else
                var color = blueprint.GetPartParameter<string>("Render", "ColorString");
                State[CST_SG_CLR] = char.ToLower(ColorUtility.FindLastForeground(color) ?? 'y');
                State[CST_SG_BLPR] = blueprint;
                State[CST_SG_SLOT] = WornOn = blueprint.GetPartParameter<string>("Armor", "WornOn");
#endif
            }
        }

        public override bool WantEvent(int ID, int Propagation)
        {
            return base.WantEvent(ID, Propagation)
                   || ID == PredicateEvent.ID
                ;
        }

        public override bool HandleEvent(PredicateEvent E)
        {
            E.Result = E.Command == WornOn;
            return base.HandleEvent(E);
        }
    }
}
