using System;
using System.Collections;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.Rules;
using Hearthpyre;
using System.Linq;
using Genkit;
using Hearthpyre.AI;

namespace Hearthpyre.Realm
{
	/// <summary>
	/// An NPC home, spanning several cells within a sector.
	/// </summary>
	public class Home : ICogentArea, IEnumerable<Location2D>
	{
		public Guid ID { get; private set; } = Guid.NewGuid();
		public Sector Sector { get; set; }

		public int Count => Locations.Count;

		public Location2D Origin;
		
		// being indexable isn't really that important
		// we really just want fast randoms and contains so a hashset which returned an element from a random bucket would be nice
		Dictionary<Location2D, int> IndexByLocation = new Dictionary<Location2D, int>();
		List<Location2D> Locations = new List<Location2D>();

		public bool Add(Location2D L)
		{
			if (IndexByLocation.ContainsKey(L)) return false;

			IndexByLocation[L] = Locations.Count;
			Locations.Add(L);

			return true;
		}

		public bool Remove(Location2D L)
		{
			if (IndexByLocation.TryGetValue(L, out var i))
			{
				Locations.RemoveAt(i);
				IndexByLocation.Remove(L);
				for (; i < Locations.Count; i++)
				{
					IndexByLocation[Locations[i]] = i;
				}
				return true;
			}

			return false;
		}

		public bool Contains(Location2D L) => IndexByLocation.ContainsKey(L);
		public bool Contains(Cell C) => IndexByLocation.ContainsKey(C.location);

		public Home(Sector Sector, SerializationReader Reader) : this(Sector, null, Reader)
		{ }

		public Home(Sector Sector, Location2D Origin = null, SerializationReader Reader = null)
		{
			this.Sector = Sector;
			this.Origin = Origin;
			if (Reader != null) Load(Reader);

			RealmSystem.Homes[ID] = this;
		}

		public void Save(SerializationWriter Writer)
		{
			var fields = new Dictionary<string, object>();
			fields.Add("ID", ID);
			fields.Add("Origin", new[] { Origin.x, Origin.y });

			var locations = new int[Locations.Count][];
			for (int i = 0; i < locations.Length; i++)
			{
				locations[i] = new[] { Locations[i].x, Locations[i].y };
			}

			fields.Add("Locations", locations);

			Writer.Write(fields);
		}

		public Home Load(SerializationReader reader)
		{
			var fields = reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("ID", out val)) ID = (Guid) val;
			if (fields.TryGetValue("Origin", out val))
			{
				var origin = (int[]) val;
				Origin = Location2D.get(origin[0], origin[1]);
			}
			if (fields.TryGetValue("Locations", out val))
			{
				var locations = (int[][]) val;
				for (int i = 0; i < locations.Length; i++)
				{
					var l = Location2D.get(locations[i][0], locations[i][1]);
					Locations.Add(l);
					IndexByLocation[l] = i;
				}
			}

			return this;
		}

		public void Add(List<Cell> Cells)
		{
			foreach (var cell in Cells) Add(cell.location);
		}

		public void Remove(List<Cell> Cells)
		{
			foreach (var cell in Cells) Remove(cell.location);
		}

		public bool Add(Cell C) => Add(C.location);

		public bool Remove(Cell C) => Remove(C.location);

		public IEnumerator<Location2D> GetEnumerator() => Locations.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => Locations.GetEnumerator();

		public Lattice GetLattice() => Sector.GetLattice();

		public List<Location2D> GetArea() => Locations;

		public override string ToString()
		{
			return $"{ID}: [{Origin}]";
		}
	}
}