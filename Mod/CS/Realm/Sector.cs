using System;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.Rules;
using Hearthpyre;
using System.Linq;
using AiUnity.Common.Extensions;
using Genkit;
using XRL;
using XRL.Messages;
using XRL.UI.Framework;
using XRL.World.Capabilities;
using static Hearthpyre.Static;

namespace Hearthpyre.Realm
{
	/// <summary>
	/// A sector, spans a single zone/screen within the settlement.
	/// </summary>
	public class Sector : ICogentArea
	{
		public Guid ID { get; private set; } = Guid.NewGuid();
		public List<Home> Homes { get; } = new List<Home>();
		public Lattice Lattice { get; } = new Lattice();
		public Settlement Settlement { get; }
		public List<string> Liminal { get; set; }
		public bool Prime { get; set; }
		
		public string ZoneID => Lattice.ZoneID;
		public string Name => GetZoneBareDisplayName(ZoneID);
		public bool IsLive => The.ZoneManager.CachedZones.ContainsKey(ZoneID);

		List<Location2D> commons;
		public List<Location2D> Commons
		{
			get
			{
				if (commons == null)
				{
					var capacity = Homes.Aggregate(80 * 25, (C, H) => C - H.Count);
					commons = new List<Location2D>(capacity);
					for (int x = 0; x < 80; x++)
					for (int y = 0; y < 25; y++)
					{
						var l = Location2D.get(x, y);
						if (!IsHomeLocation(l))
						{
							commons.Add(l);
						}
					}
				}

				return commons;
			}
		}


		public Sector(Settlement Settlement, SerializationReader Reader) : this(Settlement, null, Reader) { }
		public Sector(Settlement Settlement, Zone Zone = null, SerializationReader Reader = null)
		{
			this.Settlement = Settlement;
			if (Zone != null) Lattice.Zone = Zone;
			else if (Reader != null) Load(Reader);

			RealmSystem.Sectors[ID] = this;
			RealmSystem.SectorsByZoneID[ZoneID] = this;
			Settlement.SectorsByZoneID[ZoneID] = this;
		}

		public void Save(SerializationWriter Writer)
		{
			var fields = new Dictionary<string, object>();
			fields.Add("ID", ID);
			fields.Add("Prime", Prime);
			Lattice.SaveFields(fields);

			Writer.Write(fields);

			Writer.Write(Homes.Count);
			foreach (var home in Homes)
			{
				home.Save(Writer);
			}
			
			Writer.Write(Liminal);
		}

		public Sector Load(SerializationReader Reader)
		{
			var fields = Reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("ID", out val)) ID = (Guid) val;
			if (fields.TryGetValue("Prime", out val)) Prime = (bool) val;
			Lattice.LoadFields(fields);

			Homes.Capacity = Reader.ReadInt32();
			for (int i = 0; i < Homes.Capacity; i++)
			{
				Homes.Add(new Home(this, Reader));
			}

			Liminal = Reader.ReadList<string>();
			return this;
		}

		public Home NewHome(Cell C)
		{
			var home = new Home(this, C.location);
			var cells = C.YieldCardinalFlood(NoStructure);
			if (cells.IsOutside()) cells = cells.Take(5);

			foreach (var cell in cells)
			{
				home.Add(cell.location);
			}

			Homes.Add(home);
			return home;
		}

		public void RemoveHome(Home home) => RemoveHome(home.ID);

		public void RemoveHome(Guid ID)
		{
			if (RealmSystem.Homes.TryGetValue(ID, out var home))
			{
				home.Sector = null;
				Homes.Remove(home);
				RealmSystem.Homes.Remove(ID);
			}
		}

		public bool IsHomeLocation(Location2D L)
		{
			foreach (var home in Homes)
			{
				if (home.Contains(L)) return true;
			}

			return false;
		}

		public void AddLiminal(GameObject Object)
		{
			The.ZoneManager.CachedObjects[Object.id] = Object;
			
			Object.pBrain.Goals.Clear();
			Object.RemoveFromContext();
			Object.MakeInactive();
			
			if (Liminal == null) Liminal = new List<string>(1);
			Liminal.Add(Object.id);
		}

		public int DistanceTo(Sector Sector) => Lattice.DistanceTo(Sector.Lattice);

		public void Activated()
		{
			var Z = Lattice.Check().Zone;
			RequireCheckpoint(Z);

			if (Liminal != null)
			{
				var cache = The.ZoneManager.CachedObjects;
                var reachable = Commons.Where(x => Lattice.GetReachable(x) > 0).ToList();

                for (int i = Liminal.Count - 1; i >= 0; i--)
				{
					var C = Z.GetCell(reachable.RemoveRandomElement());
					if (C == null) continue;
					if (cache.TryGetValue(Liminal[i], out var obj))
					{
						C.AddObject(obj).MakeActive();
						cache.Remove(Liminal[i]);
					}
					
					Liminal.RemoveAt(i);
				}

				if (Liminal.Count == 0)
				{
					Liminal = null;
				}
			}
		}

		public void RequireCheckpoint(Zone Z)
		{
			var cell = Z.GetCell(0, 0);
			if (cell != null && !cell.HasObject("CheckpointWidget"))
			{
				var checkpoint = cell.AddObject("CheckpointWidget");
				checkpoint.SetStringProperty("CheckpointKey", Settlement.ID.ToString());
			}
		}

		public void Flush()
		{
			commons = null;
			Lattice.Dirty = true;
		}

		public Lattice GetLattice() => Lattice.Check();

		public List<Location2D> GetArea() => Commons;

		public override string ToString()
		{
			return $"{ID}: [{Name}]";
		}
	}
}
