﻿using System.Collections.Generic;
using Genkit;

namespace Hearthpyre.Realm
{
	public interface ICogentArea
	{
		Lattice GetLattice();
		List<Location2D> GetArea();
	}
}