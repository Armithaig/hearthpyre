using System;
using System.Linq;
using System.Collections.Generic;
using XRL;
using XRL.World;
using XRL.World.Parts.Skill;
using XRL.World.WorldBuilders;
using Genkit;
using Wintellect.PowerCollections;
using XRL.UI;
using XRL.World.Parts;
using static Hearthpyre.Static;

namespace Hearthpyre.Realm
{
	/// <summary>
	/// A settlement, spans one parasang/tile on the world map.
	/// </summary>
	public class Settlement
	{
		public Guid ID { get; private set; } = Guid.NewGuid();
		public List<Sector> Sectors { get; } = new List<Sector>();
		public Dictionary<string, Sector> SectorsByZoneID { get; } = new Dictionary<string, Sector>();
		public Dictionary<string, int> Retainers { get; set; } = new Dictionary<string, int>();
		public Location Location { get; } = new Location();
		public bool Visible { get; private set; }
		public string Name => Prime.Name;

		Sector prime;
		public Sector Prime => prime ?? (prime = Sectors.First(S => S.Prime));

		public GameObject Terrain => ZoneManager.GetTerrainObjectForZone(Location.X, Location.Y, Location.World);

		public Settlement(SerializationReader Reader) : this(null, Reader) { }
		public Settlement(Zone Zone = null, SerializationReader Reader = null, bool? Visible = null)
		{
			if (Zone != null) Location.Set(Zone);
			if (Reader != null) Load(Reader);
			if (Visible != null) this.Visible = Visible.Value;

			RealmSystem.Settlements[ID] = this;

			var CellID = String.Format("{0}.{1}.{2}", Location.World, Location.X, Location.Y);
			RealmSystem.SettlementsByCellID[CellID] = this;
		}

		public void Save(SerializationWriter Writer)
		{
			var fields = new Dictionary<string, object>();
			fields.Add("ID", ID);
			fields.Add("Visible", Visible);
			Location.SaveFields(fields);
			Writer.Write(fields);
			Writer.Write(Retainers);

			Writer.Write(Sectors.Count);
			foreach (var sector in Sectors)
			{
				sector.Save(Writer);
			}
		}

		public Settlement Load(SerializationReader Reader)
		{
			var fields = Reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("ID", out val)) ID = (Guid) val;
			if (fields.TryGetValue("Visible", out val)) Visible = (bool) val;
			Location.LoadFields(fields);
			Retainers = Reader.ReadDictionary<string, int>();

			Sectors.Capacity = Reader.ReadInt32();
			for (int i = 0; i < Sectors.Capacity; i++)
			{
				Sectors.Add(new Sector(this, Reader));
			}

			return this;
		}

		public int GetRetainer(GameObject Object) => GetRetainer(Object.id);
		public int GetRetainer(string ID) => Retainers.TryGetValue(ID, out var ret) ? ret : -1;
		public int CountRetainer(string Retainer) => CountRetainer(RetainerMap[Retainer]);
		public int CountRetainer(int Retainer) => Retainers.Count(KV => KV.Value == Retainer);
		
		public bool Move(GameObject Object)
		{
			var retainer = 0;
			var settlement = Object.TakePart<HearthpyreSettler>()?.Settlement;
			if (settlement != null)
			{
				retainer = settlement.GetRetainer(Object.id);
				if (retainer > 0)
				{
					if (CountRetainer(retainer) >= GetMaxAppointedRetainers())
					{
						Popup.ShowFail(Name + " already has the maximum appointed to that position!");
						return false;
					}
					settlement.Retainers.Remove(Object.id);
				}
			}

			var settler = Object.IncludePart<HearthpyreSettler>();
			settler.Settlement = this;
			settler.Home = null;
			Object.pBrain.Goals.Clear();
			if (retainer > 0) Retainers[Object.id] = retainer;
			return true;
		}

		public void AddLiminal(GameObject Object)
		{
			if (IsWithin(Object)) return;
			Sectors.GetRandomElement().AddLiminal(Object);
		}


		public Sector NewSector(Zone Zone)
		{
			var sector = new Sector(this, Zone);
			sector.Prime = Sectors.Count == 0;
			Sectors.Add(sector);
			return sector;
		}

		public bool IsWithin(GameObject Object) => SectorsByZoneID.ContainsKey(Object.CurrentZone.ZoneID);
		public bool HasSector(Zone Zone) => SectorsByZoneID.ContainsKey(Zone.ZoneID);
		public bool HasSector(string ZoneID) => SectorsByZoneID.ContainsKey(ZoneID);
		public Sector GetSector(Zone Zone) => GetSector(Zone.ZoneID);
		public Sector GetSector(string ZoneID)
		{
			SectorsByZoneID.TryGetValue(ZoneID, out var result);
			return result;
		}
	}

	public class Location
	{
		public int X { get; set; }
		public int Y { get; set; }
		public string World { get; set; }

		public void Set(Zone zone)
		{
			Set(zone.wX, zone.wY, zone.ZoneWorld);
		}

		public void Set(int x, int y, string world)
		{
			X = x;
			Y = y;
			World = world;
		}

		public void SaveFields(Dictionary<string, object> fields)
		{
			fields["X"] = X;
			fields["Y"] = Y;
			fields["World"] = World;
		}

		public void LoadFields(Dictionary<string, object> fields)
		{
			object val;
			if (fields.TryGetValue("X", out val)) X = (int) val;
			if (fields.TryGetValue("Y", out val)) Y = (int) val;
			if (fields.TryGetValue("World", out val)) World = (string) val;
		}

		public override string ToString()
		{
			return World + "." + X + "." + Y;
		}
	}
}