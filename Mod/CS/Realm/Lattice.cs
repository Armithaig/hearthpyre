﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ConsoleLib.Console;
using Genkit;
using Hearthpyre.AI;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.Capabilities;
using XRL.World.Parts;
using Color = UnityEngine.Color;

using static Hearthpyre.Static;
using static XRL.World.ZoneID;

namespace Hearthpyre.Realm
{
	public class Lattice : IEquatable<GlobalLocation>, IEquatable<Zone>
	{
		// todo: we could treat these as bytes and track up to 256? they don't intersect by design
		public const int GRID_REACH = 0xFF << 0; // track up to 8 diffuse areas of reachability
		public const int GRID_ROAD = 0xFF << 8;  // track up to 8 diffuse areas of road, can probably knock this down if bits needed
		
		public const int GRID_SOLID = 1 << 16;
		public const int GRID_WALL = 1 << 17;
		public const int GRID_LIQUID = 1 << 18;
		public const int GRID_CONN = 1 << 19;
		public const int GRID_DOOR = 1 << 20;
		public const int GRID_FLOOR = 1 << 21;
		public const int GRID_FLYOVER = 1 << 22;
		
		public const int GRID_FLAGS = 0x7F << 16;
		public const int GRID_NAV = 0x7F << 23;
		
		public const int GRID_CHECK_REACH = 1 << 30;
		public const int GRID_CHECK_ROAD = 1 << 31;

		private int[,] Grid;
		public int X1, Y1, X2, Y2;
		public bool Dirty = false;
		public long Built = -1;
		public int Reaches = 0;
		public int Roads = 0;
		
		// World location
		public string World;
		public int WX;
		public int WY;
		public int X;
		public int Y;
		public int Z;

		// Reachability of connection cells
		public int North;
		public int East;
		public int South;
		public int West;
		public int Up;
		public int Down;

		private string zoneId;
		public string ZoneID
		{
			get => zoneId;
			set
			{
				if (zoneId == value) return;
				
				zoneId = value;
				var parts = value.Split('.');
				World = string.IsInterned(parts[0]) ?? parts[0];
				WX = Convert.ToInt32(parts[1]);
				WY = Convert.ToInt32(parts[2]);
				X = Convert.ToInt32(parts[3]);
				Y = Convert.ToInt32(parts[4]);
				Z = Convert.ToInt32(parts[5]);
				Flush();
			}
		}
		
		private Zone zone;
		public Zone Zone
		{
			get
			{
#if RC1_PLAGUE
				if (zone == null || zone.Suspended)
#else
				if (zone == null || zone.bSuspended)
#endif
				{
					zone = The.ZoneManager.GetZone(ZoneID);
				}

				return zone;
			}
			set
			{
				ZoneID = value?.ZoneID;
				zone = value;
			}
		}
		
		public int this[int X, int Y]
		{
			get => Grid[X, Y];
			set => Grid[X, Y] = value;
		}
		
		public int this[Location2D L]
		{
			get => Grid[L.x, L.y];
			set => Grid[L.x, L.y] = value;
		}
		
		public Lattice() { }
		public Lattice(string ZoneID)
		{
			this.ZoneID = ZoneID;
		}

		public Lattice Check()
		{
			if (!IsValid()) Rebuild();
			return this;
		}

		public void Flush(bool Hard = false)
		{
			zone = null;
			if (Hard)
			{
				Grid = null;
				Built = -1;
				Dirty = false;
				Reaches = 0;
				Roads = 0;
			}
		}

		public bool IsValid()
		{
			return Grid != null && !Dirty;
		}

		public int GetWeight(Location2D L) => (Grid[L.x, L.y] & GRID_NAV) >> 23;
		public int GetWeight(Cell C) => (Grid[C.X, C.Y] & GRID_NAV) >> 23;
		public int GetWeight(int X, int Y) => (Grid[X, Y] & GRID_NAV) >> 23;

		public int GetReachable(Location2D L) => Grid[L.x, L.y] & GRID_REACH;
		public int GetReachable(Location2D L, int Perimeter)
		{
			var result = 0;
			int rx1 = Math.Max(X1, L.x - Perimeter), rx2 = Math.Min(X2, L.x + Perimeter);
			int ry1 = Math.Max(Y1, L.y - Perimeter), ry2 = Math.Min(Y2, L.y + Perimeter);

			for (int i = rx1; i <= rx2; i++)
			{
				result |= Grid[i, ry1] & GRID_REACH;
				result |= Grid[i, ry2] & GRID_REACH;
			}
			for (int i = ry1 + 1; i <= ry2 - 1; i++)
			{
				result |= Grid[rx1, i] & GRID_REACH;
				result |= Grid[rx2, i] & GRID_REACH;
			}

			return result;
		}
		public int GetReachable(Cell C)
		{
			if (Equals(C.ParentZone)) return Grid[C.X, C.Y] & GRID_REACH;
			return GetReachableCourse(CourseTo(C.ParentZone));
		}
		
		public int GetRoad(Location2D L) => Grid[L.x, L.y] & GRID_ROAD;
		public int GetRoad(Location2D L, int Perimeter)
		{
			var result = 0;
			int rx1 = Math.Max(X1, L.x - Perimeter), rx2 = Math.Min(X2, L.x + Perimeter);
			int ry1 = Math.Max(Y1, L.y - Perimeter), ry2 = Math.Min(Y2, L.y + Perimeter);

			for (int i = rx1; i <= rx2; i++)
			{
				result |= Grid[i, ry1] & GRID_ROAD;
				result |= Grid[i, ry2] & GRID_ROAD;
			}
			for (int i = ry1 + 1; i <= ry2 - 1; i++)
			{
				result |= Grid[rx1, i] & GRID_ROAD;
				result |= Grid[rx2, i] & GRID_ROAD;
			}

			return result;
		}
		public int GetRoad(Cell C)
		{
			if (Equals(C.ParentZone)) return Grid[C.X, C.Y] & GRID_ROAD;
			return 0;
		}
		
		public bool HasRoad(int X, int Y) => (Grid[X, Y] & GRID_ROAD) > 0;
		public bool HasLiquid(Location2D L) => (Grid[L.x, L.y] & GRID_LIQUID) > 0;
		public bool HasLiquid(Cell C) => (Grid[C.X, C.Y] & GRID_LIQUID) > 0;
		public bool IsEdge(Location2D L) => L.x == X1 || L.x == X2 || L.y == Y1 || L.y == Y2;
		public bool IsEdge(Cell C) => C.X == X1 || C.X == X2 || C.Y == Y1 || C.Y == Y2;
		
		public static implicit operator int[,](Lattice SG) => SG.Grid;
		
		public static bool TryGet(string ZoneID, out Lattice Lattice, bool Validate = true, bool Rebuild = true, bool Create = true)
		{
			if (RealmSystem.SectorsByZoneID.TryGetValue(ZoneID, out var sector))
			{
				Lattice = sector.Lattice;
			}
			else if (!RealmSystem.LatticesByZoneID.TryGetValue(ZoneID, out Lattice))
			{
				if (!Create) return false;
				
				Lattice = new Lattice(ZoneID);
				RealmSystem.LatticesByZoneID[ZoneID] = Lattice;
			}

			return Validate && Lattice.IsValid() || Rebuild && Lattice.Rebuild();
		}

		public static void Invalidate(Cell C) => Invalidate(C.ParentZone.ZoneID);
		public static void Invalidate(Zone Z) => Invalidate(Z.ZoneID);
		public static void Invalidate(string ZoneID)
		{
			if (RealmSystem.SectorsByZoneID.TryGetValue(ZoneID, out var sector))
			{
				sector.Lattice.Dirty = true;
			}
			else if (RealmSystem.LatticesByZoneID.TryGetValue(ZoneID, out var lattice))
			{
				lattice.Dirty = true;
			}
		}
		
		// TODO: Could spread over multiple turns
		// TODO: Coroutine structure similar to current queue system? Yield turn delay, perhaps its town type with state
		private static Queue<Location2D> CellQueue = new Queue<Location2D>();
		public static Flood FloodGrid(int[,] Grid, int X, int Y, int Mask, int ExploreMask, Predicate<int> Predicate)
		{
			Grid.Bounds(out int x1, out int y1, out int x2, out int y2);
			CellQueue.Enqueue(Location2D.get(X, Y));
			var result = new Flood(X, Y, Mask);

			while (CellQueue.Count != 0)
			{
				var SC = CellQueue.Dequeue();
				Grid[SC.x, SC.y] |= Mask;
				result.Add(SC);
				
				for (X = SC.x - 1; X <= SC.x + 1; X++)
				for (Y = SC.y - 1; Y <= SC.y + 1; Y++)
				{
					if (X < x1 || X > x2 || Y < y1 || Y > y2) continue;
					if (Grid[X, Y].HasBit(ExploreMask)) continue;
					Grid[X, Y] |= ExploreMask;
					
					if (Predicate(Grid[X, Y]))
					{
						CellQueue.Enqueue(Location2D.get(X, Y));
					}
				}
			}

			return result;
		}

		public static bool Reachable(int Flags) => !Flags.HasBit(GRID_SOLID | GRID_REACH);
		public static bool Road(int Flags) => Flags.HasBit(GRID_FLOOR | GRID_DOOR) && !Flags.HasBit(GRID_ROAD);

		public static List<Flood> FloodReachable(int[,] Grid) => FloodRange(Grid, 1, GRID_CHECK_REACH, Reachable);
		public static List<Flood> FloodRoad(int[,] Grid) => FloodRange(Grid, 1 << 8, GRID_CHECK_ROAD, Road);

		public bool Rebuild()
		{
			var Z = Zone;
			if (Z == null) return false;

			Grid = Grid ?? new int[Z.Width, Z.Height];
			Grid.Bounds(out X1, out Y1, out X2, out Y2);
			MarkGrid(Z, Grid);
			var reaches = FloodReachable(Grid);
			var i = reaches.IndexOfMax();
			Reaches = reaches.Count;
			
			var roads = FloodRoad(Grid);
			//Draw();
			Roads = roads.Count > 1 ? ConnectRoads(roads) : roads.Count;
			//Draw();

			if (i != -1)
			{
				for (int x = 0; x < Z.Width; x++)
				for (int y = 0; y < Z.Height; y++)
				{
					Z.ReachableMap[x, y] = Grid[x, y].HasBit(1 << i);
				}
			}
			else
			{
				Z.ClearReachableMap();
			}
			
			CheckConnections(Z);

			Dirty = false;
			Built = The.Game.Turns;
			return true;
		}

		public void CheckConnections(Zone Z)
		{
			North = 0;
			for (int x = 0, y = 0; x < Z.Width; x++)
				North |= Grid[x, y] & GRID_REACH;
			
			East = 0;
			for (int x = Z.Width - 1, y = 0; y < Z.Height; y++)
				East |= Grid[x, y] & GRID_REACH;
			
			South = 0;
			for (int x = 0, y = Z.Height - 1; x < Z.Width; x++)
				South |= Grid[x, y] & GRID_REACH;
			
			West = 0;
			for (int x = 0, y = 0; y < Z.Height; y++)
				West |= Grid[x, y] & GRID_REACH;
			
			Up = 0;
			Down = 0;
			for (int x = 0; x < Z.Width; x++)
			for (int y = 0; y < Z.Height; y++)
			{
				Grid[x, y] &= ~(GRID_CHECK_ROAD | GRID_CHECK_REACH); // scrub the check bits here since we're already doing a pass
				if (Grid[x, y].HasBit(GRID_CONN))
				{
					var stairs = Z.Map[x][y].GetFirstObjectWithTag("Stairs");
					if (stairs != null)
					{
						if (stairs.OwnPart<StairsUp>())
						{
							Up |= Grid[x, y] & GRID_REACH;
						}
						else if (stairs.OwnPart<StairsDown>())
						{
							Down |= Grid[x, y] & GRID_REACH;
						}
					}
				}
			}
		}

		// Connect some roads that are close together.
		public int ConnectRoads(List<Flood> Roads)
		{
			if (Roads.Count <= 1) return Roads.Count;
			
			var count = Roads.Count;
			var path = new Path();
			Location2D origin, destination, center, buf;
			Roads.Sort();

			for (int i = 0; i < Roads.Count - 1 && count > 1; i++)
			{
				var road = Roads[i];
				center = road.Center;
				origin = null;
				destination = null;
				
				// fine with a rough single pass here since we're pathfinding anyway
				for (int rx1 = Math.Max(X1, road.X1 - 2), rx2 = Math.Min(X2, road.X2 + 2); rx1 <= rx2; rx1++)
				for (int ry1 = Math.Max(Y1, road.Y1 - 2), ry2 = Math.Min(Y2, road.Y2 + 2); ry1 <= ry2; ry1++)
				{
					if (Grid[rx1, ry1].HasBit(road.Mask))
					{
						buf = Location2D.get(rx1, ry1);
						if (ReferenceEquals(origin, null)) origin = buf;
						else if (ReferenceEquals(destination, null)) origin = buf;
						else if (buf.PixelDistanceTo(destination) <= origin.PixelDistanceTo(destination)) origin = buf;
					}
					else if (Grid[rx1, ry1].HasBit(GRID_ROAD))
					{
						buf = Location2D.get(rx1, ry1);
						if (ReferenceEquals(destination, null)) destination = buf;
						else if (ReferenceEquals(origin, null)) destination = buf.DistanceTo(center) <= destination.DistanceTo(center) ? buf : destination;
						else if (buf.PixelDistanceTo(origin) <= destination.PixelDistanceTo(origin)) destination = buf;
					}
				}

				if (ReferenceEquals(origin, null) || ReferenceEquals(destination, null)) continue;
				if (!Strider.TryPathTo(path, Destination: destination, Origin: origin, Lattice: this, Limit: 10, Flags: Strider.FLAG_DIRECT)) continue;

				var mask = Grid[destination.x, destination.y] & GRID_ROAD;
				for (int s = 0; s < path.Count; s++)
				{
					int cx = path[s].CX, cy = path[s].CY;
					if (Grid[cx, cy].HasBit(mask)) break;
					Grid[cx, cy] &= ~road.Mask;
					Grid[cx, cy] |= mask;
				}
				road.ReplaceMask(Grid, mask);
				count--;
			}
			
			return count;
		}

		public void Draw()
		{
			var SB = TextConsole.ScrapBuffer;

			for (int x = X1; x <= X2; x++)
			for (int y = Y1; y <= Y2; y++)
			{
				var flags = Grid[x, y];
				var chr = SB.Buffer[x, y];
				chr.Tile = null;
				chr.Foreground = ColorUtility.ColorMap['y'];
				
				if (flags.HasBit(GRID_SOLID))  chr.Foreground  = Color.black;
				else if (flags.HasBit(GRID_LIQUID))  chr.Foreground  = Color.blue;
				else if (flags.HasBit(GRID_FLOOR))  chr.Foreground  = Color.green;
				else if (flags.HasBit(GRID_CONN))  chr.Foreground  = Color.magenta;
				else if (flags.HasBit(GRID_DOOR))  chr.Foreground  = Color.cyan;
				if (flags.HasBit(GRID_NAV))
				{
					var nav = (flags & GRID_NAV) >> 23;
					chr.Foreground = Color.Lerp(chr.Foreground, Color.red, nav / 100f);
				}
				
				if (!flags.HasBit(GRID_REACH)) chr.Char = '#';
				else if (flags.HasBit(1 << 0)) chr.Char = '1';
				else if (flags.HasBit(1 << 1)) chr.Char = '2';
				else if (flags.HasBit(1 << 2)) chr.Char = '3';
				else if (flags.HasBit(1 << 3)) chr.Char = '4';
				else if (flags.HasBit(1 << 4)) chr.Char = '5';
				else if (flags.HasBit(1 << 5)) chr.Char = '6';
				else if (flags.HasBit(1 << 6)) chr.Char = '7';
				else if (flags.HasBit(1 << 7)) chr.Char = '8';
				
				if (!flags.HasBit(GRID_ROAD)) chr.Background  = ColorUtility.ColorMap['k'];
				else if (flags.HasBit(1 << 8)) chr.Background  = ColorUtility.ColorMap['G'];
				else if (flags.HasBit(1 << 9)) chr.Background  = ColorUtility.ColorMap['B'];
				else if (flags.HasBit(1 << 10)) chr.Background  = ColorUtility.ColorMap['M'];
				else if (flags.HasBit(1 << 11)) chr.Background  = ColorUtility.ColorMap['R'];
				else if (flags.HasBit(1 << 12)) chr.Background  = ColorUtility.ColorMap['W'];
				else if (flags.HasBit(1 << 13)) chr.Background  = ColorUtility.ColorMap['O'];
				else if (flags.HasBit(1 << 14)) chr.Background  = ColorUtility.ColorMap['C'];
				else if (flags.HasBit(1 << 15)) chr.Background  = ColorUtility.ColorMap['b'];
			}

			SB.Draw();
			Keyboard.getch();
		}

		public static List<Flood> FloodRange(int[,] Grid, int Bit, int ExploreMask, Predicate<int> Predicate)
		{
			Grid.Bounds(out int x1, out int y1, out int x2, out int y2);
			var i = 0;
			var floods = new List<Flood>(8);
			
			for (int x = x1; x <= x2; x++)
			for (int y = y1; y <= y2; y++)
			{
				if (!Predicate(Grid[x, y])) continue;
				if (floods.Count >= floods.Capacity)
				{
					// exceeded capacity, replace lowest
					i = floods.IndexOfMin();
					Grid.ClearBit(Bit << i);
				}
				
				var amount = FloodGrid(Grid, x, y, Bit << i, ExploreMask, Predicate);
				if (floods.Count < floods.Capacity)
				{
					floods.Add(amount);
					i++;
				}
			}

			return floods;
		}
		
		private static bool IsSolid(GameObject Object) => Object.ConsiderSolid();

		public static int MarkCell(Cell C)
		{
			if (C.IsEmptyAtRenderLayer(0)) return 0;

			var result = 0;
			var solid = C.GetFirstObject(IsSolid);
			if (solid != null)
			{
				if (solid.HasTag("Flyover"))
				{
					result |= GRID_FLYOVER;
				}
				if (solid.HasIntProperty("Wall"))
				{
					result |= GRID_WALL | GRID_SOLID;
					return result;
				}
				if (solid.HasTag("Door"))
				{
					result |= GRID_DOOR;
				}
				else
				{
					result |= GRID_SOLID;
					return result;
				}
			}

			var weight = Math.Min(C.GetImmobileWeight(), 0x7F);
			result |= weight << 23;
				
			if (weight >= 100)
			{
				result |= GRID_SOLID;
				return result;
			}
				
			if (C.HasWadingDepthLiquid())
			{
				if (C.HasBridge()) result |= GRID_FLOOR;
				else result |= GRID_LIQUID;
			}
			else if (C.HasObjectWithTag("Floor"))
			{
				result |= GRID_FLOOR;
			}
				
			if (C.HasObjectWithTag("Door"))
			{
				result |= GRID_DOOR;
			}

			if (C.HasObjectWithTag("Stairs"))
			{
				result |= GRID_CONN;
			}
			
			return result;
		}
		
		public static void MarkGrid(Zone Z, int[,] Grid)
		{
			for (int x = 0; x < Z.Width; x++)
			for (int y = 0; y < Z.Height; y++)
			{
				Grid[x, y] = MarkCell(Z.Map[x][y]);
			}
		}
		
		public class Flood : IComparable<Flood>
		{
			public int X1;
			public int Y1;
			public int X2;
			public int Y2;
			public int Amount = 0;
			public int Mask = 0;
			
			public Location2D Center => Location2D.get((X1 + X2) / 2, (Y1 + Y2) / 2);

			public Flood(int X, int Y, int Mask)
			{
				X1 = X;
				X2 = X;
				Y1 = Y;
				Y2 = Y;
				this.Mask = Mask;
			}

			public void Add(Location2D L)
			{
				if (L.x < X1) X1 = L.x;
				else if (L.x > X2) X2 = L.x;
				if (L.y < Y1) Y1 = L.y;
				else if (L.y > Y2) Y2 = L.y;
				Amount++;
			}

			public int CompareTo(Flood other)
			{
				return Amount.CompareTo(other.Amount);
			}

			public bool IntersectsWithP1(Flood Other) => IntersectsWith(Other, 1);
			public bool IntersectsWith(Flood Other, int Padding = 0)
			{
				if (ReferenceEquals(this, Other)) return false;
				if (X1 - Padding > Other.X2 + Padding || Other.X1 - Padding > X2 + Padding) return false;
				if (Y1 - Padding > Other.Y2 + Padding || Other.Y1 - Padding > Y2 + Padding) return false;
				return true;
			}

			public void ReplaceMask(int[,] Grid, int NewMask)
			{
				for (int x = X1; x <= X2; x++)
				for (int y = Y1; y <= Y2; y++)
				{
					if (!Grid[x, y].HasBit(Mask)) continue;
				
					Grid[x, y] &= ~Mask;
					Grid[x, y] |= NewMask;
				}

				Mask = NewMask;
			}
		}

		// TODO: this is very naive, if we have lattices to the destination we can plan a better global path
		public bool TryGetNextZoneTo(Location2D From, GlobalLocation To, out int Direction, out string ZoneID, bool Permissive = false)
		{
			var reach = GetReachable(From);
			var DZ = Z - To.ZoneZ;
			if (DZ > 0 && Up.HasBit(reach))
			{
				Direction = UP;
				ZoneID = Assemble(World, WX, WY, X, Y, Z - 1);
				return true;
			}
			if (DZ < 0 && Down.HasBit(reach))
			{
				Direction = DOWN;
				ZoneID = Assemble(World, WX, WY, X, Y, Z + 1);
				return true;
			}

			var GX = WX * 3 + X;
			var GY = WY * 3 + Y;
			var DX = GX - (To.ParasangX * 3 + To.ZoneX);
			var DY = GY - (To.ParasangY * 3 + To.ZoneY);
			var RX = 0;
			var RY = 0;

			if (DX > 0 && West.HasBit(reach)) RX = WEST;
			else if (DX < 0 && East.HasBit(reach)) RX = EAST;
			
			if (DY > 0 && North.HasBit(reach)) RY = NORTH;
			else if (DY < 0 && South.HasBit(reach)) RY = SOUTH;

			if (Permissive && RX == 0 && RY == 0)
			{
				if (West.HasBit(reach)) RX = East.HasBit(reach) && 50.in100() ? EAST : WEST;
				else if (East.HasBit(reach)) RX = EAST;
				if (North.HasBit(reach)) RY = South.HasBit(reach) && 50.in100() ? SOUTH : NORTH;
				else if (South.HasBit(reach)) RY = SOUTH;
			}

			if (RX != 0)
			{
				if (RY != 0)
				{
					if (50.in100())
					{
						GX += Math.Sign(DX);
						Direction = RX;
					}
					else
					{
						GY += Math.Sign(DY);
						Direction = RY;
					}
				}
				else
				{
					GX += Math.Sign(DX);
					Direction = RX;
				}
			}
			else if (RY != 0)
			{
				GY += Math.Sign(DY);
				Direction = RY;
			}
			else
			{
				Direction = 0;
				ZoneID = null;
				return false;
			}
			
			if (GX < 0 || GY < 0 || GX >= 19200 || GY >= 1875)
			{
				Direction = 0;
				ZoneID = null;
				return false;
			}
			
			ZoneID = Assemble(World, GX / 3, GY / 3, GX % 3, GY % 3, Z);
			return true;
		}

		public int DistanceTo(Lattice Lattice)
		{
			return Math.Abs(WX * 3 + X - Lattice.WX * 3 - Lattice.X)
			       + Math.Abs(WY * 3 + Y - Lattice.WY * 3 - Lattice.Y)
			       + Math.Abs(Z - Lattice.Z)
				;
		}

		public static BallBag<int> Courses = new BallBag<int>(2);
		public int CourseTo(Lattice To) => CourseTo(To.WX, To.WY, To.X, To.Y, To.Z);
		public int CourseTo(Zone To) => CourseTo(To.wX, To.wY, To.X, To.Y, To.Z);
		public int CourseTo(int PX, int PY, int ZX, int ZY, int ZZ)
		{
			var DZ = Z - ZZ;
			if (DZ > 0 && Up > 0) return UP;
			if (DZ < 0 && Down > 0) return DOWN;

			Courses.Clear();
			var DX = WX * 3 + X - PX * 3 - ZX;
			var DY = WY * 3 + Y - PY * 3 - ZY;
			if (DX > 0 && West > 0) Courses.Add(WEST, Math.Abs(DX));
			else if (DX < 0 && East > 0) Courses.Add(EAST, Math.Abs(DX));
			if (DY > 0 && North > 0) Courses.Add(NORTH, Math.Abs(DY));
			else if (DY < 0 && South > 0) Courses.Add(SOUTH, Math.Abs(DY));

			return Courses.PeekOne();
		}

		public int GetReachableCourse(int Course)
		{
			switch (Course)
			{
				case UP: return Up;
				case DOWN: return Down;
				case NORTH: return North;
				case EAST: return East;
				case SOUTH: return South;
				case WEST: return West;
				default: return 0;
			}
		}

		public void SaveFields(Dictionary<string, object> Fields)
		{
			Fields["Zone"] = ZoneID;
			Fields["NorthConnection"] = North;
			Fields["EastConnection"] = East;
			Fields["SouthConnection"] = South;
			Fields["WestConnection"] = West;
			Fields["UpConnection"] = Up;
			Fields["DownConnection"] = Down;
		}

		public void LoadFields(Dictionary<string, object> Fields)
		{
			object val;
			if (Fields.TryGetValue("Zone", out val)) ZoneID = (string) val;
			if (Fields.TryGetValue("NorthConnection", out val)) North = (int) val;
			if (Fields.TryGetValue("EastConnection", out val)) East = (int) val;
			if (Fields.TryGetValue("SouthConnection", out val)) South = (int) val;
			if (Fields.TryGetValue("WestConnection", out val)) West = (int) val;
			if (Fields.TryGetValue("UpConnection", out val)) Up = (int) val;
			if (Fields.TryGetValue("DownConnection", out val)) Down = (int) val;
		}

		public bool Equals(GlobalLocation Location)
		{
			return WX == Location.ParasangX
			       && WY == Location.ParasangY
			       && X == Location.ZoneX
			       && Y == Location.ZoneY
			       && Z == Location.ZoneZ
			       //&& World == Location.World // lets skip the string comp for now
				;
		}

		public bool Equals(Zone Zone)
		{
			return WX == Zone.wX
			       && WY == Zone.wY
			       && X == Zone.X
			       && Y == Zone.Y
			       && Z == Zone.Z
			       //&& World == Zone.ZoneWorld // lets skip the string comp for now
				;
		}
	}
}