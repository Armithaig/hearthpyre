﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Linq;
using XRL;
using Genkit;
using Qud.API;
using XRL.Core;
using XRL.UI;
using XRL.Rules;
using XRL.World;
using XRL.World.Parts;
using XRL.World.WorldBuilders;
using static Hearthpyre.Static;

namespace Hearthpyre
{
	[JoppaWorldBuilderExtension]
	public partial class WorldBuilderExtension : IJoppaWorldBuilderExtension
	{
		private JoppaWorldBuilder Builder;
		private ZoneManager ZM;
		private Random Random;


		public override void OnBeforeBuild(JoppaWorldBuilder Builder)
		{
			this.Builder = Builder;
			ZM = The.ZoneManager;
			Random = Stat.GetSeededRandomGenerator("HearthpyreWorldBuilder");
		}

		public override void OnAfterBuild(JoppaWorldBuilder Builder)
		{
			RequireSecrets();
		}
		
		public static void RequireSecrets()
		{
			if (!The.Game.HasStringGameState(GST_SG_BLPR))
			{
				try
				{
					AddSecretXylogrifter();
				}
				catch (Exception e)
				{
					Log("Xylogrifter", e);
				}
			}

			var note = JournalAPI.GetMapNote(SCT_GRFT);
			if (note != null)
			{
				note.Weight = 25000;
			}
		}

		public static bool IsTravel(GameObject Object) => Object.OwnPart<TerrainTravel>();

		public static string GetZoneIDOfNamedLocation(string Terrain, string Name)
		{
			var world = WorldFactory.Factory.getWorld("JoppaWorld");
			var Z = The.ZoneManager.GetZone("JoppaWorld");
			for (int x = 0; x < 80; x++)
			for (int y = 0; y < 25; y++)
			{
				var terrain = Z.Map[x][y].GetFirstObject(IsTravel);
				if (terrain == null || terrain.Blueprint != Terrain) continue;
				if (!world.CellBlueprintsByApplication.TryGetValue(terrain.Blueprint, out var blueprint)) continue;

				for (int cx = 0; cx < Definitions.Width; cx++)
				for (int cy = 0; cy < Definitions.Height; cy++)
				for (int cz = 0; cz < Definitions.Layers; cz++)
				{
					var level = blueprint.LevelBlueprint[cx, cy, cz];
					if (level.ProperName || level.Name == Name)
					{
						return ZoneID.Assemble(world.Name, x, y, cx, cy, cz);
					}
				}
			}

			return null;
		}
	}
}