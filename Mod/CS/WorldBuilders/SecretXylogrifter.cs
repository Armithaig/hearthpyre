﻿using System;
using Genkit;
using Qud.API;
using XRL;
using XRL.Rules;
using XRL.UI;
using XRL.World;
using XRL.World.WorldBuilders;
using static Hearthpyre.Static;

namespace Hearthpyre
{
	public partial class WorldBuilderExtension : IJoppaWorldBuilderExtension
	{
		public static void AddSecretXylogrifter()
		{
			var random = Stat.GetSeededRandomGenerator("HearthpyreSnapgrifter");
			var info = (WorldInfo) The.Game.GetObjectGameState("JoppaWorldInfo");
			var tries = 0;
			var bag = new BallBag<string>(random)
			{
				{"DesertCanyon", 10},
				{"Saltmarsh", 10},
				{"Hills", 10},
				{"Jungle", 10},
				{"Flowerfields", 10},
				{"Ruins", 10},
				{"BananaGrove", 7},
				{"Water", 5},
				{"Mountains", 2},
				{"LakeHinnom", 2},
				{"PalladiumReef", 2},
				{"MoonStair", 2},
				{"Fungal", 0},
				{"Deathlands", 1}
			};

			string ID = null;
			RollZone:
			if (random.NextDouble() > 0.8)
			{
				var roll = random.Next(7);
				if (roll == 0) ID = GetZoneIDOfNamedLocation("TerrainJoppa", "Joppa");
				else if (roll == 1) ID = GetZoneIDOfNamedLocation("TerrainGritGate", "Grit Gate");
				else if (roll == 2) ID = GetZoneIDOfNamedLocation("TerrainKyakukya", "Kyakukya");
				else if (roll == 3) ID = GetZoneIDOfNamedLocation("TerrainOmonporch", "Ezra");
				else if (roll == 4) ID = GetZoneIDOfNamedLocation("TerrainPalladiumReef 3l", "Yd Freehold");
				else if (roll == 5) ID = The.Game.GetStringGameState("FungalTrailEnd", null);
				else if (roll == 6) ID = info.villages.GetRandomElement()?.targetZone;
			}
			if (ID.IsNullOrEmpty())
			{
				var l = info.terrainLocations.GetValue(bag.PeekOne())?.GetRandomElement(random);
				if (l == null && tries++ < 100) goto RollZone;

				var z = random.NextDouble() > 0.9 ? random.Next(11, 25) : 10;
				ID = ZoneID.Assemble("JoppaWorld", l.x, l.y, random.Next(3), random.Next(3), z);
			}
			if (The.ZoneManager.IsZoneBuilt(ID) && tries++ < 100)
			{
				ID = null;
				goto RollZone;
			}
			
			JournalAPI.AddMapNote(
				ZoneID: ID,
				text: "a peculiar snapjaw",
				category: "Oddities",
				attributes: new[] { "snapjaw", "settlement", "tech", "oddity" },
				secretId: SCT_GRFT
			);

#if BUILD_2_0_204
			The.ZoneManager.AddZonePostBuilderAfterTerrain(ID, new ZoneBuilderBlueprint("AddBlueprintBuilder", "Object", "HearthpyreSnapjawXylogrifter"));
#else
			The.ZoneManager.RequireBuilderCollection(ID).Add(ZoneBuilderBlueprint.Get("AddBlueprintBuilder", "Object", "HearthpyreSnapjawXylogrifter"), ZoneBuilderPriority.AFTER_TERRAIN);		
#endif
			if (random.NextDouble() > 0.95)
			{
				The.Game.SetStringGameState(GST_SG_BLPR, ""); // you can have it friend
				return;
			}

			bag.Clear();
			bag["BaseBoot"] = 10;
			bag["BaseGlove"] = 5;
			bag["BaseGauntlet"] = 2;
			bag["BaseArmlet"] = 2;
			bag["BaseBracelet"] = 2;
			
			var armor = PopulationManager.GenerateOne("DynamicInheritsTable:" + bag.PeekOne() + ":Tier2-4");
			The.Game.SetStringGameState(GST_SG_BLPR, armor.Blueprint);
		}
	}

}
