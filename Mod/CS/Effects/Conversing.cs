﻿using System;
using System.Collections.Generic;
using Hearthpyre;
using Hearthpyre.AI;
using XRL.World.Parts;

namespace XRL.World.Effects
{
	// TODO
	[Serializable]
	public class HearthpyreConversing : Effect
	{
		
		public const string NAME = "{{C|conversing}}";
		public const int TASK_HM = 1;
		public static readonly BallBag<int> Actions = new BallBag<int>
		{
			{0, 0}
		};

		public List<GameObject> Conversants { get; set; }
		public string Speaker { get; set; }
		public int Silence { get; set; }
		
		public HearthpyreConversing()
		{
			DisplayName = NAME;
			Duration = 1;
		}

		public override void SaveData(SerializationWriter Writer)
		{
			base.SaveData(Writer);
			
			var fields = new Dictionary<string, object>();
			fields["Speaker"] = Speaker;
			Writer.Write(fields);

			if (Speaker == Object.id) Writer.WriteGameObjectList(Conversants);
		}

		public override void LoadData(SerializationReader Reader)
		{
			base.LoadData(Reader);
			
			var fields = Reader.ReadDictionary<string, object>();
			object val;
			if (fields.TryGetValue("Initiator", out val)) Speaker = (string)val;

			if (Speaker == Object.id) Reader.ReadGameObjectList(Conversants);
		}

		public override int GetEffectType()
		{
			return TYPE_ACTIVITY | TYPE_MINOR;
		}

		public override string GetDescription() => NAME;
		public override string GetDetails() => "Gains a fixed amount of additional experience.";

		public override bool SameAs(Effect E) => false;

		public override bool Apply(GameObject Object)
		{
			if (!ApplyEffectEvent.Check(Object, nameof(HearthpyreConversing), this))
			{
				return false;
			}
			if (Object.TryTakeEffect(out HearthpyreConversing FX))
			{
				FX.Duration = Math.Max(FX.Duration, Duration);
				return false;
			}
			return true;
		}

		public const string eConversantListen = "ConversantListen";
		public const string eConversantReact = "ConversantReact";

		public override void Register(GameObject Object)
		{
			Object.RegisterEffectEvent(this, eConversantListen); // someone else is speaking, lets not interrupt
			Object.RegisterEffectEvent(this, eConversantReact); // section of conversation ended, we can make a small reaction or RUDELY interrupt with our own thing
			Object.RegisterEffectEvent(this, "ConverseGoodbye"); // someone is exiting the conversation, bye!
			Object.RegisterEffectEvent(this, "ConverseQuestion"); // shift speaker of conversation
			base.Register(Object);
		}

		public override void Unregister(GameObject Object)
		{
			base.Unregister(Object);
		}

		public override bool FireEvent(Event E)
		{
			switch (E.ID)
			{
				case eConversantListen: return Listen(E);
				case eConversantReact: return React(E);
				case "ConverseGoodbye": return Goodbye(E);
				case "ConverseQuestion": return Question(E);
			}

			return base.FireEvent(E);
		}

		public bool Speak()
		{
			Object.ParticleText("...", 'W', floatLength: 8f);
			Object.UseEnergy(1000);
			FireConversantEvent(Event.New(eConversantListen));
			return true;
		}

		public bool Listen(Event E)
		{
			Silence = 0;
			Object.UseEnergy(1000);
			return true;
		}

		public bool React(Event E)
		{
			Object.ParticleText("!", 'W', floatLength: 8f);
			Object.UseEnergy(1000);
			return true;
		}

		public bool Goodbye(Event E)
		{
			return true;
		}

		public bool Question(Event E)
		{
			Object.ParticleText("!", 'W', floatLength: 8f);
			Object.UseEnergy(1000);
			return true;
		}

		public bool FireConversantEvent(Event E)
		{
			for (int i = Conversants.Count - 1; i >= 0; i--)
			{
				if (!GameObject.validate(Conversants[i]))
				{
					Conversants.RemoveAt(i);
				}
				else if (Object.DistanceTo(Conversants[i]) <= 2)
				{
					if (!Conversants[i].FireEvent(E))
						return false;
				}
			}

			return true;
		}

		public bool ValidateConversants()
		{
			for (int i = Conversants.Count - 1; i >= 0; i--)
			{
				if (!GameObject.validate(Conversants[i]) || Object.DistanceTo(Conversants[i]) > 3)
				{
					Conversants.RemoveAt(i);
				}
			}

			return Conversants.Count > 0;
		}

		public override bool WantEvent(int ID, int cascade) {
			return base.WantEvent(ID, cascade)
			       || ID == EndTurnEvent.ID
				;
		}

		public override bool HandleEvent(EndTurnEvent E)
		{
			if (Conversants != null)
			{
				if (Speaker == Object.id)
				{
					Speak();
					return base.HandleEvent(E);
				}
				
				if (Silence >= 1 && 50.in100())
				{
					Speak();
				}
				Silence++;
			}

			var brain = Object.pBrain;
			if (!brain.IsBusy() && !Object.IsPlayer())
			{
				if (brain.Goals.Items.Find(x => x.IsType<SettlerHandler>()) is SettlerHandler handler)
				{
					// TODO
				}
			}
			
			return base.HandleEvent(E);
		}
	}
}