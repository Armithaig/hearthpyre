﻿using System;
using System.Text;
using ConsoleLib.Console;
using Hearthpyre;
using UnityEngine;
using XRL.Core;
using XRL.Rules;
using XRL.Wish;
using XRL.World.Parts;
using static Hearthpyre.Static;

namespace XRL.World.Effects
{
    [HasWishCommand]
    [Serializable]
	public class HearthpyreMirage : Effect
	{
        public int LightRadius { get; set; }
        public int MeleeToHit { get; set; }
        public int MissileToHit { get; set; }
        public int AimVariance { get; set; }
        public int FrameOffset { get; set; }

        public HearthpyreMirage()
        {
            DisplayName = "{{hologram|mirage}}";
        }

        public override int GetEffectType()
        {
            return TYPE_FIELD | TYPE_MINOR | TYPE_NEGATIVE | TYPE_REMOVABLE;
        }

        public override bool UseStandardDurationCountdown()
        {
            return true;
        }

        public override string GetDetails() {
            return "Radiates light.\nReduced accuracy.";
        }

        public override bool SameAs(Effect E) {
            return E is HearthpyreMirage M
                   && M.LightRadius == LightRadius
                   && M.MeleeToHit == MeleeToHit
                   && M.MissileToHit == MissileToHit
                   && M.AimVariance == AimVariance
                ;
        }

        public override bool Apply(GameObject Object)
        {
            if (!ApplyEffectEvent.Check(Object, nameof(HearthpyreMirage), this))
            {
                return false;
            }
            if (Object.TryTakeEffect(out HearthpyreMirage FX))
            {
                FX.Duration = Math.Max(FX.Duration, Duration);
                return false;
            }

            HoloZap(Object.CurrentCell);
            return true;
        }


        public override void Register(GameObject Object)
        {
            Object.RegisterEffectEvent(this, "AttackerRollMeleeToHit");
            Object.RegisterEffectEvent(this, "ModifyMissileWeaponToHit");
            Object.RegisterEffectEvent(this, "ModifyAimVariance");
            base.Register(Object);
        }

        public override void Unregister(GameObject Object)
        {
            Object.UnregisterEffectEvent(this, "AttackerRollMeleeToHit");
            Object.UnregisterEffectEvent(this, "ModifyMissileWeaponToHit");
            Object.UnregisterEffectEvent(this, "ModifyAimVariance");
            base.Unregister(Object);
        }

        public override bool Render(RenderEvent E)
        {
            var f = (XRLCore.CurrentFrame + FrameOffset) & 511;
            if (f < 8) {
                E.ColorString = "&b";
                E.DetailColor = "C";
            }
            else if (f < 16) {
                E.ColorString = "&C";
                E.DetailColor = "c";
            }
            else if (f < 24) {
                E.ColorString = "&c";
                E.DetailColor = "B";
            }
            else if (f < 32) {
                E.ColorString = "&B";
                E.DetailColor = "K";
            }
            else {
                E.ColorString = "&b";
                E.DetailColor = "B";
            }

            FrameOffset = unchecked(FrameOffset + Stat.RandomCosmetic(0, 24));
            if (Stat.RandomCosmetic(1, 400) == 1) {
                E.DetailColor = "Y";
                E.ColorString = "&Y";
            }

            if (Object.IsPlayer()) E.WantsToPaint = true;
            
            return true;
        }

        public override bool FireEvent(Event E)
        {
            if (E.ID == "AttackerRollMeleeToHit")
            {
                E.SetParameter("Result", E.GetIntParameter("Result") + MeleeToHit);
            }
            else if (E.ID == "ModifyMissileWeaponToHit")
            {
                E.SetParameter("Amount", E.GetIntParameter("Amount") + MissileToHit);
            }
            else if (E.ID == "ModifyAimVariance")
            {
                E.SetParameter("Amount", E.GetIntParameter("Amount") + AimVariance);
            }
            return base.FireEvent(E);
        }
        
        public override bool WantEvent(int ID, int cascade) {
            return base.WantEvent(ID, cascade)
                   || ID == AdjustVisibilityRadiusEvent.ID
                   || ID == BeforeRenderEvent.ID
                ;
        }

        public override bool HandleEvent(AdjustVisibilityRadiusEvent E)
        {
            if (E.Radius > 10) E.Radius = 10;
            return base.HandleEvent(E);
        }
        
        public override bool HandleEvent(BeforeRenderEvent E) {
            AddLight(LightRadius, LightLevel.Radar);
            /*var CC = Object.CurrentCell;
            var Z = CC.ParentZone;
            
            for (int x = CC.X - LightRadius; x <= CC.X + LightRadius; x++)
            for (int y = CC.Y - LightRadius; y <= CC.Y + LightRadius; y++) {
                var C = Z.GetCell(x, y);
                if (C != null) {
                    
                }
            }*/
            
            return base.HandleEvent(E);
        }

        public override void OnPaint(ScreenBuffer SB) {
            if (!Object.IsPlayer()) return;
            
            for (int x = 0; x < SB.Width; x++)
            for (int y = 0; y < SB.Height; y++) {
                var chr = SB.Buffer[x, y];
                var foreground = chr.ForegroundCode;
                var background = chr.BackgroundCode;
                var detail = chr.DetailCode;
                var r = Stat.RandomCosmetic(1, 800);

                switch (r) {
                    case 1:
                        chr.SetDetail('Y');
                        if (foreground != 'k') chr.SetForeground('Y');
                        if (background != 'k') chr.SetBackground('Y');
                        continue;
                    case 2:
                        chr.SetDetail('K');
                        if (foreground != 'k') chr.SetForeground('K');
                        if (background != 'k') chr.SetBackground('K');
                        continue;
                }

                if (foreground != 'k') {
                    chr.SetForeground(Char.IsLower(foreground) ? 'B' : 'c');
                }

                if (background != 'k')
                {
                    chr.SetBackground(Char.IsLower(background) ? 'C' : 'Y');
                }

                if (detail != 'k')
                {
                    chr.SetDetail(Char.IsLower(detail) ? 'C' : 'Y');
                }
            }
        }

        [WishCommand("mirage")]
        public static void HandleMirageWish()
        {
            The.Player.ApplyEffect(new HearthpyreMirage {
                LightRadius = 3,
                MeleeToHit = -1,
                MissileToHit = -1,
                AimVariance = 1,
                Duration = Stat.RollCached("3d4")
            });
        }
	}
}