﻿using System;
using Hearthpyre;
using UnityEngine;
using XRL.World.Parts;

using static Hearthpyre.Static;

namespace XRL.World.Effects
{
	[Serializable]
	public class HearthpyreRested : Effect
	{
		public const string NAME = "{{G|rested}}";

		public int TickExperience;
		public int MetaExperience;

		public int Experience => TickExperience + MetaExperience;
		public int MaxTickExperience => XPForNextLevel / 5;
		public int MaxMetaExperience => XPForNextLevel;

		public int XPForNextLevel
		{
			get
			{
				var level = Object.Stat("Level");
				return Leveler.GetXPForLevel(level + 1) - Leveler.GetXPForLevel(level);
			}
		}

		public HearthpyreRested()
		{
			DisplayName = NAME;
			Duration = 1;
		}

		public override int GetEffectType()
		{
			return TYPE_METABOLIC | TYPE_MINOR;
		}

		public override string GetDescription() => NAME;
		/*public override string GetDescription()
		{
			var p = Experience * 100 / XPForNextLevel;
			if (p < 5) return null;
			return NAME + " (" + p + "%)";
		}*/

		public override string GetDetails() => "Gains a fixed amount of additional experience.";

		public override bool SuppressInLookDisplay() => true;

		public override bool SameAs(Effect E) => false;

		public override bool Apply(GameObject Object)
		{
			if (!ApplyEffectEvent.Check(Object, nameof(HearthpyreRested), this))
			{
				return false;
			}
			if (Object.TryTakeEffect(out HearthpyreRested FX))
			{
				FX.TickExperience = Math.Max(FX.TickExperience, TickExperience);
				FX.MetaExperience = Math.Max(FX.MetaExperience, MetaExperience);
				return false;
			}
			return true;
		}

		public void AddTickExperience(int Magnitude = 1)
		{
			var max = MaxTickExperience;
			if (TickExperience >= max) return;

			TickExperience = Math.Min(max, TickExperience + max * Magnitude / Calendar.turnsPerDay);
		}

		public static bool IsBed(GameObject Object) => Object.OwnPart<Bed>();

		public static void AddMetaExperience(TimeSpan Elapsed)
		{
			var fx = The.Player.IncludeEffect<HearthpyreRested>();
			var max = fx.MaxMetaExperience;
			if (fx.MetaExperience >= max) return;

			var minutes = Elapsed.TotalMinutes;
			if (minutes < 1) return;
			if (!The.PlayerCell.YieldCardinalFlood(NoStructure).IsOutside())
			{
				minutes *= 1.2;
				if (The.PlayerCell.HasObject(IsBed)) minutes *= 1.2;
				else if (The.Player.OwnEffect<Sitting>()) minutes *= 1.2;
			}

			fx.MetaExperience = (int) Math.Min(max, fx.MetaExperience + max * minutes / 1440);
		}

		public override bool WantEvent(int ID, int cascade) {
			return base.WantEvent(ID, cascade)
			       || ID == AwardingXPEvent.ID
				;
		}

		public override bool HandleEvent(AwardingXPEvent E)
		{
			var left = E.Amount / 2;
			var add = Math.Min(left, TickExperience);
			E.Amount += add;
			left -= add;
			TickExperience -= add;
			
			if (left > 0)
			{
				add = Math.Min(left, MetaExperience);
				E.Amount += add;
				MetaExperience -= add;
			}

			if (Experience <= 0) Duration = 0;
			return base.HandleEvent(E);
		}
	}
}