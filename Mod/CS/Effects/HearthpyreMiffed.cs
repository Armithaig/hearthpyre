﻿using System;
using Hearthpyre;

namespace XRL.World.Effects
{
	[Serializable]
	public class HearthpyreMiffed : Effect
	{
		public GameObjectSource Target;

		public HearthpyreMiffed()
		{
			DisplayName = "{{r|miffed}}";
		}

		public HearthpyreMiffed(GameObject Target, int Duration) : this()
		{
			this.Target = new GameObjectSource(Target);
			this.Duration = Duration;
		}

		public override int GetEffectType() => TYPE_MENTAL | TYPE_NEUROLOGICAL;
		public override string GetDescription() => null;
		public override bool SuppressInLookDisplay() => true;
		public override bool SameAs(Effect E) => false;
		public override bool UseStandardDurationCountdown() => true;

		public override bool Apply(GameObject Object)
		{
			if (!ApplyEffectEvent.Check(Object, nameof(HearthpyreMiffed), this))
			{
				return false;
			}

			if (Object.TryTakeEffect(out HearthpyreMiffed FX))
			{
				FX.Target = Target ?? FX.Target;
				FX.Duration = Math.Max(FX.Duration, Duration);
				return false;
			}

			return true;
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == CanStartConversationEvent.ID
				;
		}

		public override bool HandleEvent(CanStartConversationEvent E)
		{
			if (GameObjectSource.Validate(ref Target, out var obj) && (E.Actor == obj || E.Object == obj))
			{
				E.FailureMessage = E.Actor == obj
						? T(E.Object) + E.Object.GetVerb("refuse") + $" to speak with {t(E.Actor)}."
						: T(E.Actor) + E.Actor.GetVerb("refuse") + $" to speak with {t(E.Object)}."
					;
				return false;
			}

			return base.HandleEvent(E);
		}

		public string T(GameObject Object, bool Capitalized = true)
		{
			if (Object.IsPlayer()) return Capitalized ? "You" : "you";
			var result = Capitalized ? Object.T(Stripped: true) : Object.t(Stripped: true);
			var comma = result.IndexOf(',');
			if (comma >= 0)
			{
				result = result.Remove(comma);
			}
			return result;
			// todo: use WithoutTitles
			// return Capitalized ? Object.T(Stripped: true, WithoutEpithet: true) : Object.t(Stripped: true, WithoutEpithet: true);
		}

		public string t(GameObject Object) => T(Object, false);
	}
}