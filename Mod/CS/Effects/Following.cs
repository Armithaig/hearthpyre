﻿using System;
using System.Collections.Generic;
using Hearthpyre;
using XRL.World.Parts;

namespace XRL.World.Effects
{
	[Serializable]
	public class HearthpyreFollowing : Effect
	{
		public const string NAME = "{{C|following}}";

		public GameObjectSource Target;
		public GameObjectSource Previous;

		public HearthpyreFollowing()
		{
			DisplayName = NAME;
		}

		public HearthpyreFollowing(GameObject Target, int Duration) : this()
		{
			this.Target = new GameObjectSource(Target);
			this.Duration = Duration;
		}

		public override int GetEffectType()
		{
			return TYPE_ACTIVITY;
		}

		public override string GetDescription()
		{
			if (Duration > 0) return NAME + " (" + Duration + ")";
			return NAME;
		}

		public override bool SameAs(Effect E)
		{
			return E is HearthpyreFollowing F
			       && F.Target.ID == Target.ID
				;
		}

		public override bool Apply(GameObject Object)
		{
			if (!ApplyEffectEvent.Check(Object, nameof(HearthpyreFollowing), this))
			{
				return false;
			}

			if (Object.TryTakeEffect(out HearthpyreFollowing FX))
			{
				FX.Duration = Math.Max(FX.Duration, Duration);
				return false;
			}

			MarkInventory(true);
			return true;
		}

		public override void Remove(GameObject Object)
		{
			SetLeader(Previous?.Value);
			MarkInventory(false);
			base.Remove(Object);
		}

		public override bool WantEvent(int ID, int cascade)
		{
			return base.WantEvent(ID, cascade)
			       || ID == EndTurnEvent.ID
				;
		}

		public override bool HandleEvent(EndTurnEvent E)
		{
			if (--Duration > 0) ValidateLeader();
			return base.HandleEvent(E);
		}

		public void ValidateLeader()
		{
			if (!Target.Validate(out var obj))
			{
				Duration = 0;
				return;
			}

			var brain = Object.pBrain;
			if (brain.PartyLeader != null)
			{
				if (brain.PartyLeader != obj)
				{
					Previous = new GameObjectSource(brain.PartyLeader);
					SetLeader(Target.Value);
				}
				else if (brain.Staying)
				{
					Duration = 0;
				}
			}
			else
			{
				SetLeader(Target.Value);
			}
		}

		public void SetLeader(GameObject Leader)
		{
			var brain = Object.pBrain;
			brain.PartyLeader = Leader;
			brain.Staying = false;
			brain.Goals.Clear();
		}

		public void MarkInventory(bool Value)
		{
			if (!Object.TryTakePart(out Inventory inventory)) return;

			foreach (var item in inventory.Objects)
			{
				if (Value)
				{
					if (item.HasStringProperty("WontSell")) continue;
					item.SetStringProperty("WontSell", nameof(HearthpyreFollowing));
				}
				else
				{
					if (item.GetStringProperty("WontSell") == nameof(HearthpyreFollowing))
						item.RemoveStringProperty("WontSell");
				}
			}
		}
	}
}